class BenchmarkLogger < Logger

end

logfile = File.open(Rails.root.to_s + '/log/benchmark.log', 'a')  #create log file
logfile.sync = true  #automatically flushes data to file
BENCHMARK_LOGGER = BenchmarkLogger.new(logfile)  #constant accessible anywhere