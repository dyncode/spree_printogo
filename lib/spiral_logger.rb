class SpiralLogger < Logger
  
end

logfile = File.open(Rails.root.to_s + '/log/spiral.log', 'a')  #create log file
logfile.sync = true  #automatically flushes data to file
SPIRAL_LOGGER = SpiralLogger.new(logfile)  #constant accessible anywhere