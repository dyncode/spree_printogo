class MultipageLogger < Logger
  
end

logfile = File.open(Rails.root.to_s + '/log/multipage.log', 'a')  #create log file
logfile.sync = true  #automatically flushes data to file
MULTIPAGE_LOGGER = MultipageLogger.new(logfile)  #constant accessible anywhere