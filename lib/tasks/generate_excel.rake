require 'csv'
namespace :spree_print2go do
  desc "Genera tutte le combinazioni della formula di zambo"
  task :preventive => :environment do
    val = [4, 8, 12, 16, 20, 24, 28, 32]
    quarters = [1, 2, 3, 4, 6, 8, 12, 16, 24, 32]
    CSV.open("doc/export_plants.csv", "wb") do |csv|
      quarters.each do |q|
        csv << ["#{q} quartini/faccate", "impianti 8colori", "impianti 4color", "impianti 4color (senza buco)", "Spree::Order.number_of_systems_and_sheet(#{q}, v, 1, 4)",
                "Spree::Order.number_of_systems_and_sheet(#{q}, v, 1, 8)"]
        val.each do |v|
          t4 = Spree::Order.number_of_systems_and_sheet(q, v, 1, 4)
          t8 = Spree::Order.number_of_systems_and_sheet(q, v, 1, 8)
          csv << [v, t8[:plants], t4[:sheet], t4[:plants], t4, t8]
        end

      end
    end
  end

  desc "Genera tutte le combinazioni della formula di zambo"
  task :preventive_2 => :environment do
    val = [4, 8, 12, 16, 20, 24, 28, 32]
    quarters = [1, 2, 3, 4, 6, 8, 12, 16, 24, 32]
    CSV.open("doc/export_plants_4_color.csv", "wb") do |csv|
      quarters.each do |q|
        csv << ["#{q} quartini/faccate", "fogli 4colori", "impianti 4color", "params"]
        val.each do |v|

          r = Spree::Order.get_plants_and_sheet_4_colors(q, v)

          csv << [v, r[:number_of_sheet], r[:plants], r[:params]]
          #r.each do |w|
          #  csv << [v, w[:sheet],w[:plants],w[:divisor],w[:sheet_raw]]
          #  puts "#{v}: [#{w[:sheet]},#{w[:plants]}]"
          #end
        end

      end
    end
    puts "Fine..."
  end


  def foo(quarter_of_printer, faciates, divisor = 1)

    #f_d % Integer(quarter_of_printer) == 0
    r1 = faciates / Integer(quarter_of_printer)
    r = []
    r_h = {}
    if r1 > 0
      r_h = {:sheet => ((r1 / Float(divisor)) <= 1) ? 1 : (r1 / Float(divisor)),
             :plants => ((r1 / Float(divisor)) <= 0.5) ? 1 : (r1 / Float(divisor)) * 2,
             :divisor => divisor,
             :sheet_raw => (r1 / Float(divisor))}
    end

    resto = faciates % Integer(quarter_of_printer)
    puts "************ VALUTO *************"
    puts "faciates: #{faciates}"
    puts "Integer(quarter_of_printer): #{Integer(quarter_of_printer)}"
    puts "resto: #{resto}"
    div = quarter_of_printer / 2
    div = 1 if quarter_of_printer == 3
    if resto > 0
      f = foo(div, resto, divisor * 2)
      unless f == {}
        sheet = r_h[:sheet].nil? ? 0 : r_h[:sheet]
        plants = r_h[:plants].nil? ? 0 : r_h[:plants]
        sheet_raw = r_h[:sheet_raw].nil? ? 0 : r_h[:sheet_raw]
        #puts "sheet #{sheet}"
        #puts "plants #{plants}"
        #puts "sheet_raw #{sheet_raw}"
        #puts "f[:sheet]: #{f[:sheet].nil?}"
        #puts "f[:plants]: #{f[:plants].nil?}"
        #puts "f[:sheet_raw]: #{f[:sheet_raw].nil?}"
        r_h[:sheet] = sheet + (f[:sheet].nil? ? 0 : f[:sheet])
        r_h[:plants] = plants + (f[:plants].nil? ? 0 : f[:plants])
        r_h[:sheet_raw] = sheet_raw + (f[:sheet_raw].nil? ? 0 : f[:sheet_raw])
        puts "r_h[:sheet]: #{r_h[:sheet]}"
        puts "r_h[:plants]: #{r_h[:plants]}"
        puts "r_h[:sheet_raw]: #{r_h[:sheet_raw]}"
      end
    end
    #r += foo(div, resto, divisor * 2) if resto > 0
    puts "*********************************"
    r_h
  end




  # Parametri
  #   optimization : può essere
  #         - i: impianti
  #         - c: carta
  def calc(quarter_of_printer,quarter_to_print,divisor = 1,optimization = "i")
    h = []
    r = quarter_to_print / quarter_of_printer
    if r > 0
      h << {:plants => get_plants(r,divisor,optimization) }
    end
  end

  def get_plants(result,divisor,optimization)
    return {:plants => result * 2, :divisor => 1} if divisor == 1
    if optimization == "i"
      # Cerco di ottenre il minor numero di impianti
      if (result / Float(divisor)) <= 0.5
        # In questo caso riesco a stampare i quartini indicati in result, sulla stessa facciata.
        # Nel caso result = 2 e divisor = 6 avrò come quartini A B
        #
        #   +----+----+----+
        #   | Af |    | Bf |
        #   +----+----+----+
        #   | Ar |    | Br |
        #   +----+----+----+
        # Quindi avrò bisogno di un solo impianto!

        # Per quanto rigurada il # di fogli che avrò bisogno dovrò fare così:
        if divisor % 3 == 0
          divisor -= 1
        end

        # facendo in questo modo quando moltiplicherò il # di copie per il 1/divisor avrò effettivamente quanti fogli
        # devo stampare.
        # Nel caso d'esempio di sora avrò che il divisor sarà uguale a:
        divisor = result * 2

        return {:plants => 1, :divisor => divisor}
      else

      end
    end
  end

  def foo2(quarter_of_printer, faciates, divisor = 1)

    #f_d % Integer(quarter_of_printer) == 0
    r1 = faciates / Integer(quarter_of_printer)
    r = []
    r_h = {}
    if r1 <= 0
      r1 = faciates / Float(quarter_of_printer)
    end

    if r1 > 0
      r_h = {:sheet => (r1 <= 1) ? 1 : (r1 == 1 ? r1 / 2 : r1),
             :plants => (r1 <= 0.5) ? 1 : (r1.ceil * 2)}
    end

    resto = faciates % Integer(quarter_of_printer)

    if r1 >= 1 && resto > 0
      puts "r1: #{r1} con qs => #{quarter_of_printer} e quartini da stampare: => #{faciates}"
      f = foo2(quarter_of_printer, resto, divisor * 2)
      unless f == {}
        sheet = r_h[:sheet].nil? ? 0 : r_h[:sheet]
        plants = r_h[:plants].nil? ? 0 : r_h[:plants]
        sheet_raw = r_h[:sheet_raw].nil? ? 0 : r_h[:sheet_raw]

        r_h[:sheet] = sheet + (f[:sheet].nil? ? 0 : f[:sheet])
        r_h[:plants] = plants + (f[:plants].nil? ? 0 : f[:plants])
        r_h[:sheet_raw] = sheet_raw + (f[:sheet_raw].nil? ? 0 : f[:sheet_raw])

      end
    end
    #r += foo(div, resto, divisor * 2) if resto > 0

    r_h
  end

end