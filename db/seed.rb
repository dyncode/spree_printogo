require 'csv'
# ========================================== Aggiungo le configurazioni ========================================== #
# Aggiungo le pagine
puts "Aggiungo le pagine"
Spree::StaticPage.create(:title => "Chi Siamo", :published_at => Time.now, :body => '<p><strong>Lorem ipsum</strong> dolor sit amet, consectetur adipiscing elit. Nunc ut leo sit amet dolor congue venenatis. Phasellus in augue vitae enim pretium ornare. Phasellus elit lectus, suscipit ut venenatis quis, faucibus sed mi. Cras sit amet dolor in ligula dictum dignissim ac at turpis. Aenean pulvinar condimentum metus, non accumsan leo laoreet eget. Nulla nec sem a metus posuere adipiscing. Quisque non risus mauris. Donec a magna eu lorem vestibulum rutrum. Quisque ut tortor nisl. Praesent nec tellus ac metus accumsan sollicitudin. Cras id commodo libero. Nullam mollis ultrices urna eu tristique. Donec eu lacinia justo. Donec fringilla, erat at lacinia rhoncus, sapien est pellentesque elit, vel cursus arcu erat nec nunc. Ut consequat, ipsum eu gravida semper, eros orci iaculis mi, nec ullamcorper eros nisl et enim. Nunc hendrerit sapien ut tellus posuere ac condimentum lectus rhoncus. Pellentesque mollis neque et tellus dapibus quis malesuada velit elementum. Fusce iaculis volutpat cursus. Nulla facilisi. Nulla facilisi. Nulla nisl metus, fermentum sit amet porttitor mollis, accumsan aliquam mauris. Etiam ultrices, dolor eget tincidunt accumsan, turpis ligula ultricies elit, quis congue nulla metus ut quam. Nunc a nunc neque, faucibus consequat lacus. Mauris egestas pretium neque, id feugiat nisi lobortis id. Nulla facilisi. Etiam sit amet ante sed augue sollicitudin accumsan. Duis eu urna vel ipsum lacinia auctor ac in metus. Sed ac enim eu quam hendrerit fringilla. Etiam non varius felis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Curabitur iaculis sagittis purus, quis pulvinar justo convallis non. Duis pulvinar, massa blandit pulvinar egestas, sem eros pretium ipsum, vitae mattis nulla turpis eu arcu. Nam id odio turpis. Etiam sagittis quam sit amet urna varius vitae bibendum justo dignissim. Morbi ullamcorper ultrices leo sed suscipit. Pellentesque ornare vestibulum velit, ut iaculis sem imperdiet ut. Lorem ipsum dolor sit amet, consectetur adipiscinsdasfct.</p>')
Spree::StaticPage.create(:title => "Info", :published_at => Time.now, :body => '<p><strong>Lorem ipsum</strong> dolor sit amet, consectetur adipiscing elit. Nunc ut leo sit amet dolor congue venenatis. Phasellus in augue vitae enim pretium ornare. Phasellus elit lectus, suscipit ut venenatis quis, faucibus sed mi. Cras sit amet dolor in ligula dictum dignissim ac at turpis. Aenean pulvinar condimentum metus, non accumsan leo laoreet eget. Nulla nec sem a metus posuere adipiscing. Quisque non risus mauris. Donec a magna eu lorem vestibulum rutrum. Quisque ut tortor nisl. Praesent nec tellus ac metus accumsan sollicitudin. Cras id commodo libero. Nullam mollis ultrices urna eu tristique. Donec eu lacinia justo. Donec fringilla, erat at lacinia rhoncus, sapien est pellentesque elit, vel cursus arcu erat nec nunc. Ut consequat, ipsum eu gravida semper, eros orci iaculis mi, nec ullamcorper eros nisl et enim. Nunc hendrerit sapien ut tellus posuere ac condimentum lectus rhoncus. Pellentesque mollis neque et tellus dapibus quis malesuada velit elementum. Fusce iaculis volutpat cursus. Nulla facilisi. Nulla facilisi. Nulla nisl metus, fermentum sit amet porttitor mollis, accumsan aliquam mauris. Etiam ultrices, dolor eget tincidunt accumsan, turpis ligula ultricies elit, quis congue nulla metus ut quam. Nunc a nunc neque, faucibus consequat lacus. Mauris egestas pretium neque, id feugiat nisi lobortis id. Nulla facilisi. Etiam sit amet ante sed augue sollicitudin accumsan. Duis eu urna vel ipsum lacinia auctor ac in metus. Sed ac enim eu quam hendrerit fringilla. Etiam non varius felis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Curabitur iaculis sagittis purus, quis pulvinar justo convallis non. Duis pulvinar, massa blandit pulvinar egestas, sem eros pretium ipsum, vitae mattis nulla turpis eu arcu. Nam id odio turpis. Etiam sagittis quam sit amet urna varius vitae bibendum justo dignissim. Morbi ullamcorper ultrices leo sed suscipit. Pellentesque ornare vestibulum velit, ut iaculis sem imperdiet ut. Lorem ipsum dolor sit amet, consectetur adipiscinsdasfct.</p>')

# Aggiungo i ruoli e l'utente admin
puts "Aggiungo i ruoli e l'utente admin"
Spree::Role.create(:name => "user")
Spree::Role.create(:name => "admin")
Spree::User.create(:email => "admin@diginess.it", :password => "asdasd", :password_confirmation => "asdasd", :login => "admin@diginess.it")

# Aggiungo le Country
puts "Aggiungo le Country"
CSV.parse(File.open('public/default/country.csv'), :col_sep => ",", :headers => :first_row) do |row|
  c = Spree::Country.new
  c.iso_name = row["iso_name"]
  c.iso = row["iso"]
  c.iso3 = row["iso3"]
  c.name = row["name"]
  c.numcode = row["numcode"]
  c.save!
end

# Aggiungo gli State
puts "Aggiungo gli State"
CSV.parse(File.open('public/default/state.csv'), :col_sep => ",", :headers => :first_row) do |row|
  s = Spree::State.new
  s.name = row["name"]
  s.abbr = row["abbr"]
  s.country_id = Spree::Country.find_by_iso("IT").id
  s.save!
end

# Aggiungo le ZoneMember
puts "Aggiungo le ZoneMember"
z = Spree::Zone.create(:name => "EU_VAT", :description => "Countries that make up the EU VAT zone.", :default_tax => true)
%w(IT GB SE ES SI SK RO PT PL NL MT LU LT LV IT IE HU DE FR FI EE DK CZ CY BG BE AT).each do |c|
  Spree::ZoneMember.create(:zoneable => Spree::Country.find_by_iso(c), :zone_id => z.id)
end

# Aggiungo i PresentationPack
puts "Aggiungo i PresentationPack"
Spree::PresentationPack.create(:name => "Anonimo", :presentation => "Pacco Anonimo", :price => 5.0, :icon => File.open('public/default/anonymous_package.png'))
Spree::PresentationPack.create(:name => "Print 2 Go", :presentation => "Pacco Print 2 Go", :price => 3.0, :icon => File.open('public/default/personalized_package.png'))

# Aggiungo la TaxRate
puts "Aggiungo la TaxRate"
tc = Spree::TaxCategory.create(:name => "IVA", :description => "IVA", :is_default => true)
tr = Spree::TaxRate.create(:zone_id => z.id, :tax_category_id => tc.id, :included_in_price => true, :amount => 0.22)
dt = Spree::Calculator::DefaultTax.new()
dt.calculable = tr
tr.calculator = dt
tr.save

# Aggiungo i metodi di Spedizione
puts "Aggiungo i metodi di Spedizione"
stc = Spree::ShippingCategory.create(:name => "Spedizione tramite corriere")
rns = Spree::ShippingCategory.create(:name => "Ritiro presso nostra sede")
sm = Spree::ShippingMethod.create(:name => "Bartolini", :zone_id => z.id, :min_weight => 0.0, :max_weight => 3.0, :shipping_category_id => stc.id)
fr = Spree::Calculator::FlatRate.new()
fr.calculable = sm
sm.calculator = fr
sm.calculator.preferred_amount = 10
sm.save

# Aggiungo le Categorie di Default
pf = Spree::Taxonomy.create(:name => "Piccolo Formato")
pf.default = true
pf.save
pf.taxons.first.default = true
pf.taxons.first.save

gf = Spree::Taxonomy.create(:name => "Grande Formato")
gf.default = true
gf.save
gf.taxons.first.default = true
gf.taxons.first.save

mp = Spree::Taxonomy.create(:name => "Multipagina")
mp.default = true
mp.save
mp.taxons.first.default = true
mp.taxons.first.save

pr = Spree::Taxonomy.create(:name => "Prodotti Ready")
pr.default = true
pr.save
pr.taxons.first.default = true
pr.taxons.first.save

# ========================================== Aggiungo il Magazzino ========================================== #
# Aggiungo i Formati Carta
puts "Aggiungo i Formati Carta"
Spree::OptionType.create(:name => 'paper_weight', :presentation => 'Grammatura Carta')
pf = Spree::OptionType.create(:name => 'paper', :presentation => 'Tipi di Carta')
papers = %w(usomano patinata_opaca patinata_lucida)
prices = {
    :usomano => {80 => 0.88, 90 => 0.88, 100 => 0.88, 110 => 0.88, 115 => 0.88, 120 => 0.88, 130 => 0.88, 140 => 0.88, 150 => 0.88, 160 => 0.88, 170 => 0.88, 200 => 0.88, 215 => 0.88, 230 => 0.88, 240 => 0.88, 250 => 0.88, 280 => 0.88, 300 => 0.88, 350 => 0.88},
    :patinata_opaca => {80 => 0.89, 90 => 0.865, 100 => 0.84, 110 => 0.84, 115 => 0.84, 120 => 0.84, 130 => 0.84, 140 => 0.84, 150 => 0.84, 160 => 0.84, 170 => 0.84, 200 => 0.84, 250 => 0.865, 300 => 0.89, 350 => 0.915},
    :patinata_lucida => {80 => 0.89, 90 => 0.865, 100 => 0.84, 110 => 0.84, 115 => 0.84, 120 => 0.84, 130 => 0.84, 140 => 0.84, 150 => 0.84, 160 => 0.84, 170 => 0.84, 200 => 0.84, 250 => 0.865, 300 => 0.89, 350 => 0.915}
}
papers.each do |p|
  sp = Spree::Paper.create!(:name => p.downcase.titleize, :presentation => p.downcase.titleize, :active => true)
  Spree::Config[:paper_weight].split(",").each do |weight|
    sp.paper_weights.create!(:weight => weight.to_s, :presentation => weight.to_s, :cost_cut => prices[p.to_sym][weight.to_i].to_f, :cost_ream => prices[p.to_sym][weight.to_i].to_f)
  end
end

# Aggiungo le grammature numero di fogli taglio
puts "Aggiungo le grammature numero di fogli taglio"
Spree::OptionType.create(:name => 'weight_cut_ream', :presentation => 'Grammatura numero di fogli taglio')

weight = %(80,90,100,110,115,120,130,140,150,160,170,200,215,230,240,250,280,300,350)
cut_weight = %(1100,1000,900,800,700,600,500,500,500,400,400,400,400,300,300,300,200,200,100)
weight.split(",").count.times do |wt|
  Spree::Weight.create(:name => weight.split(",")[wt], :presentation => cut_weight.split(",")[wt])
end

# Aggiungo le Asole
puts "Aggiungo le Asole"
Spree::OptionType.create(:name => "buttonhole", :presentation => "Asole")
Spree::Buttonhole.create(:name => "Asola Corda", :copy_cost => 1)
# Aggiungo le Asole Accessorie
puts "Aggiungo le Asole Accessorie"
Spree::OptionType.create(:name => "buttonhole_accessory", :presentation => "Asole Accessorie")

# Aggiungo l'Occhiello
puts "Aggiunto l'Occhiello"
Spree::OptionType.create(:name => "eyelet", :presentation => "Occhielli")
Spree::Eyelet.create(:name => "In alluminio zincato", :copy_cost => 1)
# Aggiungo l'Occhiello Accessorio
puts "Aggiunto l'Occhiello Accessorio"
Spree::OptionType.create(:name => "eyelet_accessory", :presentation => "Occhielli Accessori")

# Aggiungo il telaio
puts "Aggiunto il telaio"
Spree::OptionType.create(:name => "frame", :presentation => "Telaio")
Spree::Frame.create(:name => "Telaio in legno", :copy_cost => 3.5)

# Aggiungo l'appendino
puts "Aggiunto l'appendino"
Spree::OptionType.create(:name => "hanger", :presentation => "Appendino")
Spree::Hanger.create(:name => "Dorato", :copy_cost => 0.5)
Spree::Hanger.create(:name => "Argento", :copy_cost => 0.5)
# Aggiungo l'appendino Accessorio
puts "Aggiunto l'appendino Accessorio"
Spree::OptionType.create(:name => "hanger_accessory", :presentation => "Appendini Accessori")

# ========================================== Aggiungo i Formati di Stampa e le Stampanti ========================================== #
# Aggiungo i formati di stampa di default
#Spree::OptionType.create(:name => 'printer_format', :presentation => 'Formati Stampa')
puts "Aggiungo i formati di stampa di default"
Spree::OptionType.create(:name => 'plasticization', :presentation => 'Plastificazione')
f = Spree::OptionType.create(:name => 'printer_format_default', :presentation => 'Formati Stampa di Default')
[[32, 44], [35, 50], [44, 64], [50, 70], [64, 88], [70, 100]].each do |format|
  Spree::PrinterFormatDefault.create(:base => format[0], :height => format[1], :option_type_id => f.id)
end

p71 = Spree::PrinterFormatDefault.find_by_name("70x100")
p71.quantity = 1
p71.save!
p68 = Spree::PrinterFormatDefault.find_by_name("64x88")
p68.quantity = 1
p68.save!

p57 = Spree::PrinterFormatDefault.find_by_name("50x70")
p57.option_value_id = Spree::PrinterFormatDefault.find_by_name("70x100").id
p57.quantity = 2
p57.save!
p35 = Spree::PrinterFormatDefault.find_by_name("35x50")
p35.option_value_id = Spree::PrinterFormatDefault.find_by_name("70x100").id
p57.quantity = 4
p35.save!
p46 = Spree::PrinterFormatDefault.find_by_name("44x64")
p46.option_value_id = Spree::PrinterFormatDefault.find_by_name("64x88").id
p57.quantity = 2
p46.save!
p34 = Spree::PrinterFormatDefault.find_by_name("32x44")
p34.option_value_id = Spree::PrinterFormatDefault.find_by_name("64x88").id
p57.quantity = 4
p34.save!

# Aggiungo le stampanti
puts "Aggiungo le stampanti"
Spree::OptionType.create(:name => 'printer', :presentation => 'Stampante')
xp = Spree::Printer.create(:presentation => "XEROX 250", :printer_type => "digital", :cost_waiting => 0, :min_limit_waiting => 0,
                      :printer_hourly_cost_c => 60, :start_time_c => 20, :average_hourly_print_c => 1000,
                      :waste_paper_c => 1, :price_front_c => 0.0985, :price_front_and_back_c => 0.197,
                      :printer_hourly_cost_bw => 60, :start_time_bw => 20, :average_hourly_print_bw => 2000,
                      :waste_paper_bw => 1, :price_front_bw => 0.011, :price_front_and_back_bw => 0.022)
xp.printer_formats << Spree::PrinterFormatDefault.find_by_name("35x50")
xp.save!

op = Spree::Printer.create(:presentation => "OFFSET 70x100", :printer_type => "offest",
                      :color_type => 4, :cost_waiting => 20, :min_limit_waiting => 500,
                      :printer_hourly_cost_c => 120, :start_time_c => 30, :average_hourly_print_c => 6000,
                      :waste_paper_c => 200, :price_front_c => 0.0166, :price_front_and_back_c => 0.0332,
                      :plant_color_c => 68,
                      :printer_hourly_cost_bw => 55, :start_time_bw => 30, :average_hourly_print_bw => 5500,
                      :waste_paper_bw => 100, :price_front_bw => 0.01, :price_front_and_back_bw => 0.02,
                      :plant_color_bw => 17)
op.printer_formats << Spree::PrinterFormatDefault.find_by_name("70x100")
op.printer_formats << Spree::PrinterFormatDefault.find_by_name("50x70")
op.printer_formats << Spree::PrinterFormatDefault.find_by_name("64x88")
op.printer_formats << Spree::PrinterFormatDefault.find_by_name("44x64")
op.save!

p8p = Spree::Printer.create(:presentation => "8 COLORI", :printer_type => "offest",
                      :color_type => 8, :cost_waiting => 0, :min_limit_waiting => 0,
                      :printer_hourly_cost_c => 160, :start_time_c => 40, :average_hourly_print_c => 10000,
                      :waste_paper_c => 100, :price_front_c => 0.03, :price_front_and_back_c => 0.06,
                      :plant_color_c => 112,
                      :printer_hourly_cost_bw => 160, :start_time_bw => 10, :average_hourly_print_bw => 10000,
                      :waste_paper_bw => 10, :price_front_bw => 0.01, :price_front_and_back_bw => 0.02,
                      :plant_color_bw => 28)
p8p.printer_formats << Spree::PrinterFormatDefault.find_by_name("70x100")
p8p.printer_formats << Spree::PrinterFormatDefault.find_by_name("64x88")
p8p.save!

gp = Spree::Printer.create(:presentation => "GTO 35x50", :printer_type => "offest",
                      :color_type => 4, :cost_waiting => 20, :min_limit_waiting => 500,
                      :printer_hourly_cost_c => 42, :start_time_c => 90, :average_hourly_print_c => 5500,
                      :waste_paper_c => 200, :price_front_c => 0.0075, :price_front_and_back_c => 0.015,
                      :plant_color_c => 32,
                      :printer_hourly_cost_bw => 42, :start_time_bw => 90, :average_hourly_print_bw => 5500,
                      :waste_paper_bw => 200, :price_front_bw => 0.0075, :price_front_and_back_bw => 0.015,
                      :plant_color_bw => 8)
gp.printer_formats << Spree::PrinterFormatDefault.find_by_name("70x100")
gp.printer_formats << Spree::PrinterFormatDefault.find_by_name("64x88")
gp.save!

# Aggiungo i plotter
puts "Aggiungo i plotter"
Spree::OptionType.create(:name => 'plotter', :presentation => 'Plotter')
Spree::OptionType.create(:name => 'plotter_printing_cost', :presentation => 'Costi di Stampa per Plotter')

Spree::Plotter.create(:presentation => "Epson 9880", :printer_hourly_cost_c => 55, :start_time_c => 30)
Spree::Plotter.create(:presentation => "Roland - Inprint", :printer_hourly_cost_c => 60, :start_time_c => 15)

# ========================================== Aggiungo la Plastificazione ========================================== #
Spree::MasterPrice.create :name => "Film Lucido", :presentation => '55'
Spree::MasterPrice.create :name => "Film Opaco", :presentation => '75'

Spree::Plasticization.find_by_name("32x44").plasticization_prices.each do |pp|
  if pp.name == "Film Lucido" && pp.quantity == "0 - 1000"
    pp.presentation = "0.055"
  elsif pp.name == "Film Lucido" && pp.quantity == "1001 - 2000"
    pp.presentation = "0.048"
  elsif pp.name == "Film Lucido" && pp.quantity == "2001 - 3000"
    pp.presentation = "0.046"
  elsif pp.name == "Film Lucido" && pp.quantity == "oltre 3000"
    pp.presentation = "0.042"
  elsif pp.name == "Film Opaco" && pp.quantity == "0 - 1000"
    pp.presentation = "0.055"
  elsif pp.name == "Film Opaco" && pp.quantity == "1001 - 2000"
    pp.presentation = "0.078"
  elsif pp.name == "Film Opaco" && pp.quantity == "2001 - 3000"
    pp.presentation = "0.074"
  elsif pp.name == "Film Opaco" && pp.quantity == "oltre 3000"
    pp.presentation = "0.072"
  end
  pp.save!
end

Spree::Plasticization.find_by_name("35x50").plasticization_prices.each do |pp|
  if pp.name == "Film Lucido" && pp.quantity == "0 - 1000"
    pp.presentation = "0.055"
  elsif pp.name == "Film Lucido" && pp.quantity == "1001 - 2000"
    pp.presentation = "0.052"
  elsif pp.name == "Film Lucido" && pp.quantity == "2001 - 3000"
    pp.presentation = "0.050"
  elsif pp.name == "Film Lucido" && pp.quantity == "oltre 3000"
    pp.presentation = "0.047"
  elsif pp.name == "Film Opaco" && pp.quantity == "0 - 1000"
    pp.presentation = "0.080"
  elsif pp.name == "Film Opaco" && pp.quantity == "1001 - 2000"
    pp.presentation = "0.076"
  elsif pp.name == "Film Opaco" && pp.quantity == "2001 - 3000"
    pp.presentation = "0.072"
  elsif pp.name == "Film Opaco" && pp.quantity == "oltre 3000"
    pp.presentation = "0.068"
  end
  pp.save!
end

Spree::Plasticization.find_by_name("44x64").plasticization_prices.each do |pp|
  if pp.name == "Film Lucido" && pp.quantity == "0 - 1000"
    pp.presentation = "0.062"
  elsif pp.name == "Film Lucido" && pp.quantity == "1001 - 2000"
    pp.presentation = "0.060"
  elsif pp.name == "Film Lucido" && pp.quantity == "2001 - 3000"
    pp.presentation = "0.058"
  elsif pp.name == "Film Lucido" && pp.quantity == "oltre 3000"
    pp.presentation = "0.054"
  elsif pp.name == "Film Opaco" && pp.quantity == "0 - 1000"
    pp.presentation = "0.108"
  elsif pp.name == "Film Opaco" && pp.quantity == "1001 - 2000"
    pp.presentation = "0.104"
  elsif pp.name == "Film Opaco" && pp.quantity == "2001 - 3000"
    pp.presentation = "0.100"
  elsif pp.name == "Film Opaco" && pp.quantity == "oltre 3000"
    pp.presentation = "0.098"
  end
  pp.save!
end

Spree::Plasticization.find_by_name("50x70").plasticization_prices.each do |pp|
  if pp.name == "Film Lucido" && pp.quantity == "0 - 1000"
    pp.presentation = "0.070"
  elsif pp.name == "Film Lucido" && pp.quantity == "1001 - 2000"
    pp.presentation = "0.067"
  elsif pp.name == "Film Lucido" && pp.quantity == "2001 - 3000"
    pp.presentation = "0.065"
  elsif pp.name == "Film Lucido" && pp.quantity == "oltre 3000"
    pp.presentation = "0.062"
  elsif pp.name == "Film Opaco" && pp.quantity == "0 - 1000"
    pp.presentation = "0.120"
  elsif pp.name == "Film Opaco" && pp.quantity == "1001 - 2000"
    pp.presentation = "0.116"
  elsif pp.name == "Film Opaco" && pp.quantity == "2001 - 3000"
    pp.presentation = "0.112"
  elsif pp.name == "Film Opaco" && pp.quantity == "oltre 3000"
    pp.presentation = "0.108"
  end
  pp.save!
end

Spree::Plasticization.find_by_name("64x88").plasticization_prices.each do |pp|
  if pp.name == "Film Lucido" && pp.quantity == "0 - 1000"
    pp.presentation = "0.089"
  elsif pp.name == "Film Lucido" && pp.quantity == "1001 - 2000"
    pp.presentation = "0.085"
  elsif pp.name == "Film Lucido" && pp.quantity == "2001 - 3000"
    pp.presentation = "0.081"
  elsif pp.name == "Film Lucido" && pp.quantity == "oltre 3000"
    pp.presentation = "0.079"
  elsif pp.name == "Film Opaco" && pp.quantity == "0 - 1000"
    pp.presentation = "0.172"
  elsif pp.name == "Film Opaco" && pp.quantity == "1001 - 2000"
    pp.presentation = "0.116"
  elsif pp.name == "Film Opaco" && pp.quantity == "2001 - 3000"
    pp.presentation = "0.112"
  elsif pp.name == "Film Opaco" && pp.quantity == "oltre 3000"
    pp.presentation = "0.108"
  end
  pp.save!
end

Spree::Plasticization.find_by_name("70x100").plasticization_prices.each do |pp|
  if pp.name == "Film Lucido" && pp.quantity == "0 - 1000"
    pp.presentation = "0.107"
  elsif pp.name == "Film Lucido" && pp.quantity == "1001 - 2000"
    pp.presentation = "0.105"
  elsif pp.name == "Film Lucido" && pp.quantity == "2001 - 3000"
    pp.presentation = "0.102"
  elsif pp.name == "Film Lucido" && pp.quantity == "oltre 3000"
    pp.presentation = "0.97"
  elsif pp.name == "Film Opaco" && pp.quantity == "0 - 1000"
    pp.presentation = "0.192"
  elsif pp.name == "Film Opaco" && pp.quantity == "1001 - 2000"
    pp.presentation = "0.188"
  elsif pp.name == "Film Opaco" && pp.quantity == "2001 - 3000"
    pp.presentation = "0.184"
  elsif pp.name == "Film Opaco" && pp.quantity == "oltre 3000"
    pp.presentation = "0.180"
  end
  pp.save!
end

# creo le plastificazioni visibili all'utente, in base a queste recupero il valore
Spree::Plasticization.plasticization_types_all.each do |v|
  Spree::Plasticization.create(:name => v, :presentation => v)
end

# ========================================== Aggiungo le Finiture ========================================== #
# Aggiungo la Bobina
Spree::OptionType.create(:name => 'coil', :presentation => 'Bobine')
puts "Aggiungo la Bobina"
Spree::CoilValue.create(:name => "Blueback 150 gr", :height => 136, :price => 0.99)
Spree::CoilValue.create(:name => "Canvas 300 gr", :height => 106, :price => 6)

# Aggiungo i Supporti Rigidi
Spree::OptionType.create(:name => 'rigid_support', :presentation => "Supporti Rigidi")
puts "Aggiungo i Supporti Rigidi"

# Aggiungo il Taglio
puts "Aggiungo il Taglio"
c = Spree::Cut.new(:hourly_cost => 30,
              :average_hourly_print => 300,
              :start_time => 5)
c.save

# Aggiugno la Fustella
puts "Aggiungo la Fustella"
hp = Spree::HollowPunch.new(:hourly_cost => 60,
                      :average_hourly_print => 800,
                      :start_time => 12)
hp.save

# Aggiungo la Spiralatura
puts "Aggiungo la Spiralatura"
s = Spree::Spiraling.new(:hourly_cost => 0,
                    :average_hourly_work => 0,
                    :start_time => 0)
s.save

# Aggiungo l'Intecalatura
puts "Aggiungo l'Intecalatura"
i = Spree::Intercalation.new
i.save!
# Aggiungo le intecalature di default
puts "Aggiungo le intecalature di default"
vq1 = i.intercalation_quantities.new(:name => "7", :hourly_cost => 5, :average_hourly => 20, :start_time => 5)
vq1.save
vq2 = i.intercalation_quantities.new(:name => "13", :hourly_cost => 10, :average_hourly => 40, :start_time => 10)
vq2.save

# Aggiungo la Foratura
puts "Aggiungo la Foratura"
p = Spree::Puncture.new(:hourly_cost => 0,
                  :average_hourly_print => 0,
                  :start_time => 0)
p.save

# Aggiungo la Cordonatura
puts "Aggiungo la Cordonatura"
c = Spree::Creasing.new(:hourly_cost => 30,
                        :average_hourly_print => 1000,
                        :start_time => 30)
c.save

# Aggiungo il Taglio Bobine
puts "Aggiungo il Taglio Bobine"
cc = Spree::CuttingCoil.new(:active => false)
cc.save
Spree::OptionType.create(:name => 'cutting_coil_type', :presentation => 'Tipologie Taglio Bobine')

# Aggiungo la Lamintaura
puts "Aggiungo la Laminatura"
l = Spree::Lamination.create()
Spree::OptionType.create(:name => 'lamination_type', :presentation => 'Tipologie Laminature')

# Aggiungo la lavorazione plotter
puts "Aggiungo la Lavorazione Plotter"
pp = Spree::ProcessingPlotter.create()
Spree::OptionType.create(:name => 'processing_plotter_type', :presentation => 'Tipologie Lavorazione Plotter')

# Aggiungo la Piegatura
puts "Aggiungo la Legatoria"
Spree::OptionType.create(:name => 'bindery', :presentation => 'Legatoria')
Spree::OptionType.create(:name => 'bending', :presentation => 'Piegatura')
Spree::OptionType.create(:name => 'punching', :presentation => 'Punto Metallico')
Spree::OptionType.create(:name => 'seam', :presentation => 'Cucitura')
Spree::OptionType.create(:name => 'sleeve', :presentation => 'Copertina Legatoria')
Spree::OptionType.create(:name => 'milling', :presentation => 'Fresatura')

pms = Spree::Bindery.create(:name => "Punto Metallico Standard", :external => true,
                            :min_limit_bending => 0, :cost_bending => 0, :min_limit_punching => 0,
                            :cost_punching => 0, :min_limit_seam => 0, :cost_seam => 0, :min_limit_sleeve => 0,
                            :cost_sleeve => 0, :min_limit_milling => 0, :cost_milling => 0)
pms.punchings.create(:type_quantity => 1, :copy_cost => 0.02)
pms.punchings.create(:type_quantity => 2, :copy_cost => 0.025)
pms.punchings.create(:type_quantity => 3, :copy_cost => 0.035)
pms.punchings.create(:type_quantity => 4, :copy_cost => 0.05)
pms.punchings.create(:type_quantity => 5, :copy_cost => 0.06)
pms.punchings.create(:type_quantity => 6, :copy_cost => 0.075)
pms.seam(:type_quantity => 1, :copy_cost => 0)
pms.milling(:type_quantity => 1, :copy_cost => 0)
pms.sleeve(:type_quantity => 1, :copy_cost => 0, :copy_cost_fins => 0)

pmc = Spree::Bindery.create(:name => "Punto Metallico a copia", :external => true,
                            :min_limit_bending => 0, :cost_bending => 0, :min_limit_punching => 1,
                            :cost_punching => 25, :min_limit_seam => 0, :cost_seam => 0, :min_limit_sleeve => 0,
                            :cost_sleeve => 0, :min_limit_milling => 0, :cost_milling => 0, :copy_cost_punching => true)
pmc.punchings.create(:type_quantity => 1, :copy_cost => 0.4)
pmc.seam(:type_quantity => 1, :copy_cost => 0)
pmc.milling(:type_quantity => 1, :copy_cost => 0)
pmc.sleeve(:type_quantity => 1, :copy_cost => 0, :copy_cost_fins => 0)

bfc = Spree::Bindery.create(:name => "Brossura Fresata a copia", :external => true,
                            :min_limit_bending => 0, :cost_bending => 0, :min_limit_punching => 0,
                            :cost_punching => 0, :min_limit_seam => 0, :cost_seam => 0, :min_limit_sleeve => 0,
                            :cost_sleeve => 0, :min_limit_milling => 1, :cost_milling => 25, :copy_cost_milling => true)
bfc.seam(:type_quantity => 1, :copy_cost => 0)
bfc.milling(:type_quantity => 1, :copy_cost => 1.6)
bfc.sleeve(:type_quantity => 1, :copy_cost => 0, :copy_cost_fins => 0)

bffrp = Spree::Bindery.create(:name => "Brossura Cucita Filo Refe PRO", :external => true,
                            :min_limit_bending => 1, :cost_bending => 52, :min_limit_punching => 0,
                            :cost_punching => 0, :min_limit_seam => 1, :cost_seam => 80, :min_limit_sleeve => 1,
                            :cost_sleeve => 80, :min_limit_milling => 0, :cost_milling => 0)
bffrp.bendings.create(:type_quantity => 4, :copy_cost => 0.0047)
bffrp.bendings.create(:type_quantity => 8, :copy_cost => 0.0059)
bffrp.bendings.create(:type_quantity => 12, :copy_cost => 0.0077)
bffrp.bendings.create(:type_quantity => 16, :copy_cost => 0.0079)
bffrp.bendings.create(:type_quantity => 24, :copy_cost => 0.0094)
bffrp.bendings.create(:type_quantity => 32, :copy_cost => 0.01)
bffrp.seam(:type_quantity => 1, :copy_cost => 0.012)
bffrp.milling(:type_quantity => 1, :copy_cost => 0)
bffrp.sleeve(:type_quantity => 1, :copy_cost => 0.13, :copy_cost_fins => 0.18)

bcs = Spree::Bindery.create(:name => "Brossura Cucita Standard", :external => true,
                            :min_limit_bending => 0, :cost_bending => 0, :min_limit_punching => 0,
                            :cost_punching => 0, :min_limit_seam => 0, :cost_seam => 0, :min_limit_sleeve => 0,
                            :cost_sleeve => 0, :min_limit_milling => 0, :cost_milling => 0)
bcs.seam(:type_quantity => 1, :copy_cost => 0.02)
bcs.milling(:type_quantity => 1, :copy_cost => 0)
bcs.sleeve(:type_quantity => 1, :copy_cost => 0.13, :copy_cost_fins => 0.18)

bfs = Spree::Bindery.create(:name => "Brossura Fresata Standard", :external => true,
                            :min_limit_bending => 0, :cost_bending => 0, :min_limit_punching => 0,
                            :cost_punching => 0, :min_limit_seam => 0, :cost_seam => 0, :min_limit_sleeve => 0,
                            :cost_sleeve => 0, :min_limit_milling => 1, :cost_milling => 150)
bfs.seam(:type_quantity => 1, :copy_cost => 0)
bfs.milling(:type_quantity => 1, :copy_cost => 0.01)
bfs.sleeve(:type_quantity => 1, :copy_cost => 0.11, :copy_cost_fins => 0)


# Aggiungo i Tipi di Pieghe per i Formati
puts "Aggiungo i Tipi di Pieghe per i Formati"
Spree::OptionType.create(:name => 'folding_format', :presentation => 'Formati stampa per i Pieghevoli')
# Aggiungo le Pieghe
puts "Aggiungo le Pieghe"
Spree::OptionType.create(:name => 'folding', :presentation => 'Pieghe')
# Aggiungo le Pieghe per Grammatura
puts "Aggiungo le Pieghe per Grammatura"
Spree::OptionType.create(:name => 'folding_weight', :presentation => 'Pieghe per Grammatura')

f1 = Spree::Folding.create(:name => "Quartino 1 piega", :presentation => "1", :plastification => true, :active => true)
f1.create_template_format(:attachment => File.open('public/default/template_format/1.jpeg'))
f1.hourly_cost = 60
f1.start_time = 20
f1.average_hourly_plastification = 4000
f1.folding_weights.create(:average_hourly => 8000, :weight_limit => 170)
f1.save!

p2 = Spree::Folding.create(:name => "Portafoglio 2 pieghe", :presentation => "p2", :plastification => true, :active => true)
p2.create_template_format(:attachment => File.open('public/default/template_format/p2.jpeg'))
p2.hourly_cost = 60
p2.start_time = 25
p2.average_hourly_plastification = 3000
p2.folding_weights.create(:average_hourly => 6000, :weight_limit => 170)
p2.save!

zz2 = Spree::Folding.create(:name => "Zig-zag 2 pieghe", :presentation => "zz2", :plastification => true, :active => true)
zz2.create_template_format(:attachment => File.open('public/default/template_format/zz2.jpeg'))
zz2.hourly_cost = 60
zz2.start_time = 25
zz2.average_hourly_plastification = 2500
zz2.folding_weights.create(:average_hourly => 5500, :weight_limit => 170)
zz2.save!

f3 = Spree::Folding.create(:name => "Finestra 3 pieghe", :presentation => "f3", :plastification => false, :active => true)
f3.create_template_format(:attachment => File.open('public/default/template_format/f3.jpeg'))
f3.hourly_cost = 60
f3.start_time = 40
f3.folding_weights.create(:average_hourly => 4000, :weight_limit => 170)
f3.save!

c2 = Spree::Folding.create(:name => "Croce 2 pieghe", :presentation => "c2", :plastification => true, :active => true)
c2.create_template_format(:attachment => File.open('public/default/template_format/c2.jpeg'))
c2.hourly_cost = 60
c2.start_time = 20
c2.average_hourly_plastification = 3000
c2.folding_weights.create(:average_hourly => 6500, :weight_limit => 170)
c2.save!

p3 = Spree::Folding.create(:name => "Portafoglio 3 pieghe", :presentation => "p3", :plastification => true, :active => true)
p3.create_template_format(:attachment => File.open('public/default/template_format/p3.jpeg'))
p3.hourly_cost = 60
p3.start_time = 40
p3.average_hourly_plastification = 2000
p3.folding_weights.create(:average_hourly => 4000, :weight_limit => 170)
p3.save!

zz3 = Spree::Folding.create(:name => "Zig-zag 3 pieghe", :presentation => "zz3", :plastification => true, :active => true)
zz3.create_template_format(:attachment => File.open('public/default/template_format/zz3.jpeg'))
zz3.hourly_cost = 60
zz3.start_time = 40
zz3.average_hourly_plastification = 2000
zz3.folding_weights.create(:average_hourly => 4000, :weight_limit => 170)
zz3.save!

# ========================================== Aggiungo le Finiture Nascoste ========================================== #
# Aggiungo l'orientatamento come option type
puts "Aggiunto l'orientamento"
d = Spree::OptionType.create :name => 'printer_orientation', :presentation => 'Orientamento Carta'
d.option_values.create :name => 'horizontal', :presentation => 'Orizzontale'
d.option_values.create :name => 'vertical', :presentation => 'Verticale'

# Aggiungo il fronte e retro
puts "Aggiunto stampa fronte e retro"
fb = Spree::OptionType.create :name => "printer_instructions", :presentation => 'Dettagli di stampa'
fb.option_values.create :name => 'front_back', :presentation => 'Fronte e retro'
fb.option_values.create :name => 'equal_front_back', :presentation => 'Fronte e retro uguali'
fb.option_values.create :name => 'different_front_back', :presentation => 'Fronte e retro differenti'
fb.option_values.create :name => 'front', :presentation => 'Solo fronte'

# Aggiungo se a colori o bianco e nero
puts "Aggiungo Tipologia di stampa"
pc = Spree::OptionType.create :name => "printer_color", :presentation => 'Tipologia di stampa'
pc.option_values.create :name => 'true', :presentation => 'Colori'
pc.option_values.create :name => 'false', :presentation => 'Bianco e Nero'

# Aggiungo il costo orario come in OptionType che avrà un unico OptionValue
hourly_cost = Spree::OptionType.create :name => 'hourly_cost', :presentation => 'Costo orario'
hourly_cost.option_values.create :name => "60 minuti", :presentation => '50'
puts "Aggiunto costo orario"

# Aggiungo le pose
puts "Aggiungo le Pose"
Spree::OptionType.create(:name => 'pose', :presentation => 'Pose')
Spree::Pose.create(:name => 1, :presentation => 4)
Spree::Pose.create(:name => 2, :presentation => 6)
Spree::Pose.create(:name => 3, :presentation => 8)
Spree::Pose.create(:name => 4, :presentation => 8)
Spree::Pose.create(:name => 5, :presentation => 12)
Spree::Pose.create(:name => 6, :presentation => 10)
Spree::Pose.create(:name => 7, :presentation => 16)
Spree::Pose.create(:name => 8, :presentation => 12)
Spree::Pose.create(:name => 9, :presentation => 12)
Spree::Pose.create(:name => 10, :presentation => 14)
Spree::Pose.create(:name => 12, :presentation => 14)
Spree::Pose.create(:name => 14, :presentation => 18)
Spree::Pose.create(:name => 16, :presentation => 16)
Spree::Pose.create(:name => 24, :presentation => 12)
Spree::Pose.create(:name => 25, :presentation => 20)
Spree::Pose.create(:name => 32, :presentation => 14)

# Aggiungo i quartini
puts "Aggiungo i Quartini"
Spree::OptionType.create(:name => 'quarter', :presentation => 'Taglio Quartini')
Spree::Quarter.create(:name => 1, :presentation => 4)
Spree::Quarter.create(:name => 2, :presentation => 8)
Spree::Quarter.create(:name => 3, :presentation => 12)
Spree::Quarter.create(:name => 4, :presentation => 16)
Spree::Quarter.create(:name => 6, :presentation => 24)
Spree::Quarter.create(:name => 8, :presentation => 32)
Spree::Quarter.create(:name => 12, :presentation => 48)
Spree::Quarter.create(:name => 16, :presentation => 64)

# ========================================== Aggiungo i Prodotti ========================================== #
# Aggiungo il Biglietto da Visita
puts "Aggiungo il Biglietto da Visita"
tp_pfu = Spree::OptionType.create(:name => "printer_format_card", :presentation => "Formati di stampa per i Biglietti")
pfca = Spree::OptionValue.create(:name => "8,5x5 cm", :presentation => "8,5x5 cm", :option_type_id => tp_pfu.id)
pfca.base => "8.5"
pfca.height => "5.0"
pfca.create_template_format(:attachment => File.open('public/default/template_format/bc.jpg'))
#pfca.create_template(:attachment => File.open('public/default/template/8-5x5_orizzontale.pdf'))
pfcb = Spree::OptionValue.create(:name => "9x5 cm", :presentation => "9x5 cm", :option_type_id => tp_pfu.id)
pfcb.base => "9.0"
pfcb.height => "5.0"
pfcb.create_template_format(:attachment => File.open('public/default/template_format/bc.jpg'))
#pfcb.create_template(:attachment => File.open('public/default/template/9x5_orizzontale.pdf'))
pfcc = Spree::OptionValue.create(:name => "8,5x5,5 cm", :presentation => "8,5x5,5 cm", :option_type_id => tp_pfu.id)
pfcc.base => "8.5"
pfcc.height => "5.5"
pfcc.create_template_format(:attachment => File.open('public/default/template_format/bc.jpg'))
#pfcc.create_template(:attachment => File.open('public/default/template/8-5x5-5_orizzontale.pdf'))

bcp = Spree::BusinessCardProduct.new()
bcp.save
bcp.taxons << Spree::Taxon.find_by_name("Piccolo Formato")
bcp.create_template(:attachment => File.open('public/default/template.pdf'))
bcp.master.images.create(:attachment => File.open('public/default/spree/business_card.jpg'))
Spree::ShippingDateAdjustment.create(:name => "Ritiro di Base", :days => 2, :preferred_amount => 0, :product_id => bcp.id)

# Aggiungo il formato di stampa di default per i manifesti
puts "Aggiungo il formato di stampa di default per i manifesti"
Spree::OptionType.create(:name => 'poster_printer_format', :presentation => 'Formato di stampa dei manifesti')
Spree::PosterPrinterFormat.create(:name => 'personalizzato', :presentation => 'Personalizzato', :plant_color_bw => '1', :plant_color_c => '1')
Spree::PosterPrinterFormat.create(:name => '21x33cm', :presentation => '21x33cm')
# Aggiungo il Manifesto
puts "Aggiungo il Manifesto"
pp = Spree::PosterProduct.new()
pp.save
pp.taxons << Spree::Taxon.find_by_name("Grande Formato")
pp.create_template(:attachment => File.open('public/default/template.pdf'))
#pp.create_template(:attachment => File.open('public/default/template/poster_template.pdf'))
pp.master.images.create(:attachment => File.open('public/default/spree/poster.jpg'))
Spree::ShippingDateAdjustment.create(:name => "Ritiro di Base", :days => 2, :preferred_amount => 0, :product_id => pp.id)
# Aggiungo le configurazione per stampante e carta
pp.plotter = Spree::Plotter.find_by_presentation("Epson 9880").id
pp.papers << Spree::CoilValue.find_by_presentation("Blueback 150 gr").id
pp.papers << Spree::CoilValue.find_by_presentation("Canvas 300 gr").id
pp.available_on = Time.now - 2.days
pp.save

# Aggiungo il Flyer
puts "Aggiungo il Flyer"
fp = Spree::FlayerProduct.new(:label_name => "Flyer")
fp.save
fp.taxons << Spree::Taxon.find_by_name("Piccolo Formato")
fp.create_template(:attachment => File.open('public/default/template.pdf'))
#fp.create_template(:attachment => File.open('public/default/template/flyer_template.pdf'))
fp.master.images.create(:attachment => File.open('public/default/spree/flyer.jpg'))
Spree::ShippingDateAdjustment.create(:name => "Ritiro di Base", :days => 2, :preferred_amount => 0, :product_id => fp.id)

# Aggiungo il Manifesto Classico
puts "Aggiungo il Manifesto Classico"
pfp = Spree::PosterFlyerProduct.new(:label_name => "Manifesto Classico")
pfp.save
pfp.taxons << Spree::Taxon.find_by_name("Grande Formato")
pfp.create_template(:attachment => File.open('public/default/template.pdf'))
#pfp.create_template(:attachment => File.open('public/default/template/poster_classic_template.pdf'))
pfp.master.images.create(:attachment => File.open('public/default/spree/poster_flyer.jpg'))
Spree::ShippingDateAdjustment.create(:name => "Ritiro di Base", :days => 2, :preferred_amount => 0, :product_id => pfp.id)

# Aggiungo il Punto Metallico
puts "Aggiungo il Punto Metallico"
sp = Spree::StapleProduct.new()
sp.save
sp.taxons << Spree::Taxon.find_by_name("Multipagina")
sp.create_template(:attachment => File.open('public/default/template.pdf'))
#sp.create_template(:attachment => File.open('public/default/template/staple_template.pdf'))
cfp_sp = Spree::CoverFlyerProduct.new(:label_name => "Staple", :parent_id => sp.id)
cfp_sp.save
sp.master.images.create(:attachment => File.open('public/default/spree/staple.jpg'))
Spree::ShippingDateAdjustment.create(:name => "Ritiro di Base", :days => 2, :preferred_amount => 0, :product_id => sp.id)

# Aggiungo la Brossura Filo Refe
puts "Aggiungo la Brossura Filo Refe"
pbp = Spree::PaperbackProduct.new(:label_name => "Filo Refe", :finishing => "seam")
pbp.save
pbp.taxons << Spree::Taxon.find_by_name("Multipagina")
pbp.create_template(:attachment => File.open('public/default/template.pdf'))
#pbp.create_template(:attachment => File.open('public/default/template/paperback_seam_template.pdf'))
cfp_pbp = Spree::CoverFlyerProduct.new(:label_name => "PaperbackSeam", :parent_id => pbp.id)
cfp_pbp.save
pbp.master.images.create(:attachment => File.open('public/default/spree/paperback_seam.jpg'))
Spree::ShippingDateAdjustment.create(:name => "Ritiro di Base", :days => 2, :preferred_amount => 0, :product_id => pbp.id)

# Aggiungo la Brossura Fresata
puts "Aggiungo la Brossura Fresata"
pbp = Spree::PaperbackProduct.new(:label_name => "Fresata", :finishing => "milling")
pbp.save
pbp.taxons << Spree::Taxon.find_by_name("Multipagina")
pbp.create_template(:attachment => File.open('public/default/template.pdf'))
#pbp.create_template(:attachment => File.open('public/default/template/paperback_milling_template.pdf'))
cfp_pbp = Spree::CoverFlyerProduct.new(:label_name => "PaperbackMilled", :parent_id => pbp.id)
cfp_pbp.save
pbp.master.images.create(:attachment => File.open('public/default/spree/paperback_milling.jpg'))
Spree::ShippingDateAdjustment.create(:name => "Ritiro di Base", :days => 2, :preferred_amount => 0, :product_id => pbp.id)

# Aggiungo il Pieghevole
puts "Aggiungo il Pieghevole"
fdp = Spree::FoldingProduct.new()
fdp.save
fdp.taxons << Spree::Taxon.find_by_name("Piccolo Formato")
fdp.create_template(:attachment => File.open('public/default/template.pdf'))
#fdp.create_template(:attachment => File.open('public/default/template/folding_template.pdf'))
fdp.master.images.create(:attachment => File.open('public/default/spree/folding.jpg'))
Spree::ShippingDateAdjustment.create(:name => "Ritiro di Base", :days => 2, :preferred_amount => 0, :product_id => fdp.id)

# Aggiungo Termosaldatura e Rinforzo Perimetrale e Tasca o Lembo
puts "Aggiungo Termosaldatura e Rinforzo Perimetrale"
Spree::OptionType.create(:name => 'heat_sealing', :presentation => 'Termosaldatura')
Spree::OptionType.create(:name => 'reinforcement_perimeter', :presentation => 'Rinforzo Perimetrale')
Spree::OptionType.create(:name => 'pocket', :presentation => 'Tasca o Lembo per Banner')
# Aggiungo lo Striscione
puts "Aggiungo lo Striscione"
bp = Spree::BannerProduct.new()
bp.save
bp.taxons << Spree::Taxon.find_by_name("Grande Formato")
bp.create_template(:attachment => File.open('public/default/template.pdf'))
#bp.create_template(:attachment => File.open('public/default/template/banner_template.pdf'))
bp.create_banner_variant
bp.master.images.create(:attachment => File.open('public/default/spree/banner.jpg'))
Spree::ShippingDateAdjustment.create(:name => "Ritiro di Base", :days => 2, :preferred_amount => 0, :product_id => bp.id)

# Aggiungo la Spirale
puts "Aggiungo la Spirale"
sp = Spree::SpiralProduct.new()
sp.save
sp.taxons << Spree::Taxon.find_by_name("Multipagina")
sp.create_template(:attachment => File.open('public/default/template.pdf'))
#sp.create_template(:attachment => File.open('public/default/template/spiral_template.pdf'))
cfp_sp = Spree::CoverFlyerProduct.new(:label_name => "Spiral", :parent_id => sp.id)
cfp_sp.save
sp.master.images.create(:attachment => File.open('public/default/spree/spiral.jpg'))
Spree::ShippingDateAdjustment.create(:name => "Ritiro di Base", :days => 2, :preferred_amount => 0, :product_id => sp.id)

# Aggiungo il Canvas
puts "Aggiungo il Canvas"
cp = Spree::CanvasProduct.new()
cp.save
cp.taxons << Spree::Taxon.find_by_name("Grande Formato")
cp.create_template(:attachment => File.open('public/default/template.pdf'))
#cp.create_template(:attachment => File.open('public/default/template/canvas_template.pdf'))
cp.create_canvas_variant
bp.master.images.create(:attachment => File.open('public/default/spree/canvas.jpg'))
Spree::ShippingDateAdjustment.create(:name => "Ritiro di Base", :days => 2, :preferred_amount => 0, :product_id => cp.id)

# Aggiungo la Carta Intestata
puts "Aggiungo la Carta Intestata"
lh = Spree::LetterheadProduct.new(:label_name => "Carta Intestata")
lh.save
lh.taxons << Spree::Taxon.find_by_name("Piccolo Formato")
lh.create_template(:attachment => File.open('public/default/template.pdf'))
#lh.create_template(:attachment => File.open('public/default/template/letterhead_template.pdf'))
lh.master.images.create(:attachment => File.open('public/default/spree/letterhead.jpg'))
Spree::ShippingDateAdjustment.create(:name => "Ritiro di Base", :days => 2, :preferred_amount => 0, :product_id => lh.id)

# Aggiungo le Cartoline e Inviti
puts "Aggiungo le Cartoline e Inviti"
pc = Spree::PostcardProduct.new(:label_name => "Cartoline e Inviti")
pc.save
pc.taxons << Spree::Taxon.find_by_name("Piccolo Formato")
pc.create_template(:attachment => File.open('public/default/template.pdf'))
#pc.create_template(:attachment => File.open('public/default/template/postcard_template.pdf'))
pc.master.images.create(:attachment => File.open('public/default/spree/postcard.jpg'))
Spree::ShippingDateAdjustment.create(:name => "Ritiro di Base", :days => 2, :preferred_amount => 0, :product_id => pc.id)

# Aggiungo le Locandine
puts "Aggiungo le Locandine"
pb = Spree::PlaybillProduct.new(:label_name => "Locandine")
pb.save
pb.taxons << Spree::Taxon.find_by_name("Piccolo Formato")
pb.create_template(:attachment => File.open('public/default/template.pdf'))
#pb.create_template(:attachment => File.open('public/default/template/playbill_template.pdf'))
pb.master.images.create(:attachment => File.open('public/default/playbill.jpg'))
Spree::ShippingDateAdjustment.create(:name => "Ritiro di Base", :days => 2, :preferred_amount => 0, :product_id => pb.id)

# Aggiungo il PVC Adesivo
puts "Aggiungo il PVC Adesivo"
pp = Spree::PvcProduct.new()
pp.save
pp.taxons << Spree::Taxon.find_by_name("Grande Formato")
pp.create_template(:attachment => File.open('public/default/template.pdf'))
#pp.create_template(:attachment => File.open('public/default/template/pvc_template.pdf'))
pp.create_pvc_variant
pp.master.images.create(:attachment => File.open('public/default/spree/pvc_stickers.jpg'))
Spree::ShippingDateAdjustment.create(:name => "Ritiro di Base", :days => 2, :preferred_amount => 0, :product_id => pp.id)

# Aggiungo il formato di stampa di default per i manifesti ad altissima qualita
puts "Aggiungo il formato di stampa di default per i manifesti ad altissima qualita"
Spree::OptionType.create(:name => 'poster_high_quality_printer_format', :presentation => "Formato di stampa dei manifesti ad altissima qualita")
Spree::PosterHighQualityPrinterFormat.create(:name => 'personalizzato', :presentation => 'Personalizzato', :plant_color_bw => '1', :plant_color_c => '1')
Spree::PosterHighQualityPrinterFormat.create(:name => '21x33cm', :presentation => '21x33cm')
# Aggiungo il Manifesto ad altissima qualita
puts "Aggiungo il Manifesto ad altissima qualita"
phqp = Spree::PosterHighQualityProduct.new()
phqp.save
phqp.taxons << Spree::Taxon.find_by_name("Grande Formato")
phqp.create_template(:attachment => File.open('public/default/template.pdf'))
#pp.create_template(:attachment => File.open('public/default/template/poster_template.pdf'))
phqp.master.images.create(:attachment => File.open('public/default/spree/poster.jpg'))
Spree::ShippingDateAdjustment.create(:name => "Ritiro di Base", :days => 2, :preferred_amount => 0, :product_id => phqp.id)

# Aggiungo il formato di stampa di default per i Supporti Rigidi
puts "Aggiungo il formato di stampa di default per i Supporti Rigidi"
Spree::OptionType.create(:name => 'rigid_printer_format', :presentation => "Formato di stampa dei supporti rigidi")
Spree::RigidPrinterFormat.create(:name => 'personalizzato', :presentation => 'Personalizzato', :plant_color_bw => '1', :plant_color_c => '1')
Spree::RigidPrinterFormat.create(:name => '21 x 33 cm', :presentation => '21 x 33 cm')
# Aggiungo il Supporto Rigido
puts "Aggiungo il Supporto Rigido"
rp = Spree::RigidProduct.new(:label_name => "Duplicato")
rp.save
rp.taxons << Spree::Taxon.find_by_name("Grande Formato")
rp.create_template(:attachment => File.open('public/default/template.pdf'))
#rp.create_template(:attachment => File.open('public/default/template/poster_template.pdf'))
rp.master.images.create(:attachment => File.open('public/default/spree/poster.jpg'))
Spree::ShippingDateAdjustment.create(:name => "Ritiro di Base", :days => 2, :preferred_amount => 0, :product_id => rp.id)

# Aggiungo il formato di stampa di default per le Etichette
puts "Aggiungo il formato di stampa di default per le Etichette"
Spree::OptionType.create(:name => 'sticker_printer_format', :presentation => "Formato di stampa delle etichette")
# Aggiungo le Etichette
puts "Aggiungo le Etichette"
st = Spree::StickerProduct.new(:label_name => "Plotter")
st.save
st.taxons << Spree::Taxon.find_by_name("Grande Formato")
st.create_template(:attachment => File.open('public/default/template.pdf'))
#st.create_template(:attachment => File.open('public/default/template/sticker_template.pdf'))
st.master.images.create(:attachment => File.open('public/default/spree/sticker.jpg'))
st.sticker_printer_formats.create(:name => 'personalizzato', :presentation => 'Personalizzato', :plant_color_bw => '1', :plant_color_c => '1')
st.sticker_printer_formats.create(:name => '21 x 33 cm', :presentation => '21 x 33 cm')
Spree::ShippingDateAdjustment.create(:name => "Ritiro di Base", :days => 2, :preferred_amount => 0, :product_id => st.id)
Spree::OptionType.create(:name => 'processing_plotter_combination', :presentation => 'Combinazioni di Lavorazione Plotter')

# Aggiungo le Etichette
puts "Aggiungo le Etichette"
st = Spree::StickerProduct.new(:label_name => "Etichette Copia 1")
st.save
st.taxons << Spree::Taxon.find_by_name("Grande Formato")
st.create_template(:attachment => File.open('public/default/template.pdf'))
#st.create_template(:attachment => File.open('public/default/template/sticker_template.pdf'))
st.master.images.create(:attachment => File.open('public/default/spree/sticker.jpg'))
st.sticker_printer_formats.create(:name => 'personalizzato', :presentation => 'Personalizzato', :plant_color_bw => '1', :plant_color_c => '1')
st.sticker_printer_formats.create(:name => '21 x 33 cm', :presentation => '21 x 33 cm')
Spree::ShippingDateAdjustment.create(:name => "Ritiro di Base", :days => 2, :preferred_amount => 0, :product_id => st.id)

# Aggiungo le Etichette
puts "Aggiungo le Etichette"
st = Spree::StickerProduct.new(:label_name => "B X H - 3")
st.save
st.taxons << Spree::Taxon.find_by_name("Grande Formato")
st.create_template(:attachment => File.open('public/default/template.pdf'))
#st.create_template(:attachment => File.open('public/default/template/sticker_template.pdf'))
st.master.images.create(:attachment => File.open('public/default/spree/sticker.jpg'))
st.sticker_printer_formats.create(:name => 'personalizzato', :presentation => 'Personalizzato', :plant_color_bw => '1', :plant_color_c => '1')
st.sticker_printer_formats.create(:name => '21 x 33 cm', :presentation => '21 x 33 cm')
Spree::ShippingDateAdjustment.create(:name => "Ritiro di Base", :days => 2, :preferred_amount => 0, :product_id => st.id)

# Aggiungo le Etichette
puts "Aggiungo le Etichette"
st = Spree::StickerProduct.new(:label_name => "B X H - 4")
st.save
st.taxons << Spree::Taxon.find_by_name("Grande Formato")
st.create_template(:attachment => File.open('public/default/template.pdf'))
#st.create_template(:attachment => File.open('public/default/template/sticker_template.pdf'))
st.master.images.create(:attachment => File.open('public/default/spree/sticker.jpg'))
st.sticker_printer_formats.create(:name => 'personalizzato', :presentation => 'Personalizzato', :plant_color_bw => '1', :plant_color_c => '1')
st.sticker_printer_formats.create(:name => '21 x 33 cm', :presentation => '21 x 33 cm')
Spree::ShippingDateAdjustment.create(:name => "Ritiro di Base", :days => 2, :preferred_amount => 0, :product_id => st.id)

# Aggiungo le Etichette
puts "Aggiungo le Etichette"
st = Spree::StickerProduct.new(:label_name => "B X H - 5")
st.save
st.taxons << Spree::Taxon.find_by_name("Grande Formato")
st.create_template(:attachment => File.open('public/default/template.pdf'))
#st.create_template(:attachment => File.open('public/default/template/sticker_template.pdf'))
st.master.images.create(:attachment => File.open('public/default/spree/sticker.jpg'))
st.sticker_printer_formats.create(:name => 'personalizzato', :presentation => 'Personalizzato', :plant_color_bw => '1', :plant_color_c => '1')
st.sticker_printer_formats.create(:name => '21 x 33 cm', :presentation => '21 x 33 cm')
Spree::ShippingDateAdjustment.create(:name => "Ritiro di Base", :days => 2, :preferred_amount => 0, :product_id => st.id)

# Aggiungo le Etichette
puts "Aggiungo le Etichette"
st = Spree::StickerProduct.new(:label_name => "B X H - 6")
st.save
st.taxons << Spree::Taxon.find_by_name("Grande Formato")
st.create_template(:attachment => File.open('public/default/template.pdf'))
#st.create_template(:attachment => File.open('public/default/template/sticker_template.pdf'))
st.master.images.create(:attachment => File.open('public/default/spree/sticker.jpg'))
st.sticker_printer_formats.create(:name => 'personalizzato', :presentation => 'Personalizzato', :plant_color_bw => '1', :plant_color_c => '1')
st.sticker_printer_formats.create(:name => '21 x 33 cm', :presentation => '21 x 33 cm')
Spree::ShippingDateAdjustment.create(:name => "Ritiro di Base", :days => 2, :preferred_amount => 0, :product_id => st.id)

# Aggiungo le Etichette
puts "Aggiungo le Etichette"
st = Spree::StickerProduct.new(:label_name => "B X H - 7")
st.save
st.taxons << Spree::Taxon.find_by_name("Grande Formato")
st.create_template(:attachment => File.open('public/default/template.pdf'))
#st.create_template(:attachment => File.open('public/default/template/sticker_template.pdf'))
st.master.images.create(:attachment => File.open('public/default/spree/sticker.jpg'))
st.sticker_printer_formats.create(:name => 'personalizzato', :presentation => 'Personalizzato', :plant_color_bw => '1', :plant_color_c => '1')
st.sticker_printer_formats.create(:name => '21 x 33 cm', :presentation => '21 x 33 cm')
Spree::ShippingDateAdjustment.create(:name => "Ritiro di Base", :days => 2, :preferred_amount => 0, :product_id => st.id)

# Aggiungo le Etichette
puts "Aggiungo le Etichette"
st = Spree::StickerProduct.new(:label_name => "B X H - 8")
st.save
st.taxons << Spree::Taxon.find_by_name("Grande Formato")
st.create_template(:attachment => File.open('public/default/template.pdf'))
#st.create_template(:attachment => File.open('public/default/template/sticker_template.pdf'))
st.master.images.create(:attachment => File.open('public/default/spree/sticker.jpg'))
st.sticker_printer_formats.create(:name => 'personalizzato', :presentation => 'Personalizzato', :plant_color_bw => '1', :plant_color_c => '1')
st.sticker_printer_formats.create(:name => '21 x 33 cm', :presentation => '21 x 33 cm')
Spree::ShippingDateAdjustment.create(:name => "Ritiro di Base", :days => 2, :preferred_amount => 0, :product_id => st.id)

# Aggiungo le Etichette
puts "Aggiungo le Etichette"
st = Spree::StickerProduct.new(:label_name => "B X H - 4")
st.save
st.taxons << Spree::Taxon.find_by_name("Grande Formato")
st.create_template(:attachment => File.open('public/default/template.pdf'))
#st.create_template(:attachment => File.open('public/default/template/sticker_template.pdf'))
st.master.images.create(:attachment => File.open('public/default/spree/sticker.jpg'))
st.sticker_printer_formats.create(:name => 'personalizzato', :presentation => 'Personalizzato', :plant_color_bw => '1', :plant_color_c => '1')
st.sticker_printer_formats.create(:name => '21 x 33 cm', :presentation => '21 x 33 cm')
Spree::ShippingDateAdjustment.create(:name => "Ritiro di Base", :days => 2, :preferred_amount => 0, :product_id => st.id)

# Aggiungo le Etichette IN BOBINA
puts "Aggiunto l'orientamento bobina"
d = Spree::OptionType.create :name => 'printer_orientation_coil', :presentation => 'Orientamento Bobina'

# hr = d.option_values.create :name => 'horizontal_right', :presentation => 'Svolgimento Orizzontale Destra'
# hr.create_template_format :attachment => File.open('public/default/spree/printer_coil/horizontal_right.jpg')
# 
# vd = d.option_values.create :name => 'vertical_down', :presentation => 'Svolgimento Verticale Sotto'
# vd.create_template_format :attachment => File.open('public/default/spree/printer_coil/vertical_down.jpg')
# 
# hl = d.option_values.create :name => 'horizontal_left', :presentation => 'Svolgimento Orizzontale Sinistra'
# hl.create_template_format :attachment => File.open('public/default/spree/printer_coil/horizontal_left.jpg')
# 
# vu = d.option_values.create :name => 'vertical_up', :presentation => 'Svolgimento Verticale Sopra'
# vu.create_template_format :attachment => File.open('public/default/spree/printer_coil/vertical_up.jpg')

hr = d.option_values.create :name => 'horizontal_up_right', :presentation => 'Svolgimento Orizzontale'
hr.create_template_format :attachment => File.open('public/default/spree/printer_coil/horizontal_up_right.jpg')

vd = d.option_values.create :name => 'vertical_up_down', :presentation => 'Svolgimento Verticale'
vd.create_template_format :attachment => File.open('public/default/spree/printer_coil/vertical_up_down.jpg')

hl = d.option_values.create :name => 'horizontal_up_left', :presentation => 'Svolgimento Orizzontale'
hl.create_template_format :attachment => File.open('public/default/spree/printer_coil/horizontal_up_left.jpg')

vu = d.option_values.create :name => 'vertical_up_up', :presentation => 'Svolgimento Verticale'
vu.create_template_format :attachment => File.open('public/default/spree/printer_coil/vertical_up_up.jpg')

puts "Aggiungo le Etichette"
st = Spree::StickerProduct.new(:label_name => "in bobina", :finishing => "coil")
st.save
st.taxons << Spree::Taxon.find_by_name("Grande Formato")
st.create_template(:attachment => File.open('public/default/template.pdf'))
#st.create_template(:attachment => File.open('public/default/template/sticker_template.pdf'))
st.master.images.create(:attachment => File.open('public/default/spree/sticker.jpg'))
st.sticker_printer_formats.create(:name => 'personalizzato', :presentation => 'Personalizzato', :plant_color_bw => '1', :plant_color_c => '1')
st.sticker_printer_formats.create(:name => '21 x 33 cm', :presentation => '21 x 33 cm')
Spree::ShippingDateAdjustment.create(:name => "Ritiro di Base", :days => 2, :preferred_amount => 0, :product_id => st.id)

Spree::SlideshowType.create(:category => "home", :enabled => false, :enable_pagination => true)
Spree::OptionType.create(:name => 'format_simple_variant', :presentation => 'Modelli Prodotti Semplici')
Spree::Template.create(:attachment => File.open('public/default/template.pdf'), :default => true)
