require 'csv'
# ========================================== Aggiungo le configurazioni ========================================== #
# Aggiungo le pagine
puts "Aggiungo le pagine"
Spree::StaticPage.create(:name => "Chi Siamo", :published_at => Time.now, :content => '<p><strong>Lorem ipsum</strong> dolor sit amet, consectetur adipiscing elit. Nunc ut leo sit amet dolor congue venenatis. Phasellus in augue vitae enim pretium ornare. Phasellus elit lectus, suscipit ut venenatis quis, faucibus sed mi. Cras sit amet dolor in ligula dictum dignissim ac at turpis. Aenean pulvinar condimentum metus, non accumsan leo laoreet eget. Nulla nec sem a metus posuere adipiscing. Quisque non risus mauris. Donec a magna eu lorem vestibulum rutrum. Quisque ut tortor nisl. Praesent nec tellus ac metus accumsan sollicitudin. Cras id commodo libero. Nullam mollis ultrices urna eu tristique. Donec eu lacinia justo. Donec fringilla, erat at lacinia rhoncus, sapien est pellentesque elit, vel cursus arcu erat nec nunc. Ut consequat, ipsum eu gravida semper, eros orci iaculis mi, nec ullamcorper eros nisl et enim. Nunc hendrerit sapien ut tellus posuere ac condimentum lectus rhoncus. Pellentesque mollis neque et tellus dapibus quis malesuada velit elementum. Fusce iaculis volutpat cursus. Nulla facilisi. Nulla facilisi. Nulla nisl metus, fermentum sit amet porttitor mollis, accumsan aliquam mauris. Etiam ultrices, dolor eget tincidunt accumsan, turpis ligula ultricies elit, quis congue nulla metus ut quam. Nunc a nunc neque, faucibus consequat lacus. Mauris egestas pretium neque, id feugiat nisi lobortis id. Nulla facilisi. Etiam sit amet ante sed augue sollicitudin accumsan. Duis eu urna vel ipsum lacinia auctor ac in metus. Sed ac enim eu quam hendrerit fringilla. Etiam non varius felis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Curabitur iaculis sagittis purus, quis pulvinar justo convallis non. Duis pulvinar, massa blandit pulvinar egestas, sem eros pretium ipsum, vitae mattis nulla turpis eu arcu. Nam id odio turpis. Etiam sagittis quam sit amet urna varius vitae bibendum justo dignissim. Morbi ullamcorper ultrices leo sed suscipit. Pellentesque ornare vestibulum velit, ut iaculis sem imperdiet ut. Lorem ipsum dolor sit amet, consectetur adipiscinsdasfct.</p>')
Spree::StaticPage.create(:name => "Info", :published_at => Time.now, :content => '<p><strong>Lorem ipsum</strong> dolor sit amet, consectetur adipiscing elit. Nunc ut leo sit amet dolor congue venenatis. Phasellus in augue vitae enim pretium ornare. Phasellus elit lectus, suscipit ut venenatis quis, faucibus sed mi. Cras sit amet dolor in ligula dictum dignissim ac at turpis. Aenean pulvinar condimentum metus, non accumsan leo laoreet eget. Nulla nec sem a metus posuere adipiscing. Quisque non risus mauris. Donec a magna eu lorem vestibulum rutrum. Quisque ut tortor nisl. Praesent nec tellus ac metus accumsan sollicitudin. Cras id commodo libero. Nullam mollis ultrices urna eu tristique. Donec eu lacinia justo. Donec fringilla, erat at lacinia rhoncus, sapien est pellentesque elit, vel cursus arcu erat nec nunc. Ut consequat, ipsum eu gravida semper, eros orci iaculis mi, nec ullamcorper eros nisl et enim. Nunc hendrerit sapien ut tellus posuere ac condimentum lectus rhoncus. Pellentesque mollis neque et tellus dapibus quis malesuada velit elementum. Fusce iaculis volutpat cursus. Nulla facilisi. Nulla facilisi. Nulla nisl metus, fermentum sit amet porttitor mollis, accumsan aliquam mauris. Etiam ultrices, dolor eget tincidunt accumsan, turpis ligula ultricies elit, quis congue nulla metus ut quam. Nunc a nunc neque, faucibus consequat lacus. Mauris egestas pretium neque, id feugiat nisi lobortis id. Nulla facilisi. Etiam sit amet ante sed augue sollicitudin accumsan. Duis eu urna vel ipsum lacinia auctor ac in metus. Sed ac enim eu quam hendrerit fringilla. Etiam non varius felis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Curabitur iaculis sagittis purus, quis pulvinar justo convallis non. Duis pulvinar, massa blandit pulvinar egestas, sem eros pretium ipsum, vitae mattis nulla turpis eu arcu. Nam id odio turpis. Etiam sagittis quam sit amet urna varius vitae bibendum justo dignissim. Morbi ullamcorper ultrices leo sed suscipit. Pellentesque ornare vestibulum velit, ut iaculis sem imperdiet ut. Lorem ipsum dolor sit amet, consectetur adipiscinsdasfct.</p>')

# Aggiungo i ruoli e l'utente admin
puts "Aggiungo i ruoli e l'utente admin"
Spree::Role.create(:name => "user")
Spree::Role.create(:name => "admin")
Spree::User.create(:email => "admin@diginess.it", :password => "asdasd", :password_confirmation => "asdasd", :login => "admin@diginess.it")

# Aggiungo le Country
puts "Aggiungo le Country"
CSV.parse(File.open('public/default/country.csv'), :col_sep => ",", :headers => :first_row) do |row|
  c = Spree::Country.new
  c.iso_name = row["iso_name"]
  c.iso = row["iso"]
  c.iso3 = row["iso3"]
  c.name = row["name"]
  c.numcode = row["numcode"]
  c.save!
end

# Aggiungo gli State
puts "Aggiungo gli State"
CSV.parse(File.open('public/default/state.csv'), :col_sep => ",", :headers => :first_row) do |row|
  s = Spree::State.new
  s.name = row["name"]
  s.abbr = row["abbr"]
  s.country_id = Spree::Country.find_by_iso("IT").id
  s.save!
end

# Aggiungo le ZoneMember
puts "Aggiungo le ZoneMember"
z = Spree::Zone.create(:name => "EU_VAT", :description => "Countries that make up the EU VAT zone.", :default_tax => true)
%w(IT GB SE ES SI SK RO PT PL NL MT LU LT LV IT IE HU DE FR FI EE DK CZ CY BG BE AT).each do |c|
  Spree::ZoneMember.create(:zoneable => Spree::Country.find_by_iso(c), :zone_id => z.id)
end

# Aggiungo i PresentationPack
puts "Aggiungo i PresentationPack"
Spree::PresentationPack.create(:name => "Anonimo", :presentation => "Pacco Anonimo", :price => 5.0, :icon => File.open('public/default/anonymous_package.png'))
Spree::PresentationPack.create(:name => "Print 2 Go", :presentation => "Pacco Print 2 Go", :price => 3.0, :icon => File.open('public/default/personalized_package.png'))

# Aggiungo la TaxRate
puts "Aggiungo la TaxRate"
tc = Spree::TaxCategory.create(:name => "IVA", :description => "IVA", :is_default => true)
tr = Spree::TaxRate.create(:zone_id => z.id, :tax_category_id => tc.id, :included_in_price => true, :amount => 0.21)
dt = Spree::Calculator::DefaultTax.new()
dt.calculable = tr
tr.calculator = dt
tr.save

# Aggiungo i metodi di Spedizione
puts "Aggiungo i metodi di Spedizione"
stc = Spree::ShippingCategory.create(:name => "Spedizione tramite corriere")
rns = Spree::ShippingCategory.create(:name => "Ritiro presso nostra sede")
sm = Spree::ShippingMethod.create(:name => "Bartolini", :zone_id => z.id, :min_weight => 0.0, :max_weight => 3.0, :shipping_category_id => stc.id)
fr = Spree::Calculator::FlatRate.new()
fr.calculable = sm
sm.calculator = fr
sm.calculator.preferred_amount = 10
sm.save

# Aggiungo le Categorie di Default
pf = Spree::Taxonomy.create(:name => "Piccolo Formato")
pf.default = true
pf.save
pf.taxons.first.default = true
pf.taxons.first.save

gf = Spree::Taxonomy.create(:name => "Grande Formato")
gf.default = true
gf.save
gf.taxons.first.default = true
gf.taxons.first.save

mp = Spree::Taxonomy.create(:name => "Multipagina")
mp.default = true
mp.save
mp.taxons.first.default = true
mp.taxons.first.save

pr = Spree::Taxonomy.create(:name => "Prodotti Ready")
pr.default = true
pr.save
pr.taxons.first.default = true
pr.taxons.first.save