class AddWastePaperForPrinter < ActiveRecord::Migration
  def up
    add_column :spree_option_values, :waste_paper_bw, :integer, :defalut => 0
    add_column :spree_option_values, :waste_paper_c, :integer, :defalut => 0
  end

  def down
    remove_column :spree_option_values, :waste_paper_bw
    remove_column :spree_option_values, :waste_paper_c
  end
end
