class AddPaperWeightLimitToProduct < ActiveRecord::Migration
  def change
    add_column :spree_products, :max_facades_cover, :integer
    add_column :spree_products, :max_weight_cover, :integer
    add_column :spree_products, :max_facades_weight_cover, :integer
  end
end
