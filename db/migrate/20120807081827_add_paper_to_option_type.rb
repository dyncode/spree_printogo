class AddPaperToOptionType < ActiveRecord::Migration
  def change
    add_column :spree_option_values, :price, :decimal
    add_column :spree_option_values, :is_cut, :boolean
  end
end
