class CreateEntityAttributeValue < ActiveRecord::Migration
  def change
    create_table :spree_entity_attribute_values do |t|
      t.integer :entity_id, :attribute_id, :value_id
      
      t.timestamps
    end
  end
end
