class CreateSimpleValue < ActiveRecord::Migration
  def change
    create_table :spree_values do |t|
      t.integer :attribute_id
      t.string :presentation
      
      t.timestamps
    end
  end
end
