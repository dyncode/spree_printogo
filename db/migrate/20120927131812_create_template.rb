class CreateTemplate < ActiveRecord::Migration
  def up
    create_table :spree_templates do |t|
      t.has_attached_file :template
      t.integer :imageable_id
      t.string :imageable_type

      t.timestamps
    end
  end

  def down
    drop_table :spree_templates
  end
end
