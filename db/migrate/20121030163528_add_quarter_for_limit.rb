class AddQuarterForLimit < ActiveRecord::Migration
  def up
    add_column :spree_limits, :quarter_before_c_id, :integer
    add_column :spree_limits, :quarter_after_c_id, :integer
    add_column :spree_limits, :quarter_before_bw_id, :integer
    add_column :spree_limits, :quarter_after_bw_id, :integer
    add_column :spree_limits, :quarter_bw, :integer
    add_column :spree_limits, :quarter_c, :integer
  end

  def down
    remove_column :spree_limits, :quarter_before_c_id
    remove_column :spree_limits, :quarter_after_c_id
    remove_column :spree_limits, :quarter_before_bw_id
    remove_column :spree_limits, :quarter_after_bw_id
    remove_column :spree_limits, :quarter_bw
    remove_column :spree_limits, :quarter_c
  end
end
