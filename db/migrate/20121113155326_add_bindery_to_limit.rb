class AddBinderyToLimit < ActiveRecord::Migration
  def up
    add_column :spree_limits, :bindery_before_min_c_id, :integer
    add_column :spree_limits, :bindery_before_max_c_id, :integer
    add_column :spree_limits, :bindery_after_c_id, :integer
    add_column :spree_limits, :bindery_before_min_bw_id, :integer
    add_column :spree_limits, :bindery_before_max_bw_id, :integer
    add_column :spree_limits, :bindery_after_bw_id, :integer
    add_column :spree_limits, :bindery_bw, :integer
    add_column :spree_limits, :bindery_c, :integer
  end

  def down
    remove_column :spree_limits, :bindery_before_min_c_id
    remove_column :spree_limits, :bindery_before_max_c_id
    remove_column :spree_limits, :bindery_after_c_id
    add_column :spree_limits, :bindery_before_min_bw_id
    add_column :spree_limits, :bindery_before_max_bw_id
    add_column :spree_limits, :bindery_after_bw_id
    remove_column :spree_limits, :bindery_bw
    remove_column :spree_limits, :bindery_c
  end
end
