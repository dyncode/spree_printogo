class CreateLimits < ActiveRecord::Migration
  def change
    create_table :spree_limits do |t|
      t.integer :printer_before_c_id
      t.integer :pose_before_c_id
      t.integer :format_before_c_id

      t.integer :printer_after_c_id
      t.integer :pose_after_c_id
      t.integer :format_after_c_id


      t.integer :printer_before_bw_id
      t.integer :pose_before_bw_id
      t.integer :format_before_bw_id

      t.integer :printer_after_bw_id
      t.integer :pose_after_bw_id
      t.integer :format_after_bw_id

      t.integer :variant_id
      t.integer :quantity_c
      t.integer :quantity_bw

      t.timestamps
    end
  end
end
