class AddEnabledToSpreeHomepageElements < ActiveRecord::Migration
  def change
    add_column :spree_homepage_elements, :enabled, :boolean, :after => 'title'
  end
end
