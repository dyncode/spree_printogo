class CreateEntity < ActiveRecord::Migration
  def change
    create_table :spree_entities do |t|
      t.integer :variant_id
      t.decimal :price, {:precision => 10, :scale => 5}
      t.decimal :weight, {:precision => 10, :scale => 5}
      
      t.timestamps
    end
  end
end
