class AddMarginalityToProduct < ActiveRecord::Migration
  def up
    add_column :spree_products, :marginality, :integer, :default => 0
  end
  
  def down
    remove_column :spree_products, :marginality
  end
end
