class CreateShippingDateAdjustments < ActiveRecord::Migration
  def change
    create_table :spree_shipping_date_adjustments do |t|
      t.string :name
      t.integer :product_id

      t.timestamps
    end
  end
end
