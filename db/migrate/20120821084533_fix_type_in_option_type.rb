class FixTypeInOptionType < ActiveRecord::Migration
  def up
    remove_column :spree_option_types, :type
    add_column :spree_option_types, :text, :string
  end

  def down
    remove_column :spree_option_types, :text
    add_column :spree_option_types, :type, :string
  end
end
