class AddCheckToOrder < ActiveRecord::Migration
  def up
    add_column :spree_orders, :file_check, :boolean, :default => false
  end
  
  def down
    remove_column :spree_orders, :file_check
  end
end
