class CreateSimpleAttribute < ActiveRecord::Migration
  def change
    create_table :spree_attributes do |t|
      t.integer :variant_id
      t.string :name, :presentation
      
      t.timestamps
    end
  end
end
