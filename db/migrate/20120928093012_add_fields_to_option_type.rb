class AddFieldsToOptionType < ActiveRecord::Migration
  def change
    add_column :spree_option_types, :hourly_cost, :decimal
    add_column :spree_option_types, :start_time, :decimal
    add_column :spree_option_types, :average_hourly_work, :float
  end
end
