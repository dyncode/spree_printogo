class AddPrinterToOptionType < ActiveRecord::Migration
  def change
    add_column :spree_option_values, :start_up_cost_bw, :decimal
    add_column :spree_option_values, :plant_color_bw, :decimal
    add_column :spree_option_values, :price_front_bw, :decimal
    add_column :spree_option_values, :price_front_and_back_bw, :decimal

    add_column :spree_option_values, :start_up_cost_c, :decimal
    add_column :spree_option_values, :plant_color_c, :decimal
    add_column :spree_option_values, :price_front_c, :decimal
    add_column :spree_option_values, :price_front_and_back_c, :decimal

    #add_column :spree_option_values, :formats, :string
  end
end
