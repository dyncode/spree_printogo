class AddTypeToVariants < ActiveRecord::Migration
  def change
    add_column :spree_variants, :type, :string
  end
end
