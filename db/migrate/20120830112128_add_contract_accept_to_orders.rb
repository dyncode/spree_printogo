class AddContractAcceptToOrders < ActiveRecord::Migration
  def change
    add_column :spree_orders, :contract_accept, :boolean, :default => false
  end
end
