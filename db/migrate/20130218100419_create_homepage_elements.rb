class CreateHomepageElements < ActiveRecord::Migration
	def change
	    create_table :spree_homepage_elements do |t|
	      t.string	:title, :url
	      t.text 	:description
	      t.integer :position
	      t.string 	:class_list
	      t.integer :homepageble_id
    	  t.string 	:homepageble_type

	      t.string   :attachment_content_type, :attachment_file_name, :attachment_content_type
	      t.datetime :attachment_updated_at
	      t.integer  :attachment_size
	      
	      t.timestamps
	    end
	end
end
