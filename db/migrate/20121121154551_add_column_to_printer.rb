class AddColumnToPrinter < ActiveRecord::Migration
  def up
    add_column :spree_option_values, :color_type, :integer
    add_column :spree_option_values, :min_limit_waiting, :integer
    add_column :spree_option_values, :cost_waiting, :integer
  end
  
  def down
    remove_column :spree_option_values, :color_type
    remove_column :spree_option_values, :min_limit_waiting
    remove_column :spree_option_values, :cost_waiting
  end
end
