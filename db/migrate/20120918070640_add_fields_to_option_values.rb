class AddFieldsToOptionValues < ActiveRecord::Migration
  def change
    add_column :spree_option_values, :printer_hourly_cost_c, :decimal
    add_column :spree_option_values, :start_time_c, :float
    add_column :spree_option_values, :average_hourly_print_c, :float

    add_column :spree_option_values, :printer_hourly_cost_bw, :decimal
    add_column :spree_option_values, :start_time_bw, :float
    add_column :spree_option_values, :average_hourly_print_bw, :float
  end
end
