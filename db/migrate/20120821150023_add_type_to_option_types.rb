class AddTypeToOptionTypes < ActiveRecord::Migration
  def change
    add_column :spree_option_types, :type, :string
  end
end
