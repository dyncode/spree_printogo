class AddFieldToLineItemPromotion < ActiveRecord::Migration
  def up
    add_column :spree_line_item_promotions, :original_price, :decimal, :after => :price
    add_column :spree_line_item_promotions, :original_total, :decimal, :after => :original_price
    change_column :spree_line_item_promotions, :original_price, :decimal, {:precision => 10, :scale => 5}
    change_column :spree_line_item_promotions, :original_total, :decimal, {:precision => 10, :scale => 5}
    add_column :spree_line_item_promotions, :promo_code, :string, :after => :expires_at
  end
  
  def down
    remove_column :spree_line_item_promotions, :original_price
    remove_column :spree_line_item_promotions, :original_total
    remove_column :spree_line_item_promotions, :promo_code
  end
end
