class AddReferencesToOptionValueAndPaperProduct < ActiveRecord::Migration
  def up
    add_column :spree_paper_products, :folding_format_id, :integer
    add_index :spree_paper_products, :folding_format_id
    add_column :spree_option_values, :variant_id, :integer, :after => :option_type_id
    add_index :spree_option_values, :variant_id
  end
  
  def down
    remove_column :spree_paper_products, :folding_format_id
    remove_index :spree_paper_products, :folding_format_id
    remove_column :spree_option_values, :variant_id
    remove_index :spree_option_values, :variant_id
  end
end
