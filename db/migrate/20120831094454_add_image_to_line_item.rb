class AddImageToLineItem < ActiveRecord::Migration
  def self.up
      return if column_exists?(:spree_line_items, :image_file_name)
      add_column :spree_line_items, :image_file_name, :string
      add_column :spree_line_items, :image_content_type, :string
      add_column :spree_line_items, :image_file_size, :integer
      add_column :spree_line_items, :image_updated_at, :datetime
      add_column :spree_line_items, :image_width, :integer
      add_column :spree_line_items, :image_height, :integer
    end
  
    def self.down
      remove_column :spree_line_items, :image_file_name
      remove_column :spree_line_items, :image_content_type
      remove_column :spree_line_items, :image_file_size
      remove_column :spree_line_items, :image_updated_at
      remove_column :spree_line_items, :image_width, :integer
      remove_column :spree_line_items, :image_height, :integer
    end
end
