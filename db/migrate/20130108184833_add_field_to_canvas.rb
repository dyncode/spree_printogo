class AddFieldToCanvas < ActiveRecord::Migration
  def up
    add_column :spree_products, :max_width, :integer
    add_column :spree_products, :max_height, :integer
  end
  
  def down
    remove_column :spree_products, :max_width
    remove_column :spree_products, :max_height
  end
end
