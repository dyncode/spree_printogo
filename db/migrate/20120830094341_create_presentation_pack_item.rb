class CreatePresentationPackItem < ActiveRecord::Migration
  def up
    create_table :spree_presentation_packs do |t|
      t.string :name
      t.string :presentation

      t.string :icon_file_name
      t.string :icon_content_type
      t.integer  :icon_file_size
      t.integer  :icon_width
      t.integer  :icon_height
      t.datetime :icon_updated_at

      t.timestamps
    end
  end

  def down
    drop_table :spree_presentation_packs
  end
end
