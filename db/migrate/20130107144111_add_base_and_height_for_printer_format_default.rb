class AddBaseAndHeightForPrinterFormatDefault < ActiveRecord::Migration
  def up
    add_column :spree_option_values, :base, :string
    add_column :spree_option_values, :height, :string
  end

  def down
    remove_column :spree_option_values, :base
    remove_column :spree_option_values, :height
  end
end
