class AddQuantityToOptionValues < ActiveRecord::Migration
  def change
    add_column :spree_option_values, :quantity, :string
  end
end
