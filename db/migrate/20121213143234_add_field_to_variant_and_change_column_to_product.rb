class AddFieldToVariantAndChangeColumnToProduct < ActiveRecord::Migration
  def up
    add_column :spree_variants, :default_cover_price, :decimal, {:precision => 10, :scale => 5}
    change_column :spree_products, :max_weight_cover, :decimal, {:precision => 10, :scale => 5}
  end
  
  def down
    remove_column :spree_variants, :default_cover_price
    change_column :spree_products, :max_weight_cover, :integer
  end
end
