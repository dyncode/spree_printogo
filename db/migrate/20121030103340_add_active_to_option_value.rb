class AddActiveToOptionValue < ActiveRecord::Migration
  def change
    add_column :spree_option_values, :active, :boolean
  end
end
