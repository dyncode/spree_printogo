class UpdateTypeToOptionValues < ActiveRecord::Migration
  def up
    change_column :spree_option_values, :start_up_cost_bw, :decimal, {:precision => 10, :scale => 2}
    change_column :spree_option_values, :plant_color_bw, :decimal, {:precision => 10, :scale => 2}
    change_column :spree_option_values, :price_front_bw, :decimal, {:precision => 10, :scale => 2}
    change_column :spree_option_values, :price_front_and_back_bw, :decimal, {:precision => 10, :scale => 2}
    change_column :spree_option_values, :start_up_cost_c, :decimal, {:precision => 10, :scale => 2}
    change_column :spree_option_values, :plant_color_c, :decimal, {:precision => 10, :scale => 2}
    change_column :spree_option_values, :price_front_c, :decimal, {:precision => 10, :scale => 2}
    change_column :spree_option_values, :price_front_and_back_c, :decimal, {:precision => 10, :scale => 2}
  end

  def down
    change_column :spree_option_values, :start_up_cost_bw, :decimal
    change_column :spree_option_values, :plant_color_bw, :decimal
    change_column :spree_option_values, :price_front_bw, :decimal
    change_column :spree_option_values, :price_front_and_back_bw, :decimal
    change_column :spree_option_values, :start_up_cost_c, :decimal
    change_column :spree_option_values, :plant_color_c, :decimal
    change_column :spree_option_values, :price_front_c, :decimal
    change_column :spree_option_values, :price_front_and_back_c, :decimal
  end
end
