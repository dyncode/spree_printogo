Spree::Core::Engine.routes.draw do
  match 'admin/create_business_cards/:id', :to => 'admin/products#create_business_cards', :as => :create_business_card
  match 'admin/update_business_cards/:id', :to => 'admin/products#update_business_cards', :as => :update_business_card
  match 'admin/new_business_cards', :to => 'admin/products#new_business_cards', :as => :new_business_card
  match 'admin/delete_business_cards/:id', :to => 'admin/products#delete_business_cards', :as => :delete_business_card

  namespace 'admin' do
    match 'retrieve_printer_format/:id', :to => 'printers#retrieve_printer_format'
    match 'calculate_convenience', :to => 'printers#calculate_convenience'
    match ':product_id/retrive_paper_weight/:paper_id/:id', :to => 'preventive#retrive_paper_weight'
    match ':product_id/retrive_paper/:id', :to => 'preventive#retrive_paper'
    match ':product_id/retrive_paper_weight_cover/:paper_id/:id', :to => 'preventive#retrive_paper_weight_cover'
    match ':product_id/retrive_paper_cover/:id', :to => 'preventive#retrive_paper_cover'
    match ':product_id/retrive_folding_format/:id', :to => 'preventive#retrive_folding_format'
    match 'precalculate', :to => 'preventive#precalculate'
    
    match 'paper/edit', :to => 'papers#edit', :as => :papers
    match 'hourly_costs/edit', :to => 'hourly_costs#edit', :as => :hourly_costs
    match 'cut/edit', :to => 'cut#edit', :as => :cut
    match 'hollow_punch/edit', :to => 'hollow_punch#edit', :as => :hollow_punch
    match 'plasticizations/edit', :to => 'plasticizations#edit', :as => :plasticizations
    match 'cutting_coil/edit', :to => 'cutting_coil#edit', :as => :cutting_coil
    match 'cutting_coil/:id', :to => 'cutting_coil#update', :as => :cutting_coil_update
    match 'lamination/edit', :to => 'lamination#edit', :as => :lamination
    match 'lamination/:id', :to => 'lamination#update', :as => :lamination_update
    match 'processing_plotter/edit', :to => 'processing_plotter#edit', :as => :processing_plotter
    match 'processing_plotter/:id', :to => 'processing_plotter#update', :as => :processing_plotter_update
    
    match 'edit_business_cards/:product_id/images/update/:id', :to => 'images#update', :as => :business_card_product_image

    match 'intercalations/', :to => 'intercalations#show', :as => :intercalations
    match 'intercalations/:intercalation_id/quantity/edit/:id', :to => 'intercalation_quantities#edit', :as => :edit_intercalation_quantities
    match 'intercalations/:intercalation_id/quantity:id', :to => 'intercalation_quantities#show', :as => :show_intercalation_quantities
    match 'intercalations/:intercalation_id/quantity/update/:id', :to => 'intercalation_quantities#update', :as => :update_intercalation_quantities

    match 'puncture/edit', :to => 'punctures#edit', :as => :punctures
    match 'puncture/update', :to => 'punctures#update', :as => :puncture_update
    match 'creasing/edit', :to => 'creasings#edit', :as => :creasings
    match 'creasing/update', :to => 'creasings#update', :as => :creasing_update
    
    match 'business_cards/edit/:id', :to => 'products#edit_business_cards', :as => :business_cards_edit
    match 'business_cards/:id/delete_template', :to => 'products#delete_template_business_cards', :as => :delete_template_business_card
    
    # Routing for Poster (MANIFESTO)
    match 'posters/edit/:id', :to => 'products#edit_posters', :as => :posters_edit
    match 'posters/update/:id', :to => 'products#update_posters', :as => :posters_update, :via => :put
    match 'posters/:product_id/images/update/:id', :to => 'images#update', :as => :poster_product_image
    # Routing for Poster Printer Format (FORMATI MANIFESTO)
    match 'posters/edit/:product_id/poster_printer_formats/index', :to => 'poster_printer_formats#index', :as => :poster_printer_formats_index
    match 'posters/edit/:product_id/poster_printer_formats/new', :to => 'poster_printer_formats#new', :as => :poster_printer_formats_new
    match 'posters/edit/:product_id/poster_printer_formats/create', :to => 'poster_printer_formats#create', :as => :poster_printer_formats_create
    match 'posters/edit/:product_id/poster_printer_formats/destroy/:id', :to => 'poster_printer_formats#destroy', :as => :poster_printer_formats_destroy
    match 'posters/edit/:product_id/poster_printer_formats/edit/:id', :to => 'poster_printer_formats#edit', :as => :poster_printer_formats_edit
    match 'posters/edit/:product_id/poster_printer_formats/update/:id', :to => 'poster_printer_formats#update', :as => :poster_printer_formats_update
    
    # Routing for PosterFlyer (MANIFESTO CLASSICO)
    match 'poster_flyers/edit/:id', :to => 'products#edit_poster_flyers', :as => :poster_flyers_edit
    match 'poster_flyers/update/:id', :to => 'products#update_poster_flyers', :as => :poster_flyers_update, :via => :put
    match 'poster_flyers/:product_id/images/update/:id', :to => 'images#update', :as => :poster_flyer_product_image
    # Routing for PosterFlyer Variant (FORMATI MANIFESTO CLASSICO)
    match 'poster_flyers/:poster_flyer_id/variants', :to => 'poster_flyer_variants#index', :as => :poster_flyer_variants
    match 'poster_flyers/:poster_flyer_id/variants/new', :to => 'poster_flyer_variants#new', :as => :new_poster_flyer_variants
    match 'poster_flyers/:poster_flyer_id/variants/create', :to => 'poster_flyer_variants#create', :as => :create_poster_flyer_variant
    match 'poster_flyers/:poster_flyer_id/variants/edit/:id', :to => 'poster_flyer_variants#edit', :as => :edit_poster_flyer_variant
    match 'poster_flyers/:poster_flyer_id/variants/update/:id', :to => 'poster_flyer_variants#update', :as => :update_poster_flyer_variant
    match 'poster_flyers/:poster_flyer_id/variants/:id', :to => 'poster_flyer_variants#destroy', :as => :delete_poster_flyer_variant, :method => :delete
    
    # Routing for Poster (MANIFESTO AD ALTISSIMA QUALITA)
    match 'poster_high_qualities/edit/:id', :to => 'products#edit_poster_high_qualities', :as => :poster_high_qualities_edit
    match 'poster_high_qualities/update/:id', :to => 'products#update_poster_high_qualities', :as => :poster_high_qualities_update, :via => :put
    match 'poster_high_qualities/:product_id/images/update/:id', :to => 'images#update', :as => :poster_high_quality_product_image
    # Routing for Poster Printer Format (FORMATI MANIFESTO AD ALTISSIMA QUALITA)
    match 'poster_high_qualities/edit/:product_id/poster_high_quality_printer_formats/index', :to => 'poster_high_quality_printer_formats#index', :as => :poster_high_quality_printer_formats_index
    match 'poster_high_qualities/edit/:product_id/poster_high_quality_printer_formats/new', :to => 'poster_high_quality_printer_formats#new', :as => :poster_high_quality_printer_formats_new
    match 'poster_high_qualities/edit/:product_id/poster_high_quality_printer_formats/create', :to => 'poster_high_quality_printer_formats#create', :as => :poster_high_quality_printer_formats_create
    match 'poster_high_qualities/edit/:product_id/poster_high_quality_printer_formats/destroy/:id', :to => 'poster_high_quality_printer_formats#destroy', :as => :poster_high_quality_printer_formats_destroy
    match 'poster_high_qualities/edit/:product_id/poster_high_quality_printer_formats/edit/:id', :to => 'poster_high_quality_printer_formats#edit', :as => :poster_high_quality_printer_formats_edit
    match 'poster_high_qualities/edit/:product_id/poster_high_quality_printer_formats/update/:id', :to => 'poster_high_quality_printer_formats#update', :as => :poster_high_quality_printer_formats_update
    
    # Routing for Flyer (FLYER)
    match 'flyers/edit/:id', :to => 'products#edit_flayers', :as => :flayers_edit
    match 'flyers/update/:id', :to => 'products#update_flayers', :as => :flayers_update, :via => :put
    match 'flyers/:product_id/images/update/:id', :to => 'images#update', :as => :flayer_product_image
    # Routing for Flyer Variant (FORMATI FLYER)
    match 'flyers/:flayer_id/variants', :to => 'flayer_variants#index', :as => :flayer_variants
    match 'flyers/:flayer_id/variants/new', :to => 'flayer_variants#new', :as => :new_flayer_variants
    match 'flyers/:flayer_id/variants/create', :to => 'flayer_variants#create', :as => :create_flayer_variant
    match 'flyers/:flayer_id/variants/edit/:id', :to => 'flayer_variants#edit', :as => :edit_flayer_variant
    match 'flyers/:flayer_id/variants/update/:id', :to => 'flayer_variants#update', :as => :update_flayer_variant
    match 'flyers/:flayer_id/variants/:id', :to => 'flayer_variants#destroy', :as => :delete_flayer_variant, :method => :delete
    
    # Routing for Letterhead (CARTA INTESTATA)
    match 'letterheads/edit/:id', :to => 'products#edit_letterheads', :as => :letterheads_edit
    match 'letterheads/update/:id', :to => 'products#update_letterheads', :as => :letterheads_update, :via => :put
    match 'letterheads/:product_id/images/update/:id', :to => 'images#update', :as => :letterhead_product_image
    # Routing for Letterhead Variant (FORMATI CARTA INTESTATA)
    match 'letterheads/:letterhead_id/variants', :to => 'letterhead_variants#index', :as => :letterhead_variants
    match 'letterheads/:letterhead_id/variants/new', :to => 'letterhead_variants#new', :as => :new_letterhead_variants
    match 'letterheads/:letterhead_id/variants/create', :to => 'letterhead_variants#create', :as => :create_letterhead_variant
    match 'letterheads/:letterhead_id/variants/edit/:id', :to => 'letterhead_variants#edit', :as => :edit_letterhead_variant
    match 'letterheads/:letterhead_id/variants/update/:id', :to => 'letterhead_variants#update', :as => :update_letterhead_variant
    match 'letterheads/:letterhead_id/variants/:id', :to => 'letterhead_variants#destroy', :as => :delete_letterhead_variant, :method => :delete
    
    # Routing for Playbill (LOCANDINE)
    match 'playbills/edit/:id', :to => 'products#edit_playbills', :as => :playbills_edit
    match 'playbills/update/:id', :to => 'products#update_playbills', :as => :playbills_update, :via => :put
    match 'playbills/:product_id/images/update/:id', :to => 'images#update', :as => :playbill_product_image
    # Routing for Playbill Variant (FORMATI LOCANDINE)
    match 'playbills/:playbill_id/variants', :to => 'playbill_variants#index', :as => :playbill_variants
    match 'playbills/:playbill_id/variants/new', :to => 'playbill_variants#new', :as => :new_playbill_variants
    match 'playbills/:playbill_id/variants/create', :to => 'playbill_variants#create', :as => :create_playbill_variant
    match 'playbills/:playbill_id/variants/edit/:id', :to => 'playbill_variants#edit', :as => :edit_playbill_variant
    match 'playbills/:playbill_id/variants/update/:id', :to => 'playbill_variants#update', :as => :update_playbill_variant
    match 'playbills/:playbill_id/variants/:id', :to => 'playbill_variants#destroy', :as => :delete_playbill_variant, :method => :delete
    
    # Routing for Postcard (CARTOLINA ED INVITO)
    match 'postcards/edit/:id', :to => 'products#edit_postcards', :as => :postcards_edit
    match 'postcards/update/:id', :to => 'products#update_postcards', :as => :postcards_update, :via => :put
    match 'postcards/:product_id/images/update/:id', :to => 'images#update', :as => :postcard_product_image
    # Routing for Postcard Variant (FORMATI CARTOLINA ED INVITO)
    match 'postcards/:postcard_id/variants', :to => 'postcard_variants#index', :as => :postcard_variants
    match 'postcards/:postcard_id/variants/new', :to => 'postcard_variants#new', :as => :new_postcard_variants
    match 'postcards/:postcard_id/variants/create', :to => 'postcard_variants#create', :as => :create_postcard_variant
    match 'postcards/:postcard_id/variants/edit/:id', :to => 'postcard_variants#edit', :as => :edit_postcard_variant
    match 'postcards/:postcard_id/variants/update/:id', :to => 'postcard_variants#update', :as => :update_postcard_variant
    match 'postcards/:postcard_id/variants/:id', :to => 'postcard_variants#destroy', :as => :delete_postcard_variant, :method => :delete
    
    # Routing for Paperback (BROSSURA)
    match 'paperbacks/edit/:id', :to => 'products#edit_paperbacks', :as => :paperbacks_edit
    match 'paperbacks/update/:id', :to => 'products#update_paperbacks', :as => :paperbacks_update, :via => :put
    match 'edit_paperbacks/:product_id/images/update/:id', :to => 'images#update', :as => :paperback_product_image
    # Routing for Paperback Variant (FORMATI BROSSURA)
    match 'paperbacks/:paperback_id/variants', :to => 'paperback_variants#index', :as => :paperback_variants
    match 'paperbacks/:paperback_id/variants/new', :to => 'paperback_variants#new', :as => :new_paperback_variants
    match 'paperbacks/:paperback_id/variants/create', :to => 'paperback_variants#create', :as => :create_paperback_variant
    match 'paperbacks/:paperback_id/variants/edit/:id', :to => 'paperback_variants#edit', :as => :edit_paperback_variant
    match 'paperbacks/:paperback_id/variants/update/:id', :to => 'paperback_variants#update', :as => :update_paperback_variant
    match 'paperbacks/:paperback_id/variants/:id', :to => 'paperback_variants#destroy', :as => :delete_paperback_variant, :method => :delete
    
    # Routing for Spiral (SPIRALE)
    match 'spirals/edit/:id', :to => 'products#edit_spirals', :as => :spirals_edit
    match 'spirals/update/:id', :to => 'products#update_spirals', :as => :spirals_update, :via => :put
    match 'edit_spirals/:product_id/images/update/:id', :to => 'images#update', :as => :spiral_product_image
    # Routing for Spiral Variant (FORMATI SPIRALE)
    match 'spirals/:spiral_id/variants', :to => 'spiral_variants#index', :as => :spiral_variants
    match 'spirals/:spiral_id/variants/new', :to => 'spiral_variants#new', :as => :new_spiral_variants
    match 'spirals/:spiral_id/variants/create', :to => 'spiral_variants#create', :as => :create_spiral_variant
    match 'spirals/:spiral_id/variants/edit/:id', :to => 'spiral_variants#edit', :as => :edit_spiral_variant
    match 'spirals/:spiral_id/variants/update/:id', :to => 'spiral_variants#update', :as => :update_spiral_variant
    match 'spirals/:spiral_id/variants/:id', :to => 'spiral_variants#destroy', :as => :delete_spiral_variant, :method => :delete
    
    # Routing for Staple (PUNTO METALLICO)
    match 'staples/edit/:id', :to => 'products#edit_staples', :as => :staples_edit
    match 'staples/update/:id', :to => 'products#update_staples', :as => :staples_update, :via => :put
    match 'edit_staples/:product_id/images/update/:id', :to => 'images#update', :as => :staple_product_image
    # Routing for Staple Variant (FORMATI PUNTO METALLICO)
    match 'staples/:staple_id/variants', :to => 'staple_variants#index', :as => :staple_variants
    match 'staples/:staple_id/variants/new', :to => 'staple_variants#new', :as => :new_staple_variants
    match 'staples/:staple_id/variants/create', :to => 'staple_variants#create', :as => :create_staple_variant
    match 'staples/:staple_id/variants/edit/:id', :to => 'staple_variants#edit', :as => :edit_staple_variant
    match 'staples/:staple_id/variants/update/:id', :to => 'staple_variants#update', :as => :update_staple_variant
    match 'staples/:staple_id/variants/:id', :to => 'staple_variants#destroy', :as => :delete_staple_variant, :method => :delete
    
    # Routing for Folding (PIEGHEVOLE)
    match 'foldings/edit/:id', :to => 'products#edit_foldings', :as => :foldings_edit
    match 'foldings/update/:id', :to => 'products#update_foldings', :as => :foldings_update, :via => :put
    match 'edit_foldings/:product_id/images/update/:id', :to => 'images#update', :as => :folding_product_image
    # Routing for Folding Variant (FORMATI PIEGHEVOLE)
    match 'foldings/:folding_id/variants', :to => 'folding_variants#index', :as => :folding_variants
    match 'foldings/:folding_id/variants/new', :to => 'folding_variants#new', :as => :new_folding_variants
    match 'foldings/:folding_id/variants/create', :to => 'folding_variants#create', :as => :create_folding_variant
    match 'foldings/:folding_id/variants/edit/:id', :to => 'folding_variants#edit', :as => :edit_folding_variant
    match 'foldings/:folding_id/variants/update/:id', :to => 'folding_variants#update', :as => :update_folding_variant
    match 'foldings/:folding_id/variants/:id', :to => 'folding_variants#destroy', :as => :delete_folding_variant, :method => :delete
    
    # Routing for Banner (STRISCIONE)
    match 'banners/edit/:id', :to => 'products#edit_banners', :as => :banners_edit
    match 'banners/update/:id', :to => 'products#update_banners', :as => :banners_update, :via => :put
    match 'banners/:product_id/images/update/:id', :to => 'images#update', :as => :banner_product_image
    
    # Routing for Pvc (PVC ADESIVO)
    match 'pvces/edit/:id', :to => 'products#edit_pvces', :as => :pvces_edit
    match 'pvces/update/:id', :to => 'products#update_pvces', :as => :pvces_update, :via => :put
    match 'pvces/:product_id/images/update/:id', :to => 'images#update', :as => :pvc_product_image
    
    # Routing for Canvas (TELA)
    match 'canvas/edit/:id', :to => 'products#edit_canvases', :as => :canvases_edit
    match 'canvas/update/:id', :to => 'products#update_canvases', :as => :canvases_update, :via => :put
    match 'canvas/:product_id/images/update/:id', :to => 'images#update', :as => :canvas_product_image
    
    # Routing for Rigid (SUPPORTO RIGIDO)
    match 'rigids/edit/:id', :to => 'products#edit_rigids', :as => :rigids_edit
    match 'rigids/update/:id', :to => 'products#update_rigids', :as => :rigids_update, :via => :put
    match 'rigids/:product_id/images/update/:id', :to => 'images#update', :as => :rigid_product_image
    # Routing for Rigid Printer Format (FORMATI SUPPORTO RIGIDO)
    match 'rigids/:rigid_id/rigid_printer_formats/index', :to => 'rigid_printer_formats#index', :as => :rigid_printer_formats_index
    match 'rigids/:rigid_id/rigid_printer_formats/new', :to => 'rigid_printer_formats#new', :as => :rigid_printer_formats_new
    match 'rigids/:rigid_id/rigid_printer_formats/create', :to => 'rigid_printer_formats#create', :as => :rigid_printer_formats_create
    match 'rigids/:rigid_id/rigid_printer_formats/destroy/:id', :to => 'rigid_printer_formats#destroy', :as => :rigid_printer_formats_destroy
    match 'rigids/:rigid_id/rigid_printer_formats/edit/:id', :to => 'rigid_printer_formats#edit', :as => :rigid_printer_formats_edit
    match 'rigids/:rigid_id/rigid_printer_formats/update/:id', :to => 'rigid_printer_formats#update', :as => :rigid_printer_formats_update
    # Routing for Rigid Variant (CONFIGURAZIONI SUPPORTI RIGIDI)
    match 'rigids/:rigid_id/variants', :to => 'rigid_variants#index', :as => :rigid_variants
    match 'rigids/:rigid_id/variants/new', :to => 'rigid_variants#new', :as => :new_rigid_variants
    match 'rigids/:rigid_id/variants/create', :to => 'rigid_variants#create', :as => :create_rigid_variant
    match 'rigids/:rigid_id/variants/edit/:id', :to => 'rigid_variants#edit', :as => :edit_rigid_variant
    match 'rigids/:rigid_id/variants/update/:id', :to => 'rigid_variants#update', :as => :update_rigid_variant
    match 'rigids/:rigid_id/variants/:id', :to => 'rigid_variants#destroy', :as => :delete_rigid_variant, :method => :delete
    
    # Routing for Sticker (ETICHETTE)
    match 'stickers/edit/:id', :to => 'products#edit_stickers', :as => :stickers_edit
    match 'stickers/update/:id', :to => 'products#update_stickers', :as => :stickers_update, :via => :put
    match 'stickers/:product_id/images/update/:id', :to => 'images#update', :as => :sticker_product_image
    # Routing for Sticker Printer Format (FORMATI ETICHETTE)
    match 'stickers/:sticker_id/sticker_printer_formats/index', :to => 'sticker_printer_formats#index', :as => :sticker_printer_formats_index
    match 'stickers/:sticker_id/sticker_printer_formats/new', :to => 'sticker_printer_formats#new', :as => :sticker_printer_formats_new
    match 'stickers/:sticker_id/sticker_printer_formats/create', :to => 'sticker_printer_formats#create', :as => :sticker_printer_formats_create
    match 'stickers/:sticker_id/sticker_printer_formats/destroy/:id', :to => 'sticker_printer_formats#destroy', :as => :sticker_printer_formats_destroy
    match 'stickers/:sticker_id/sticker_printer_formats/edit/:id', :to => 'sticker_printer_formats#edit', :as => :sticker_printer_formats_edit
    match 'stickers/:sticker_id/sticker_printer_formats/update/:id', :to => 'sticker_printer_formats#update', :as => :sticker_printer_formats_update
    # Routing for Sticker Variant (CONFIGURAZIONI ETICHETTE)
    match 'stickers/:sticker_id/variants', :to => 'sticker_variants#index', :as => :sticker_variants
    match 'stickers/:sticker_id/variants/new', :to => 'sticker_variants#new', :as => :new_sticker_variants
    match 'stickers/:sticker_id/variants/create', :to => 'sticker_variants#create', :as => :create_sticker_variant
    match 'stickers/:sticker_id/variants/edit/:id', :to => 'sticker_variants#edit', :as => :edit_sticker_variant
    match 'stickers/:sticker_id/variants/update/:id', :to => 'sticker_variants#update', :as => :update_sticker_variant
    match 'stickers/:sticker_id/variants/:id', :to => 'sticker_variants#destroy', :as => :delete_sticker_variant, :method => :delete

    # routing per le immagini
    match '/products/:product_id/images/new', :to => 'products#add_image'
    
    # SPIRALATURA
    match 'spiralings', :to => 'spiralings#edit', :as => :spiralings, :via => :get
    match 'spiralings', :to => 'spiralings#update', :via => :update
    resource :spiralings
    # Routing for SpiralingQuantity (FORMATI SPIRALI) 
    match 'spiraling/:spiraling_id/quantity/index', :to => 'spiraling_quantities#index', :as => :spiraling_quantities_index
    match 'spiraling/:spiraling_id/quantity/new', :to => 'spiraling_quantities#new', :as => :spiraling_quantities_new
    match 'spiraling/:spiraling_id/quantity/create', :to => 'spiraling_quantities#create', :as => :spiraling_quantities_create
    match 'spiraling/:spiraling_id/quantity/destroy/:id', :to => 'spiraling_quantities#destroy', :as => :spiraling_quantities_destroy
    match 'spiraling/:spiraling_id/quantity/edit/:id', :to => 'spiraling_quantities#edit', :as => :spiraling_quantities_edit
    match 'spiraling/:spiraling_id/quantity/update/:id', :to => 'spiraling_quantities#update', :as => :spiraling_quantities_update
    
    resources :coil_values # BOBINE   
    resources :rigid_supports # SUPPORTI RIGIDI
    resources :printer_format_defaults # FORMATI FOGLI
    resources :printers # STAMPANTI
    resources :plotters # PLOTTER
    resources :binderies # LEGATORIE
    resources :foldings # PIEGATRICI
    resources :eyelets # OCCHIELLI
    resources :buttonholes # ASOLE
    resources :frames # TELAI
    resources :hangers # APPENDINI
    
    match 'printers/edit/:id', :to => 'printers#edit', :as => :edit_printer
    match 'printers/delete/:id', :to => 'printers#delete', :as => :delete_printer
    match 'printers/new', :to => 'printers#new', :as => :new_printer
    resources :presentation_packs
    resources :shipping_date_adjustments
    
    # TIPI DI CARTE
    resources :paper_types do
      get :active, :on => :member
      get :deactive, :on => :member
    end
    resources :poses # POSE
    resources :quarters # QUARTINI
    
    match 'paper_types/create', :to => 'paper_types#create', :as => :create_paper_type
    match 'paper_types/update/:id', :to => 'paper_types#update', :as => :update_paper_type
    
    resources :products do
      get :deactive, :on => :member
      get :active, :on => :member
    end
    
    resources :simple_products do
      resources :images
      get :deactive, :on => :member
      get :active, :on => :member
      get :add_image, :on => :member
      resources :simple_variants
    end
    
    resources :advanced_products do
      resources :images
      get :deactive, :on => :member
      get :active, :on => :member
      get :add_image, :on => :member
      resources :advanced_variants do
        match 'remove_entity/:entity_id', :to => 'advanced_variants#remove_entity', :via => :delete, :as => :remove_entity
      end
      resources :simple_attributes
    end
    
    resources :line_item_promotions do 
      get :destroy, :on => :member
    end
    match 'line_item_promotions/products/:id', :to => 'line_item_promotions#index', :as => :products_line_item_promotions
    match 'line_item_promotions/new/:product_id', :to => 'line_item_promotions#new', :as => :new_line_item_promotion
    
    resources :import_configurations
    resources :templates do
      match '/:variant_id/new_variant', :to => 'templates#new_variant', :as => :new_variant, :on => :collection, :via => :get
      match '/:variant_id/create_variant', :to => 'templates#create_variant', :as => :create_variant, :on => :collection, :via => [:post, :put]
      match '/:option_value_id/new_option', :to => 'templates#new_option_value', :as => :new_option_value, :on => :collection, :via => :get
      match '/:option_value_id/create_option_value', :to => 'templates#create_option_value', :as => :create_option_value, :on => :collection, :via => [:post, :put]
    end
    
    resources :slides
    resources :slideshow_types do
      resources :slides do
        collection do
          post :update_positions
        end
      end
    end
    
    match '/homepage_elements/:id/remove_image' => 'homepage_elements#remove_image', :as => :remove_image_homepage_elements
    resources :homepage_elements
  end
  
  # JSON for Paper Weight
  match '/retrieve_paper_weight', :to => 'base#retrieve_paper_weight'
  match '/:folding_id/retrieve_folding_paper_weight/:id', :to => 'foldings#retrieve_folding_paper_weight'
  match 'checkout/line_item/upload', :to => 'checkout#upload_file', :as => :upload_line_item
  match '/download/template/:id', :to => 'products#download_template', :as => :download_template
  
  match '/mbanner/bmodal/:id', :to => 'base#open_modal', :via => :get, :as => :open_modal
  
  match 'get_promotion_by_date/:code', :to => 'base#retrieve_promo_date'
  
  resources :business_cards
  match '/business_cards/:id(/:promo_code)', :to => 'business_cards#show'
  resources :posters
  match '/posters/:id(/:promo_code)', :to => 'posters#show'
  resources :flayers
  match '/flayers/:id(/:promo_code)', :to => 'flayers#show'
  resources :letterheads
  match '/letterheads/:id(/:promo_code)', :to => 'letterheads#show'
  resources :postcards
  match '/postcards/:id(/:promo_code)', :to => 'postcards#show'
  resources :playbills
  match '/playbills/:id(/:promo_code)', :to => 'playbills#show'
  resources :paperbacks
  match '/paperbacks/:id(/:promo_code)', :to => 'paperbacks#show'
  resources :staples
  match '/staples/:id(/:promo_code)', :to => 'staples#show'
  resources :foldings
  match '/foldings/:id(/:promo_code)', :to => 'foldings#show'
  resources :banners
  match '/banners/:id(/:promo_code)', :to => 'banners#show'
  resources :pvc_stickers
  match '/pvc_stickers/:id(/:promo_code)', :to => 'pvc_stickers#show'
  resources :spirals
  match '/spirals/:id(/:promo_code)', :to => 'spirals#show'
  resources :canvases
  match '/canvases/:id(/:promo_code)', :to => 'canvases#show'
  resources :rigids
  match '/rigids/:id(/:promo_code)', :to => 'rigids#show'
  resources :stickers
  match '/stickers/:id(/:promo_code)', :to => 'stickers#show'
  resources :simple_products
  resources :advanced_products
  
  match '/preventive', :to => 'orders#preventive', :as => :preventive, :via => [:post, :put]
	match '/preventive', to: redirect('/cart'), :via => :get
  match '/business_card_product/:id', :to => 'products#show', :as => :business_card_product
  match '/flayers/:id', :to => 'products#show', :as => :flayer_product
  match '/letterheads/:id', :to => 'products#show', :as => :letterhead_product
  match '/playbills/:id', :to => 'products#show', :as => :playbill_product
  match '/postcards/:id', :to => 'products#show', :as => :postcard_product
  match '/posters/:id', :to => 'products#show', :as => :poster_product
  match '/paperback/:id', :to => 'products#show', :as => :paperback_product
  match '/staple/:id', :to => 'products#show', :as => :staple_product
  match '/folding/:id', :to => 'products#show', :as => :folding_product
  match '/banner/:id', :to => 'products#show', :as => :banner_product
  match '/pvc_sticker/:id', :to => 'products#show', :as => :pvc_product
  match '/spiral/:id', :to => 'products#show', :as => :spiral_product
  match '/canvas/:id', :to => 'products#show', :as => :canvas_product
  match '/rigids/:id', :to => 'products#show', :as => :rigids_product
  match '/stickers/:id', :to => 'products#show', :as => :stickers_product
  match '/orders/update_image/:id', :to => 'orders#update_image', :as => :update_image
  match '/orders/first_step/(:id)', :to => 'orders#first_step', :as => :first_step

	match '/confirm/checkout/:id', :to => 'orders#confirm', :as => :confirm_checkout, :via => :get

  match "/404", :to => "errors#not_found"
  match "/500", :to => "errors#internal_error"
end
