Spree::Admin::MenusController.class_eval do
	before_filter :modify_params, :only => :update 

	def index
		@categories = Spree::Menu.only_parent.group("category")
		@menus = Spree::Menu.only_parent
	end

	def modify_params
		if !params[:menu][:linkable_type].blank?
			params[:menu][:url] = ""
		end
		if !params[:menu][:url].blank? 
			params[:menu][:linkable_type] = nil
			params[:menu][:linkable_id] = nil
		end
	end
end