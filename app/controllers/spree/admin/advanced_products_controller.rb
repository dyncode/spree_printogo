module Spree
  module Admin
    class AdvancedProductsController < ResourceController
      respond_to :html, :js
      
      def index
        respond_with(@collection)
      end
      
      def show
        redirect_to( :action => :edit )
      end
      
      def new
        @advanced_product = Spree::AdvancedProduct.new(:price => 0, :max_width => 1)
      end
      
      def create
        @advanced_product = Spree::AdvancedProduct.new(params[:advanced_product])
        @advanced_product.price = 0
        @advanced_product.save
        redirect_to edit_admin_advanced_product_path(@advanced_product)
      end
      
      def destroy
        @advanced_product = Spree::AdvancedProduct.where(:permalink => params[:id]).first!
        @advanced_product.destroy

        flash.notice = I18n.t('notice_messages.advanced_product_deleted')

        respond_with(@advanced_product) do |format|
          format.html { redirect_to collection_url }
          format.js  { render_js_for_destroy }
        end
      end
      
      def update
        @advanced_product.update_attributes(params[:advanced_product])
        redirect_to edit_admin_advanced_product_path(@advanced_product)
      end
      
      def add_image
        @image = @advanced_product.images.first if !@advanced_product.images.blank?
        @image = @advanced_product.images.new if @advanced_product.images.blank?
      end
      
      def add_options
        @simple_attributes = @advanced_product.simple_attributes
      end
      
      def deactive
        @advanced_product.available_on = Time.now + 2.years
        @advanced_product.save
        Spree::HomepageElement.find_all_by_homepageble_id(@advanced_product.id).each do |h|
          h.update_attribute(:enabled, false) if (!h.blank? && h.homepageble_type == "Spree::Product")
        end
        Spree::Menu.find_all_by_linkable_id(@advanced_product.id).each do |m|
          m.update_attribute(:visible, false) if (!m.blank? && m.linkable.class == "Spree::Product")
        end
        redirect_to :action => :index
      end

      def active
        if @advanced_product.advanced_variants.any? && @advanced_product.advanced_variants.any?{|av| av.entities.any?}
          @advanced_product.available_on = Time.now - 2.days
          @advanced_product.save
          flash.notice = "#{@advanced_product.name.titleize} attivato con successo!"
        else
          flash[:error] = "Impossibile attivare il prodotto #{@advanced_product.name.titleize} in quanto mancano alcune configurazioni!"
        end
        redirect_to :action => :index
      end
      
      protected
      def find_resource
        AdvancedProduct.find_by_permalink!(params[:id])
      end

      def location_after_save
        edit_admin_advanced_product_url(@advanced_product)
      end
      
      def collection
        return @collection if @collection.present?

        unless request.xhr?
          params[:q] ||= {}

          params[:q][:s] ||= "name asc"

          @search = Spree::AdvancedProduct.ransack(params[:q])
          @collection = @search.result.
            group_by_products_id.
            includes([:master, {:variants => [:images, :option_values]}]).
            page(params[:page]).
            per(Spree::Config[:admin_products_per_page])
        else
          includes = [{:variants => [:images,  {:option_values => :option_type}]}, {:master => :images}]

          @collection = super.where(["name #{LIKE} ?", "%#{params[:q]}%"])
          @collection = @collection.includes(includes).limit(params[:limit] || 10)
        end
        @collection
      end
    end
  end
end