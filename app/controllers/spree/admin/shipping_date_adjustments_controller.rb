module Spree
  module Admin
    class ShippingDateAdjustmentsController < ResourceController
      def index
        @collection = Spree::ShippingDateAdjustment.order("days ASC").page(params[:page]).per(12)
      end

      def new
        @shipping_date_adjustment = Spree::ShippingDateAdjustment.new
        @shipping_date_adjustment.init_calculator
      end

    end
  end
end