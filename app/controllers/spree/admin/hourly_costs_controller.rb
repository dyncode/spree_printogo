module Spree
  module Admin
    class HourlyCostsController < Spree::Admin::BaseController
      def edit
        option_type = Spree::OptionType.find_by_name 'hourly_cost'
        @hourly_cost = option_type.option_values.first
        if params[:hourly_cost]
          if @hourly_cost.update_attribute(:presentation, params[:hourly_cost])
            flash[:notice] = t(:edit_success)
          else
            flash[:error] = t(:edit_fail)
          end
        end
      end
    end
  end
end