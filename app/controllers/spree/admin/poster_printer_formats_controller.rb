module Spree
  module Admin
    class PosterPrinterFormatsController < Spree::Admin::BaseController
      def create
        @product = Spree::PosterProduct.find_by_permalink params[:product_id]
        @poster_printer_formats = Spree::PosterPrinterFormat.create(params[:poster_printer_formats])
        @poster_printer_formats.save
        @product.master.option_values << Spree::OptionValue.find(@poster_printer_formats.id)
        flash[:notice] = "Quantita' creata correttamente"
        redirect_to admin_poster_printer_formats_index_path(@product)
      end

      def destroy
        @product = Spree::PosterProduct.find_by_permalink params[:product_id]
        @poster_printer_formats = Spree::PosterPrinterFormat.find params[:id]
        @poster_printer_formats.destroy
        redirect_to admin_poster_printer_formats_index_path(@product)
      end

      def index
        @product = Spree::PosterProduct.find_by_permalink params[:product_id]
        @poster_printer_formats = @product.poster_printer_formats
      end

      def new
        @product = Spree::PosterProduct.find_by_permalink params[:product_id]
        @poster_printer_formats = Spree::PosterPrinterFormat.new
      end

      def edit
        @product = Spree::PosterProduct.find_by_permalink params[:product_id]
        @poster_printer_formats = Spree::PosterPrinterFormat.find params[:id]
      end

      def update
        @product = Spree::PosterProduct.find_by_permalink params[:product_id]
        @poster_printer_formats = Spree::PosterPrinterFormat.find params[:id]
        if @poster_printer_formats.update_attributes(params[:poster_printer_formats])
          @poster_printer_formats.save
          flash[:notice] = t(:edit_success)
          redirect_to admin_poster_printer_formats_index_path(@product)
        else
          flash[:error] = t(:edit_fail)
          redirect_to admin_poster_printer_formats_edit_path(@product, poster_printer_formats.id)
        end

      end

    end
  end
end