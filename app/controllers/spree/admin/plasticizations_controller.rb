module Spree
  module Admin
    class PlasticizationsController < Spree::Admin::BaseController
      def edit

        if params[:plasticization]
          Spree::Plasticization.all.each do |p|
            p.price_start_up = params[:plasticization_all][:price_start_up]
            p.max_quantity = params[:plasticization_all][:max_quantity]
            p.save
          end
          params[:plasticization].keys.each do |id|
            Spree::PlasticizationPrice.find(id).update_attribute(:presentation, params[:plasticization][id])
          end
        end

        @plasticizations = Spree::Plasticization.all
      end
    end
  end
end