module Spree
  module Admin
    class PosterFlyerVariantsController < Spree::Admin::BaseController
      respond_to :html, :js

      def index
        @product = Spree::PosterFlyerProduct.find_by_permalink params[:poster_flyer_id]
        @variants = @product.poster_flyer_variants
      end

      def new
        @product = Spree::PosterFlyerProduct.find_by_permalink params[:poster_flyer_id]
        @variant = @product.poster_flyer_variants.create()
      end
      
      def create
        @product = Spree::PosterFlyerProduct.find_by_permalink params[:poster_flyer_id]
        @variant = @product.poster_flyer_variants.find(params[:variant_id])
        @variant.update_attribute(:sku,params[:variant][:format])
        #limit_update = @variant.limit.update_attributes params[:variant][:limit]
        limit = params[:variant].delete(:limit)
        weights = params[:variant].delete :paper_weight
        papers = params[:variant].delete :paper

        if update_values(@variant, limit, weights, papers) && @variant.update_attributes(params[:variant])
          flash[:notice] = t(:update_success)
          redirect_to :action => :index
        end
      end

      def edit
        @product = Spree::PosterFlyerProduct.find_by_permalink params[:poster_flyer_id]
        @variant = @product.poster_flyer_variants.find(params[:id])
      end

      def update
        @product = Spree::PosterFlyerProduct.find_by_permalink params[:poster_flyer_id]
        @variant = @product.poster_flyer_variants.find(params[:variant_id])
        limit = params[:variant].delete(:limit)
        weights = params[:variant].delete :paper_weight
        papers = params[:variant].delete :paper
        img = params[:variant].delete(:image_attachment)
        viewable = params[:variant].delete(:image_viewable_id)
          if update_values(@variant, limit, weights, papers) && @variant.update_attributes(params[:variant])
          flash[:notice] = t(:update_success)
          redirect_to :action => :index
        end
      end

      def destroy
        @product = Spree::PosterFlyerProduct.find_by_permalink params[:poster_flyer_id]
        @variant = @product.poster_flyer_variants.find params[:id]
        @variant.paper_products.clear
        @variant.destroy
        flash[:notice] = t(:delete_success)
        redirect_to :action => :index
      end
      
      private
      def update_values(variant, limit, weights, papers)
        limit_update = variant.limit.update_attributes limit

				variant.paper_products.destroy_all if !variant.paper_products.blank?
        if !weights.blank? && !papers.blank?
          weights.each do |weight|
            papers.each do |paper|
              paper_weight = Spree::PaperWeight.find_by_name_and_option_value_id(weight.to_s, Spree::Paper.find(paper).id)
              if paper_weight
                Spree::PaperProduct.create :product_id => variant.id, :paper_id => paper_weight.paper.id, :weight_id => paper_weight.id
              end
            end
          end
        end
        limit_update
      end
    end
  end
end
