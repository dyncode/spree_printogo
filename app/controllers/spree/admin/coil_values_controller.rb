module Spree
  module Admin
    class CoilValuesController < Spree::Admin::BaseController
      def index
        @coil_values = Spree::CoilValue.all
      end

      def new
        @coil_value = Spree::CoilValue.new()
      end
      
      def create
        @coil_value = Spree::CoilValue.new(params[:coil_value])
        if @coil_value.save
          flash[:notice] = t(:create_success)
          redirect_to :action => :index
        else
          flash[:error] = @coil_value.errors.full_messages.join(", ")
          render :new
        end
      end

      def edit
        @coil_value = Spree::CoilValue.find(params[:id])
      end

      def update
        @coil_value = Spree::CoilValue.find(params[:id])
        @coil_value.update_attributes(params[:coil_value])

        flash[:notice] = t(:edit_success)
        redirect_to :action => :index
      end
      
      def destroy
        @coil_value = Spree::CoilValue.find(params[:id])
        @coil_value.destroy
        
        respond_to do |format|
          format.html {redirect_to :action => :index}
          format.js {render :layout => false}
        end
      end
    end
  end
end