module Spree
  module Admin
    class SpiralingsController < Spree::Admin::BaseController
      def edit
        @spiraling = Spree::Spiraling.find_or_initialize_by_name('spiraling')
      end
      
      def update
        @spiraling = Spree::Spiraling.find_or_initialize_by_name('spiraling')
        if params[:spiraling][:external] == "0"
          params[:spiraling].delete(:cost_spiraling)
          params[:spiraling].delete(:min_limit_spiraling)
        end
        if @spiraling.update_attributes(params[:spiraling])
          flash[:notice] = t(:edit_success)
        else
          flash[:error] = t(:edit_fail)
        end
        redirect_to :action => :edit
      end
    end
  end
end

