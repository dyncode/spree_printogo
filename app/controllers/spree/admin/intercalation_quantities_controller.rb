module Spree
  module Admin
    class IntercalationQuantitiesController < Spree::Admin::BaseController
      def update
        @intercalation = Spree::Intercalation.find params[:intercalation_id]
        @intercalation_quantity = @intercalation.intercalation_quantities.find params[:id]
        @intercalation_quantity.update_attributes params[:intercalation_quantity]
      end

      def edit
        @intercalation = Spree::Intercalation.find params[:intercalation_id]
        @intercalation_quantity = @intercalation.intercalation_quantities.find params[:id]
      end

      def show
        @intercalation = Spree::Intercalation.find params[:intercalation_id]
        @intercalation_quantity = @intercalation.intercalation_quantities.find params[:id]
      end
    end
  end
end