module Spree
  module Admin
    class PaperTypesController < Spree::Admin::BaseController
      include Spree::Admin::PaperTypesHelper
      
      def index
        @papers = Spree::Paper.masters_all.group(:name)
      end

			def destroy
				@paper = Spree::Paper.find(params[:id])
				if @paper.delete
					flash[:notice] = t(:delete_success)
				else
					flash[:error] = t(:deleted_error)
				end
				redirect_to :action => :index
			end

      def edit
        @paper = Spree::Paper.find params[:id]
      end

      def new
        @paper = Spree::Paper.new
      end

      def update
        @paper = Spree::Paper.find(params[:id])
        @paper.update_attributes(params[:paper])
        flash[:notice] = "#{@paper.name} modificata con successo"
        redirect_to :action => :edit
      end

      def create
        if params[:paper][:name].empty?
          flash[:error] = "Dati incompleti"
          redirect_to :action => :new
        else
          @paper = Spree::Paper.new(params[:paper])
          @paper.save
          flash[:notice] = "#{@paper.name} creata con successo."
          redirect_to edit_admin_paper_type_url(@paper)
        end
      end

    end
  end
end


