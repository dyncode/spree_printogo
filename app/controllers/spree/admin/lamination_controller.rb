module Spree
  module Admin
    class LaminationController < Spree::Admin::BaseController
      
      def edit
        @lamination = Spree::Lamination.last
      end

      def update
        @lamination = Spree::Lamination.last
        @lamination.update_attributes(params[:lamination])
        flash[:notice] = "#{@lamination.presentation} modificata con successo"
        redirect_to :action => :edit
      end
    end
  end
end
