Spree::Admin::OrdersController.class_eval do
  before_filter :initialize_percent, :only => :index
  
  def show
    session[:only_read] = true
    respond_with(@order)
  end

  def new
    @order = Order.create
    respond_with(@order)
  end

  def edit
    session[:only_read] = false
    respond_with(@order)
  end
  
  def update
    return_path = nil
    if @order.update_attributes(params[:order]) && @order.line_items.present?
      @order.update!
      unless @order.complete?
        # Jump to next step if order is not complete.
        return_path = admin_order_customer_path(@order)
      else
        # Otherwise, go back to first page since all necessary information has been filled out.
        return_path = admin_order_path(@order)
      end
    else
      @order.errors.add(:line_items, t('errors.messages.blank')) if @order.line_items.empty?
    end
    
    if @order.line_items.present?
      @order.line_items.each do |li|
        li.calculate_update_quantity
      end
    end
    
    respond_with(@order) do |format|
      format.html do
        if return_path
          redirect_to return_path
        else
          render :action => :edit
        end
      end
    end
  end
  
  def initialize_percent
    @all = Spree::Order.all.count
    @comp = Spree::Order.complete
    @completed = ((100 * @comp.count).to_f/@all).to_f
    @cart = ((100 * Spree::Order.by_state('cart').count).to_f/@all).to_f
    @paied = ((100 * @comp.by_state('complete').sum{|o| 1 if o.payments.completed }).to_f/@all).to_f
    @total = @comp.by_state('complete').sum{|o| o.total if(o.payments.try(:first).completed? || o.payments.try(:first).checkout?) } rescue 0
  end
end