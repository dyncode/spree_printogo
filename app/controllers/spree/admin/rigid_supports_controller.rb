module Spree
  module Admin
    class RigidSupportsController < Spree::Admin::BaseController
      def index
        @rigid_supports = Spree::RigidSupport.all
      end

      def new
        @rigid_support = Spree::RigidSupport.new()
      end
      
      def create
        @rigid_support = Spree::RigidSupport.new(params[:rigid_support])
        if @rigid_support.save
          flash[:notice] = t(:create_success)
          redirect_to :action => :index
        else
          flash[:error] = @rigid_support.errors.full_messages.join(", ")
          render :new
        end
      end

      def edit
        @rigid_support = Spree::RigidSupport.find(params[:id])
      end

      def update
        @rigid_support = Spree::RigidSupport.find(params[:id])
        @rigid_support.update_attributes(params[:rigid_support])

        flash[:notice] = t(:edit_success)
        redirect_to :action => :index
      end
      
      def destroy
        @rigid_support = Spree::RigidSupport.find(params[:id])
        @rigid_support.destroy
        
        respond_to do |format|
          format.html {redirect_to :action => :index}
          format.js {render :layout => false}
        end
      end
    end
  end
end