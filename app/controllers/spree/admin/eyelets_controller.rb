module Spree
  module Admin
    class EyeletsController < Spree::Admin::BaseController
      
      def index
        @eyelets = Spree::Eyelet.all
      end

      def destroy
        @eyelet = Spree::Eyelet.find params[:id]
        @eyelet.destroy
      end

      def edit
        @eyelet = Spree::Eyelet.find params[:id]
      end

      def new
        @eyelet = Spree::Eyelet.new
      end

      def update
        @eyelet = Spree::Eyelet.find(params[:id])
        @eyelet.update_attributes(params[:eyelet])
        flash[:notice] = "#{@eyelet.name} modificata con successo"
        redirect_to :action => :index
      end

      def create
        if params[:eyelet][:name].empty?
          flash[:error] = "Dati incompleti"
          redirect_to :action => :new
        else
          @eyelet = Spree::Eyelet.new(params[:eyelet])
          @eyelet.save
          flash[:notice] = "#{@eyelet.name} creata con successo."
          redirect_to :action => :index
        end
      end
    end
  end
end


