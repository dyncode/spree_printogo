module Spree
  module Admin
    class StickerVariantsController < Spree::Admin::BaseController
      respond_to :html, :js

      def index
        @product = Spree::StickerProduct.find_by_permalink params[:sticker_id]
        @variants = @product.sticker_variants
      end

      def new
        @product = Spree::StickerProduct.find_by_permalink params[:sticker_id]
        @variant = @product.sticker_variants.create()
      end
      
      def create
        @product = Spree::StickerProduct.find_by_permalink params[:sticker_id]
        @variant = @product.sticker_variants.find(params[:variant_id])
        if @variant.update_attributes(params[:sticker_variant])
          flash[:notice] = t(:update_success)
          redirect_to :action => :index
        end
      end

      def edit
        @product = Spree::StickerProduct.find_by_permalink params[:sticker_id]
        @variant = @product.sticker_variants.find(params[:id])
      end

      def update
        @product = Spree::StickerProduct.find_by_permalink params[:sticker_id]
        @variant = @product.sticker_variants.find(params[:variant_id])
          if @variant.update_attributes(params[:sticker_variant])
          flash[:notice] = t(:update_success)
          redirect_to :action => :index
        end
      end

      def destroy
        @product = Spree::StickerProduct.find_by_permalink params[:sticker_id]
        @variant = @product.sticker_variants.find params[:id]
        @variant.destroy
        flash[:notice] = t(:delete_success)
        redirect_to :action => :index
      end
      
    end
  end
end
