module Spree
  module Admin
    class RigidPrinterFormatsController < Spree::Admin::BaseController
      def create
        @product = Spree::RigidProduct.find_by_permalink params[:rigid_id]
        @rigid_printer_format = Spree::RigidPrinterFormat.create(params[:rigid_printer_format])
        @rigid_printer_format.save
        @product.master.option_values << Spree::OptionValue.find(@rigid_printer_format.id)
        flash[:notice] = "Quantita' creata correttamente"
        redirect_to admin_rigid_printer_formats_index_path(@product)
      end

      def destroy
        @product = Spree::RigidProduct.find_by_permalink params[:rigid_id]
        @rigid_printer_format = Spree::RigidPrinterFormat.find params[:id]
        @rigid_printer_format.destroy
        redirect_to admin_rigid_printer_formats_index_path(@product)
      end

      def index
        @product = Spree::RigidProduct.find_by_permalink params[:rigid_id]
        @rigid_printer_formats = @product.rigid_printer_formats
      end

      def new
        @product = Spree::RigidProduct.find_by_permalink params[:rigid_id]
        @rigid_printer_format = Spree::RigidPrinterFormat.new
      end

      def edit
        @product = Spree::RigidProduct.find_by_permalink params[:rigid_id]
        @rigid_printer_format = Spree::RigidPrinterFormat.find params[:id]
        @rigid_printer_format.build_template_format if @rigid_printer_format.template_format.blank?
      end

      def update
        @product = Spree::RigidProduct.find_by_permalink params[:rigid_id]
        @rigid_printer_format = Spree::RigidPrinterFormat.find params[:id]
        if @rigid_printer_format.update_attributes(params[:rigid_printer_format])
          @rigid_printer_format.save
          flash[:notice] = t(:edit_success)
          respond_to do |format|
            format.html { redirect_to admin_sticker_printer_formats_index_path(@product) }
            format.js {
              @rigid_printer_formats = @product.rigid_printer_formats
              render :action => :index
            }
          end
        else
          flash[:error] = t(:edit_fail)
          redirect_to admin_rigid_printer_formats_edit_path(@product, @rigid_printer_format.id)
        end

      end

    end
  end
end