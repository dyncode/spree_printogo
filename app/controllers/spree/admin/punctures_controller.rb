module Spree
  module Admin
    class PuncturesController < Spree::Admin::BaseController
      def edit
        @puncture = Spree::Puncture.find_or_initialize_by_name('puncture')
      end

      def update
        @puncture = Spree::Puncture.find_or_initialize_by_name 'puncture'
        @puncture.update_attributes(params[:puncture])
        redirect_to :action => :edit
      end
    end
  end
end