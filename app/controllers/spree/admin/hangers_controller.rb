module Spree
  module Admin
    class HangersController < Spree::Admin::BaseController
      
      def index
        @hangers = Spree::Hanger.all
      end

      def destroy
        @hanger = Spree::Hanger.find params[:id]
        render :partial => "delete"
      end

      def edit
        @hanger = Spree::Hanger.find params[:id]
      end

      def new
        @hanger = Spree::Hanger.new
      end

      def update
        @hanger = Spree::Hanger.find(params[:id])
        @hanger.update_attributes(params[:hanger])
        flash[:notice] = "#{@hanger.name} modificato con successo"
        redirect_to :action => :index
      end

      def create
        if params[:hanger][:name].empty?
          flash[:error] = "Dati incompleti"
          redirect_to :action => :new
        else
          @hanger = Spree::Hanger.new(params[:hanger])
          @hanger.save
          flash[:notice] = "#{@hanger.name} creato con successo."
          redirect_to :action => :index
        end
      end
    end
  end
end


