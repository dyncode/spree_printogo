module Spree
  module Admin
    class PrintersController < Spree::Admin::BaseController
      include Spree::Admin::PrintersHelper
      
      def index
        @digital = Spree::Printer.digital
        @offset = Spree::Printer.offset
      end

      def update
        @printer = Spree::Printer.find(params[:id])
        @printer.update_attributes(params[:printer])
        #if  @printer.price_front_c
        #  @printer.update_attribute(:price_front_and_back_c, @printer.price_front_c * 2)
        #end
        #if @printer.price_front_bw
        #  @printer.update_attribute(:price_front_and_back_bw, @printer.price_front_bw * 2)
        #end

        flash[:notice] = t(:edit_success)
        redirect_to :action => :index
      end

      def edit
        @printer = Spree::Printer.find params[:id]
        @printer.printer_formats.build if @printer.printer_formats.empty?
      end

      def delete
        ov = Spree::Printer.find params[:id]
        ov.destroy
        flash[:notice] = t(:delete_success)
        redirect_to :action => :index
      end

      def new
        @printer = Spree::Printer.new(:printer_type => "digital")
      end

      def create
        @printer = Spree::Printer.new(params[:printer])
        if @printer.save
          flash[:notice] = t(:create_success)
          redirect_to :action => :index
        else
          flash[:error] = t(:create_error)
          redirect_to :action => :new
        end
      end
      
      def retrieve_printer_format
        render :json => Spree::Printer.find(params[:id]).printer_formats.collect {|pf| {:id => pf.id, :presentation => pf.presentation}}.to_json
      end
      
      def calculate_convenience
        tot = 0
        printer_b = Spree::Printer.find(params[:printer_b])
        pose_b = Spree::Pose.find(params[:pose_b]).name.to_i
        printer_a = Spree::Printer.find(params[:printer_a])
        pose_a = Spree::Pose.find(params[:pose_a]).name.to_i
        if params[:bw] == "true" # sono in bianco e nero
          avv_b = printer_b.printer_hourly_cost_bw.to_f * (printer_b.start_time_bw.to_f/60.to_f)
          ink_b = printer_b.price_front_bw.to_f
          imp_b = printer_b.plant_color_bw.to_f
          pass_b = (printer_b.average_hourly_print_bw.to_f > 0) ? (printer_b.printer_hourly_cost_bw.to_f/printer_b.average_hourly_print_bw.to_f) : 0
          waste_b = printer_b.price_front_bw.to_f * printer_b.waste_paper_bw
          
          avv_a = printer_a.printer_hourly_cost_bw.to_f * (printer_a.start_time_bw.to_f/60.to_f)
          ink_a = printer_a.price_front_bw.to_f
          imp_a = printer_a.plant_color_bw.to_f
          pass_a = (printer_a.average_hourly_print_bw.to_f > 0) ? (printer_a.printer_hourly_cost_bw.to_f/printer_a.average_hourly_print_bw.to_f) : 0
          waste_a = printer_a.price_front_bw.to_f * printer_a.waste_paper_bw
          
          if ((pose_a*(ink_b+pass_b))-(pose_b*(ink_a+pass_a))) > 0
            tot = (((avv_a-avv_b)+(imp_a-imp_b)+(waste_a-waste_b)) * (pose_a*pose_b)) / ((pose_a*(ink_b+pass_b))-(pose_b*(ink_a+pass_a)))
          else
            tot = (((avv_a-avv_b)+(imp_a-imp_b)+(waste_a-waste_b)) * (pose_a*pose_b))
          end
        else # sono a colori
          avv_b = printer_b.printer_hourly_cost_c.to_f * (printer_b.start_time_c.to_f/60.to_f)
          ink_b = printer_b.price_front_c.to_f
          imp_b = printer_b.plant_color_c.to_f
          pass_b = (printer_b.average_hourly_print_c.to_f > 0) ? (printer_b.printer_hourly_cost_c.to_f/printer_b.average_hourly_print_c.to_f) : 0
          waste_b = printer_b.price_front_c.to_f * printer_b.waste_paper_c
          
          avv_a = printer_a.printer_hourly_cost_c.to_f * (printer_a.start_time_c.to_f/60.to_f)
          ink_a = printer_a.price_front_c.to_f
          imp_a = printer_a.plant_color_c.to_f
          pass_a = (printer_a.average_hourly_print_c.to_f > 0) ? (printer_a.printer_hourly_cost_c.to_f/printer_a.average_hourly_print_c.to_f) : 0
          waste_a = printer_a.price_front_c.to_f * printer_a.waste_paper_c
          
          if ((pose_a*(ink_b+pass_b))-(pose_b*(ink_a+pass_a))) > 0
            tot = (((avv_a-avv_b)+(imp_a-imp_b)+(waste_a-waste_b)) * (pose_a*pose_b)) / ((pose_a*(ink_b+pass_b))-(pose_b*(ink_a+pass_a)))
          else
            tot = (((avv_a-avv_b)+(imp_a-imp_b)+(waste_a-waste_b)) * (pose_a*pose_b))
          end
        end
        
        render :json => tot.ceil.abs.to_i
      end
    end
  end
end