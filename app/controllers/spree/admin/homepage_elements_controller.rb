module Spree
	module Admin
  		class HomepageElementsController < ResourceController
  			#respond_to :js, :only => [:destroy, :remove_image]
  			respond_to :html, :except => [:remove_image]

  			before_filter :modify_params, :only => :update 

  			def remove_image
  				@hp = Spree::HomepageElement.find(params[:id])
  				@hp.attachment = nil
  				@hp.save
				respond_with(@hp) do |format|
					format.js
				end
  			end

			def destroy
				@hp = Spree::HomepageElement.find(params[:id])
				if @hp.destroy
					flash.notice = flash_message_for(@object, :successfully_removed)
					respond_with(@hp) do |format|
						format.js
					end
				else
					invoke_callbacks(:destroy, :fails)
			      	respond_with(@object) do |format|
			        	format.html { redirect_to collection_url }
			      	end
			    end
			end

			def show
		        redirect_to(:action => :edit)
		    end
	
  			def update_positions
		        params[:positions].each do |id, index|
		          Spree::HomepageElement.update_all(['position=?', index], ['id=?', id])
		        end

		        respond_to do |format|
		          format.html { redirect_to admin_homepage_elements_url() }
		          format.js  { render :text => 'Ok' }
		        end
	      	end

	      	protected

			def modify_params
			  	if !params[:homepage_element][:homepageble_type].blank?
  					params[:homepage_element][:url] = ""
  				end
  				if !params[:homepage_element][:url].blank? 
  					params[:homepage_element][:homepageble_type] = nil
  					params[:homepage_element][:homepageble_id] = nil
  				end
  			end

  		end
  	end
end