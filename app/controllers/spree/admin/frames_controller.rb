module Spree
  module Admin
    class FramesController < Spree::Admin::BaseController
      
      def index
        @frames = Spree::Frame.page(params[:page]).per(Spree::Config[:admin_products_per_page])
      end

      def destroy
        @frame = Spree::Frame.find params[:id]
        @frame.destroy
      end

      def edit
        @frame = Spree::Frame.find params[:id]
      end

      def new
        @frame = Spree::Frame.new
      end

      def update
        @frame = Spree::Frame.find(params[:id])
        @frame.update_attributes(params[:frame])
        flash[:notice] = "#{@frame.name} modificato con successo"
        redirect_to :action => :index
      end

      def create
        @frame = Spree::Frame.new(params[:frame])
        if @frame.save
          flash[:notice] = t(:created_successfully)
          redirect_to :action => :index
        else
          flash[:error] = t(:create_error)
          redirect_to :action => :index
        end
      end
    end
  end
end


