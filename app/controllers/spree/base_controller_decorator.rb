Spree::BaseController.class_eval do
  def open_modal
    @banner = Spree::Banner.find(params[:id])
    respond_to do |format|
      format.js
    end
  end
  
  def getPromotion(prod, promo_code)
    if promo_code
      Spree::LineItemPromotion.active.find_by_promo_code_and_product_id(promo_code, prod.id)
    else
      nil
    end
  end
  
  def retrieve_paper_weight
    render :json => Spree::Config[:paper_weight].split(",").collect {|w| w}.to_json
  end
  
  def retrieve_promo_date
    render :json => Spree::LineItemPromotion.active.find_by_promo_code(params[:code]).to_calculate.to_json
  end
  
  def render_root
    flash[:notice] = t(:product_not_active)
    redirect_to root_path
  end

  def set_current_order
    if user = current_user
			if user.respond_to?(:last_incomplete_order)
	      last_incomplete_order = user.last_incomplete_order
	      if (last_incomplete_order && (((last_incomplete_order.created_at + 1.weeks) > Time.now) || ((last_incomplete_order.updated_at + 1.weeks) > Time.now)))
	        if session[:order_id].nil? && last_incomplete_order
	            session[:order_id] = last_incomplete_order.id
	        elsif current_order(true) && last_incomplete_order && current_order != last_incomplete_order
	          current_order.merge!(last_incomplete_order)
	        end
	      else
	        current_order(true)
	        last_incomplete_order.destroy if last_incomplete_order
	      end
			end
    end
  end
end