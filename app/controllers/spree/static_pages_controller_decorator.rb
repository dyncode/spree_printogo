Spree::StaticPagesController.class_eval do
	def index
		redirect_to(root_url)
	end
	
  private
  def accurate_title
    @static_page ? @static_page.tag_title : super
  end
end