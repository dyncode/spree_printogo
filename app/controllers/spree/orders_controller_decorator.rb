Spree::OrdersController.class_eval do
  def populate
    @order = current_order(true)
    user = user_signed_in? ? current_user : nil
    if params[:poster_product_id]
      @order.add_variant(Spree::PosterProduct.find(params[:poster_product_id]), params, user)
    elsif params[:poster_high_quality_product_id]
        @order.add_variant(Spree::PosterHighQualityProduct.find(params[:poster_high_quality_product_id]), params, user)
    elsif params[:business_card_product_id]
      @order.add_variant(Spree::BusinessCardProduct.find(params[:business_card_product_id]).master, params, user)
    elsif params[:poster_flyer_product_id]
      @order.add_variant(Spree::PosterFlyerVariant.find(params[:order][:format]), params, user)
    elsif params[:letterhead_product_id]
      @order.add_variant(Spree::LetterheadVariant.find(params[:order][:format]), params, user)
    elsif params[:postcard_product_id]
      @order.add_variant(Spree::PostcardVariant.find(params[:order][:format]), params, user)
    elsif params[:playbill_product_id]
      @order.add_variant(Spree::PlaybillVariant.find(params[:order][:format]), params, user)
    elsif params[:flayer_product_id]
      @order.add_variant(Spree::FlayerVariant.find(params[:order][:format]), params, user)
    elsif params[:staple_product_id]
      @order.add_variant(Spree::StapleVariant.find(params[:order][:format]), params, user)
    elsif params[:paperback_product_id]
      @order.add_variant(Spree::PaperbackVariant.find(params[:order][:format]), params, user)
    elsif params[:folding_product_id]
      @order.add_variant(Spree::FoldingVariant.find(params[:order][:format]), params, user)
    elsif params[:banner_product_id]
      @order.add_variant(Spree::BannerProduct.find(params[:banner_product_id]).banner_variant, params, user)
    elsif params[:pvc_product_id]
      @order.add_variant(Spree::PvcProduct.find(params[:pvc_product_id]).pvc_variant, params, user)
    elsif params[:spiral_product_id]
      @order.add_variant(Spree::SpiralVariant.find(params[:order][:format]), params, user)
    elsif params[:canvas_product_id]
      @order.add_variant(Spree::CanvasProduct.find(params[:canvas_product_id]).canvas_variant, params, user)
    elsif params[:rigid_product_id]
      @order.add_variant(Spree::RigidProduct.find(params[:rigid_product_id]), params, user)
    elsif params[:sticker_product_id]
      @order.add_variant(Spree::StickerProduct.find(params[:sticker_product_id]), params, user)
    elsif params[:advanced_product_id]
      @order.add_variant(Spree::AdvancedVariant.find(params[:order][:model]), params, user)
    else
      @order.add_variant(Spree::SimpleVariant.find(params[:order][:model]), params, user)
    end

    @order.adjustments.where(:originator_type => 'Spree::ShippingMethod').each { |ad| ad.destroy }
    @order.update_attribute(:shipping_method_id, select_default_shipping_method_by_weight(@order.weight_item).id)

    if @order.shipping_method.present?
      shipping_methods = get_all_shipping(@order)
      shipping_methods.each do |sp|
        sp.adjust(@order, sp.max_weight.to_s)
      end
      @order.update!
    else
      @order.line_items.last.destroy if !@order.line_items.empty?
      @order.update!
      flash.notice = "Impossibile caricare il prodotto in quanto non sono previsti metodi di spedizione adeguati."
    end

    tax_rate = Spree::TaxRate.first
    tax_rate.adjust(@order)
    #@order.shipping_method.adjust(@order)

    fire_event('spree.cart.add')
    fire_event('spree.order.contents_changed')
    respond_with(@order) { |format| format.html { redirect_to cart_path } }
  end

  def edit
    @order = current_order(true)
    @order.update!
    associate_user
  end

	def confirm
		@order = Spree::Order.find_by_number!(params[:id])
		respond_with(@order)
	end

  def update
    @order = current_order
    if @order.update_attributes(params[:order])
      @order.line_items = @order.line_items.select { |li| li.quantity > 0 }
      @order.adjustments.where(:originator_type => 'Spree::ShippingMethod').each { |ad| ad.destroy }
      shipping_methods = get_all_shipping(@order)
      shipping_methods.each do |sp|
        sp.adjust(@order, sp.max_weight.to_s)
      end
      @order.update!

      @order.adjustments.where(:originator_type => 'Spree::TaxRate').each { |ad| ad.destroy }
      tax_rate = Spree::TaxRate.first
      tax_rate.adjust(@order)

      fire_event('spree.order.contents_changed')
      respond_with(@order) { |format| format.html { redirect_to cart_path } }
    else
      respond_with(@order)
    end
  end

  def preventive
    @error = false
    begin
      user = user_signed_in? ? current_user : nil      
      if params[:business_card_product_id]
        @variant = Spree::BusinessCardProduct.find(params[:business_card_product_id]).master
        calc = current_order(true).add_variant(@variant, params, user, 1, true)
      elsif params[:poster_product_id]
        @product = Spree::PosterProduct.find(params[:poster_product_id])
        calc = current_order(true).add_variant(@product, params, user, 1, true)
        @variant = Spree::PosterPrinterFormat.find(params[:order][:format])
      elsif params[:poster_high_quality_product_id]
        @product = Spree::PosterHighQualityProduct.find(params[:poster_high_quality_product_id])
        calc = current_order(true).add_variant(@product, params, user, 1, true)
        @variant = Spree::PosterHighQualityPrinterFormat.find(params[:order][:format])
      elsif params[:poster_flyer_product_id]
        @variant = Spree::PosterFlyerVariant.find(params[:order][:format])
        calc = current_order(true).add_variant(@variant, params, user, 1, true)
      elsif params[:letterhead_product_id]
        @variant = Spree::LetterheadVariant.find(params[:order][:format])
        calc = current_order(true).add_variant(@variant, params, user, 1, true)
      elsif params[:postcard_product_id]
        @variant = Spree::PostcardVariant.find(params[:order][:format])
        calc = current_order(true).add_variant(@variant, params, user, 1, true)
      elsif params[:playbill_product_id]
        @variant = Spree::PlaybillVariant.find(params[:order][:format])
        calc = current_order(true).add_variant(@variant, params, user, 1, true)
      elsif params[:flayer_product_id]
        @variant = Spree::FlayerVariant.find(params[:order][:format])
        calc = current_order(true).add_variant(@variant, params, user, 1, true)
      elsif params[:staple_product_id]
        @variant = Spree::StapleVariant.find(params[:order][:format])
        calc = current_order(true).add_variant(@variant, params, user, 1, true)
      elsif params[:paperback_product_id]
        @variant = Spree::PaperbackVariant.find(params[:order][:format])
        calc = current_order(true).add_variant(@variant, params, user, 1, true)
      elsif params[:folding_product_id]
        @variant = Spree::FoldingVariant.find(params[:order][:format])
        calc = current_order(true).add_variant(@variant, params, user, 1, true)
      elsif params[:banner_product_id]
        @variant = Spree::BannerProduct.find(params[:banner_product_id]).banner_variant
        calc = current_order(true).add_variant(@variant, params, user, 1, true)
      elsif params[:pvc_product_id]
        @variant = Spree::PvcProduct.find(params[:pvc_product_id]).pvc_variant
        calc = current_order(true).add_variant(@variant, params, user, 1, true)
      elsif params[:spiral_product_id]
        @variant = Spree::SpiralVariant.find(params[:order][:format])
        calc = current_order(true).add_variant(@variant, params, user, 1, true)
      elsif params[:canvas_product_id]
        @variant = Spree::CanvasProduct.find(params[:canvas_product_id]).canvas_variant
        calc = current_order(true).add_variant(@variant, params, user, 1, true)
      elsif params[:rigid_product_id]
        @variant = Spree::RigidProduct.find(params[:rigid_product_id])
        calc = current_order(true).add_variant(@variant, params, user, 1, true)
      elsif params[:sticker_product_id]
        @product = Spree::StickerProduct.find(params[:sticker_product_id])
        calc = current_order(true).add_variant(@product, params, user, 1, true)
        @variant = @product.sticker_printer_formats.find(params[:order][:format].to_i)
      elsif params[:advanced_product_id]
        @variant = Spree::AdvancedVariant.find(params[:order][:model])
        calc = current_order(true).add_variant(@variant, params, user, 1, true)
      else
        @variant = Spree::SimpleVariant.find(params[:order][:model])
        calc = current_order(true).add_variant(@variant, params, user, 1, true)
      end

      #@total_vat = calc[:total_vat]
      #@total_with_vat_and_shipping = calc[:total_with_vat_and_shipping]
      #@total_with_vat = calc[:total_with_vat]
      @total = calc[:total]
      @price = calc[:price]
      @number_of_copy = Integer(calc[:number_of_copy]) if calc[:number_of_copy]
      @total_whitout_date = calc[:total_whitout_date]
      @promo = calc[:promo]
      @total_filecheck = calc[:total_filecheck]
      @promotion = calc[:line_item_promotion]
    rescue Exception => e
      logger.info "+=+=+=+=+=+=+=+=+=+=+=+=+=+= #{e} +=+=+=+=+=+=+=+=+=+=+=+=+=+="
      @error = true
    end
  end

  def first_step
    @order = current_order
    if params[:order]
      shipping = Spree::ShippingCategory.find_by_id(params[:order].delete(:shipping_category))
      if @order.shipping_method.shipping_category != shipping
        @order.update_attribute(:shipping_method, select_shipping_method_by_weight(@order.weight_item, shipping))
        @order.adjustments.where(:originator_type => 'Spree::ShippingMethod').each { |ad| ad.destroy }
        shipping_methods = get_all_shipping(@order)
        shipping_methods.each do |sp|
          sp.adjust(@order, sp.max_weight.to_s)
        end
      end
      @order.update!
      @order.update_attributes(params[:order])
      @order.presentation_pack.adjust(@order)

      @order.adjustments.where(:originator_type => 'Spree::TaxRate').each { |ad| ad.destroy }
      tax_rate = Spree::TaxRate.first
      tax_rate.adjust(@order)

      redirect_to checkout_path
    else
      respond_with(@order) { |format| format.html { redirect_to cart_path } }
    end
  end
end