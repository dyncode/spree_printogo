module Spree
  class BusinessCardsController < BaseController
    before_filter :load_product, :only => :show
    rescue_from ActiveRecord::RecordNotFound, :with => :render_root
    helper 'spree/taxons'

    respond_to :html
    
    def index
      @searcher = Config.searcher_class.new(params)
      @products = @searcher.retrieve_products
      respond_with(@products)
    end

    def load_product
      @product = Spree::BusinessCardProduct.active.find_by_permalink!(params[:id])
      @promotions = Spree::LineItemPromotion.active.available.find_all_by_product_id(@product.id)
    end

    def show
      return unless @product
      @title = @product.seo_title
      @promo = getPromotion(@product, params[:promo_code])
      @variants = Spree::BusinessCardVariant.active.includes([:option_values, :images]).where(:product_id => @product.id, :active => true)
      @product_properties = Spree::ProductProperty.includes(:property).where(:product_id => @product.id)
      
      respond_with(@product, @promo)
    end

  end
end