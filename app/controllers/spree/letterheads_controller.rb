module Spree
  class LetterheadsController < BaseController
    before_filter :load_product, :only => :show
    rescue_from ActiveRecord::RecordNotFound, :with => :render_root
    helper 'spree/taxons'

    respond_to :html

    def load_product
      @product = Spree::LetterheadProduct.active.find_by_permalink!(params[:id])
      @promotions = Spree::LineItemPromotion.active.available.find_all_by_product_id(@product.id)
    end


    def show
      return unless @product
      @title = @product.seo_title
      @promo = getPromotion(@product, params[:promo_code])
      @variants = @product.letterhead_variants.active.includes([:option_values, :images]).where(:product_id => @product.id, :active => true)
      
      respond_with(@product, @promo)
    end

  end
end