Spree::HomeController.class_eval do
  #caches_action :index, :cache_path => Proc.new { |c| c.params }, :expires_in => 1.days.seconds
  
  def index

    @index_homepage_elements = []
    Spree::HomepageElement.enabled.all.each do |hpe| 
        @index_homepage_elements << hpe if hpe.is_available? && @index_homepage_elements.count < Spree::Config[:homepage_elements_number].to_i
    end

    @business_card = Spree::BusinessCardProduct.active.last
    @poster = Spree::PosterProduct.active.last
    @flayer = Spree::FlayerProduct.only_flyer.active.last
    @letterhead = Spree::LetterheadProduct.active.last
    @postcard = Spree::PostcardProduct.active.last
    @playbill = Spree::PlaybillProduct.active.last
    @paperbacks = Spree::PaperbackProduct.active
    @staple = Spree::StapleProduct.active.last
    @folding = Spree::FoldingProduct.active.last
    @banner = Spree::BannerProduct.only_banner.active.last
    @pvc = Spree::PvcProduct.active.last
    @spiral = Spree::SpiralProduct.active.last
    @canvas = Spree::CanvasProduct.active.last
  end
end