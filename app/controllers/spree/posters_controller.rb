module Spree
  class PostersController < BaseController
    before_filter :load_product, :only => :show
    rescue_from ActiveRecord::RecordNotFound, :with => :render_root
    helper 'spree/taxons'

    respond_to :html
    
    def index
      @products = Spree::PosterProduct.active
      @title = @products.first.seo_title
    end

    def load_product
      @product = Spree::PosterProduct.active.find_by_permalink!(params[:id])
      @promotions = Spree::LineItemPromotion.active.available.find_all_by_product_id(@product.id)
    end
    
    def show
      return unless @product
      @title = @product.seo_title
      @promo = getPromotion(@product, params[:promo_code])
      if @product.type == "Spree::PosterFlyerProduct"
        @variants = @product.poster_flyer_variants.active.includes([:option_values]).where(:product_id => @product.id, :active => true)
        respond_with(@product, @promo) do |format|
          format.html { render :action => "show_flyer" }
        end
      elsif @product.type == "Spree::PosterHighQualityProduct"
        respond_with(@product, @promo) do |format|
          format.html { render :action => "show_high_quality" }
        end
      else
        respond_with(@product, @promo)
      end
    end

  end
end