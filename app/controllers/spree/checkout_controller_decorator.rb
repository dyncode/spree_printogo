Spree::CheckoutController.class_eval do
  def upload_file
    @order.update_attributes(object_params)
    @order.update!
    @order.adjustments.where(:originator_type => 'Spree::TaxRate').each { |ad| ad.destroy }
    tax_rate = Spree::TaxRate.first
    tax_rate.adjust(@order)
  end

  def update
    if object_params[:line_item]
      object_params[:line_item].each do |line_item|
        line_item = line_item[1]
        li = Spree::LineItem.find line_item[:id]
        line_item.delete(:id)
        li.update_attributes(line_item)
      end
      object_params.delete :line_item
    end

    if @order.update_attributes(object_params)
      fire_event('spree.checkout.update')
      if @order.next
        state_callback(:after)
      else
        flash[:error] = t(:payment_processing_failed)
        respond_with(@order, :location => checkout_state_path(@order.state))
        return
      end

      if @order.state == "complete" || @order.completed?
        flash.notice = t(:order_processed_successfully)
        flash[:commerce_tracking] = "nothing special"
        respond_with(@order, :location => completion_route)
      else
        respond_with(@order, :location => checkout_state_path(@order.state))
      end
    else
      flash.notice = @order.errors.full_messages
      respond_with(@order) { |format| format.html { render :edit } }
    end
  end

	# Provides a route to redirect after order completion
	def completion_route
		begin
			if current_user.ship_address.blank?
				current_user.ship_address = @order.ship_address.clone
				current_user.ship_address.save
				# current_user.ship_address.clone!(@order.ship_address)
			end
		rescue Exception => e
			logger.info "+=+=+=+=+=+=+=+=+=+=+=+=+=+= #{e} +=+=+=+=+=+=+=+=+=+=+=+=+=+="
		end
		confirm_checkout_path(@order)
	end

  def before_address
    if current_user
      @order.bill_address ||= (!current_user.bill_address.blank? && current_user.bill_address.valid?) ? current_user.bill_address.clone : Spree::Address.default
      @order.ship_address ||= (!current_user.ship_address.blank? && current_user.ship_address.valid?) ? current_user.ship_address : Spree::Address.default
    else
      @order.bill_address ||= Spree::Address.default
      @order.ship_address ||= Spree::Address.default
    end
  end

  def before_payment
    @order.update!
    @order.adjustments.where(:originator_type => 'Spree::TaxRate').each { |ad| ad.destroy }
    tax_rate = Spree::TaxRate.first
    tax_rate.adjust(@order)
    current_order.payments.destroy_all if request.put?
    #@order.shipping_method = Spree::ShippingMethod.first
    #@order.save
  end

  def order_opts(order, payment_method_id, stage)
    items = order.line_items.map do |item|
      price = (item.price * 100).to_i # convert for gateway
      { :name        => item.variant.product.name,
        :description => (ActionController::Base.helpers.strip_tags(item.variant.product.description[0..120]) if item.variant.product.description),
        #:description => (item.variant.product.description[0..120] if item.variant.product.description),
        :number      => item.variant.sku,
        :quantity    => item.quantity,
        :amount      => price + (item.adjustments.eligible.sum(&:amount) * 100).to_i,
        :weight      => item.variant.weight,
        :height      => item.variant.height,
        :width       => item.variant.width,
        :depth       => item.variant.weight }
    end
    
    credits = order.adjustments.eligible.map do |credit|
      if credit.amount < 0.00
        { :name        => credit.label,
          :description => credit.label,
          :sku         => credit.id,
          :quantity    => 1,
          :amount      => (credit.amount*100).to_i }
      end
    end
    
    credits_total = 0
    credits.compact!
    if credits.present?
      items.concat credits
      credits_total = credits.map {|i| i[:amount] * i[:quantity] }.sum
    end

    if payment_method.preferred_cart_checkout and (order.shipping_method.blank? or order.ship_total == 0)
      shipping_cost  = shipping_options[:shipping_options].first[:amount]
      order_total    = (order.total * 100 + (shipping_cost)).to_i
      shipping_total = (shipping_cost).to_i
    else
      order_total    = (order.total * 100).to_i
      shipping_total = (order.ship_total * 100).to_i
    end
    
    opts = { :return_url        => paypal_confirm_order_checkout_url(order, :payment_method_id => payment_method_id),
      :cancel_return_url => edit_order_checkout_url(order, :state => :payment),
      :order_id          => order.number,
      :custom            => order.number,
      :items             => items,
      :subtotal          => ((order.item_total * 100) + credits_total).to_i,
      :tax               => (order.tax_total*100).to_i,
      :shipping          => shipping_total + (order.adjustments.eligible.find_by_originator_type("Spree::PresentationPack").amount * 100).to_i,
      :money             => order_total,
      :max_amount        => (order.total * 300).to_i}
    
    if stage == "checkout"
      opts[:handling] = 0
      
      opts[:callback_url] = spree.root_url + "paypal_express_callbacks/#{order.number}"
      opts[:callback_timeout] = 3
    elsif stage == "payment"
      #hack to add float rounding difference in as handling fee - prevents PayPal from rejecting orders
      #because the integer totals are different from the float based total. This is temporary and will be
      #removed once Spree's currency values are persisted as integers (normally only 1c)
      if payment_method.preferred_cart_checkout
        opts[:handling] = 0
      else
        opts[:handling] = (order.total*100).to_i - opts.slice(:subtotal, :tax, :shipping).values.sum
      end
    end
    
    opts
  end
end