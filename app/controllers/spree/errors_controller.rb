module Spree
  class ErrorsController < BaseController
    def internal_error
      @body_id = "internal_error"
      respond_to do |format|
       format.html { render :template => "spree/errors/500", :status => 500 } 
       format.all  { render :nothing => true, :status => 500 } 
      end
    end

    def not_found
      @body_id = "not_found"
      respond_to do |format|
       format.html { render :template => "spree/errors/404", :status => 404 } 
       format.all  { render :nothing => true, :status => 404 } 
      end
    end
  end
end