/*
 * Traduzione dei messaggi di default per il pugin jQuery validation.
 * Language: IT
 * Traduzione a cura di Davide Falchetto
 * E-mail: d.falchetto@d4solutions.it
 * Web: www.d4solutions.it
 */
jQuery.extend(jQuery.validator.messages, {
        required: Spree.translations.required,
        remote: Spree.translations.remote,
        email: Spree.translations.email,
        url: Spree.translations.url,
        date: Spree.translations.date,
        dateISO: Spree.translations.dateISO,
        number: Spree.translations.number,
        digits: Spree.translations.digits,
        creditcard: Spree.translations.creditcard,
        equalTo: Spree.translations.equalTo,
        accept: Spree.translations.accept,
        maxlength: jQuery.format(Spree.translations.maxlength),
        minlength: jQuery.format(Spree.translations.minlength),
        rangelength: jQuery.format(Spree.translations.minlength),
        range: jQuery.format(Spree.translations.range),
        max: jQuery.format(Spree.translations.max),
        min: jQuery.format(Spree.translations.min)
});