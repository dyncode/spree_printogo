$(function () {
	$('#nav > li > ul').each(function(i) {
		$(this).parent('li').find('a:first').click(function(e) {
			e.preventDefault()
			currentMenu($(this));
			return false;
		});
	})
	
	$('#nav li > ul').hide('blind');
});

function currentMenu(elem) {
	$('#nav li > ul').hide('blind', {}, 1000);
	elem.next('ul:first').show('blind', {}, 1000);
	elem.parent('li').find('ul > li ul').show('blind', {}, 2000);
}