(function($){
	$(function(){
		// need for populate state select
		if($('#login-form').is('*') || $('#user_new').is('*') || $('.edit_user').is('*') || $('.edit_spree_user').is('*')) {

			var get_states = function (region) {
				country = $('div#' + region + 'country' + ' span#' + region + 'country :only-child').val();
				return state_mapper[country];
			};

			var update_state = function (region) {
				states = get_states(region);

				state_select = $('div#' + region + 'state select');
				state_input = $('div#' + region + 'state input');

				if (states) {
					selected = state_select.val();
					state_select.html('');
					states_with_blank = [
					["", Spree.translations.empty_state]
					].concat(states);
					$.each(states_with_blank, function (pos, id_nm) {
						var opt = $(document.createElement('option'))
						.attr('value', id_nm[0])
						.html(id_nm[1]);
						if (selected == id_nm[0]) {
							opt.prop("selected", true);
						}
						state_select.append(opt);
					});
					state_select.prop("disabled", false).show();
					state_input.hide().prop("disabled", true);

				} else {
					state_input.prop("disabled", false).show();
					state_select.hide().prop("disabled", true);
				}

			};

			$('div#bcountry select').change(function() { update_state('b'); });
			$('div#scountry select').change(function() { update_state('s'); });
			update_state('b');
			update_state('s');
		}
	});
})(jQuery);

$(document).ready(function() {
	$('input#user_bill_address_attributes_is_company').click(function() {
		if (($(this)).is(':checked')) {
			$('#is_company_uncheck').hide();
			$('#is_company_check').show();
			$('#is_company_uncheck input').prop('disabled', true);
			$('#is_company_check input').prop('disabled', false);
		} else {
			$('#is_company_check').hide();
			$('#is_company_uncheck').show();
			$('#is_company_check input').prop('disabled', true);
			$('#is_company_uncheck input').prop('disabled', false);
		}
	}).triggerHandler('click');

	if($('input#user_bill_address_attributes_is_company').is(':checked')) {
		$('#is_company_uncheck').hide();
		$('#is_company_check').show();
		$('#is_company_uncheck input').prop('disabled', true);
		$('#is_company_check input').prop('disabled', false);
	} else {
		$('#is_company_check').hide();
		$('#is_company_uncheck').show();
		$('#is_company_check input').prop('disabled', true);
		$('#is_company_uncheck input').prop('disabled', false);
	}
});