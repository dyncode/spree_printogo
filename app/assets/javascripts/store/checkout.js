(function($){
	$(document).ready(function(){
		if($('#checkout_form_address').is('*')){

			$('#checkout_form_address').validate();
			$('#is_company_check input').prop('disabled', true);

			var get_states = function(region){
				country = $('div#' + region + 'country' + ' span#' + region + 'country :only-child').val();
				return state_mapper[country];
			}

			var update_state = function(region) {
        states = get_states(region);

        state_select = $('div#' + region + 'state select');
        state_input = $('div#' + region + 'state input');

        if(states) {
          selected = parseInt(state_select.val());
          state_select.html('');
          states_with_blank = [["",Spree.translations.empty_state]].concat(states);
          $.each(states_with_blank, function(pos,id_nm) {
            var opt = $(document.createElement('option'))
                      .attr('value', id_nm[0])
                      .html(id_nm[1]);
            if(selected==id_nm[0]){
              opt.prop("selected", true);
            }
            state_select.append(opt);
          });
          state_select.prop("disabled", false).show();
          state_input.hide().prop("disabled", true);

        } else {
          state_input.prop("disabled", false).show();
          state_select.hide().prop("disabled", true);
        }
			};

			$('div#bcountry select').change(function() { update_state('b'); });
			$('div#scountry select').change(function() { update_state('s'); });
			update_state('b');
			update_state('s');

			$('input#order_bill_address_attributes_is_company').click(function() {
				if (($(this)).is(':checked')) {
					$('#is_company_uncheck').hide();
					$('#is_company_check').show();
					$('#is_company_uncheck input').prop('disabled', true);
					$('#is_company_check input').prop('disabled', false);
				} else {
					$('#is_company_check').hide();
					$('#is_company_uncheck').show();
					$('#is_company_check input').prop('disabled', true);
					$('#is_company_uncheck input').prop('disabled', false);
				}
			}).triggerHandler('click');

			$('input#order_other_bill').click(function() {
				if (($(this)).is(':checked')) {
					$('#billing .inner').show();
					$('#billing .inner input, #billing .inner select').prop('disabled', false);
					if (get_states('s')) {
						return $('div#bstate input').hide().prop('disabled', true);
					} else {
						return $('div#bstate select').hide().prop('disabled', true);
					}
				} else {
					$('#billing .inner').hide();
					return $('#billing .inner input, #billing .inner select').prop('disabled', true);
				}

			}).triggerHandler('click');
    }

    if($('#checkout_form_payment').is('*')){
      // Show fields for the selected payment method
      $("input[type='radio'][name='order[payments_attributes][][payment_method_id]']").click(function(){
        $('#payment-methods li').hide();
        if(this.checked){ $('#payment_method_'+this.value).show(); }
      }).triggerHandler('click');
    }
  });

})(jQuery);

function disableSaveOnClick() {
  $('form.edit_order').submit(function() {
    $(this).find(':submit, :image').attr('disabled', true).removeClass('primary').addClass('disabled');
  });
}
