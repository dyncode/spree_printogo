$(function () {	
    $('.custom-size input').attr('disabled', true);
    //$(".format-check input[type=radio]:checked").change();
	
	var paper = $('select[name="order[paper]"]').val();
	updatePapers(papers, paper);
	updatePlotter(papers, paper);
	
	$(".format-check input[type=radio]").change(function () {
        if ($(this).data('custom') == true) {
            if ($('.custom-size input').attr('disabled') == 'disabled') {
                $('.custom-size input').attr('disabled', false);
            }
        } else {
            $('.custom-size input').attr('disabled', true);
        }
        $('select[name="order[cutting_coil]"]').prop("selectedIndex",0);
		if ($(this).data('cutting-coil') == false) {
			$('select[name="order[cutting_coil]"]').attr('disabled', true);
		} else {
			$('select[name="order[cutting_coil]"]').attr('disabled', false);
		}
		
		updatePapers(papers, $('select[name="order[paper]"]').val());
		updatePlotter(papers, $('select[name="order[paper]"]').val());
    });
	
	$('select[name="order[paper]"]').change(function() {
		updatePapers(papers, $(this).val());
	});
	
	// aggiungo la descrizione dei tipi di stampa
	$('select[name="order[print_color]"]').change(function() {
		updatePlotter(papers, paper);
	});
	
	//aggiungo la descrizione delle singole lavorazioni plotter selezionate
	$('select[name^="order[processing_plotter_combination]"]').change(function() {
		updateProcessingPlotterPresentation(papers, paper, $(this).attr("processing_plotter_id"));
	});
	
	$(".format-check input[type=radio]:first").click();
  
  $('input[name="order[custom][width]"]').change(function() {
    if(is_limit && $(this).val() > max_width) { $(".error_custom_size").show(); $(this).val(max_width); $(this).change(); }
    if($(this).val() > max_width) { $(".error_custom_size").show(); }
  });
  $('input[name="order[custom][height]"]').change(function() {
    if(is_limit && $(this).val() > max_height) { $(".error_custom_size").show(); $(this).val(max_height); $(this).change(); }
    if($(this).val() > max_height) { $(".error_custom_size").show(); }
  })
});


function updatePapers(papers, current) {
	//nascondo e resetto tutti i div conteneti processing_plotter_combination
	$('select[name^="order[processing_plotter_combination]"]').prop("selectedIndex",0);
	$('select[name^="order[processing_plotter_combination]"]').parent().hide();
	
	$.each(papers, function(k, pv) {
		if (pv.id == current) {
			appendToOption('select[name="order[print_color]"]', pv.plotters);
			$('select[name="order[print_color]"]').change();
			
			$.each(pv.processing_plotters, function(it, st) {
				var elem = '#order_processing_plotter_combination_'+ st.id +'';
				$(elem).parent().show();
			});
			
			// CuttingCoil
			$.each($('select[name="order[cutting_coil]"]').find("option"), function(k, cce) {
				if ($.inArray(Math.round($(cce).val()), pv.cutting_coils) < 0) {
					$(cce).attr("disabled", "disabled");
				} else {
					$(cce).removeAttr("disabled");
				}
			});
			$('select[name="order[cutting_coil]"]').find("option:first").removeAttr("disabled");
			$('select[name="order[cutting_coil]"]').prop("selectedIndex",0);
			
			//aggiungo la descrizione del tipo di materiale selezionato
			$('select[name="order[paper]"]').parent().data("powertip", pv.description);
		}
	});
}

// aggiungo la descrizione dei tipi di stampa
function updatePlotter(papers, current){
	$.each(papers, function(k, pv) {
		if (pv.id == current) {
			$.each(pv.plotters, function(i, pl) {
				if (pl.id == $('select[name="order[print_color]"]').val()){
					$('select[name="order[print_color]"]').parent().data("powertip", pl.presentation);
				};
			});
		}
	});
}

//aggiungo la descrizione delle singole lavorazioni plotter selezionate
function updateProcessingPlotterPresentation(papers, current, id){
	$.each(papers, function(k, pv) {
		if (pv.id == current) {
			$.each(pv.processing_plotters, function(i, pp) {
				$.each(pp.processing_plotter_types, function(i, ppp) {
					if (ppp.id == $('#order_processing_plotter_combination_'+ id +'').val()){
						$('#order_processing_plotter_combination_'+ id +'').parent().data("powertip", ppp.presentation);
					};
				});
			});
		}
	});
}


function appendToOptionWithOneDefault(tag, elements) {
    var def1 = $(tag).children()[0];
	$(tag).html("").append(def1);
    $.each(elements, function (i, v) {
        $(tag).append(
            $("<option></option>").val(v.id).text(v.name)
        );
    });
}

function appendToOption(tag, elements) {
	$(tag).html("");
    $.each(elements, function (i, v) {
        $(tag).append(
            $("<option></option>").val(v.id).text(v.name)
        );
    });
}

//devo ritornare una stringa tipo
//{"24130"=>"", "24581"=>"", "22962"=>"24127", "22963"=>"22958", "26160"=>"", "26159"=>"", "26158"=>""}
//che indica gli id dei processing_plotter_type da applicare

function get_processing_plotter_types(paper_id) {
	var json = {};
	$.each(papers, function(k, pv) {
		if (pv.id == paper_id) {
			$.each(pv.processing_plotters, function(it, st) {
				var ppc = $('#order_processing_plotter_combination_'+ st.id +'').val();
				if (ppc != "") {
					json[st.id] = ppc;
				}
			});
		}
	});
	
	return json;
}