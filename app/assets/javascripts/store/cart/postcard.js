//paper_products_selected
var availableGr = retrieve_paper_weight();

$(function () {
    if ($('select[name="order[format]"]').length > 0) {

        if ($('select[name="order[format]"] option:selected').length == 0) {
            $('select[name="order[format]"] option:first').attr('selected','selected');
        }
        var papers = updatePaper($('select[name="order[format]"] option:selected').val());
        appendToOption('select[name="order[paper]"]', papers.available_paper);
        var gr = fetchGr($('select[name="order[format]"] option:selected').val(), $('select[name="order[paper]"] > option:selected').text());
        appendToOptionAndDisabled(gr, 'select[name="order[weight]"]');

        $('select[name="order[format]"]').change(function () {
            var papers = updatePaper($(this).val());
            appendToOption('select[name="order[paper]"]', papers.available_paper);
            $('select[name="order[paper]"]').change();
        });

        $('select[name="order[paper]"]').change(function () {
            var gr = fetchGr($('select[name="order[format]"] option:selected').val(), $(this).find("option:selected").text());
            appendToOptionAndDisabled(gr, 'select[name="order[weight]"]');
            $('select[name="order[weight]"]').change();
        });

        $('select[name="order[format]"]').change();

    } else {

        if ($('input[name="order[format]"]:checked').length == 0) {
            $('input[name="order[format]"]').first().attr('checked', 'checked');
        }
        var papers = updatePaper($('input[name="order[format]"]').val());
        appendToOption('select[name="order[paper]"]', papers.available_paper);
        var gr = fetchGr($('input[name="order[format]"]:checked').val(), $('select[name="order[paper]"] > option:selected').text());
        appendToOptionAndDisabled(gr, 'select[name="order[weight]"]');

        $('input[name="order[format]"]').change(function () {
            var papers = updatePaper($(this).val());
            appendToOption('select[name="order[paper]"]', papers.available_paper);
            $('select[name="order[paper]"]').change();
        });

        $('select[name="order[paper]"]').change(function () {
            var gr = fetchGr($('input[name="order[format]"]:checked').val(), $(this).find("option:selected").text());
            appendToOptionAndDisabled(gr, 'select[name="order[weight]"]');
            $('select[name="order[weight]"]').change();
        });

        $('input[name="order[format]"]').change();
    }
});

function appendToOptionAndDisabled(gr, tag) {
    $(tag).html("");
    $.each(availableGr, function (i, v) {
        $(tag).append(
            $("<option></option>").val(v).text(v).attr('disabled', ($.inArray(v, gr) < 0))
        );
		$(tag).find("option:not(:disabled)").first().attr("selected", true);
    });
}

function appendToOption(tag, available_paper) {
    $(tag).html("");
    $.each(available_paper, function (i, v) {
        $(tag).append(
            $("<option></option>").val(v[1]).text(v[0])
        );
    });
}

function fetchGr(variant_id, paper_name) {
    var available_gr = [];
    $.each(paper_products_selected, function (i, v) {
        if (v.variant_id == parseInt(variant_id)) {
            $.each(v.values, function (i1, v1) {
				if (v1.weight.paper.name == paper_name) {
					available_gr.push(v1.weight.quantity);
				}
            });
        }
    });
	
    return available_gr;
}

function updatePaper(variant_id) {
    var available_paper = [];
    $.each(paper_products_selected, function (i, v) {
        if (v.variant_id == variant_id) {
            $.each(v.values, function (i1, v1) {
                var find = false;
                $.each(available_paper, function (i2, v2) {
                    if (v2[0] == v1.weight.paper.presentation && v2[1] == v1.weight.paper.id) {
                        find = true;
                    }
                });
                if (!find) {
                    available_paper.push([v1.weight.paper.presentation, v1.weight.paper.id]);
                }
            });
        }
    });
	
    return {available_paper:available_paper };
}