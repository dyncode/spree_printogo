$(function () {
	var frame = $('select[name="order[frame]"]').val();
	updateFormat(frame);
	$('select[name="order[frame]"]').change(function() {
		updateFormat($(this).val());
	});
	
	$('select[name="order[frame]"]').change();

	var paper = $('select[name="order[paper]"]').val();
	updatePapers(papers, paper);
	$('select[name="order[paper]"]').change(function() {
		updatePapers(papers, $(this).val());
	});
	
});

function updatePapers(papers, current) {
	$.each(papers, function(k, pv) {
		if (pv.id == current) {
			$('select[name="order[paper]"]').parent().data("powertip", pv.description);
		}
	});
}

function updateFormat(fr) {
	if (fr == "") {
		$('select[name="order[hanger_accessory]"] option').removeAttr("selected").first().attr("selected", true);
		$('select[name="order[hanger_accessory]"]').attr("disabled", true);
		$('#custom_format').find("input").addClass("required").removeAttr("disabled");
		$('#custom_format').show();
		$('#canvas_format').find("select").removeClass("required");
		$('#canvas_format').hide();
	} else {
		$('select[name="order[hanger_accessory]"]').attr("disabled", false);
		$('#custom_format').find("input").removeClass("required").attr("disabled", true);
		$('#custom_format').hide();
		$('#canvas_format').find("select").addClass("required");
		$('#canvas_format').show();
	}
}
