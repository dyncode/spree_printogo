$(function () {
	var buttonhole = jQuery.parseJSON($('#order_pocket').attr("data-pocket"));
	$.each($('#order_pocket').find("option"), function(k, v) {
		if ($.inArray($(v).val(), buttonhole) < 0) {
			$(v).attr("disabled", "disabled");
		} else {
			$(v).removeAttr("disabled");
		}
	});
	$('#order_pocket').find("option:first").removeAttr("disabled");
	
	var eyeS = $('select[name="order[eyelet]"]').val();
	updateEyeletAC(available_eyelet_accessories, eyeS);
	$('select[name="order[eyelet]"]').change(function() {
		updateEyeletAC(available_eyelet_accessories, $(this).val());
	});
	$('select[name="order[eyelet_accessory]"]').change(function() {
		updateEyeletQT($(this).attr("data-limit"));
	});
	
	$('input[name="order[custom][width]"]').change(function() {
		updateEyeletQT($('select[name="order[eyelet_accessory]"]').attr("data-limit"));
	});
	$('input[name="order[custom][height]"]').change(function() {
		updateEyeletQT($('select[name="order[eyelet_accessory]"]').attr("data-limit"));
	});
	
	$('input[name="order[orientation]"]:checked').change();

	var paper = $('select[name="order[paper]"]').val();
	updatePapers(papers, paper);
	$('select[name="order[paper]"]').change(function() {
		updatePapers(papers, $(this).val());
	});

	var number_of_copy_id = parseInt($('#order_number_of_copy').val());
	$('input, select').not("#order_number_of_copy").change(function() {
		CheckQuote(parseInt($('#order_number_of_copy').val()));
	})
	$('#order_number_of_copy').change(function () {
		var number_of_copy_id = $(this).val();
		CheckQuote(parseInt(number_of_copy_id));
	});

	// Gestico la validazione
	$('#add_banners').validate({
		errorPlacement:function (error, element) {
			$("div.error:first").remove();
			var error_message = error.html();
			error = "<label class='error'><div class='error-arrow'></div><div class='error-inner'><div class='error-content'>"+error_message+"</div></div></label>";
			element.after(error);
		}
	});
});

function CheckQuote(noc) {
	if(noc != undefined && noc != "" && noc > 0) {
		if(checkAndCallAjaxIfValidCustom('input[name="order[custom][width]"]', 'form[id*=add_]') && checkAndCallAjaxIfValidCustom('input[name="order[custom][height]"]', 'form[id*=add_]') && checkAndCallAjaxIfValid('input[name="order[orientation]"]', 'form[id*=add_]') && checkAndCallAjaxIfValidCustom('[name="order[number_of_copy]"]', 'form[id*=add_]') && checkAndCallAjaxIfValid('[name="order[paper]"]', 'form[id*=add_]') && checkAndCallAjaxIfValid('[name="order[print_color]"]', 'form[id*=add_]') && checkAndCallAjaxIfValidCustom('[name="order[eyelet]"]', 'form[id*=add_]') && checkAndCallAjaxIfValidCustom('[name="order[eyelet_accessory]"]', 'form[id*=add_]') && checkAndCallAjaxIfValidCustom('[name="order[eyelet_quantity]"]', 'form[id*=add_]') && checkAndCallAjaxIfValidCustom('[name="order[pocket]"]', 'form[id*=add_]') && checkAndCallAjaxIfValid('[name="order[reinforcement_perimeter]"]', 'form[id*=add_]') && checkAndCallAjaxIfValid('[name="order[file_check]"]', 'form[id*=add_]') && checkAndCallAjaxIfValid('[id*="order_calculator_data_"]', 'form[id*=add_]')) {
			QuotePreload();
		}
	} else {
		$("#total-no-iva").html("0.00 €");
		$("#price-for-element").html("0.00 €");
		$("#file_check").html("0.00 €");
		$("#iva").html("0.00 €");
		$("#preventive-total").html("0.00 €");
	}
}

function updatePapers(papers, current) {
	$.each(papers, function(k, pv) {
		if (pv.id == current) {
			$('select[name="order[paper]"]').parent().data("powertip", pv.description);
		}
	});
}

function QuotePreload() {
  var data = {
		"order[paper]": $('[name="order[paper]"]').val(),
		"order[orientation]": $('[name="order[orientation]"]:checked').val(),
		"order[eyelet]": $('[name="order[eyelet]"]').val(),
		"order[eyelet_accessory]": $('[name="order[eyelet_accessory]"]').val(),
		"order[eyelet_quantity]": $('[name="order[eyelet_quantity]"]').val(),
		"order[pocket]": $('[name="order[pocket]"]').val(),
		"order[pocket_quantity]": $('[name="order[pocket_quantity]"]').val(),
		"order[reinforcement_perimeter]": $('[name="order[reinforcement_perimeter]"]').val(),
		"order[print_color]": $('[name="order[print_color]"]').val(),
		"order_calculator[data]": $('[name="order_calculator[data]"]:checked').val(),
		"banner_product_id": $('#banner_product_id').val(),
	}
	if($('select[name="order[number_of_copy]"]').length > 0){
		data["order[number_of_copy]"] = $('select[name="order[number_of_copy]"] option:selected').val();
	} else {
		data["order[number_of_copy]"] = $('input[name="order[number_of_copy]"]').val();
	}
	if ($('input[name="order[custom][height]"]').val() != "" && $('input[name="order[custom][width]"]').val() != "") {
		data["order[custom][height]"] = $('input[name="order[custom][height]"]').val();
		data["order[custom][width]"] = $('input[name="order[custom][width]"]').val();
	}
	if ($('select[name="order[file_check]"]').length > 0) {
		data["order[file_check]"] = $('select[name="order[file_check]"] option:selected').val();
	}
	$('select').each(function(s) {
		if($(this).find("option:not(:disabled)").length == 0) {
			$(this).attr("disabled", "disabled");
		} else {
			$(this).removeAttr("disabled");
		}
	});

	$('.loader-box').show();
	$.ajax({
		type: "POST",
		url: preventiveUrl,
		data: data
	});
}

function updateEyeletAC(avail_ea, id) {
	var limit = 0
	$.each(avail_ea, function(k, av) {
		if (av.type == id) {
			limit = av.limit;
			$('select[name="order[eyelet_accessory]"]').attr("disabled", "disabled");
			$.each($('select[name="order[eyelet_accessory]"]').find("option"), function(k, ve) {
				if ($.inArray($(ve).val(), av.values) < 0) {
					$(ve).attr("disabled", "disabled");
				} else {
					$(ve).removeAttr("disabled");
				}
			});
		} else {
			$.each($('select[name="order[eyelet_accessory]"]').find("option"), function(k, ve) {
				$(ve).attr("disabled", "disabled");
			});
		}
		$('select[name="order[eyelet_accessory]"]').find("option:not(:disabled)").first().attr("selected", true);
	});
	$('select[name="order[eyelet_accessory]"]').attr("data-limit", limit);
	updateEyeletQT(limit);
  
  $('input[name="order[custom][width]"]').change(function() {
    if(max_width != undefined && $(this).val() > max_width) { $(".error_custom_size").show(); }
  });
  $('input[name="order[custom][height]"]').change(function() {
    if(max_height != undefined && $(this).val() > max_height) { $(".error_custom_size").show(); }
  })
}

function updateEyeletQT(lim) {
	var width = $('input[name="order[custom][width]"]').val();
	var height = $('input[name="order[custom][height]"]').val();
	var disposition = $('select[name="order[eyelet_accessory]"]').val();
	var tot = 0
	
	switch(disposition) {
		case "all_perimeter":
			tot = Math.ceil(((width * 2) + (height * 2))/lim)
			break;
		case "top_side":
			tot = Math.ceil((width)/lim)
			break;
		case "bottom_side":
			tot = Math.ceil((width)/lim)
			break;
		case "right_side":
			tot = Math.ceil((height)/lim)
			break;
		case "left_side":
			tot = Math.ceil((height)/lim)
			break;
		case "side_upper_lower":
			tot = Math.ceil((width * 2)/lim)
			break;
		case "right_left_side":
			tot = Math.ceil((height * 2)/lim)
			break;
	}
	
	$('input[name="order[eyelet_quantity]"]').attr("min", tot);
	$('input[name="order[eyelet_quantity]"]').val(tot);
	
	$('input[name="order[eyelet_quantity]"]').change();
}