$(function () {
	var frame = $('select[name="order[frame]"]').val();
	updateFormat(frame);
	$('select[name="order[frame]"]').change(function() {
		updateFormat($(this).val());
	});
	
	$('select[name="order[frame]"]').change();

	var paper = $('select[name="order[paper]"]').val();
	updatePapers(papers, paper);
	$('select[name="order[paper]"]').change(function() {
		updatePapers(papers, $(this).val());
	});

	var number_of_copy_id = parseInt($('#order_number_of_copy').val());
	$('input, select').not("#order_number_of_copy").change(function() {
		CheckQuote(parseInt($('#order_number_of_copy').val()));
	})
	$('#order_number_of_copy').change(function () {
		var number_of_copy_id = $(this).val();
		CheckQuote(parseInt(number_of_copy_id));
	});
});

function CheckQuote(noc) {
	if(noc != undefined && noc != "" && noc > 0) {
		if(checkAndCallAjaxIfValidCustom('[name="order[custom][width]"]', 'form[id*=add_]') && checkAndCallAjaxIfValidCustom('[name="order[custom][height]"]', 'form[id*=add_]') && checkAndCallAjaxCustom('[name="order[number_of_copy]"]') && checkAndCallAjax('[name="order[paper]"]') && checkAndCallAjax('[name="order[print_color]"]') && checkAndCallAjax('select[name="order[hanger_accessory]"]') && checkAndCallAjax('select[name="order[file_check]"]') && checkAndCallAjax('[id*="order_calculator_data_"]')) {
			QuotePreload();
		}
	} else {
		$("#total-no-iva").html("0.00 €");
		$("#price-for-element").html("0.00 €");
		$("#file_check").html("0.00 €");
		$("#iva").html("0.00 €");
		$("#preventive-total").html("0.00 €");
	}
}

function QuotePreload() {
  var data = {
		"order[custom][height]": $('[name="order[custom][height]"]').val(),
		"order[custom][width]": $('[name="order[custom][width]"]').val(),
		"order[paper]": $('[name="order[paper]"]').val(),
		"order[print_color]": $('[name="order[print_color]"]').val(),
		"order_calculator[data]": $('[name="order_calculator[data]"]:checked').val(),
		"order[frame]": $('select[name="order[frame]"] option:selected').val(),
		"order[hanger_accessory]": $('select[name="order[hanger_accessory]"]').val(),
		"canvas_product_id": $('#canvas_product_id').val()
	}
	if($('select[name="order[number_of_copy]"]').length > 0){
		data["order[number_of_copy]"] = $('select[name="order[number_of_copy]"] option:selected').val();
	} else {
		data["order[number_of_copy]"] = $('input[name="order[number_of_copy]"]').val();
	}
	if ($('select[name="order[file_check]"]').length > 0) {
		data["order[file_check]"] = $('select[name="order[file_check]"] option:selected').val();
	}
	$('select').each(function(s) {
		if($(this).find("option:not(:disabled)").length == 0) {
			$(this).attr("disabled", "disabled");
		} else {
			$(this).removeAttr("disabled");
		}
	});

	$('.loader-box').show();
	$.ajax({
		type: "POST",
		url: preventiveUrl,
		data: data
	});
}

function updatePapers(papers, current) {
	$.each(papers, function(k, pv) {
		if (pv.id == current) {
			$('select[name="order[paper]"]').parent().data("powertip", pv.description);
		}
	});
}

function updateFormat(fr) {
	if (fr == "") {
		$('select[name="order[hanger_accessory]"] option').removeAttr("selected").first().attr("selected", true);
		$('select[name="order[hanger_accessory]"]').attr("disabled", true);
		$('#custom_format').find("input").addClass("required").removeAttr("disabled");
		$('#custom_format').show();
		$('#canvas_format').find("select").removeClass("required");
		$('#canvas_format').hide();
	} else {
		$('select[name="order[hanger_accessory]"]').attr("disabled", false);
		$('#custom_format').find("input").removeClass("required").attr("disabled", true);
		$('#custom_format').hide();
		$('#canvas_format').find("select").addClass("required");
		$('#canvas_format').show();
	}
}
