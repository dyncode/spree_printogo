$(function () {
	$(".format-check input[type=radio]").change(function () {
		if ($(this).data('custom') == true) {
			if ($('.custom-size input').attr('disabled') == 'disabled') {
				$('.custom-size input').attr('disabled', false);
			}
		} else {
			$('.custom-size input').attr('disabled', true);
		}
	});
	$('.custom-size input').attr('disabled', true);
	$(".format-check input[type=radio]:checked").change();

	if ($('#add_poster_high_qualities').is('*')) {
		$('#add_to_cart_js').click(function () {
			$('#add_poster_high_qualities').submit();
		});
	}
    
	var paper = $('select[name="order[paper]"]').val();
	updatePapers(papers, paper);
	$('select[name="order[paper]"]').change(function() {
		updatePapers(papers, $(this).val());
	});

	var number_of_copy_id = parseInt($('#order_number_of_copy').val());
	$('input, select').not("#order_number_of_copy").change(function() {
		CheckQuote(parseInt($('#order_number_of_copy').val()));
	})
	$('#order_number_of_copy').change(function () {
		var number_of_copy_id = $(this).val();
		CheckQuote(parseInt(number_of_copy_id));
	});
});

function CheckQuote(noc) {
	if(noc != undefined && noc != "" && noc > 0) {
		if(checkAndCallAjaxIfValidCustom('[name="order[number_of_copy]"]', 'form[id*=add_]') &&  checkAndCallAjaxIfValid('[name="order[format]"]', 'form[id*=add_]') && checkAndCallAjaxIfValid('[name="order[paper]"]', 'form[id*=add_]') && checkAndCallAjaxIfValid('[name="order[print_color]"]', 'form[id*=add_]') && checkAndCallAjaxIfValid('[name="order[file_check]"]', 'form[id*=add_]') && checkAndCallAjaxIfValid('[id*="order_calculator_data_"]', 'form[id*=add_]')) {
			QuotePreload();
		}
	} else {
		$("#total-no-iva").html("0.00 €");
		$("#price-for-element").html("0.00 €");
		$("#file_check").html("0.00 €");
		$("#iva").html("0.00 €");
		$("#preventive-total").html("0.00 €");
	}
}

function QuotePreload() {
  var data = {
		"order[paper]": $('[name="order[paper]"]').val(),
		"order[print_color]": $('[name="order[print_color]"]').val(),
		"order_calculator[data]": $('input[name="order_calculator[data]"]:checked').val(),
		"poster_high_quality_product_id": $('#poster_high_quality_product_id').val()
	}
	if($('select[name="order[number_of_copy]"]').length > 0){
		data["order[number_of_copy]"] = $('select[name="order[number_of_copy]"] option:selected').val();
	} else {
		data["order[number_of_copy]"] = $('input[name="order[number_of_copy]"]').val();
	}
	if ($('input[name="order[custom][height]"]').val() != "" && $('input[name="order[custom][width]"]').val() != "") {
		data["order[custom][height]"] = $('input[name="order[custom][height]"]').val();
		data["order[custom][width]"] = $('input[name="order[custom][width]"]').val();
	}
	if($('select[name="order[format]"]').length > 0){
		data["order[format]"] = $('[name="order[format]"] option:selected').val();
	} else {
		data["order[format]"] = $('[name="order[format]"]:checked').val();
	}
	if ($('select[name="order[file_check]"]').length > 0) {
		data["order[file_check]"] = $('select[name="order[file_check]"] option:selected').val();
	}
	$('select').each(function(s) {
		if($(this).find("option:not(:disabled)").length == 0) {
			$(this).attr("disabled", "disabled");
		} else {
			$(this).removeAttr("disabled");
		}
	});

	$('.loader-box').show();
	$.ajax({
		type: "POST",
		url: preventiveUrl,
		data: data
	});
}

function updatePapers(papers, current) {
    $.each(papers, function(k, pv) {
        if (pv.id == current) {
            $('select[name="order[paper]"]').parent().data("powertip", pv.description);
        }
    });
}