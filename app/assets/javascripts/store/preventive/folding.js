//paper_products_selected
var availableGr = retrieve_paper_weight();

$(function () {
	// verifico cosa e` stato selezionato e mi prendo carte e grammature associate
	var fold = $('input[name="order[folding]"]:checked');
	var format = $('input[name="order[format]"]:checked');
	if ($.inArray(jQuery.parseJSON(fold.val()), jQuery.parseJSON(format.attr("data-folding"))) != -1) {
		var selectedPapers = retrieve_folding_paper_weight(jQuery.parseJSON(fold.val()), jQuery.parseJSON(format.val()));
		var papers = updatePaper(selectedPapers);
		appendToOption('select[name="order[paper]"]', papers.available_paper);
		var gr = fetchGr(selectedPapers, $('select[name="order[paper]"] :selected').text());
		appendToOptionAndDisabled(gr, 'select[name="order[weight]"]');
		$('select[name="order[weight]"]').change();
		$('input[name="order[folding]"]').change();
	} else {
		var format_fold = jQuery.parseJSON(fold.attr("data-format"))[0]
		$('#radio_format_'+format_fold).attr("checked", "checked");
		$('input[name="order[format]"]').change();
	}
	
	if (fold.attr("data-plasticization") == "0") {
		$.each($('select[name="order[plasticization]"]'), function(k, v) {
			$(v).attr("disabled", "disabled");
		});
	} else {
		$.each($('select[name="order[plasticization]"]'), function(k, v) {
			$(v).removeAttr("disabled");
		});
	}
	
	if (fold.attr("data-orientation") == "1") {
		$.each($('input[name="order[orientation]"]'), function(k, v) {
			if ($(v).attr("data-orientation") == "verticale") {
				$(v).attr("disabled", "disabled");
			}
		});
	} else {
		$.each($('input[name="order[orientation]"]'), function(k, v) {
			$(v).removeAttr("disabled");
		});
	}
	
	// se cambio piega devo:
	// disattivare i formati non presenti nella piega
	// selezionare il primo formato disponibile se e` stato disattivato
	// recuperare carte e grammature
	$('input[name="order[folding]"]').on("change", function () {
		var fold = $(this);
		// ciclo sui formati per disattivare nel caso
		$.each($('input[name="order[format]"]'), function(k, v) {
			if ($.inArray(jQuery.parseJSON($(v).val()), jQuery.parseJSON(fold.attr("data-format"))) < 0) {
				$(v).attr("disabled", "disabled");
			} else {
				$(v).removeAttr("disabled");
			}
		});
		// setto il nuovo formato se e` stato disattivato
		if ($('input[name="order[format]"]:checked').is(":disabled")) {
			var format_fold = jQuery.parseJSON(fold.attr("data-format"))[0];
			$('#radio_format_'+format_fold).attr("checked", "checked");
			$('input[name="order[format]"]').change();
		} else {
			// recupero carte e grammature
			var format = $('input[name="order[format]"]:checked');
			var selectedPapers = retrieve_folding_paper_weight(jQuery.parseJSON(fold.val()), jQuery.parseJSON(format.val()));
			var papers = updatePaper(selectedPapers);
			appendToOption('select[name="order[paper]"]', papers.available_paper);
			var gr = fetchGr(selectedPapers, $('select[name="order[paper]"] :selected').text());
			appendToOptionAndDisabled(gr, 'select[name="order[weight]"]');
			$('select[name="order[weight]"]').change();
		}
		if (fold.attr("data-plasticization") == "0") {
			$.each($('select[name="order[plasticization]"]'), function(k, v) {
				$(v).attr("disabled", "disabled");
			});
		} else {
			$.each($('select[name="order[plasticization]"]'), function(k, v) {
				$(v).removeAttr("disabled");
			});
		}
		if (fold.attr("data-orientation") == "1") {
			$.each($('input[name="order[orientation]"]'), function(k, v) {
				if ($(v).attr("data-orientation") == "verticale") {
					$(v).attr("disabled", "disabled");
				}
			});
		} else {
			$.each($('input[name="order[orientation]"]'), function(k, v) {
				$(v).removeAttr("disabled");
			});
		}
	});
	
	// se cambio formato devo:
	// disattivare i le pieghe non presenti nel formato
	// selezionare la prima piegha disponibile se disattivata
	// recuperare carte e grammature
    $('input[name="order[format]"]').on("change", function () {
		var format = $(this);
		var fold = $('input[name="order[folding]"]:checked');
		// recupero carte e grammature
		var selectedPapers = retrieve_folding_paper_weight(jQuery.parseJSON(fold.val()), jQuery.parseJSON(format.val()));
		var papers = updatePaper(selectedPapers);
		appendToOption('select[name="order[paper]"]', papers.available_paper);
		var gr = fetchGr(selectedPapers, $('select[name="order[paper]"] :selected').text());
		appendToOptionAndDisabled(gr, 'select[name="order[weight]"]');
		$('select[name="order[weight]"]').change();
		if (fold.attr("data-plasticization") == "0") {
			$.each($('select[name="order[plasticization]"]'), function(k, v) {
				$(v).attr("disabled", "disabled");
			});
		} else {
			$.each($('select[name="order[plasticization]"]'), function(k, v) {
				$(v).removeAttr("disabled");
			});
		}
		if (fold.attr("data-orientation") == "1") {
			$.each($('input[name="order[orientation]"]'), function(k, v) {
				if ($(v).attr("data-orientation") == "verticale") {
					$(v).attr("disabled", "disabled");
				}
			});
		} else {
			$.each($('input[name="order[orientation]"]'), function(k, v) {
				$(v).removeAttr("disabled");
			});
		}
    });
	
    $('select[name="order[paper]"]').on("change", function () {
		var format = $('input[name="order[format]"]:checked');
		var fold = $('input[name="order[folding]"]:checked');
		var selectedPapers = retrieve_folding_paper_weight(jQuery.parseJSON(fold.val()), jQuery.parseJSON(format.val()));
		var gr = fetchGr(selectedPapers, $('select[name="order[paper]"] :selected').text());
		appendToOptionAndDisabled(gr, 'select[name="order[weight]"]');
		$('select[name="order[weight]"]').change();
	});

	var number_of_copy_id = parseInt($('#order_number_of_copy').val());
	$('input, select').not("#order_number_of_copy").change(function() {
		CheckQuote(parseInt($('#order_number_of_copy').val()));
	})
	$('#order_number_of_copy').change(function () {
		var number_of_copy_id = $(this).val();
		CheckQuote(parseInt(number_of_copy_id));
	});
});

function CheckQuote(noc) {
	if(noc != undefined && noc != "" && noc > 0) {
		if(checkAndCallAjax('input[name="order[folding]"]') && checkAndCallAjax('input[name="order[format]"]') && checkAndCallAjaxCustom('[name="order[number_of_copy]"]') && checkAndCallAjaxIfEnableElementAndIsValid('select[name="order[weight]"]', 'select[name="order[paper]"]', 'form[id*=add_]') && checkAndCallAjax('select[name="order[plasticization]"]') && checkAndCallAjax('select[name="order[instructions]"]') && checkAndCallAjax('select[name="order[print_color]"]') && checkAndCallAjax('select[name="order[file_check]"]') && checkAndCallAjax('[id*="order_calculator_data_"]')) {
			QuotePreload();
		}
	} else {
		$("#total-no-iva").html("0.00 €");
		$("#price-for-element").html("0.00 €");
		$("#file_check").html("0.00 €");
		$("#iva").html("0.00 €");
		$("#preventive-total").html("0.00 €");
	}
}

function QuotePreload() {
  var data = {
		"folding_product_id": $('#folding_product_id').val(),
		"order[folding]": $('input[name="order[folding]"]:checked').val(),
		"order[orientation]": $('input[name="order[orientation]"]:checked').val(),
		"order[instructions]": $('select[name="order[instructions]"] option:selected').val(),
		"order[paper]": $('select[name="order[paper]"] option:selected').val(),
		"order[weight]": $('select[name="order[weight]"] option:selected').val(),
		"order[plasticization]": $('select[name="order[plasticization]"] option:selected').val(),
		"order[print_color]": $('select[name="order[print_color]"] option:selected').val(),
		"order_calculator[data]": $('input[name="order_calculator[data]"]:checked').val(),
	}
	if($('select[name="order[number_of_copy]"]').length > 0){
		data["order[number_of_copy]"] = $('select[name="order[number_of_copy]"] option:selected').val();
	} else {
		data["order[number_of_copy]"] = $('input[name="order[number_of_copy]"]').val();
	}
	if($('select[name="order[format]"]').length > 0){
		data["order[format]"] = $('[name="order[format]"] option:selected').val();
	} else {
		data["order[format]"] = $('[name="order[format]"]:checked').val();
	}
	if ($('select[name="order[file_check]"]').length > 0) {
		data["order[file_check]"] = $('select[name="order[file_check]"] option:selected').val();
	}
	$('select').each(function(s) {
		if($(this).find("option:not(:disabled)").length == 0) {
			$(this).attr("disabled", "disabled");
		} else {
			$(this).removeAttr("disabled");
		}
	});

	$('.loader-box').show();
	$.ajax({
		type: "POST",
		url: preventiveUrl,
		data: data
	});
}

function retrieve_folding_paper_weight(fold_id, form_id) {
	var json = null;
	$.ajax({
		'async': false,
		'global': false,
		'url': '/'+fold_id+'/retrieve_folding_paper_weight/'+form_id+'.json',
		'dataType': "json",
		'success': function (data) { json = data; }
	});
	return json;
}

function appendToOptionAndDisabled(gr, tag) {
    $(tag).html("");
    $.each(availableGr, function (i, v) {
        $(tag).append(
            $("<option></option>").val(v).text(v).attr('disabled', ($.inArray(v, gr) < 0))
        );
		$(tag).find("option:not(:disabled)").first().attr("selected", true);
    });
}

function appendToOption(tag, available_paper) {
    $(tag).html("");
    $.each(available_paper, function (i, v) {
        $(tag).append(
            $("<option></option>").val(v[1]).text(v[0])
        );
    });
}

function fetchGr(paper_weight, paper_name) {
	var available_gr = [];
	$.each(paper_weight, function (i, v) {
		$.each(v.values, function (i1, v1) {
			$.each(v1.weight.paper, function (i2, v2) {
				if (v2.name == paper_name) {
					if($.inArray(v1.weight.quantity, available_gr) == -1) {
						available_gr.push(v1.weight.quantity);
					}
				}
			});
		});
	});

	return available_gr;
}

function updatePaper(paper_weight) {
    var available_paper = [];
    $.each(paper_weight, function (i, v) {
        //if (v.variant_id == variant_id) {
            $.each(v.values, function (i1, v1) {
                var find = false;
                $.each(available_paper, function (i2, v2) {
                    if (v2[0] == v1.weight.paper.presentation && v2[1] == v1.weight.paper.id) {
                        find = true;
                    }
                });
                if (!find) {
                    available_paper.push([v1.weight.paper.presentation, v1.weight.paper.id]);
                }
            });
        //}
    });
    return {available_paper:available_paper };
}