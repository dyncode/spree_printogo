$(function () {
	$('input[name="order[orientation]"]:checked').change();

	var paper = $('select[name="order[paper]"]').val();
	updatePapers(papers, paper);
	$('select[name="order[paper]"]').change(function() {
		updatePapers(papers, $(this).val());
	});

	var number_of_copy_id = parseInt($('#order_number_of_copy').val());
	$('input, select').not("#order_number_of_copy").change(function() {
		CheckQuote(parseInt($('#order_number_of_copy').val()));
	})
	$('#order_number_of_copy').change(function () {
		var number_of_copy_id = $(this).val();
		CheckQuote(parseInt(number_of_copy_id));
	});
});

function CheckQuote(noc) {
	if(noc != undefined && noc != "" && noc > 0) {
		if(checkAndCallAjaxIfValidCustom('[name="order[custom][width]"]', 'form[id*=add_]') && checkAndCallAjaxIfValidCustom('[name="order[custom][height]"]', 'form[id*=add_]') && checkAndCallAjaxIfValid('[name="order[orientation]"]', 'form[id*=add_]') && checkAndCallAjaxIfValidCustom('[name="order[number_of_copy]"]', 'form[id*=add_]') && checkAndCallAjaxIfValid('select[name="order[paper]"]', 'form[id*=add_]') && checkAndCallAjaxIfValid('select[name="order[print_color]"]', 'form[id*=add_]') && checkAndCallAjaxIfValid('select[name="order[lamination]"]', 'form[id*=add_]') && checkAndCallAjaxIfValid('select[name="order[cutting_coil]"]', 'form[id*=add_]') && checkAndCallAjaxIfValid('select[name="order[file_check]"]', 'form[id*=add_]') && checkAndCallAjaxIfValid('[id*="order_calculator_data_"]', 'form[id*=add_]')) {
			QuotePreload();
		}
	} else {
		$("#total-no-iva").html("0.00 €");
		$("#price-for-element").html("0.00 €");
		$("#file_check").html("0.00 €");
		$("#iva").html("0.00 €");
		$("#preventive-total").html("0.00 €");
	}
}

function QuotePreload() {
  var data = {
		"order[custom][height]": $('input[name="order[custom][height]"]').val(),
		"order[custom][width]": $('input[name="order[custom][width]"]').val(),
		"order[paper]": $('[name="order[paper]"]').val(),
		"order[orientation]": $('[name="order[orientation]"]:checked').val(),
		"order[print_color]": $('[name="order[print_color]"]').val(),
		"order_calculator[data]": $('[name="order_calculator[data]"]:checked').val(),
		"order[lamination]": $('[name="order[lamination]"]').val(),
		"order[cutting_coil]": $('[name="order[cutting_coil]"]').val(),
		"pvc_product_id": $('#pvc_product_id').val(),
	}
	if($('select[name="order[number_of_copy]"]').length > 0){
		data["order[number_of_copy]"] = $('select[name="order[number_of_copy]"] option:selected').val();
	} else {
		data["order[number_of_copy]"] = $('input[name="order[number_of_copy]"]').val();
	}
	if ($('select[name="order[file_check]"]').length > 0) {
		data["order[file_check]"] = $('select[name="order[file_check]"] option:selected').val();
	}
	$('select').each(function(s) {
		if($(this).find("option:not(:disabled)").length == 0) {
			$(this).attr("disabled", "disabled");
		} else {
			$(this).removeAttr("disabled");
		}
	});

	$('.loader-box').show();
	$.ajax({
		type: "POST",
		url: preventiveUrl,
		data: data
	});
}

function updatePapers(papers, current) {
	$.each(papers, function(k, pv) {
		if (pv.id == current) {
			$('select[name="order[paper]"]').parent().data("powertip", pv.description);
		}
	});
}