$(function () {
	var availablePaper = getAvailablePaper();
	var availableGr = retrieve_paper_weight();

	populateSelect(availablePaper, 'paper', false);

	var availableWeight = getAvailableWeightFromPaper(availablePaper, $('#paper option:selected').first().text());
	populateSelectWithDisabled(availableWeight, 'weight', true, availableGr);

	$('#paper').change(function () {
		var availableWeight = getAvailableWeightFromPaper(availablePaper, $('#paper option:selected').first().text());
		populateSelectWithDisabled(availableWeight, 'weight', true, availableGr);
	});

	var number_of_copy_id = parseInt($('#order_number_of_copy').val());
	$('input, select').not("#order_number_of_copy").change(function() {
		CheckQuote(parseInt($('#order_number_of_copy').val()));
	})
	$('#order_number_of_copy').change(function () {
		var number_of_copy_id = $(this).val();
		CheckQuote(parseInt(number_of_copy_id));
	});

});

function CheckQuote(noc) {
	if(noc != undefined && noc != "" && noc > 0) {
		if(checkAndCallAjaxIfValidCustom('[name="order[number_of_copy]"]', 'form[id*=add_]') &&  checkAndCallAjaxIfValid('[name="order[format]"]', 'form[id*=add_]') && checkAndCallAjaxIfValid('[name="order[paper]"]', 'form[id*=add_]') && checkAndCallAjaxIfValid('[name="order[weight]"]', 'form[id*=add_]') && checkAndCallAjaxIfValid('[name="order[plasticization]"]', 'form[id*=add_]') && checkAndCallAjaxIfValid('[name="order[instructions]"]', 'form[id*=add_]') && checkAndCallAjaxIfValid('[name="order[print_color]"]', 'form[id*=add_]') && checkAndCallAjaxIfValid('[name="order[file_check]"]', 'form[id*=add_]') && checkAndCallAjaxIfValid('[id*="order_calculator_data_"]', 'form[id*=add_]')) {
			QuotePreload();
		}
	} else {
		$("#total-no-iva").html("0.00 €");
		$("#price-for-element").html("0.00 €");
		$("#file_check").html("0.00 €");
		$("#iva").html("0.00 €");
		$("#preventive-total").html("0.00 €");
	}
}

function QuotePreload() {
  var data = {
		"order[orientation]": $('[name="order[orientation]"]:checked').val(),
		"order[instructions]": $('[name="order[instructions]"]').val(),
		"order[paper]": $('[name="order[paper]"]').val(),
		"order[weight]": $('[name="order[weight]"]').val(),
		"order[plasticization]": $('[name="order[plasticization]"]').val(),
		"order[print_color]": $('[name="order[print_color]"]').val(),
		"order_calculator[data]": $('[name="order_calculator[data]"]:checked').val(),
		"business_card_product_id": $('#business_card_product_id').val()
	}
	if($('select[name="order[number_of_copy]"]').length > 0){
		data["order[number_of_copy]"] = $('select[name="order[number_of_copy]"] option:selected').val();
	} else {
		data["order[number_of_copy]"] = $('input[name="order[number_of_copy]"]').val();
	}
	if($('select[name="order[format]"]').length > 0){
		data["order[format]"] = $('[name="order[format]"] option:selected').val();
	} else {
		data["order[format]"] = $('[name="order[format]"]:checked').val();
	}
	if ($('select[name="order[file_check]"]').length > 0) {
		data["order[file_check]"] = $('select[name="order[file_check]"] option:selected').val();
	}
	$('select').each(function(s) {
		if($(this).find("option:not(:disabled)").length == 0) {
			$(this).attr("disabled", "disabled");
		} else {
			$(this).removeAttr("disabled");
		}
	});

	$('.loader-box').show();
	$.ajax({
		type: "POST",
		url: preventiveUrl,
		data: data
	});
}

function setPlastification(number_of_copy_id) {
    var one_sided_glossy;
    var single_sided_matte;
    var front_back_glossy;
    var front_back_matte;
    $.each(variants.val, function (i, v) {
        if (v.variant.number_of_copy_id == number_of_copy_id) {
            one_sided_glossy = v.variant.plastifications.one_sided_glossy;
            single_sided_matte = v.variant.plastifications.single_sided_matte;
            front_back_glossy = v.variant.plastifications.front_back_glossy;
            front_back_matte = v.variant.plastifications.front_back_matte;
        }
    });

    $('#plasticization').html("");
    $('#plasticization').append($("<option></option>").
        attr('value', "none").
        text(Spree.translations.empty_plasticization));
    appendOption('plasticization', Spree.translations.one_sided_glossy_plasticization, 'one_sided_glossy', one_sided_glossy);
    appendOption('plasticization', Spree.translations.single_sided_matte_plasticization, 'single_sided_matte', single_sided_matte);
    appendOption('plasticization', Spree.translations.front_back_glossy_plasticization, 'front_back_glossy', front_back_glossy);
    appendOption('plasticization', Spree.translations.front_back_matte_plasticization, 'front_back_matte', front_back_matte);

}

function appendOption(selectId, optTxet, optValue, disabled) {
    if (disabled) {
        $('#' + selectId).append($("<option></option>").
            attr('value', optValue).
            text(optTxet));
    } else {
        $('#' + selectId).append($("<option></option>").
            attr('value', optValue).
            attr('disabled', 'disabled').
            text(optTxet));
    }
}

function setColorOrBW(number_of_copy_id) {
    var color = false;
    var only_bw = false;
    var only_color = false;
    if (number_of_copy_id.indexOf('-') < 0) {
        $.each(variants.val, function (i, v) {
            if (v.variant.number_of_copy_id == number_of_copy_id) {
                color = v.variant.color;
                only_bw = v.variant.print_black_and_white;
                only_color = v.variant.print_color;
            }
        });
    } else {
        color = true;
    }

    $('#print_color').html('');
    if (color) {
        $('#print_color').append($("<option></option>").
            attr('value', "false").
            text(Spree.translations.print_color_bn));
        $('#print_color').append($("<option></option>").
            attr('value', "true").
            text(Spree.translations.print_color_c));

    } else {
        if (only_bw) {
            $('#print_color').append($("<option></option>").
                attr('value', "false").
                text(Spree.translations.print_color_bn));
            $('#print_color').append($("<option></option>").
                attr('value', "true").
                attr('disabled', 'disabled').
                text(Spree.translations.print_color_c));
        }
        if (only_color) {
            $('#print_color').append($("<option></option>").
                attr('value', "false").
                attr('disabled', 'disabled').
                text(Spree.translations.print_color_bn));
            $('#print_color').append($("<option></option>").
                attr('value', "true").
                text(Spree.translations.print_color_c));
        }
    }
}


function setFrontAndBack(number_of_copy_id) {
    var result = false;
    if (number_of_copy_id.indexOf('-') >= 0) {
        number_of_copy_id = number_of_copy_id.substring(0, number_of_copy_id.indexOf('-'));
    }
    $.each(variants.val, function (i, v) {
        if (v.variant.number_of_copy_id == number_of_copy_id) {
            result = v.variant.front_and_back;
        }
    });

    $('#instructions').html('');
    if (result) {
        $('#instructions').append($("<option></option>").
            attr('value', "true").
            text(Spree.translations.print_type_front_back));
    }
    $('#instructions').append($("<option></option>").
        attr('value', "false").
        text(Spree.translations.print_type_front));
}


function findVariantFromOptionValueAndOptionType(option_value, option_type) {
    var r = null;
    jQuery.each(variants, function (index, itemData) {
        $.each(itemData, function (k, v) {
            $.each(v.variant.option_values, function (k1, v1) {
                if (v1.name == option_value && v1.option_type_name == option_type) {
                    r = v;
                }
            })
        })
    });
    return r;
}

function getAvailablePaper() {
    var h = new Array();
    $.each(paper_products_selected, function (i, v) {
        var v1 = v.weight;
        if (!find_paper_in_array(h, v1.paper.id)) {
            var gr = getAvailablePaperType(v1.paper.id);
            var t = {'name':v1.paper.name, 'presentation':v1.paper.presentation, 'id':v1.paper.id, 'gr':gr };
            h.push(t);
        }
    });
    return h;
}

function find_paper_in_array(array, paper_id) {
    var result = false;
    $.each(array, function (i, v) {

        if (v.id == paper_id) {
            result = true;
        }
    });
    return result;
}


function getAvailableWeightFromPaper(availablePaper, paperSelect) {
    var tmp = [];
    $.each(availablePaper, function (i, v) {
        if (v.presentation == paperSelect) {
            $.each(v.gr, function (i1, v1) {
                tmp.push(v1);
            });
        }
    });
    return tmp;
}

function getAvailablePaperFromWeight(availablePaper, weightSelected) {
    var tmp = [];
    $.each(availablePaper, function (i, v) {
        $.each(v.gr, function (i1, v1) {
            if (v1.name == weightSelected && $.inArray(v, tmp) < 0) {
                tmp.push(v);
            }

        });
    });
    return tmp;
}

function getAvailablePaperType(paper_id) {
    var h = new Array();
    $.each(paper_products_selected, function (i, v) {
        v1 = v.weight;
        if (v1.paper.id == paper_id) {
            h.push({ name:v1.quantity, presentation:v1.value, id:v1.id});
        }
    });
    return h;
}

/**
 * Popola la select identifcata dal select_id (senza #) con i valori dell'array passato.
 * Come campo value viene messo l'id, mentre come campo text viene messo presentation.
 * @param array array contente hash con id e presentation
 * @param select_id id della select
 */
function populateSelect(array, select_id, inverse) {
    var tmp = [];
    $("#" + select_id).html('');
    $.each(array, function (i, v) {
        if ($.inArray(v.name, tmp) < 0) {
            tmp.push(v.name);

            if (inverse) {
                $("#" + select_id).append($("<option></option>").
                    attr('value', v.id).
                    text(v.name));
            } else {
                $("#" + select_id).append($("<option></option>").
                    attr('value', v.id).
                    text(v.presentation));
            }
        }
    });
}

function populateSelectWithDisabled(array, select_id, inverse, availableGr) {
    var tmp = [];
    $("#" + select_id).html('');
    $.each(array, function (i, v) {
        if ($.inArray(v.name, tmp) < 0) {
            tmp.push(v.name);
        }
    });

    $.each(availableGr, function (i, v) {
        if ($.inArray(v, tmp) < 0) {
            if (inverse) {
                $("#" + select_id).append($("<option disabled='disabled'></option>").
                    attr('value', v).
                    text(v));
            } else {
                $("#" + select_id).append($("<option disabled='disabled'></option>").
                    attr('value', v).
                    text(v));
            }
        } else {
            if (inverse) {
                $("#" + select_id).append($("<option></option>").
                    attr('value', v).
                    text(v));
            } else {
                $("#" + select_id).append($("<option></option>").
                    attr('value', v).
                    text(v));
            }
        }
		$("#" + select_id).find("option:not(:disabled)").first().attr("selected", true);
    });
}


