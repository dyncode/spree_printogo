var remove_bill_address = 'Rimuovi indirizzo di fatturazione';
var remove_ship_address = 'Rimuovi indirizzo di consegna';
var add_bill_address = 'Aggiungi indirizzo di fatturazione';
var add_ship_address = 'Aggiungi indirizzo di consegna';

$(function() {
	$('#new-customer #shipping .inner').css('display', 'initial');
	$('.edit-user #shipping .inner').css('display', 'initial');
	$('#login-form #shipping .inner').css('display', 'initial');

	// TODO make multilanguage
	$('#add_ship_address').click(function () {
		if ($('#shipping').css('display') == 'none') {
			$('#shipping').show();
			$('#add_ship_address').html(remove_ship_address);
			$('#delete_ship_address').val('true');
		} else {
			$('#shipping').hide();
			$('#add_ship_address').html(add_ship_address);
			$('#delete_ship_address').val('false');
			$("#user_ship_address_country_id option[value='98']").attr('selected', true);
			$("#user_ship_address_country_id").change();
		}
		return false;
	});

	$('#add_bill_address').click(function () {
		if ($('#billing').css('display') == 'none') {
			$('#billing').show();
			$('#add_bill_address').html(remove_bill_address);
			$('#delete_bill_address').val('true');
		} else {
			$('#billing').hide();
			$('#add_bill_address').html(add_bill_address);
			$('#delete_bill_address').val('false');
			$("#user_bill_address_country_id option[value='98']").attr('selected', true);
			$("#user_bill_address_country_id").change();
		}
		return false;
	});
	
	// Visible or hide address if is present
	if ($('.edit_user').is('*') || $('.edit_spree_user').is('*')) {
		if (!is_present('bill')) {
			$('#billing').toggle();
		} else {
			$('#add_bill_address').html(remove_bill_address);
		}
		if (!is_present('ship')) {
			$('#shipping').toggle();
		} else {
			$('#add_ship_address').html(remove_ship_address);
		}
	}
});

function is_present(prefix) {
    var count = 0;
    if ($('#user_' + prefix + '_address_attributes_firstname').val() == '') {
        count++;
    }
    if ($('#user_' + prefix + '_address_attributes_lastname').val() == '') {
        count++;
    }
    if ($('#user_' + prefix + '_address_attributes_address1').val() == '') {
        count++;
    }
    if ($('#user_' + prefix + '_address_attributes_city').val() == '') {
        count++;
    }
    if ($('#user_' + prefix + '_address_attributes_zipcode').val() == '') {
        count++;
    }
    if ($('#user_' + prefix + '_address_attributes_phone').val() == '') {
        count++;
    }

    if (count == 6) {
        return false;
    } else {
        return true;
    }
}
