$(function () {
    if ($('#business_card_product_paper_weight_').length > 0) {
        var tag_name = '#business_card_product_paper_weight_';
        var tag_prefix = 'business_card_product';
        init_pg(tag_name, tag_prefix, paper_weight_hash, paper_products_selected, paper_products);
    }

    if ($('#variant_paper_weight_').length > 0) {
        var tag_name = '#variant_paper_weight_';
        var tag_prefix = 'variant';
        init_pg(tag_name, tag_prefix, paper_weight_hash, paper_products_selected, paper_products);
    }
});

function init_pg(tag_name, tag_prefix) {
	init_pg(tag_name, tag_prefix, paper_weight_hash, paper_products_selected, paper_products);
}

function init_pg(tag_name, tag_prefix, weight_hash, products_selected, pproducts) {
    $(tag_name).each(function () {
        $(tag_name).attr('multiple', 'multiple');
        for (var i = 0; i < weight_hash.length; i++) {
            appendOption($(this).attr('id'), weight_hash[i].weight.quantity, weight_hash[i].weight.quantity, true, false);
        }
    });

    $(tag_name).change(function () {
        var selected_weight = $(this).find('option:selected');
        var results = [];
        var old_val = [];
        $(tag_name + ' option').each(function (val, k) {
            old_val.push(k);
        });

        for (var j = 0; j < selected_weight.length; j++) {
            var papers = find_wight(selected_weight[j].value, weight_hash);
            for (var i = 0; i < papers.length; i++) {
                if (!contains(results, papers[i])) {
                    results.push(papers[i]);
                }
            }
        }
        add_selects($(this).val(), results, tag_prefix, products_selected, pproducts);
    });

    //$('.chzn-choices .search-choice span').first().text()
    updateSelected(tag_name, products_selected);
    $(tag_name).change();

    $(tag_name).chosen({no_results_text:"Nessun risultato trovato"});
}


/**********************************************************************************************************************
 *                                               Funzioni ausiliarie
 **********************************************************************************************************************/
function updateSelected(tag_name) {
	updateSelected(tag_name, paper_products_selected);
}

function updateSelected(tag_name, products_selected) {
    // Mette a select tutti i dati già selezionati
    for (var i = 0; i < products_selected.length; i++) {
        $(tag_name + ' option[value=' + products_selected[i].weight.quantity + ']').attr('selected', 'selected');
    }
}


function contains(a, value) {
    for (var i = 0; i < a.length; i++) {
        if (a[i].name == value.name && a[i].id == value.id) {
            return true;
        }
    }
    return false;
}

function add_selects(val, papers, tag_prefix) {
	add_selects(val, papers, tag_prefix, paper_products_selected, paper_products);
}

function add_selects(val, papers, tag_prefix, products_selected, pproducts) {
    var select = $("<select></select>").
        attr('id', tag_prefix + '_paper_w').
        attr('name', tag_prefix + '[paper][]').
        attr('size', '4').
        attr('multiple', 'multiple').data('placeholder', "asdasd");

    for (var i = 0; i < papers.length; i++) {
        if (find_default_id(val, papers[i].id, pproducts) != undefined) {
            select.append(
                $("<option></option>").
                    attr('value', papers[i].id).
                    attr('selected', 'selected').
                    text(papers[i].name)
            );
        } else {
            select.append(
                $("<option></option>").
                    attr('value', papers[i].id).
                    text(papers[i].name)
            );
        }
    }
    $("#" + tag_prefix + '_paper').html(select);
    for (var i = 0; i < products_selected.length; i++) {
        $("#" + tag_prefix + "_paper_w option[value=" + products_selected[i].weight.paper.id + "]").attr('selected', 'selected');
    }
    $("#" + tag_prefix + '_paper_w').chosen({no_results_text:"Nessun risultato trovato"});
}

function find_default_id(val, paper_id) {
	find_default_id(val, paper_id, paper_products);
}

function find_default_id(val, paper_id, pproducts) {
    for (var i = 0; i < pproducts.length; i++) {
        if (pproducts[i].weight == val && pproducts[i].paper_id == paper_id) {
            return pproducts[i].paper_id;
        }
    }
    return undefined;
}

function find_wight(val) {
	find_wight(val, paper_weight_hash);
}

function find_wight(val, weight_hash) {
    for (var i = 0; i < weight_hash.length; i++) {
        if (weight_hash[i].weight.quantity == val) {
            return weight_hash[i].weight.papers;
        }
    }
}

function appendOption(selectId, optTxet, optValue, enabled, selected) {
    $('#' + selectId).append($("<option></option>").
        attr('value', optValue).
        attr('selected', selected).
        attr('disabled', !enabled).
        text(optTxet));
}
