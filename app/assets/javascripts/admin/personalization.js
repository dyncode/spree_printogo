$(function() {
	$('.chosen').chosen();
	
	$("a[data-url-tab]").on("click", function(e) {
		var elem = $(this).attr("href");
		$.ajax({
			url: $(this).attr("data-url-tab"),
			type: 'GET',
			dataType: 'script',
			data: ({authenticity_token: jQuery('meta[name="csrf-token"]').attr('content')}),
			beforeSend: function() { $(elem).html('<div class="loader center"><img src="/assets/admin/ajax-loader.gif"></div>'); },
			success: function() { init_link(); }
		});
		e.preventDefault();
	});
	
	$("a[data-remote]").on("click", function(e) {
		e.preventDefault()
		$("a[rel='tab']").parents('div.tab-pane:first').html('<div class="loader center"><img src="/assets/admin/ajax-loader.gif"></div>');
	});
	
	$("form[data-remote]").on("submit", function(e) {
		e.preventDefault()
		$(this).parent('div.tab-pane').html('<div class="loader center"><img src="/assets/admin/ajax-loader.gif"></div>');
	});
	
	// FUNCTION FOR PRINTER
	$('#printer_price_front_c').change(function () {
        $('#printer_price_front_and_back_c').val(($(this).val() * 2));
    });

    $('#printer_price_front_bw').change(function () {
        $('#printer_price_front_and_back_bw').val(($(this).val() * 2));
    });
	
	// FUNCTION FOR HOLLOW PUNCH AND CUT
	if($('#hollow_punch').length > 0) { $('#hollow_punch').validate(); }
	
	if($('#cut').length > 0) { $('#cut').validate(); }
	
	// FUNCTION FOR PUNCHING
	if ($("#bindery_copy_cost_punching").is(':checked')) {
		$("#not_copy_cost").hide();
		$("#copy_cost").show();
	} else {
		$("#not_copy_cost").show();
		$("#copy_cost").hide();
	}
	
	$("#bindery_copy_cost_punching").change(function() {
		var check = $(this).is(':checked');
		if (!check) {
			$("#not_copy_cost").show();
			$("#copy_cost").hide();
		} else {
			for(i=0; i < ($(".punching_not_copy").find("a").length - 1); i++) {
				$($(".punching_not_copy").find("a")[i]).attr("onclick", "remove_fields(this); return false;").click();
			}
			$('#copy_cost input[name$="[copy_cost]"]').val(0);
			$("#not_copy_cost").hide();
			$("#copy_cost").show();
		}
	});
	
	// FUNCTION FOR SEAM
	if ($("#bindery_copy_cost_seam").is(':checked')) {
		$("#not_copy_cost_seam").hide();
		$('#copy_cost_seam #bindery_seam_attributes_copy_cost').attr("name", "bindery[seam_attributes][copy_cost]");
		$("#copy_cost_seam").show();
	} else {
		$("#not_copy_cost_seam").show();
		$('#copy_cost_seam #bindery_seam_attributes_copy_cost').attr("name", "");
		$("#copy_cost_seam").hide();
	}
	
	$("#bindery_copy_cost_seam").change(function() {
		var check = $(this).is(':checked');
		if (!check) {
			$("#not_copy_cost_seam").show();
			$('#copy_cost_seam #bindery_seam_attributes_copy_cost').attr("name", "");
			$("#copy_cost_seam").hide();
		} else {
			$('#copy_cost_seam #bindery_seam_attributes_copy_cost').attr("name", "bindery[seam_attributes][copy_cost]").val(0);
			$("#not_copy_cost_seam").hide();
			$("#copy_cost_seam").show();
		}
	});
	
	// FUNCTION FOR MILLING
	if ($("#bindery_copy_cost_milling").is(':checked')) {
		$("#not_copy_cost_milling").hide();
		$('#copy_cost_milling #bindery_milling_attributes_copy_cost').attr("name", "bindery[milling_attributes][copy_cost]");
		$("#copy_cost_milling").show();
	} else {
		$("#not_copy_cost_milling").show();
		$('#copy_cost_milling #bindery_milling_attributes_copy_cost').attr("name", "");
		$("#copy_cost_milling").hide();
	}
	
	$("#bindery_copy_cost_milling").change(function() {
		var check = $(this).is(':checked');
		if (!check) {
			$("#not_copy_cost_milling").show();
			$('#copy_cost_milling #bindery_milling_attributes_copy_cost').attr("name", "");
			$("#copy_cost_milling").hide();
		} else {
			$('#copy_cost_milling #bindery_milling_attributes_copy_cost').attr("name", "bindery[milling_attributes][copy_cost]").val(0);
			$("#not_copy_cost_milling").hide();
			$("#copy_cost_milling").show();
		}
	});
	
	// FUNCTION FOR PRINTER
	if ($("#printer_printer_type_digital").is(":checked")) {
		$("#plant_color_bw").hide();
		$("#plant_color_c").hide();
		$("#color_type").hide();
	} else {
		$("#plant_color_bw").show();
		$("#plant_color_c").show();
		$("#color_type").show();
	}
	$("input[name='printer[printer_type]']").change(function() {
		if ($(this).val() == 'digital') {
			$("#plant_color_bw").hide();
			$("#plant_color_c").hide();
			$("#color_type").hide();
		} else {
			$("#plant_color_bw").show();
			$("#plant_color_c").show();
			$("#color_type").show();
		}
	});
	// FUNCTION FOR PRINTER
});

function init_link() {
	$("a[data-remote]").on("click", function(e) {
		e.preventDefault();
		$("a[rel='tab']").parents('div.tab-pane:first').html('<div class="loader center"><img src="/assets/admin/ajax-loader.gif"></div>');
	});
	
	$("form[data-remote]").on("submit", function(e) {
		e.preventDefault();
		$(this).parent('div.tab-pane').html('<div class="loader center"><img src="/assets/admin/ajax-loader.gif"></div>');
	});
}

function add_fields(link, association, content, control) {
    var new_id = new Date().getTime();
    var regexp = new RegExp("new_" + association, "g")
    $(link).parent().before(content.replace(regexp, new_id));
	if (control || false) {
		setTimeout(function() { $('*[rel=popover-right]').popover({placement:'right', delay:{ show:0, hide:100 }, trigger: 'hover'}); }, 500);
	}
}

function add_fields_with_limit(link, association, limit, content, control) {
	var new_id = new Date().getTime();
    var regexp = new RegExp("new_" + association, "g")
    $(link).parent().before(content.replace(regexp, new_id));
	if (control || false) {
		setTimeout(function() { $('*[rel=popover-right]').popover({placement:'right', delay:{ show:0, hide:100 }, trigger: 'hover'}); }, 500);
	}
	count_for_hide_and_show(limit);
}

function remove_fields(link) {
    $(link).prev("input[type=hidden]").val("1");
	$(link).closest(".fields").find(".required").removeClass("required");
    $(link).closest(".fields").hide();
}

function confirm_remove_fields(link) {
	var r = confirm("Sicuro di voler procedere?");
	if (r == true) {
		remove_fields(link);
	}
}

function confirm_remove_fields_with_limit(link, limit) {
	var r = confirm("Sicuro di voler procedere?");
	if (r == true) {
		remove_fields(link);
		count_for_hide_and_show(limit);
	}
}

function view_all_accordion() {
	$(".accordion-body").removeAttr("style");
	$(".accordion-body").addClass("in");
}

function SetPromotion(elem) {
	if (elem.val() != "") {
		$('#slide_title').val(elem.text());
		$('#slide_url').val("");
		$('#slide_content').val("");
		$('#cslide_url').hide();
		$('#cslide_content').hide();
	} else {
		$('#slide_title').val($('#slide_title').attr("value"));
		$('#cslide_url').show();
		$('#slide_content').val($('#slide_content').attr("data-value"));
		$('#cslide_content').show();
	}
}

function SetPromotionBanner(elem) {
	if (elem.val() != "") {
		$('#banner_url').val("");
		$('#cbanner_url').hide();
	} else {
		$('#cbanner_url').show();
	}
}

function count_for_hide_and_show(limit) {
	if($('div[class^="fields"]').filter(function() { return $(this).css("display") != "none"; }).length >= limit){
		$("a:contains('Aggiungi un nuova lavorazione')").hide();
	} else {
		$("a:contains('Aggiungi un nuova lavorazione')").show();
	}
}