Deface::Override.new(:virtual_path => %q{spree/admin/orders/index},
                     :insert_after => "[data-hook='admin_orders_index_header_actions']",
                     :name => "invoice_print_header",
                     :text => "<th><%= t(:invoice, :scope => :spree) %></th>",
                     :disabled => true)

Deface::Override.new(:virtual_path => %q{spree/admin/orders/index},
                     :insert_bottom => "[data-hook='admin_orders_index_row_actions']",
                     :name => "invoice_print_link",
                     :text => %q{<%= link_to_personalized_bootstrap_url(pdf_invoice_path(:order_id => order.id, :format => :pdf), :title => t(:print_invoice, :scope => :spree), :icon => "icon-print", :class => "btn") if order.bill_address %>})

Deface::Override.new(:virtual_path => %q{spree/admin/orders/show},
                     :insert_bottom => "[data-hook='admin_order_show_buttons']",
                     :name => "invoice_print_show_link",
                     :text => %q{<%= link_to("#{t(:print_invoice, :scope => :spree)} <i class='icon-print'></i>".html_safe, pdf_invoice_path(:order_id => @order.id, :format => :pdf), :class => "btn") if @order.bill_address %>})
