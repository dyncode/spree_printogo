# REMOVE ADMIN MENU

Deface::Override.new(:virtual_path => "spree/layouts/admin",
                     :name => "shipping_date_adjustments_tab",
                     :insert_bottom => "[data-hook='admin_tabs']",
                     :text => "<%= tab(:shipping_date_adjustments) %>",
                     :disabled => true)
Deface::Override.new(:virtual_path => "spree/layouts/admin",
                     :name => "presentation_packs_tab",
                     :insert_bottom => "[data-hook='admin_tabs']",
                     :text => "<%= tab(:presentation_packs) %>",
                     :disabled => true)
Deface::Override.new(:virtual_path => "spree/layouts/admin",
                     :name => "posts_tab",
                     :insert_bottom => "[data-hook='admin_tabs'], #admin_tabs[data-hook]",
                     :text => "<%= tab(:posts, :url => spree.admin_posts_path) %>",
                     :disabled => true)
Deface::Override.new(:virtual_path => "spree/layouts/admin",
                     :name => "slideshow_type_admin_tab",
                     :insert_bottom => "[data-hook='admin_tabs']",
                     :text => "<%= tab(:slideshow_types) %>",
                     :disabled => true)
Deface::Override.new(:virtual_path => "spree/layouts/admin",
                     :name => "banner_admin_tab",
                     :insert_bottom => "[data-hook='admin_tabs'], #admin_tabs[data-hook]",
                     :text => "<%= tab(:banners, :url => spree.admin_banners_path) %>",
                     :disabled => true)
Deface::Override.new(:virtual_path => "spree/layouts/admin",
                     :name => "pages_tab",
                     :insert_bottom => "[data-hook='admin_tabs'], #admin_tabs[data-hook]",
                     :text => "<%= tab(t('simple_cms.pages'), :url => spree.admin_static_pages_path) %>",
                     :disabled => true)
Deface::Override.new(:virtual_path => "spree/layouts/admin",
                    :name => "menu_tab",
                    :insert_bottom => "[data-hook='admin_tabs']",
                    :text => "<%= tab((:menus) %>",
                    :disabled => true)

# CREATE NEW ADMIN MENU

Deface::Override.new(:virtual_path => "spree/layouts/admin",
                     :name => "cms_types_tab",
                     :insert_bottom => "[data-hook='admin_tabs']",
                     :text => "<%= tab(:static_pages, :slideshow_types, :banners, :menus, :taxonomies, :label => 'Layout', :css_class => 'dropdown', :dropdown => ['static_pages', 'slideshow_types', 'banners', 'menus', 'taxonomies', 'homepage_elements']) %>")
Deface::Override.new(:virtual_path => "spree/layouts/admin",
                     :name => "printers_tab",
                     :insert_bottom => "[data-hook='admin_tabs']",
                     :text => "<%= tab(:printers,:plotters, :label => 'Macchine da stampa', :css_class => 'dropdown', :dropdown => ['printers', 'plotters']) %>")
Deface::Override.new(:virtual_path => "spree/layouts/admin",
                     :name => "edit_materials_tab",
                     :insert_bottom => "[data-hook='admin_tabs']",
                     :text => "<%= tab(:printer_format_defaults, :paper_types, :coil_values, :rigid_supports, :eyelets, :buttonholes, :frames, :hangers, :label => 'Magazzino', :css_class => 'dropdown', :dropdown => ['printer_format_defaults', 'paper_types', 'coil_values', 'rigid_supports', 'eyelets', 'buttonholes', 'frames', 'hangers']) %>")
Deface::Override.new(:virtual_path => "spree/layouts/admin",
                     :name => "cut_types_tab",
                     :insert_bottom => "[data-hook='admin_tabs']",
                     :text => "<%= tab(:cut, :hollow_punch, :plasticizations, :spiralings, :binderies, :foldings, :cutting_coil, :lamination, :label => 'Lavorazioni', :css_class => 'dropdown', :dropdown => ['cut', 'hollow_punch', 'plasticizations', 'spiralings', 'intercalations', 'punctures', 'creasings', 'binderies', 'foldings', 'cutting_coil', 'lamination', 'processing_plotter']) %>")
Deface::Override.new(:virtual_path => "spree/layouts/admin",
                     :name => "logout_tab",
                     :insert_bottom => "[data-hook='admin_tabs']",
                     :text => "<%= link_to('Sign Out', '/user/logout', :class => 'btn', :method => :get, :style => 'margin-left: 20px;') %>")

# RIMOZIONI
Deface::Override.new(:virtual_path => "spree/layouts/admin",
                     :name => "promo_admin_tabs",
                     :disabled => true)

