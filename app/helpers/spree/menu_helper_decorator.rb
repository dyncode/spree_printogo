# encoding: UTF-8
Spree::MenuHelper.module_eval do
  def link(menu)
    if menu.url.blank? && !menu.linkable.blank?
      if menu.linkable.class == Spree::BusinessCardProduct
        return business_card_path(menu.linkable)
      elsif menu.linkable.class == Spree::FlayerProduct
        return flayer_product_path(menu.linkable)
      elsif menu.linkable.class == Spree::LetterheadProduct
        return letterhead_path(menu.linkable)
      elsif menu.linkable.class == Spree::PostcardProduct
        return postcard_path(menu.linkable)
      elsif menu.linkable.class == Spree::PosterProduct
        return poster_path(menu.linkable)
      elsif menu.linkable.class == Spree::PlaybillProduct
        return playbill_path(menu.linkable)
      elsif menu.linkable.class == Spree::PosterFlyerProduct
        return poster_path(menu.linkable)
      elsif menu.linkable.class == Spree::PosterHighQualityProduct
        return poster_path(menu.linkable)
      elsif menu.linkable.class == Spree::PaperbackProduct
        return paperback_path(menu.linkable)
      elsif menu.linkable.class == Spree::StapleProduct
        return staple_path(menu.linkable)
      elsif menu.linkable.class == Spree::FoldingProduct
        return folding_path(menu.linkable)
      elsif menu.linkable.class == Spree::BannerProduct
        return banner_path(menu.linkable)
      elsif menu.linkable.class == Spree::PvcProduct
        return pvc_sticker_path(menu.linkable)
      elsif menu.linkable.class == Spree::SpiralProduct
        return spiral_path(menu.linkable)
      elsif menu.linkable.class == Spree::CanvasProduct
        return canvase_path(menu.linkable)
      elsif menu.linkable.class == Spree::RigidProduct
        return rigid_path(menu.linkable)
      elsif menu.linkable.class == Spree::StickerProduct
        return sticker_path(menu.linkable)
      elsif menu.linkable.class == Spree::SimpleProduct
        return simple_product_path(menu.linkable)
      elsif menu.linkable.class == Spree::AdvancedProduct
        return advanced_product_path(menu.linkable)
      end
      #return product_path(menu.linkable) if menu.linkable.class == Spree::Product
      return seo_url(menu.linkable) if menu.linkable.class == Spree::Taxon
      return page_path(menu.linkable) if menu.linkable.class == Spree::StaticPage
    else
      return menu.url
    end
  end
end