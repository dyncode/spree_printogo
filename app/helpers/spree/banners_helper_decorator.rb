Spree::BannersHelper.module_eval do  
  def insert_banner(params={})
    params[:max] ||= 1
    params[:category] ||= "home"
    params[:class] ||= "banner"
    params[:list] ||= false
    params[:style] ||= "custom"
    @@banner = Spree::Banner.enable(params[:category]).limit(params[:max])
    if @@banner.blank?
      return ''
    end
    @@banner = @@banner.sort_by { |ban| ban.position }
    
    @@banner.map do |ban|
      if !ban.promotion && ban.url.match(/youtu.be|youtube/)
        content_tag(:div, link_to(image_tag(ban.attachment.url(params[:style].to_sym)) + (content_tag(:div, content_tag(:div, ban.title.upcase, :class => "caption-title"), :class => "caption-content") if !ban.title.blank?), open_modal_url(ban), :class => "clearfix", :remote => true), :class => "#{params[:class]} clearfix") if ((ban.is_promotion? && ban.promotion.is_available?) || !ban.is_promotion?)
      else
        content_tag(:div, link_to(image_tag(ban.attachment.url(params[:style].to_sym)) + (content_tag(:div, content_tag(:div, ban.title.upcase, :class => "caption-title"), :class => "caption-content") if !ban.title.blank?), (ban.promotion ? link_promo(ban.promotion.product, ban.promotion.promo_code) : (ban.url.blank? ? "javascript: void(0)" : ban.url)), :class => "clearfix", :target => "_blank"), :class => "#{params[:class]} clearfix") if ((ban.is_promotion? && ban.promotion.is_available?) || !ban.is_promotion?)
      end
    end.join.html_safe
  end
  
  def link_promo(product, promo)
    case product.type
    when "Spree::BusinessCardProduct"
      "#{Spree::Config[:site_url]}/business_cards/#{product.permalink}/#{promo}"
    when "Spree::PosterProduct"
      "#{Spree::Config[:site_url]}/posters/#{product.permalink}/#{promo}"
    when "Spree::PosterFlyerProduct"
      "#{Spree::Config[:site_url]}/posters/#{product.permalink}/#{promo}"
    when "Spree::PosterHighQualityProduct"
      "#{Spree::Config[:site_url]}/posters/#{product.permalink}/#{promo}"
    when "Spree::FlayerProduct"
      "#{Spree::Config[:site_url]}/flayers/#{product.permalink}/#{promo}"
    when "Spree::LetterheadProduct"
      "#{Spree::Config[:site_url]}/letterheads/#{product.permalink}/#{promo}"
    when "Spree::PostcardProduct"
      "#{Spree::Config[:site_url]}/postcards/#{product.permalink}/#{promo}"
    when "Spree::PlaybillProduct"
      "#{Spree::Config[:site_url]}/playbills/#{product.permalink}/#{promo}"
    when "Spree::PaperbackProduct"
      "#{Spree::Config[:site_url]}/paperbacks/#{product.permalink}/#{promo}"
    when "Spree::StapleProduct"
      "#{Spree::Config[:site_url]}/staples/#{product.permalink}/#{promo}"
    when "Spree::FoldingProduct"
      "#{Spree::Config[:site_url]}/foldings/#{product.permalink}/#{promo}"
    when "Spree::BannerProduct"
      "#{Spree::Config[:site_url]}/banners/#{product.permalink}/#{promo}"
    when "Spree::PvcProduct"
      "#{Spree::Config[:site_url]}/pvc_stickers/#{product.permalink}/#{promo}"
    when "Spree::SpiralProduct"
      "#{Spree::Config[:site_url]}/spirals/#{product.permalink}/#{promo}"
    when "Spree::CanvasProduct"
      "#{Spree::Config[:site_url]}/canvases/#{product.permalink}/#{promo}"
    when "Spree::RigidsProduct"
      "#{Spree::Config[:site_url]}/rigids/#{product.permalink}/#{promo}"
    when "Spree::StickersProduct"
      "#{Spree::Config[:site_url]}/stickers/#{product.permalink}/#{promo}"
    else
      "javascript: void(0)"
    end
  end
end