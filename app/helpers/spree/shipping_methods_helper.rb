module Spree
  module ShippingMethodsHelper
    def select_default_shipping_method_by_weight(weight)
      result = nil
      ShippingMethod.where("shipping_category_id = ?", Spree::ShippingCategory.find_by_name("Spedizione tramite corriere").id).each do |shipping_method|
        result = shipping_method if (shipping_method.min_weight.to_f < (weight.to_f) && shipping_method.max_weight.to_f >= (weight.to_f))
      end
      result
    end
    
    def select_shipping_method_by_weight(weight, category)
      result = nil
      ShippingMethod.where("shipping_category_id = ?", category.id).each do |shipping_method|
        result = shipping_method if shipping_method.min_weight.to_f < (weight.to_f) && shipping_method.max_weight.to_f >= (weight.to_f)
      end
      result
    end
    
    def get_all_shipping(order)
      dates = []
      weights = []
      order.line_items.each do |item|
        if dates.blank?
          dates << item.shipping_date
          weights << item.weight.to_f
        elsif !dates.include?(item.shipping_date)
          dates << item.shipping_date
          weights << item.weight.to_f
        elsif dates.include?(item.shipping_date)
          weights[dates.index(item.shipping_date)] += item.weight.to_f
        end
      end
      weights.map do |w|
        select_shipping_method_by_weight(w, order.shipping_method.shipping_category)
      end
    end
  end
end