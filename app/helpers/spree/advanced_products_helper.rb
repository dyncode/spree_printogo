module Spree
  module AdvancedProductsHelper
    def attribute_variant_print(product)
      i = 0
      content = product.simple_attributes.map do |sa|
        content_tag(:div, :class => "clearfix field_content") do
          divs = []
          divs << content_tag(:label, sa.name.titleize, :for => "attributes_#{sa.name.downcase}")
          divs << select_tag("attributes[attribute_#{sa.id}]", nil, { :include_blank => false, 'data-position' => i })
          i = i + 1
          #options_for_select(sa.simple_values.collect{|sv| [sv.presentation, sv.id, {:disabled => true}]})
          divs.join().html_safe
        end
      end
      
      content.join().html_safe
    end
  end
end
