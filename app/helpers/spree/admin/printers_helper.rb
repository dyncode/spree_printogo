module Spree
  module Admin
    module PrintersHelper
      def data_print_color(printer)
        res = []
        res << content_tag(:legend, t(:price_for_color))
        
        res << content_tag(:p, "#{content_tag(:strong, t(:printer_hourly_cost))} #{printer.printer_hourly_cost_c} #{t(:currency)}/ora".html_safe, :class => "small")
        res << content_tag(:p, "#{content_tag(:strong, t(:start_time))} #{printer.start_time_c} minuti".html_safe, :class => "small")
        res << content_tag(:p, "#{content_tag(:strong, t(:average_hourly_print))} #{printer.average_hourly_print_c} fogli/ora".html_safe, :class => "small")
        res << content_tag(:p, "#{content_tag(:strong, t(:waste_paper))} #{printer.waste_paper_c} n. fogli".html_safe, :class => "small")
        res << content_tag(:p, "#{content_tag(:strong, t(:plant_color))} #{printer.plant_color_c} #{t(:currency)}".html_safe, :class => "small")
        res << content_tag(:p, "#{content_tag(:strong, t(:single_paper_color))} #{printer.price_front_c} #{t(:currency)}".html_safe, :class => "small")
        res << content_tag(:p, "#{content_tag(:strong, t(:front_back_color))} #{printer.price_front_and_back_c} #{t(:currency)}".html_safe, :class => "small")
        
        res.join().html_safe
      end
      
      def data_print_bw(printer)
        res = []
        res << content_tag(:legend, t(:price_for_bw))
        
        res << content_tag(:p, "#{content_tag(:strong, t(:printer_hourly_cost))} #{printer.printer_hourly_cost_bw} #{t(:currency)}/ora".html_safe, :class => "small")
        res << content_tag(:p, "#{content_tag(:strong, t(:start_time))} #{printer.start_time_bw} minuti".html_safe, :class => "small")
        res << content_tag(:p, "#{content_tag(:strong, t(:average_hourly_print))} #{printer.average_hourly_print_bw} fogli/ora".html_safe, :class => "small")
        res << content_tag(:p, "#{content_tag(:strong, t(:waste_paper))} #{printer.waste_paper_bw} n. fogli".html_safe, :class => "small")
        res << content_tag(:p, "#{content_tag(:strong, t(:plant_bw))} #{printer.plant_color_bw} #{t(:currency)}".html_safe, :class => "small")
        res << content_tag(:p, "#{content_tag(:strong, t(:single_paper_bw))} #{printer.price_front_bw} #{t(:currency)}".html_safe, :class => "small")
        res << content_tag(:p, "#{content_tag(:strong, t(:front_back_bw))} #{printer.price_front_and_back_bw} #{t(:currency)}".html_safe, :class => "small")
        
        res.join().html_safe
      end
    end
  end
end