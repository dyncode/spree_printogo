module Spree
  module Admin
    module ImagesHelper
      def retrive_path(product)
        if product.type == "Spree::BusinessCardProduct"
          admin_business_cards_edit_path
        elsif product.type == "Spree::PostersProduct"
          admin_posters_edit_path
        elsif product.type == "Spree::PosterFlyerProduct"
          admin_poster_flyers_edit_path
        elsif product.type == "Spree::PosterHighQualityProduct"
          admin_poster_high_qualities_edit_path
        elsif product.type == "Spree::FlayerProduct"
          admin_flayers_edit_path
        elsif product.type == "Spree::PlaybillProduct"
          admin_playbills_edit_path
        elsif product.type == "Spree::PostcardProduct"
          admin_postcards_edit_path
        elsif product.type == "Spree::LetterheadProduct"
          admin_letterheads_edit_path
        elsif product.type == "Spree::PaperbackProduct"
          admin_paperbacks_edit_path
        elsif product.type == "Spree::StapleProduct"
          admin_staples_edit_path
        elsif product.type == "Spree::BannerProduct"
          admin_banners_edit_path
        elsif product.type == "Spree::PvcProduct"
          admin_pvces_edit_path
        elsif product.type == "Spree::SpiralProduct"
          admin_spirals_edit_path
        elsif product.type == "Spree::CanvasProduct"
          admin_canvases_edit_path
        elsif product.type == "Spree::RigidProduct"
          admin_rigids_edit_path
        elsif product.type == "Spree::StickerProduct"
          admin_stickers_edit_path
        end
      end
    end
  end
end