# Finitura SPIRALATURA
module Spree
  class Spiraling < OptionType
    has_many :spiraling_quantities, :class_name => "Spree::SpiralingQuantity", :foreign_key => "option_type_id"
    has_many :spiraling_copy_costs, :class_name => "Spree::SpiralingCopyCost", :foreign_key => "option_type_id"
    
    after_initialize :add_name_and_presentation
    before_save :ensure_internal
    attr_accessible :hourly_cost, :average_hourly_work, :start_time, :external, :spiraling_copy_costs,
                    :spiraling_copy_costs_attributes, :min_limit_spiraling, :cost_spiraling
    validates :hourly_cost, :average_hourly_work, :start_time, :presence => true
        
    accepts_nested_attributes_for :spiraling_copy_costs, :allow_destroy => true
    
    def external
      self.is_hide
    end
    
    def external=value
      self.is_hide = value
    end
    
    def ensure_internal
      if !self.external
        self.spiraling_copy_costs.clear
      end
    end
    
    def min_limit_spiraling
      self.hourly_cost
    end
    
    def min_limit_spiraling=value
      self.hourly_cost=value
    end
    
    def cost_spiraling
      self.average_hourly_work
    end
    
    def cost_spiraling=value
      self.average_hourly_work = value
    end
    
    def add_name_and_presentation
      if self.new_record?
        self.name = "spiraling"
        self.presentation = "Spiralatura"
      end
    end
  end
end