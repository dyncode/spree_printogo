#!/bin/env ruby
# encoding: utf-8

Spree::AppConfiguration.class_eval do
  preference :paper_weight, :string, :default => "80,90,100,110,115,120,130,140,150,160,170,200,215,230,240,250,280,300,350"
  
  preference :file_check, :boolean, :default => false
  preference :file_check_cost, :string, :default => "0.00"
  preference :file_check_desc, :string, :default => "Richiedi la verifica con operatore dei tuoi file per essere sicuro che siano corretti."
  
  preference :homepage_new_version_enabled, :boolean, :default => false
  preference :homepage_elements_number, :string, :default => "12"
  
  preference :privacy_description, :text, :default => "Acconsenti al trattamento dei dati personali"
  
  preference :order_notes, :text, :default => ""
	preference :cookie_law, :boolean, :default => false

	preference :notify_bill_user_change, :boolean, :default => false
	preference :notify_email, :string, :default => "admin@printtogo.it"
end