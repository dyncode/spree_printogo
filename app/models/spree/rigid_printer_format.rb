module Spree
  class RigidPrinterFormat < OptionValue
    after_initialize :associate_option_type
    attr_accessible :template, :width, :height, :min_width_dimension, :min_height_dimension, :sided, :eyelet, :cutting_coil,
                    :template_format_attributes, :template_format
    
    before_validation :set_presentation
    
    def set_presentation
      self.presentation = self.name
    end
    
    def sided # bifacciale
      self.is_cut || false
    end
    
    def eyelet # occhielli
      self.copy_cost_seam || false
    end
    
    def cutting_coil # taglio
      self.copy_cost_milling || false
    end
    
    def width # larghezza
      self.start_up_cost_bw || 0
    end
    
    def height # altezza
      self.start_up_cost_c || 0
    end
    
    def min_width_dimension # minimo larghezza
      self.plant_color_bw || 0
    end
    
    def min_height_dimension # minimo altezza
      self.plant_color_c || 0
    end
    
    def sided=value
      self.is_cut = value
    end
    
    def eyelet=value
      self.copy_cost_seam = value
    end
    
    def cutting_coil=value
      self.copy_cost_milling = value
    end
    
    def width=value
      self.start_up_cost_bw = value
    end
    
    def height=value
      self.start_up_cost_c = value
    end

    def min_width_dimension=value
      self.plant_color_bw = value
    end
    
    def min_height_dimension=value
      self.plant_color_c = value
    end

    private
    def associate_option_type
      if self.new_record?
        self.option_type = Spree::OptionType.find_by_name('rigid_printer_format')
        self.name = self.presentation.downcase if self.presentation
      end
    end
  end
end