Spree::Address.class_eval do
	_validators.reject!{ |key, value| (key == :firstname) || (key == :lastname) }
	_validate_callbacks.each do |callback|
		callback.raw_filter.attributes.reject! { |key| (key == :firstname) || (key == :lastname) } if callback.raw_filter.respond_to?(:attributes)
	end

	attr_accessor :is_company, :is_user
	attr_accessible :vat_number, :tax_id_number, :is_company

	validates :firstname, :lastname, :presence => true, :if => Proc.new {|a| a.company.blank? }
  validates :vat_number, :length => {:in => 8..16, :message => I18n.t(:vat_error)}, :if => Proc.new {|a| !a.vat_number.blank? }
  validates :tax_id_number, :length => {:in => 11..16}, :if => Proc.new {|a| !a.tax_id_number.blank? }
	before_validation :check_company_vat

	before_update :check_changed_address

	def check_changed_address
		if self.changed?
			user = Spree::User.find_by_bill_address_id(self.id)
			Spree::UserMailer.bill_change_email(self, user).deliver if Spree::Config[:notify_bill_user_change] && !user.blank?
		end
	end
	
	def check_company_vat
		unless(self.is_company.blank?)
			if(self.is_company != "0")
				self.firstname = ""
				self.lastname = ""
				self.tax_id_number = ""
			else
				self.company = ""
				self.vat_number = ""
				self.is_vies = false
			end
		end
	end

	def empty?
		attributes.except('id', 'created_at', 'updated_at', 'order_id', 'country_id', 'tax_id_number', 'vat_number').all? { |_, v| v.nil? }
	end

	def to_s
		"#{full_name} #{"<br />C.F.: " + tax_id_number if tax_id_number} #{"<br />P.IVA: "  + vat_number if vat_number}<br />Tel. #{phone}<br />#{address1} - #{zipcode}<br />#{city rescue nil}, (#{state_text}) [#{country.try(:name)}]".html_safe
	end

	def formatted
		"#{address1} - #{zipcode}, #{city rescue nil} - (#{state_text}) [#{country.try(:name)}]".html_safe
	end
	
	def to_ship
		"#{address1} #{address2 rescue nil} - #{zipcode}, #{city rescue nil} - (#{state_text}) [#{country.try(:name)}]".html_safe
	end

	def full_name
		rtn = self.company
		rtn = [self.firstname, self.lastname].join(' ') if rtn.blank?
		rtn = rtn.titleize if !rtn.blank?
		rtn
	end
end