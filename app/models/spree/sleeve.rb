# COPERTINA LEGATORIA
module Spree
  class Sleeve < OptionValue
    after_save :associate_option_type
    belongs_to :bindery, :class_name => "Spree::Bindery", :foreign_key => "option_value_id"
    attr_accessible :type_quantity, :copy_cost, :start_time, :average_hourly_sleeve, 
                    :copy_cost_fins, :start_time_fins, :average_hourly_sleeve_fins
    attr_accessor :_destroy
    
    def type_quantity
      self.presentation
    end
    
    def copy_cost
      self.price_front_bw || 0
    end
    
    def copy_cost_fins
      self.price_front_c || 0
    end
    
    def start_time
      self.start_time_bw || 0
    end

    def average_hourly_sleeve
      self.average_hourly_print_bw || 0
    end
    
    def start_time_fins
      self.start_time_c || 0
    end

    def average_hourly_sleeve_fins
      self.average_hourly_print_c || 0
    end
    
    def type_quantity=value
      self.presentation = value
    end
    
    def copy_cost=value
      self.price_front_bw = value
    end
    
    def copy_cost_fins=value
      self.price_front_c = value
    end

    def start_time=value
      self.start_time_bw = value
    end

    def average_hourly_sleeve=value
      self.average_hourly_print_bw = value
    end
    
    def start_time_fins=value
      self.start_time_c = value
    end

    def average_hourly_sleeve_fins=value
      self.average_hourly_print_c = value
    end

    private
    def associate_option_type
      self.update_attribute(:option_type_id, Spree::OptionType.find_by_name('sleeve').id) if self.option_type.nil?
    end
  end
end