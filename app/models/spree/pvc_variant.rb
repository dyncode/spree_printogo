# PVC Variant
module Spree
  class PvcVariant < BannerVariant
    before_create :after_init
    belongs_to :pvc_product, :class_name => Spree::PvcProduct, :foreign_key => "product_id"
    attr_accessible :laminations, :cutting_coils

    # Accesso agli option values
    def laminations
      self.option_values.where(:option_type_id => Spree::OptionType.find_by_name('lamination_type').id)
    end

    def cutting_coils
      self.option_values.where(:option_type_id => Spree::OptionType.find_by_name('cutting_coil_type').id)
    end
    
    # Setto i tipi di laminatura
    def laminations= values
      self.laminations.each do |l|
        unless values.include?(l.id)
          self.option_values.delete(l)
        end
      end

      values.compact.each do |v|
        unless v.blank?
          ov = Spree::OptionValue.find v
          unless self.laminations.include?(ov)
            self.option_values << ov
          end
        end
      end
    end
    
    # Setto i tipi di sagomatura
    def cutting_coils= values
      self.cutting_coils.each do |cc|
        unless values.include?(cc.id)
          self.option_values.delete(cc)
        end
      end

      values.compact.each do |v|
        unless v.blank?
          ov = Spree::OptionValue.find v
          unless self.cutting_coils.include?(ov)
            self.option_values << ov
          end
        end
      end
    end
    
    private
    def after_init
      if self.new_record?
        self.active = true
        self.on_hand = 1
      end
    end
  end
end