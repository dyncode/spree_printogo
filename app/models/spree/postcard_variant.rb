module Spree
  class PostcardVariant < FlayerVariant
    belongs_to :postcard_product, :class_name => Spree::PostcardProduct, :foreign_key => "product_id"
  end
end