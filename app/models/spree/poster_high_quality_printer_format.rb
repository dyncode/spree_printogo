module Spree
  class PosterHighQualityPrinterFormat < OptionValue
    after_initialize :associate_option_type
    attr_accessible :template, :width, :height, :min_width_dimension, :min_height_dimension
    
    before_validation :set_presentation
    
    def set_presentation
      self.presentation = self.name
    end
    
    def width
      self.start_up_cost_bw || 0
    end
    
    def height
      self.start_up_cost_c || 0
    end
    
    def min_width_dimension
      self.plant_color_bw || 0
    end
    
    def min_height_dimension
      self.plant_color_c || 0
    end
    
    def width=value
      self.start_up_cost_bw = value
    end
    
    def height=value
      self.start_up_cost_c = value
    end
    
    def min_width_dimension=value
      self.plant_color_bw = value
    end
    
    def min_height_dimension=value
      self.plant_color_c = value
    end

    private
    def associate_option_type
      if self.new_record?
        self.option_type = Spree::OptionType.find_by_name('poster_high_quality_printer_format')
        self.name = self.presentation.downcase if self.presentation
      end
    end
  end
end