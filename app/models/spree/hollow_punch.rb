# Finitura FUSTELLA
module Spree
  class HollowPunch < OptionType
    after_initialize :add_name_and_presentation

    attr_accessible :hourly_cost, :average_hourly_print, :start_time
    validates :hourly_cost, :average_hourly_print, :start_time, :presence => true

    def add_name_and_presentation
      if self.new_record?
        self.name = "hollowpunch"
        self.presentation = "Fustella"
        option_values.new :name => 'hollowpunch', :presentation => 'Fustella'
      end
    end

    def average_hourly_print
      self.average_hourly_work
    end

    def average_hourly_print= value
      self.average_hourly_work = value
    end

  end
end