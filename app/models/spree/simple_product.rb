# Prodotto Semplice
module Spree
  class SimpleProduct < Product
    has_many :simple_variants, :class_name => Spree::SimpleVariant, :foreign_key => "product_id", :dependent => :delete_all
    
    attr_accessible :marginality, :seo_title, :simple_variants, :simple_variants_attributes, :max_width,
                    :taxons, :taxon_ids
    
    before_create :generate_sku_number
    after_create :setting_default_shipping_date
    
    has_one :master,
          :class_name => 'Spree::Variant',
          :foreign_key => "product_id",
          :conditions => ["#{Variant.quoted_table_name}.is_master = ?", true],
          :dependent => :destroy
    
    delegate_belongs_to :master, :sku, :is_master, :weight
    validate :weight, :presence => true
    
    after_initialize :ensure_master
    
    def accept_upload?
      self.max_width == 1
    end
    
    def ensure_master
      return unless new_record?
      self.master ||= Variant.new(:price => 0)
    end
    
    def generate_sku_number
      record = true
      while record
        random = "R#{Array.new(9){rand(9)}.join}"
        record = self.class.joins(:master).where("spree_variants.sku = '#{random}'").first
      end
      self.master.sku = random if self.master.sku.blank?
      self.master.sku
    end
    
    def simple_variants_copies
      h = Array.new()
      simple_variants.map{|sv| h << sv.to_preventive}
      h.to_json
    end
    
    def setting_default_shipping_date
      Spree::ShippingDateAdjustment.create(:name => "Ritiro di Base", :days => 2, :preferred_amount => 0, :product_id => self.id)
    end
  end
end