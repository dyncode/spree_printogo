# Striscione
module Spree
  class BannerVariant < Variant
    before_create :after_init
    belongs_to :banner_product, :class_name => Spree::BannerProduct, :foreign_key => "product_id"
    attr_accessible :printer, :papers, :eyelet_accessories, :eyelet_accessories_attributes, :pocket,
                    :pocket_attributes, :heat_sealing, :heat_sealing_attributes, :reinforcement_perimeter,
                    :reinforcement_perimeter_attributes, :plotter
    
    has_many :eyelet_accessories, :class_name => "Spree::EyeletAccessory", :foreign_key => "variant_id", :dependent => :delete_all
    #has_one :buttonhole_accessory, :class_name => "Spree::ButtonholeAccessory", :foreign_key => "variant_id", :dependent => :destroy
    has_one :pocket, :class_name => "Spree::Pocket", :foreign_key => "variant_id", :dependent => :destroy
    has_one :heat_sealing, :class_name => "Spree::HeatSealing", :foreign_key => "variant_id", :dependent => :destroy
    has_one :reinforcement_perimeter, :class_name => "Spree::ReinforcementPerimeter", :foreign_key => "variant_id", :dependent => :destroy
    
    accepts_nested_attributes_for :eyelet_accessories, :allow_destroy => true
    #accepts_nested_attributes_for :buttonhole_accessory
    accepts_nested_attributes_for :pocket
    accepts_nested_attributes_for :heat_sealing
    accepts_nested_attributes_for :reinforcement_perimeter

    # Accesso agli option values
    def papers
      self.option_values.where(:option_type_id => Spree::OptionType.find_by_name('coil').id)
    end

    def plotter
      self.option_values.where(:option_type_id => Spree::OptionType.find_by_name('plotter').id).last
    end
    
    # Setto i tipi di carta che per questo prodotto sono le coil (BOBINE)
    def papers= values
      self.papers.each do |p|
        unless values.include?(p.id)
          self.option_values.delete(p)
        end
      end

      values.compact.each do |v|
        unless v.blank?
          ov = Spree::OptionValue.find v
          unless self.papers.include?(ov)
            self.option_values << ov
          end
        end
      end
    end
    
    # Setto la stampante che per questo prodotto e` il plotter
    def plotter= value
      self.option_values.delete(self.plotter) if !self.plotter.blank?
      self.option_values << Spree::Plotter.find(value)
    end
    
    # Ritorno un JSON formato da Disposizione => {Tipi di Occhiello}
    def eyelet_accessories_json
      res = []
      self.eyelet_accessories.each do |ea|
        res << {:type => ea.id, :values => JSON.parse(ea.disposition), :limit => ea.available_meter}
      end
      res.to_json
    end
    
    private
    def after_init
      if self.new_record?
        self.active = true
        self.on_hand = 1
      end
    end
  end
end