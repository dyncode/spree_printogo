# TAGLIO BOBINE
module Spree
  class CuttingCoil < OptionType
    after_initialize :add_name_and_presentation
    attr_accessible :active
    has_many :cutting_coil_types, :class_name => "Spree::CuttingCoilType", :foreign_key => "option_value_id", :dependent => :delete_all
    attr_accessible :cutting_coil_types_attributes, :active, :cutting_coil_types
    
    accepts_nested_attributes_for :cutting_coil_types, :allow_destroy => true
    
    def add_name_and_presentation
      if self.new_record?
        self.name = "cutting_coil"
        self.presentation = "Taglio Bobine"
      end
    end

    def active
      self.is_hide
    end
    
    def active=value
      self.is_hide = value
    end
  end
end