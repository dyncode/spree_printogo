Spree::StaticPage.class_eval do
	before_destroy :delete_related_menu

	def delete_related_menu
		
		Spree::Menu.find_all_by_linkable_id(self.id).each do |m|
			if (!m.blank? && m.linkable_type == "Spree::StaticPage")
				m.destroy
			end
		end
	end

end