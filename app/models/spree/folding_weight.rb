# PIEGATURA secondo GRAMMATURA
module Spree
  class FoldingWeight < OptionValue
    after_save :associate_option_type
    after_create :generate_folding_format
    after_update :update_folding_format
    before_destroy :remove_folding_format
    
    belongs_to :folding, :class_name => "Spree::Folding", :foreign_key => "option_value_id"

    attr_accessible :weight_limit, :average_hourly, :copy_cost

    validates :weight_limit, :presence => true
    
    default_scope :order => 'start_up_cost_bw ASC'
    
    def average_hourly
      self.start_up_cost_c
    end
    
    def copy_cost
      self.price_front_and_back_bw
    end

    def weight_limit
      self.start_up_cost_bw
    end
    
    def average_hourly= value
      self.start_up_cost_c = value
    end
    
    def copy_cost= value
      self.price_front_and_back_bw = value
    end
    
    def weight_limit= value
      self.start_up_cost_bw = value
    end
    
    def generate_folding_format
      Spree::FoldingVariant.all.each do |fv|
        if (ff = fv.folding_formats.find_by_option_value_id(self.folding.id))
          ff.update_paper_products
        else
          ff = Spree::FoldingFormat.create(:option_value_id => self.folding.id, :variant_id => fv.id)
          ff.generate_paper_products
          fv.option_values << ff
        end
      end
    end
    
    def update_folding_format
      Spree::FoldingVariant.all.each do |fv|
        if (ff = fv.folding_formats.find_by_option_value_id(self.folding.id))
          ff.update_paper_products
        else
          ff = Spree::FoldingFormat.create(:option_value_id => self.folding.id, :variant_id => fv.id)
          ff.generate_paper_products
          fv.option_values << ff
        end
      end
    end
    
    def remove_folding_format
      if self.folding.folding_weights.empty?
        tmp = Spree::FoldingFormat.find_all_by_option_value_id(self.folding.id)
        if !tmp.empty?
          Spree::FoldingFormat.delete_all("option_value_id = #{self.folding.id}")
        end
      end
    end
    
    private
    def associate_option_type
      self.update_attribute(:option_type_id, Spree::OptionType.find_by_name('folding_weight').id) if self.option_type.nil?
    end
  end
end