# APPENDINO ACCESSORIO (abbinato al CANVAS)
module Spree
  class HangerAccessory < OptionValue
    after_save :associate_option_type
    belongs_to :canvas_product, :class_name => "Spree::CanvasProduct", :foreign_key => "variant_id"
    belongs_to :hanger, :class_name => "Spree::Hanger", :foreign_key => "option_value_id"
    attr_accessible :available_meter, :hanger, :hanger_id, :option_value_id
    
    attr_accessor :_destroy
        
    def available_meter
      self.start_time_bw
    end
    
    def available_meter=value
      self.start_time_bw = value
    end
    
    private
    def associate_option_type
      self.update_attribute(:option_type_id, Spree::OptionType.find_by_name('hanger_accessory').id) if self.option_type.nil?
    end
  end
end