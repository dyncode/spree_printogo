Spree::Slide.class_eval do
  belongs_to :slideable, :polymorphic => true
  
  attr_accessible :slideable, :slideable_id, :slideable_type, :position
  
  def is_promotion?
    self.slideable_type == "Spree::LineItemPromotion" && !self.slideable_id.blank?
  end
  
  def promotion
    if self.is_promotion?
      Spree::LineItemPromotion.find(self.slideable_id)
    else
      nil
    end
  end
end