# LOCANDINA
module Spree
  class PlaybillProduct < FlayerProduct
    has_many :playbill_variants, :class_name => Spree::PlaybillVariant, :foreign_key => "product_id"
    attr_accessible :playbill_variants, :label_name, :seo_title
    
    def get_selected_values_json
      a = []
      self.playbill_variants.each do |v|
        t = []
        v.paper_products.each do |pp|
          tmp = t.select { |z| z[:weight][:quantity] == pp.paper_weight.weight if z[:weight] }
          if !tmp.empty?
            tmp.first[:weight][:paper] << {:name => pp.paper.name, :id => pp.paper.id, :presentation => pp.paper.presentation}
          else
            t << {'weight' => {:quantity => pp.paper_weight.weight,
                               :id => pp.paper_weight.id,
                               :paper => {:name => pp.paper.name, :id => pp.paper.id, :presentation => pp.paper.presentation}}}
          end
        end


        a << {:variant_id => v.id, :values => t}

      end
      a.to_json
    end
  end
end