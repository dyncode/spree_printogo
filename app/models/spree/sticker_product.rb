# Etichette
module Spree
  class StickerProduct < Product
    before_create :add_option_type
    has_many :sticker_variants, :class_name => "Spree::StickerVariant", :foreign_key => "product_id", :dependent => :delete_all
    has_many :sticker_printer_formats, :class_name => "Spree::StickerPrinterFormat", :foreign_key => "variant_id", :dependent => :delete_all

    attr_accessor :label_name
    attr_accessible :min_width, :min_height, :sticker_variants, :sticker_variants_attributes, :seo_title,
                    :label_name, :finishing
    accepts_nested_attributes_for :sticker_variants

    def min_width
      self.max_weight_cover
    end
    
    def min_height
      self.max_facades_weight_cover
    end
    
    def min_width=value
      self.max_weight_cover = value
    end
    
    def min_height=value
      self.max_facades_weight_cover = value
    end
    
    def initialize(val = nil)
      super(val)
      if self.new_record?
        self.name = "Etichette #{label_name}" if self.name.empty?
        #self.available_on = Time.now if self.available_on.nil?
        self.master.price = 0 if self.master && self.master.price.nil?
      end
    end

    # Accesso agli option types
    def print_colors
      self.option_types.where(:name => "print_color").first.option_values
    end

    def papers_option_type
      self.option_types.where(:name => "coil").first
    end

    def plotter_option_type
      self.option_types.where(:name => "plotter").first
    end

    def instructions_option_type
      self.option_types.where(:name => "printer_instructions").first
    end
    
    def directings
      self.option_types.where(:name => "printer_orientation").first
    end
    
    def sticker_printer_format_option_type
      self.option_types.where(:name => "sticker_printer_format").first
    end
    
    #def sticker_printer_formats(id = nil)
    #  if id
    #    self.master.option_values.where(:id => id).first
    #  else
    #    self.master.option_values.where(:option_type_id => self.sticker_printer_format_option_type.id)
    #  end
    #end
    
    # devo tornare tutti i tipi di materiali con le relative configurazioni
    def all_value_json
      res = []
      self.sticker_variants.map do |rv|
        rv.papers.map do |p|
          res << {
              :id => p.id,
              :name => p.name,
              :description => p.description.to_s,
              :plotters => rv.plotter.plotter_printing_costs.map {|ppc| {:id => ppc.id, :name => ppc.name, :presentation => ppc.presentation}},
              :cutting_coils => rv.cutting_coils.map {|cc| cc.id},
              :cutting_coils_limit => rv.cutting_coils.map {|ccl| {:id => ccl.id, :min => ccl.min_copies_active}},
              :processing_plotters => rv.processing_plotter_combinations.map {|sc|
                      {:id => sc.id, :name => sc.name, :min => sc.min_copies_active, :processing_plotter_types => sc.processing_plotter_types.map {|ppt|
                            {:id => ppt.id, :name => ppt.name, :presentation => ppt.presentation}
                          }
                      }
              }
          }
        end
      end
      
      res.to_json
    end
    
    def papers_variant
      tmp = []
      res = []
      
      self.sticker_variants.map do |rv|
        rv.papers.map do |p|
          if tmp.empty?
            tmp << p
            res << p
          elsif !tmp.include?(p)
            tmp << p
            res << p
          end
        end
      end
      
      res
    end
    
    def cutting_coil_variants
      tmp = []
      res = []
      
      self.sticker_variants.map do |rv|
        rv.cutting_coils.map do |cc|
          if tmp.empty?
            tmp << cc
            res << cc
          elsif !tmp.include?(cc)
            tmp << cc
            res << cc
          end
        end
      end
      
      res
    end

    def processing_plotter_variants
      tmp = []
      res = []
      
      self.sticker_variants.map do |rv|
        rv.processing_plotters.map do |l|
          if tmp.empty?
            tmp << l
            res << l
          elsif !tmp.include?(l)
            tmp << l
            res << l
          end
        end
      end
      
      res
    end

    def plotters_variant
      tmp = []
      res = []
      
      plotters = self.sticker_variants.map {|rv| rv.plotter}
      
      plotters.map do |pl|
        pl.plotter_printing_costs.map do |ppc|
          if tmp.empty?
            tmp << ppc.name.downcase
            res << ppc
          elsif !tmp.include?(ppc.name.downcase)
            tmp << ppc.name.downcase
            res << ppc
          end
        end
      end
            
      res
    end
    
    def find_stickers_variants(paper, print_color)
      self.sticker_variants.each do |rv|
        if rv.papers.include?(paper) && rv.plotter.plotter_printing_costs.include?(print_color)
          return rv
        end
      end
    end
    
    private
    def add_option_type
      if self.new_record?
        print_color_ot = Spree::OptionType.find_by_name('printer_color')
        plotter_ot = Spree::OptionType.find_by_name('plotter')
        paper_ot = Spree::OptionType.find_by_name('coil')
        printer_instructions_ot = Spree::OptionType.find_by_name('printer_instructions')
        printer_orientation_ot = Spree::OptionType.find_by_name('printer_orientation')
        
        sticker_printer_format_ot = Spree::OptionType.find_by_name('sticker_printer_format')

        self.option_types << print_color_ot
        self.option_types << paper_ot
        self.option_types << printer_instructions_ot
        self.option_types << plotter_ot
        self.option_types << printer_orientation_ot
        
        self.option_types << sticker_printer_format_ot
        self.master.option_values << sticker_printer_format_ot.option_values
      end
    end
  end
end