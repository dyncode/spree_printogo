# Manifesto
module Spree
  class PosterProduct < Product
    before_create :add_option_type
    attr_accessible :printer, :papers, :plotter, :seo_title
    
    def self.user_formats
      %w(45x70 90x70 90x140 6x3)
    end
    
    def self.printing_instruction
      ["Solo fronte", "Fronte e retro"]
    end
    
    def initialize(val = nil)
      super(val)
      if self.new_record?
        self.name = "Manifesto"
        #self.available_on = Time.now if self.available_on.nil?
        self.master.price = 0 if self.master && self.master.price.nil?
      end
    end
    
    def add_option_type
      if self.new_record?
        print_color_ot = Spree::OptionType.find_by_name('printer_color')
        plotter_ot = Spree::OptionType.find_by_name 'plotter'
        paper_ot = Spree::OptionType.find_by_name 'coil'
        poster_printer_format_ot = Spree::OptionType.find_by_name('poster_printer_format')
        printer_instructions_ot = Spree::OptionType.find_by_name('printer_instructions')

        self.option_types << print_color_ot
        self.option_types << paper_ot
        self.option_types << poster_printer_format_ot
        self.option_types << printer_instructions_ot
        self.option_types << plotter_ot
        
        self.master.option_values << poster_printer_format_ot.option_values
      end
    end

    # devo tornare tutti i tipi di materiali con le relative configurazioni
    def all_value_json
      res = []
      self.papers.map do |p|
        res << {
            :id => p.id,
            :name => p.name,
            :description => p.description.to_s,
        }
      end
      
      res.to_json
    end

    # Accesso agli option types
    def print_colors
      self.option_types.where(:name => "printer_color").first.option_values
    end

    def papers_option_type
      self.option_types.where(:name => "coil").first
    end

    def plotter_option_type
      self.option_types.where(:name => "plotter").first
    end

    def poster_printer_format_option_type
      self.option_types.where(:name => "poster_printer_format").first
    end

    def instructions_option_type
      self.option_types.where(:name => "printer_instructions").first
    end

    # Accesso agli option values
    def papers
      self.master.option_values.where(:option_type_id => self.papers_option_type.id)
    end
    
    def papers= values
      self.papers.each do |p|
        unless values.include?(p.id)
          self.master.option_values.delete(p)
        end
      end
      
      values.compact.each do |v|
        unless v.blank?
          ov = Spree::OptionValue.find v
          unless self.papers.include?(ov)
            self.master.option_values << ov
          end
        end
      end
    end
    
    def plotter
      self.master.option_values.where(:option_type_id => self.plotter_option_type.id).last
    end
    
    # Setto la stampante che per questo prodotto e` il plotter
    def plotter= value
      self.master.option_values.delete(self.plotter) if !self.plotter.blank?
      self.master.option_values << Spree::Plotter.find(value)
    end

    def poster_printer_formats(id = nil)
      if id
        self.master.option_values.where(:id => id).first
      else
        self.master.option_values.where(:option_type_id => self.poster_printer_format_option_type.id)
      end
    end
    
    def instructions
      self.instructions_option_type.option_values
    end
  end
end