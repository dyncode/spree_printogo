# TIPI LAMINATURE
module Spree
  class LaminationType < OptionValue
    after_save :associate_option_type
    belongs_to :lamination, :class_name => "Spree::Lamination", :foreign_key => "option_value_id"
    attr_accessible :available_meter, :lamination, :lamination_id, :option_value_id
    
    attr_accessor :_destroy
    
    def available_meter
      self.plant_color_bw
    end
    
    def available_meter=value
      self.plant_color_bw = value
    end
    
    private
    def associate_option_type
      self.update_attribute(:option_type_id, Spree::OptionType.find_by_name('lamination_type').id) if self.option_type.nil?
    end
  end
end