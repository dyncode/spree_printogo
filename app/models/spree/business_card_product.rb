module Spree
  class BusinessCardProduct < Product
    before_create :add_option_type
    has_many :business_card_variants, :class_name => Spree::BusinessCardVariant, :foreign_key => "product_id"
    attr_accessible :business_card_variants, :seo_title

    def initialize(val = nil)
      variants_hash = val.delete :business_card_variant if val
      super(val)
      if val.blank? || val.empty?
        # Impostazioni di default
        self.name = "Biglietti da visita" if self.name.empty?
        #self.available_on = Time.now if self.available_on.nil?
        if self.master.present?
          self.master.price = 0 if self.master.price.nil?
          self.master.on_hand = 1
        else
          self.master = Spree::Variant.new
          self.master.price = 0
          self.master.on_hand = 1
        end

        self.master.limit = Spree::Limit.new
      end

    end

    def limit
      self.master.limit
    end
    
    def printer_format_cards
      self.option_types.where(:name => "printer_format_card").last.option_values
    end
    
    def poses_option_type
      self.option_types.where(:name == "pose").last
    end
    
    def number_of_copy_option_type
      self.option_types.where(:name => 'number_of_copy').last
    end
    
    alias :number_of_copy :number_of_copy_option_type
    
    def plastification_option_type
      self.option_types.where(:name => "plastification" ).last
    end

    def print_color_option_type
      self.option_types.where(:name => "printer_color").last
    end

    alias :print_color :print_color_option_type

    def printer_type_option_type
      self.option_types.where(:name => "printer").last
    end
    
    def printer_format_card_option_type
      self.option_types.where(:name => "printer_format_card").last
    end

    def directing_option_type
      self.option_types.where(:name => "printer_orientation").last
    end

    def hourly_cost_option_type
      self.option_types.where(:name => "hourly_cost").last
    end

    def cut
      self.option_types.where(:name => "cut").last
    end
    
    def option_values_from_option_type(option_type_name)
      self.business_card_variants.each do |variant|
        r = variant.option_values.map { |ov| ov.option_type.name == option_type_name }.first
        return r.option_type if r
      end
      []
    end
    
    def printer_format_type_option_type
      self.option_types.where(:name => "printer_format_default").last
    end
    
    def generate_printer_format_json
      Hash[printer_format_type_option_type.name =>
        printer_format_type_option_type.option_values.map { |ot| {:id => ot.id, :presentation => ot.presentation, :name => ot.name} }].to_json
    end

    # Ritorna un json di questo tipo:
    # supponendo p1 = carta di tipo 1,p2 = carta di tipo 2,p3 = carta di tipo 3
    # { 90 => {p1,p2}, 100 => {p2,p3} }
    def generate_weight_paper_json
      h = []
      Spree::Config[:paper_weight].split(",").collect { |w| w.to_i }.each do |weight|
        paper_weight = Spree::PaperWeight.find_all_by_name(weight.to_s)
        if !paper_weight.empty?
          t = []
          paper_weight.each do |pw|
            t << {:name => pw.paper.name, :id => pw.paper.id} if pw.paper && pw.paper.active
          end
          h << {'weight' => {:quantity => paper_weight.first.weight, :papers => t}}
        end
      end
      h.to_json
    end

    def get_selected_values_json
      a = []
      self.paper_products.each do |pp|
        a << {'weight' => {:quantity => pp.paper_weight.weight, :id => pp.paper_weight.id, :value => pp.paper_weight.presentation, :paper => {:name => pp.paper.name, :id => pp.paper.id, :presentation => pp.paper.presentation}}}
      end
      a.to_json
    end

    def get_paper_product_json
      h = []
      begin
      self.paper_products.each do |paper_page|
        h << {:paper_name => paper_page.paper.name, :paper_id => paper_page.paper.id, :weight => paper_page.paper_weight.weight}
      end
      rescue
        logger.info("Error for get_paper_product_json for BUSINESS CARD")
      end
      h.to_json
    end
    
    def get_papers
      h = []
      tmp = []
      self.paper_products.each do |paper_page|
        if tmp.empty?
          h << [paper_page.paper.name, paper_page.paper.id]
          tmp << paper_page.paper.id
        elsif !tmp.include?(paper_page.paper.id)
          h << [paper_page.paper.name, paper_page.paper.id]
          tmp << paper_page.paper.id
        end
      end
      h
    end

    def printer_format_cards_json
      Hash[:printer_format_cards => self.printer_format_cards.map { |printer_format| {:name => printer_format.name,
                                                                                      :presentation => printer_format.presentation,
                                                                                      :template_url => (printer_format.template.template.url(:original) rescue nil),
                                                                                      :id => printer_format.id
      } }].to_json
    end
    
    def get_paper_weight_hash
      a = []
      paper_products.each do |paper_product|
        tmp = a.select { |v| v[:name] == paper_product.name }.first
        h = tmp.nil? ? {} : tmp
        if h.empty?
          # todo da sistemare.
        end
      end
    end
    
    def print_color?(v)
      self.variants.each do |variant|
        unless variant.id == v.id
          if v.number_of_copy_option_values == variant.number_of_copy_option_values &&
              v.print_color_option_values != variant.print_color_option_values
            true
          end
        end
      end
      false
    end
    
    def update_variants(variants)
      variants.each do |variant|
        variant = variant[1]
        v = Spree::BusinessCardVariant.find variant[:id]
        if variant[:create].nil?
          v.destroy
        else
          v.pose=variant[:pose]
          v.print_color=variant[:print_color]
          v.printer=Spree::Printer.find variant[:printer]
          v.printer_format = Spree::OptionValue.find variant[:printer_format]
          v.active = true
          v.product=self
          v.save
        end
      end
    end
    
    private
    def add_option_type
      if self.new_record?
        # Recupero tutti gli option type che mi servono
        printer_format_default_ot = Spree::OptionType.find_by_name('printer_format_default')
        print_color_ot = Spree::OptionType.find_by_name('printer_color')
        printer_ot = Spree::OptionType.find_by_name 'printer'
        printer_format_card_ot = Spree::OptionType.find_by_name('printer_format_card')
        printer_orientation_ot = Spree::OptionType.find_by_name 'printer_orientation'
        hourly_cost = Spree::OptionType.find_by_name 'hourly_cost'
        cut = Spree::OptionType.find_by_name 'cut'
        plasticization = Spree::OptionType.find_by_name 'plasticization'

        # Associo gli option type al prodotto
        self.option_types << printer_format_default_ot
        self.option_types << print_color_ot
        self.option_types << printer_ot
        self.option_types << printer_format_card_ot
        self.option_types << printer_orientation_ot
        self.option_types << hourly_cost
        self.option_types << cut
        self.option_types << plasticization
      end
    end
  end
end