# Finitura TAGLIO
module Spree
  # Questo option type ha solo un option value caratterizzato dai seguenti valori:
  #   - printer_hourly_cost_c => COSTO ORARIO
  #   - start_time_c => TEMPO DI AVVIAMENTO
  #   - average_hourly_print_c => MEDIA ORARIA FOGLI STAMPATI
  class Cut < OptionType
    after_initialize :add_name_and_presentation

    attr_accessible :hourly_cost, :average_hourly_print, :start_time
    validates :hourly_cost, :average_hourly_print, :start_time, :presence => true

    def add_name_and_presentation
      if self.new_record?
        self.name = "cut"
        self.presentation = "Tagliato"
      end
    end

    def average_hourly_print
      self.average_hourly_work
    end

    def average_hourly_print= value
      self.average_hourly_work = value
    end

  end
end