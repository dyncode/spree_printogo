# ETICHETTATURA
module Spree
  class ProcessingPlotter < OptionType
    after_initialize :add_name_and_presentation
    attr_accessible :active
    has_many :processing_plotter_types, :class_name => "Spree::ProcessingPlotterType", :foreign_key => "option_value_id"
    attr_accessible :processing_plotter_types, :processing_plotter_types_attributes, :active 
    
    accepts_nested_attributes_for :processing_plotter_types, :allow_destroy => true
    
    default_scope order("name ASC")

    def add_name_and_presentation
      if self.new_record?
        self.name = "processing_plotter"
        self.presentation = "Lavorazione Plotter"
      end
    end

    def active
      self.is_hide
    end
    
    def active=value
      self.is_hide = value
    end
  end
end