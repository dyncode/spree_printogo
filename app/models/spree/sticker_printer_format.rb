module Spree
  class StickerPrinterFormat < OptionValue
    after_initialize :associate_option_type
    belongs_to :sticker_product, :class_name => "Spree::StickerProduct", :foreign_key => "variant_id"
    
    attr_accessible :template, :width, :height, :min_width_dimension, :min_height_dimension, :cutting_coil,
                    :template_format_attributes, :template_format, :max_width_dimension, :max_height_dimension, :limit_max
    
    def cutting_coil
      self.copy_cost_milling || false
    end
    
    def width
      self.start_up_cost_bw || 0
    end
    
    def height
      self.start_up_cost_c || 0
    end
    
    def min_width_dimension
      self.plant_color_bw || 0
    end
    
    def min_height_dimension
      self.plant_color_c || 0
    end
    
    # DIMENSIONE MASSIMA LARGHEZZA
    def max_width_dimension
      self.price_front_bw || 0
    end
    
    # DIMENSIONE MASSIMA ALTEZZA
    def max_height_dimension
      self.price_front_c || 0
    end
    
    # LIMITE DIMENSIONI
    def limit_max
      self.is_cut || false
    end

    def cutting_coil=value
      self.copy_cost_milling = value
    end
    
    def limit_max=value
      self.is_cut = value
    end
    
    def width=value
      self.start_up_cost_bw = value
    end
    
    def height=value
      self.start_up_cost_c = value
    end

    def min_width_dimension=value
      self.plant_color_bw = value
    end
    
    def min_height_dimension=value
      self.plant_color_c = value
    end
    
    def max_width_dimension=value
      self.price_front_bw = value
    end
    
    def max_height_dimension=value
      self.price_front_c = value
    end
    
    private
    def associate_option_type
      if self.new_record?
        self.option_type = Spree::OptionType.find_by_name('sticker_printer_format')
        # self.name = self.presentation.downcase if self.presentation
      end
    end
  end
end