# OCCHIELLO
module Spree
  class Eyelet < OptionValue
    after_save :associate_option_type
    has_many :eyelet_accessories, :class_name => "Spree::EyeletAccessory", :foreign_key => "option_value_id", :dependent => :delete_all
    attr_accessible :copy_cost
    attr_accessor :_destroy
    
    def copy_cost
      self.start_time_bw
    end
    
    def copy_cost=value
      self.start_time_bw = value
    end

    private
    def associate_option_type
      self.update_attribute(:option_type_id, Spree::OptionType.find_by_name('eyelet').id) if self.option_type.nil?
    end
  end
end