# Corrisponde al template da scaricare per il formato
module Spree
  class Template < ActiveRecord::Base
    attr_accessible :attachment, :imageable, :default
    belongs_to :imageable, :polymorphic => true
    
    has_attached_file :attachment,
                      :url => '/spree/template/:id/:basename.:extension',
                      :path => ':rails_root/public/spree/template/:id/:basename.:extension'

    def to_jq_upload
      {
        "name" => read_attribute(:attachment_file_name),
        "size" => read_attribute(:attachment_file_size),
        "url" => attachment.url(:original),
        "delete_url" => "/admin/templates/#{self.id}",
        "delete_type" => "DELETE" 
      }
    end
  end
end