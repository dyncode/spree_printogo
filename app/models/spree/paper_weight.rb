module Spree
  class PaperWeight < OptionValue
    belongs_to :paper, :class_name => "Spree::Paper", :foreign_key => "option_value_id"
    has_many :paper_products, :class_name => "Spree::PaperProduct", :foreign_key => "weight_id", :dependent => :nullify

    before_create :associate_option_type
    before_validation :set_presentation
		after_save :check_product_or_folding

    attr_accessible :weight, :presentation, :cost_cut, :cost_ream, :thickness

    validates_uniqueness_of :name, :scope => [:option_value_id]

    # default_scope :order => 'name ASC'

		def check_product_or_folding
			Spree::PaperProduct.where("weight_id = ?", self.id).each do |pp|
				if pp.product_id.blank? && pp.folding_format_id.blank?
					pp.delete
				end
			end
		end

    def cut?
      !self.cost_cut.blank? && self.cost_cut != 0
    end
    
    def ream?
      !self.cost_ream.blank? && self.cost_ream != 0
    end
    
    def set_presentation
      self.presentation = self.paper.presentation rescue ""
    end
    
    def weight
      self.name
    end
    
    def cost_cut
      self.start_time_c
    end
    
    def cost_ream
      self.start_time_bw
    end
    
    def thickness
      self.average_hourly_print_c
    end
    
    def weight=value
      self.name=value
    end
    
    def cost_cut=value
      self.start_time_c=value
    end
    
    def cost_ream=value
      self.start_time_bw=value
    end
    
    def thickness=value
      self.average_hourly_print_c=value
    end
    
    private
    def associate_option_type
      self.option_type_id = Spree::OptionType.find_by_name('paper_weight').id if !self.option_type.present?
    end
  end
end
