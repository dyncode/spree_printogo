module Spree
  class SpiralingQuantity < OptionValue
    attr_accessible :color, :copy_cost, :diameter
    validates :color, :copy_cost, :diameter, :presence => true
    belongs_to :spiraling, :class_name => "Spree::Spiraling", :foreign_key => "option_type_id"
    
    def color
      self.name
    end
    
    def copy_cost
      self.price_front_bw
    end
    
    def diameter
      self.price_front_c
    end
    
    def color=(value)
      self.name = value
    end
    
    def copy_cost=(value)
      self.price_front_bw = value
    end
    
    def diameter=(value)
      self.price_front_c = value
    end
  end
end