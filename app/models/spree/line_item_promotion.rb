module Spree
  class LineItemPromotion < ActiveRecord::Base
    belongs_to :variant, :class_name => "Spree::Variant"
    belongs_to :product, :class_name => "Spree::Product"
    belongs_to :shipping_date_adjustment, :class_name => "Spree::ShippingDateAdjustment", :foreign_key => "shipping_date_adjustment_id", :dependent => :destroy
    
    has_one :slide, :as => :slideable
    has_one :banner, :as => :bannerable
    
    has_many :line_items_option_values, :class_name => "Spree::LineItemsOptionValues", :foreign_key => "line_item_id", :dependent => :destroy
    has_many :adjustments, :as => :adjustable, :class_name => "Spree::Adjustment", :dependent => :destroy
    
    before_create :set_promo_code
    after_create :create_disable_banner
    before_destroy :remove_banner
    before_destroy :remove_related_homepage_elements
    
    validates :shipping_date, :presence => true
    validates :quantity, :numericality => { :only_integer => true, :message => I18n.t('validation.must_be_int') }
    validates :price, :numericality => true

    attr_accessible :quantity, :variant_id, :product_id, :price, :weight_item, :shipping_date_adjustment_id,
                    :shipping_date_adjustment, :shipping_date, :variant, :product, :number_of_copy, :format, :custom, :orientation,
                    :instructions, :print_color, :paper, :plasticization, :weight, :eyelet, :eyelet_accessory, :eyelet_quantity,
                    :buttonhole_accessory, :reinforcement_perimeter, :plasticization_option, :cover_bw, :paper_cover_weight, :paper_cover,
                    :hanger_accessory, :frame, :spiraling, :folding, :sleeve, :number_of_facades, :unlimited, :expires_at,
                    :original_price, :original_total, :promo_code, :active
    
    scope :active, {:conditions => {:active => true}}
    scope :available, lambda {where("(quantity > 0 AND expires_at >= ?) OR (expires_at >= ? AND unlimited IS NOT NULL AND unlimited = ?)", Date.today, Date.today, true)}
    
    def is_available?
      (quantity > 0 && expires_at >= Date.today) || (expires_at >= Date.today && !unlimited.blank? && unlimited == true)
    end
    
    def set_promo_code
      self.promo_code = Digest::SHA1.hexdigest(Time.now.to_s + rand(12341234).to_s)[0..7]
    end
    
    # Create a new banner for new promotion
    def create_disable_banner
      Spree::Banner.create(:title => "Offerta #{self.number_of_copy} #{self.product.name}", :category => "promo", :enabled => false, :bannerable => self, :attachment => File.open('public/default/banner.png'))
    end
    
    def remove_banner
      if (b = Spree::Banner.find_by_bannerable_id(self.id))
        b.destroy
      end
      if (s = Spree::Slide.find_by_slideable_id(self.id))
        s.destroy
      end
    end

    def remove_related_homepage_elements
      Spree::HomepageElement.find_all_by_homepageble_id(self.id).each do |h|
        h.destroy if (!h.blank? && h.homepageble_type == "Spree::LineItemPromotion")
      end
    end
   
    # Formato selezionato e inserito
    def format_name
      if self.product.is_a?(Spree::BusinessCardProduct)
        tmp = self.line_items_option_values.joins(:option_value).where("spree_option_values.option_type_id = ?", Spree::OptionType.find_by_name("printer_format_card").id)
        tmp.last.option_value.presentation
      elsif (self.product.is_a?(Spree::PosterProduct) && !self.variant.product.is_a?(Spree::PosterFlyerProduct) && !self.variant.product.is_a?(Spree::PosterHighQualityProduct))
        tmp2 = self.line_items_option_values.joins(:option_value).where("spree_option_values.option_type_id = ?", Spree::OptionType.find_by_name("poster_printer_format").id)
        if !tmp2.empty? && tmp2.last.option_value.presentation != "personalizzato"
          tmp2.last.option_value.presentation
        else
          # vuol dire che il formato nn c'è quindi devo prendere il formato custom:
          if (width = self.line_items_option_values.joins(:option_value).where("spree_option_values.name = 'custom_width'").first && height = self.line_items_option_values.joins(:option_value).where("spree_option_values.name = 'custom_height'").first)
            "#{width.option_value.presentation}x#{height.option_value.presentation}cm"
          else
            ""
          end
        end
      elsif (self.product.is_a?(Spree::PosterHighQualityProduct))
        tmp2 = self.line_items_option_values.joins(:option_value).where("spree_option_values.option_type_id = ?", Spree::OptionType.find_by_name("poster_high_quality_printer_format").id)
        if !tmp2.empty? && tmp2.last.option_value.presentation != "personalizzato"
          tmp2.last.option_value.presentation
        else
          # vuol dire che il formato nn c'è quindi devo prendere il formato custom:
          if (width = self.line_items_option_values.joins(:option_value).where("spree_option_values.name = 'custom_width'").first && height = self.line_items_option_values.joins(:option_value).where("spree_option_values.name = 'custom_height'").first)
            "#{width.option_value.presentation}x#{height.option_value.presentation}cm"
          else
            ""
          end
        end
      elsif self.product.is_a?(Spree::BannerProduct)
        width = self.line_items_option_values.joins(:option_value).where("spree_option_values.name = 'custom_width'").first.option_value
        height = self.line_items_option_values.joins(:option_value).where("spree_option_values.name = 'custom_height'").first.option_value
        "#{width.presentation}x#{height.presentation}cm"
      elsif self.product.is_a?(Spree::CanvasProduct)
        # vuol dire che il formato nn c'è quindi devo prendere il formato custom:
        width = self.line_items_option_values.joins(:option_value).where("spree_option_values.name = 'custom_width'").first.option_value
        height = self.line_items_option_values.joins(:option_value).where("spree_option_values.name = 'custom_height'").first.option_value
        "#{width.presentation}x#{height.presentation}cm"
      elsif self.product.is_a?(Spree::SimpleProduct)
        tmp = self.line_items_option_values.joins(:option_value).where("spree_option_values.name = 'format'").first.option_value
        tmp.blank? ? nil : tmp.presentation
      else
        self.variant.format
      end
    end
    
    # Retrive all option or default
    def number_of_copy
      self.retrive_option_value_from_name('number_of_copy').presentation.to_i rescue nil
    end
    
    def format
      if self.product.is_a?(Spree::BusinessCardProduct)
        self.retrive_option_value_from_option_type_name('printer_format_card').id rescue nil
      elsif self.product.is_a?(Spree::PosterProduct)
        tmp = self.retrive_option_value_from_option_type_name('poster_printer_format') rescue nil
        if !tmp.blank? && tmp.name != "custom"
          tmp.id
        else
          self.retrive_option_value_from_name('custom').id rescue nil
        end
      elsif (self.product.is_a?(Spree::CanvasProduct) || self.product.is_a?(Spree::BannerProduct))
        nil
      else
        self.variant.id rescue nil
      end
    end
    
    def custom_width
      self.retrive_option_value_from_name('custom_width').presentation.to_i rescue nil
    end
    
    def custom_height
      self.retrive_option_value_from_name('custom_height').presentation.to_i rescue nil
    end
    
    def number_of_facades
      self.retrive_option_value_from_name('number_of_facades').presentation.to_i rescue nil
    end
    
    def printer_orientation(conf = false)
      if conf
        self.retrive_option_value_from_option_type_name('printer_orientation').presentation rescue nil
      else
        self.retrive_option_value_from_option_type_name('printer_orientation').id rescue nil
      end
    end
    
    def printer_instructions(conf = false)
      if conf
        self.retrive_option_value_from_option_type_name('printer_instructions').presentation rescue nil
      else
        self.retrive_option_value_from_option_type_name('printer_instructions').name rescue nil
      end
    end
    
    def printer_instructions_cover
      self.retrive_cover_option_value_from_option_type_name('printer_instructions').name rescue nil
    end
    
    def printer_color(conf = false)
      if conf
        self.retrive_option_value_from_option_type_name('printer_color').presentation rescue nil
      else
        self.retrive_option_value_from_option_type_name('printer_color').name rescue nil
      end
    end
    
    def paper(conf = false)
      if conf
        if (self.product.is_a?(Spree::PosterProduct) || self.product.is_a?(Spree::BannerProduct) || self.product.is_a?(Spree::CanvasProduct)) && !self.product.is_a?(Spree::PosterFlyerProduct)
          self.retrive_option_value_from_option_type_name('coil').name rescue nil
        else
          self.retrive_option_value_from_option_type_name('paper').presentation rescue nil
        end
      else
        if (self.product.is_a?(Spree::PosterProduct) || self.product.is_a?(Spree::BannerProduct) || self.product.is_a?(Spree::CanvasProduct)) && !self.product.is_a?(Spree::PosterFlyerProduct)
          self.retrive_option_value_from_option_type_name('coil').id rescue nil
        else
          self.retrive_option_value_from_option_type_name('paper').id rescue nil
        end
      end
    end
    
    def paper_cover
      self.retrive_cover_option_value_from_option_type_name('paper').id rescue nil
    end
    
    def paper_cover_weight
      self.retrive_cover_option_value_from_type('Spree::PaperWeight').weight.to_i rescue nil
    end
    
    def weight_paper
      if (self.product.is_a?(Spree::PosterProduct) || self.product.is_a?(Spree::BannerProduct) || self.product.is_a?(Spree::CanvasProduct)) && !self.product.is_a?(Spree::PosterFlyerProduct)
        self.retrive_option_value_from_option_type_name('coil').weight.to_i rescue nil
      else
        self.retrive_option_value_from_type('Spree::PaperWeight').weight.to_i rescue nil
      end
    end
    
    def plasticization
      self.retrive_option_value_from_option_type_name('plasticization').presentation rescue nil
    end
    
    def folding
      self.retrive_option_value_from_option_type_name('folding_format').folding.id rescue nil
    end
    
    def folding_format
      self.retrive_option_value_from_option_type_name('folding_format').id rescue nil
    end
    
    def spiraling
      self.retrive_option_value_from_type('SpiralingQuantity').id rescue nil
    end
    
    def sleeve
      self.retrive_option_value_from_name('sleeve').presentation rescue nil
    end
    
    def frame
      self.retrive_option_value_from_option_type_name('frame').id rescue nil
    end
    
    def hanger_accessory
      self.retrive_option_value_from_option_type_name('hanger_accessory').id rescue nil
    end
    
    def eyelet
      self.retrive_option_value_from_option_type_name('eyelet_accessory').id rescue nil
    end
    
    def buttonhole_accessory_disposition
      self.retrive_option_value_from_name('buttonhole_accessory_disposition').presentation rescue nil
    end
    
    def eyelet_accessory_quantity
      self.retrive_option_value_from_name('eyelet_accessory_quantity').presentation rescue nil
    end
    
    def eyelet_accessory_disposition
      self.retrive_option_value_from_name('eyelet_accessory_disposition').presentation rescue nil
    end
    
    def reinforcement_perimeter
      self.retrive_option_value_from_option_type_name('reinforcement_perimeter').id rescue nil
    end
    
    def cover_bw(conf = false)
      if conf
        self.retrive_cover_option_value_from_option_type_name('printer_color').presentation rescue nil
      else
        self.retrive_cover_option_value_from_option_type_name('printer_color').name rescue nil
      end
    end
    
    # Creo il mio hash con tutte le configurazioni
    def to_calculate
      {:id => self.id, :number_of_copy => self.number_of_copy, :paper => self.paper, :weight => self.weight_paper,
       :plastification => self.plasticization, :print_color => self.printer_color, :front_back => self.printer_instructions,
       :orientation => self.printer_orientation, :format => self.format, :number_of_facades => self.number_of_facades,
       :custom_width => self.custom_width, :custom_height => self.custom_height, :folding => self.folding,
       :spiraling => self.spiraling, :cover_bw => self.cover_bw, :paper_cover => self.paper_cover,
       :cover_weight => self.paper_cover_weight, :front_back_cover => self.printer_instructions_cover,
       :sleeve => self.sleeve, :frame => self.frame, :hanger_accessory => self.hanger_accessory, :folding_format => self.folding_format,
       :eyelet => self.eyelet, :buttonhole_accessory_disposition => self.buttonhole_accessory_disposition,
       :eyelet_accessory_quantity => self.eyelet_accessory_quantity, :eyelet_accessory_disposition => self.eyelet_accessory_disposition,
       :reinforcement_perimeter => self.reinforcement_perimeter, :shipping_date => self.shipping_date}
       #, :order_calculator => {:data => (self.build_shipping_date_adjustment({:name => "Promozione", :product_id => self.product_id, :days => Date.today.business_days_until(self.shipping_date.to_date)})) }
    end
    
    def clone
      Spree::LineItem.new(self.attributes.except('id', 'active', 'updated_at', 'created_at', 'shipping_date', 'product_id', 'shipping_method_id', 'shipping_date_adjustment_id', 'price', 'original_price', 'original_total', 'weight_item', 'unlimited', 'expires_at', 'promo_code'))
    end
    
    def amount
      (price) + adjustments.eligible.map(&:amount).sum
    end
    
    # solo IVA
    def total_vat
      self.adjustments.select { |adj| adj.originator_type == "Spree::TaxRate" }.first.amount rescue 0
    end

    def total_with_vat_and_shipping
      total_with_vat + total_shipping
    end

    def total_with_vat
      total + total_vat
    end

    def total
      tot = 0
      self.adjustments.each { |ad| tot += ad.amount if ad.originator_type != "Spree::TaxRate" }
      (price) + tot
    end

    def total_whitout_date
      tot = 0
      self.adjustments.each { |ad| tot += ad.amount if ad.originator_type != "Spree::ShippingMethod" &&
          ad.originator_type != "Spree::TaxRate" && ad.originator_type != "Spree::ShippingDateAdjustment" }
      (price) + tot
    end

    def total_shipping
      self.adjustments.select { |adj| adj.originator_type == "Spree::ShippingMethod" }.first.amount rescue 0
    end

    def retrive_option_value_from_option_type_name(option_type_name)
      self.line_items_option_values.joins(:option_value).where("spree_option_values.option_type_id = ?", Spree::OptionType.find_by_name(option_type_name).id).first.option_value
    end

    def retrive_option_value_from_name(option_value_name)
      self.line_items_option_values.joins(:option_value).where("spree_option_values.name = ?", option_value_name).first.option_value
    end
    
    def retrive_option_value_from_type(option_value_type)
      self.line_items_option_values.joins(:option_value).where("spree_option_values.type = ?", option_value_type).first.option_value
    end
    
    def retrive_cover_option_value_from_option_type_name(option_type_name)
      self.line_items_option_values.joins(:option_value).where("variant_parent_id = ? AND spree_option_values.option_type_id = ?", self.variant.cover_variant.id, Spree::OptionType.find_by_name(option_type_name).id).first.option_value
    end

    def retrive_cover_option_value_from_name(option_value_name)
      self.line_items_option_values.joins(:option_value).where("variant_parent_id = ? AND spree_option_values.name = ?", self.variant.cover_variant.id, option_value_name).first.option_value
    end
    
    def retrive_cover_option_value_from_type(option_value_type)
      self.line_items_option_values.joins(:option_value).where("variant_parent_id = ? AND spree_option_values.type = ?", self.variant.cover_variant.id, option_value_type).first.option_value
    end
    
    def generate_configuration(params)
      paper_ov = Spree::Paper.find(params[:line_item_promotion][:paper]) rescue nil
      print_color_ot = Spree::OptionType.find_by_name('printer_color')
      front_back_ot = Spree::OptionType.find_by_name('printer_instructions')
      
      if ((self.product.is_a?(Spree::CanvasProduct)) || (self.product.is_a?(Spree::PvcProduct)) || (self.product.is_a?(Spree::BannerProduct)) || (self.product.is_a?(Spree::PosterProduct)) || (self.product.is_a?(Spree::PosterHighQualityProduct)) || (self.product.is_a?(Spree::RigidProduct)) || (self.product.is_a?(Spree::StickerProduct))) && (!self.product.is_a?(Spree::PosterFlyerProduct))
        paper_ov = Spree::CoilValue.find(params[:line_item_promotion][:paper])
        
        self.variant = self.product.canvas_variant if self.product.is_a?(Spree::CanvasProduct)
        self.variant = self.product.banner_variant if self.product.is_a?(Spree::BannerProduct)
        
        if self.product.is_a?(Spree::PosterProduct)
          format = self.product.poster_printer_formats(params[:line_item_promotion][:format].to_i)
          self.variant = self.product.master
          self.line_items_option_values.new :option_value => format
          if format.name == 'custom' && params[:line_item_promotion][:custom] && !params[:line_item_promotion][:custom].blank?
            self.line_items_option_values.new :option_value => Spree::OptionValue.create(:name => 'custom_width', :presentation => params[:line_item_promotion][:custom][:width])
            self.line_items_option_values.new :option_value => Spree::OptionValue.create(:name => 'custom_height', :presentation => params[:line_item_promotion][:custom][:height])
          end
        elsif self.product.is_a?(Spree::PosterHighQualityProduct)
          format = self.product.poster_high_quality_printer_formats(params[:line_item_promotion][:format].to_i)
          self.variant = self.product.master
          self.line_items_option_values.new :option_value => format
          if format.name == 'custom' && params[:line_item_promotion][:custom] && !params[:line_item_promotion][:custom].blank?
            self.line_items_option_values.new :option_value => Spree::OptionValue.create(:name => 'custom_width', :presentation => params[:line_item_promotion][:custom][:width])
            self.line_items_option_values.new :option_value => Spree::OptionValue.create(:name => 'custom_height', :presentation => params[:line_item_promotion][:custom][:height])
          end
        else
          if params[:line_item_promotion][:custom] && !params[:line_item_promotion][:custom].blank?
            self.line_items_option_values.new :option_value => Spree::OptionValue.create(:name => 'custom_width', :presentation => params[:line_item_promotion][:custom][:width])
            self.line_items_option_values.new :option_value => Spree::OptionValue.create(:name => 'custom_height', :presentation => params[:line_item_promotion][:custom][:height])
          end
        end
        
        # FROM PVC
        #line_params[:orientation] = Spree::OptionValue.find(params[:order][:orientation])
        #line_params[:custom_format] = params[:order][:custom]
        #line_params[:paper] = Spree::CoilValue.find(params[:order][:paper])
        #line_params[:weight] = Spree::OptionValue.new(:name => "weight", :presentation => line_params[:paper].weight.to_i)
        #line_params[:front_back_ot] = Spree::OptionType.find_by_name('printer_instructions')
        #line_params[:front_back] = line_params[:front_back_ot].option_values.where("name = 'front'").first
        #line_params[:print_color] = Spree::PlotterPrintingCost.find(params[:order][:print_color])
        #line_params[:number_of_copy] = Spree::OptionValue.create(:name => "number_of_copy", :presentation => params[:order][:number_of_copy])

        #line_params[:lamination_type] = Spree::LaminationType.find(params[:order][:lamination]) rescue false
        #line_params[:cutting_coil_type] = Spree::CuttingCoilType.find(params[:order][:cutting_coil]) rescue false

        #line_params[:custom_width] = Spree::OptionValue.create(:name => 'custom_width', :presentation => line_params[:custom_format][:width])
        #line_params[:custom_height] = Spree::OptionValue.create(:name => 'custom_height', :presentation => line_params[:custom_format][:height])

        #current_item.line_items_option_values.new :option_value => line_params[:custom_width]
        #current_item.line_items_option_values.new :option_value => line_params[:custom_height]
        #current_item.line_items_option_values.new :option_value => line_params[:orientation]
        #current_item.line_items_option_values.new :option_value => line_params[:paper]
        #current_item.line_items_option_values.new :option_value => line_params[:front_back]
        #current_item.line_items_option_values.new :option_value => line_params[:print_color]
        #current_item.line_items_option_values.new :option_value => line_params[:number_of_copy]
        #current_item.line_items_option_values.new :option_value => line_params[:lamination_type] if line_params[:lamination_type]
        #current_item.line_items_option_values.new :option_value => line_params[:cutting_coil_type] if line_params[:cutting_coil_type]
        
        
        self.line_items_option_values.new :option_value => Spree::OptionValue.new(:name => "weight", :presentation => Spree::CoilValue.find(params[:line_item_promotion][:paper]).weight.to_i)
        self.line_items_option_values.new :option_value => Spree::OptionType.find_by_name('printer_instructions').option_values.where("name = 'front'").first
        self.line_items_option_values.new :option_value => Spree::PlotterPrintingCost.find(params[:line_item_promotion][:print_color])
        # FROM PVC
        
        self.line_items_option_values.new :option_value => Spree::Frame.find(params[:line_item_promotion][:frame]) if params[:line_item_promotion][:frame] && (params[:line_item_promotion][:frame] != "")
        self.line_items_option_values.new :option_value => self.variant.hanger_accessories.find(params[:line_item_promotion][:hanger_accessory]) if params[:line_item_promotion][:hanger_accessory] && (params[:line_item_promotion][:hanger_accessory] != "")
        self.line_items_option_values.new :option_value => self.variant.buttonhole_accessory if self.variant.is_a?(Spree::BannerVariant)
        self.line_items_option_values.new :option_value => Spree::OptionValue.create(:name => :buttonhole_accessory_disposition, :presentation => params[:line_item_promotion][:buttonhole_accessory]) if params[:line_item_promotion][:buttonhole_accessory] && !params[:line_item_promotion][:buttonhole_accessory].blank?
        self.line_items_option_values.new :option_value => Spree::OptionValue.create(:name => :eyelet_accessory_quantity, :presentation => params[:line_item_promotion][:eyelet_quantity]) if params[:line_item_promotion][:eyelet_quantity] && !params[:line_item_promotion][:eyelet_quantity].blank?
        self.line_items_option_values.new :option_value => Spree::OptionValue.create(:name => :eyelet_accessory_disposition, :presentation => params[:line_item_promotion][:eyelet_accessory]) if params[:line_item_promotion][:eyelet_accessory] && !params[:line_item_promotion][:eyelet_accessory].blank?
        self.line_items_option_values.new :option_value => Spree::OptionValue.find(params[:line_item_promotion][:orientation])
        self.line_items_option_values.new :option_value => paper_ov
        self.line_items_option_values.new :option_value => front_back_ot.option_values.where("name = '#{params[:line_item_promotion][:instructions].to_s}'").first
        self.line_items_option_values.new :option_value => print_color_ot.option_values.where("name = '#{params[:line_item_promotion][:print_color].to_s}'").first
        self.line_items_option_values.new :option_value => Spree::OptionValue.create(:name => :number_of_copy, :presentation => params[:line_item_promotion][:number_of_copy])
        self.line_items_option_values.new :option_value => self.variant.reinforcement_perimeter if params[:line_item_promotion][:reinforcement_perimeter] != "0" && self.variant.is_a?(Spree::BannerVariant)
        self.line_items_option_values.new :option_value => self.variant.eyelet_accessories.find(params[:line_item_promotion][:eyelet]) if params[:line_item_promotion][:eyelet] && !params[:line_item_promotion][:eyelet].blank?
        
      elsif (self.product.is_a?(Spree::BusinessCardProduct)) || (self.product.is_a?(Spree::FlayerProduct)) || (self.product.is_a?(Spree::LetterheadProduct)) || (self.product.is_a?(Spree::PlaybillProduct)) || (self.product.is_a?(Spree::FoldingProduct)) || (self.product.is_a?(Spree::PosterFlyerProduct))
        if self.product.is_a?(Spree::BusinessCardProduct)
          self.line_items_option_values.new :option_value => Spree::OptionValue.find(params[:line_item_promotion][:format])
          self.variant = self.product.master
        else
          self.variant = Spree::Variant.find(params[:line_item_promotion][:format])
        end
        
        folding_format = Spree::FoldingFormat.find_by_option_value_id_and_variant_id(params[:line_item_promotion][:folding], params[:line_item_promotion][:format]) if params[:line_item_promotion][:folding] && !params[:line_item_promotion][:folding].blank?
        
        self.line_items_option_values.new :option_value => Spree::OptionValue.find(params[:line_item_promotion][:orientation])
        self.line_items_option_values.new :option_value => paper_ov
        self.line_items_option_values.new :option_value => paper_ov.paper_weights.where("name = '#{params[:line_item_promotion][:weight].to_s}'").first
        self.line_items_option_values.new :option_value => front_back_ot.option_values.where("name = '#{params[:line_item_promotion][:instructions].to_s}'").first
        self.line_items_option_values.new :option_value => print_color_ot.option_values.where("name = '#{params[:line_item_promotion][:print_color].to_s}'").first
        self.line_items_option_values.new :option_value => Spree::OptionValue.where(:name => params[:line_item_promotion][:plasticization], :option_type_id => Spree::OptionType.find_by_name("plasticization")).first if params[:line_item_promotion][:plasticization] != 'none'
        self.line_items_option_values.new :option_value => folding_format if params[:line_item_promotion][:folding] && !params[:line_item_promotion][:folding].blank?
        self.line_items_option_values.new :option_value => Spree::OptionValue.create(:name => :number_of_copy, :presentation => params[:line_item_promotion][:number_of_copy])
      else
        self.variant = Spree::Variant.find(params[:line_item_promotion][:format])
        # Informazioni per entrambi riguardo la plastificazione
        if params[:line_item_promotion][:plasticization_option] == "Solo copertina - solo fronte"
          plastification = Spree::OptionValue.where(:name => "#{params[:line_item_promotion][:plasticization]} - solo fronte",
                                                                  :option_type_id => Spree::OptionType.find_by_name("plasticization")).first
        elsif params[:line_item_promotion][:plasticization_option] == "Solo copertina - fronte e retro" || params[:line_item_promotion][:plasticization_option] == "Tutto"
          plastification = Spree::OptionValue.where(:name => "#{params[:line_item_promotion][:plasticization]} - fronte e retro",
                                                                  :option_type_id => Spree::OptionType.find_by_name("plasticization")).first
        end
        # Informazioni per la copertina
        if params[:line_item_promotion][:paper_cover] == "-1"
          paper_cover = paper_ov
          cover_weight = paper_ov.paper_weights.where("name = '#{params[:line_item_promotion][:weight].to_s}'").first
        else
          paper_cover = Spree::Paper.find(params[:line_item_promotion][:paper_cover])
          cover_weight = paper_cover.paper_weights.where("name = '#{params[:line_item_promotion][:paper_cover_weight].to_s}'").first
        end
        if params[:line_item_promotion][:instructions].blank?
          front_back = front_back_ot.option_values.where("name = 'different_front_back'").first
          front_back_cover = front_back_ot.option_values.where(:name => "different_front_back").first
        else
          front_back = front_back_ot.option_values.where("name = '#{params[:line_item_promotion][:instructions].to_s}'").first
          front_back_cover = front_back_ot.option_values.where(:name => "#{params[:line_item_promotion][:instructions].to_s}").first
        end
      
        # Aggiungo i dati per la variante
        self.line_items_option_values.new :option_value => paper_ov, :variant => self.variant
        self.line_items_option_values.new :option_value => paper_ov.paper_weights.where("name = '#{params[:line_item_promotion][:weight].to_s}'").first, :variant_parent_id => self.variant.id
        self.line_items_option_values.new :option_value => print_color_ot.option_values.where(:name => params[:line_item_promotion][:print_color].to_s).first, :variant_parent_id => self.variant.id
        self.line_items_option_values.new :option_value => front_back, :variant => variant
        self.line_items_option_values.new :option_value => Spree::OptionValue.create(:name => :number_of_copy, :presentation => params[:line_item_promotion][:number_of_copy]), :variant_parent_id => self.variant.id
        self.line_items_option_values.new :option_value => Spree::OptionValue.create(:name => :number_of_facades, :presentation => params[:line_item_promotion][:number_of_facades]), :variant_parent_id => self.variant.id
        self.line_items_option_values.new :option_value => Spree::SpiralingQuantity.find(params[:line_item_promotion][:spiraling]), :variant_parent_id => variant.id if params[:line_item_promotion][:spiraling]
        self.line_items_option_values.new :option_value => Spree::OptionValue.create(:name => :sleeve, :presentation => params[:line_item_promotion][:sleeve]), :variant_parent_id => self.variant.id if params[:line_item_promotion][:sleeve]
        self.line_items_option_values.new :option_value => plastification, :variant_parent_id => variant.id if params[:line_item_promotion][:plasticization_option] == "Tutto"
        # Aggiungo i dati per la copertina
        self.line_items_option_values.new :option_value => Spree::OptionValue.create(:name => :default_cover, :presentation => params[:line_item_promotion][:default_cover]), :variant_parent_id => self.variant.cover_variant.id if params[:line_item_promotion][:paper_cover] == "-2"
        self.line_items_option_values.new :option_value => Spree::OptionValue.find_by_name('vertical'), :variant_parent_id => self.variant.cover_variant.id
        self.line_items_option_values.new :option_value => front_back_cover, :variant_parent_id => self.variant.cover_variant.id
        self.line_items_option_values.new :option_value => paper_cover, :variant_parent_id => self.variant.cover_variant.id
        self.line_items_option_values.new :option_value => cover_weight, :variant_parent_id => self.variant.cover_variant.id
        self.line_items_option_values.new :option_value => plastification, :variant_parent_id => self.variant.cover_variant.id unless params[:line_item_promotion][:plasticization_option] == "Nessuno"
        self.line_items_option_values.new :option_value => print_color_ot.option_values.where(:name => params[:line_item_promotion][:cover_bw].to_s).first, :variant_parent_id => self.variant.cover_variant.id
      end
      
      # Aggiungo gli adjustment per spedizione e tasse
      #self.shipping_date_adjustment.adjust(self)
      #tax_rate = Spree::TaxRate.first
      #tax_rate.adjust(self)
      
      self.save!
    end
  end
end