# LAMINATURA
module Spree
  class Lamination < OptionType
    after_initialize :add_name_and_presentation
    attr_accessible :active
    has_many :lamination_types, :class_name => "Spree::LaminationType", :foreign_key => "option_value_id", :dependent => :delete_all
    attr_accessible :lamination_types_attributes, :lamination_types, :hourly_cost, :start_time
    
    accepts_nested_attributes_for :lamination_types, :allow_destroy => true
    
    def add_name_and_presentation
      if self.new_record?
        self.name = "lamination"
        self.presentation = "Laminatura"
      end
    end
  end
end