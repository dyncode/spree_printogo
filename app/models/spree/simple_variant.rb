# Variante Semplice
module Spree
  class SimpleVariant < Variant
    before_create :after_init
    belongs_to :product, :touch => true, :class_name => "Spree::SimpleProduct", :foreign_key => "product_id"
    has_many :format_simple_variants, :class_name => "Spree::FormatSimpleVariant", :foreign_key => "variant_id", :dependent => :delete_all
    
    attr_accessible :format_simple_variants, :format_simple_variants_attributes, :format
    
    accepts_nested_attributes_for :format_simple_variants, :allow_destroy => true
		alias_attribute :format, :sku

    def to_preventive
      sim_fv = self.format_simple_variants.order("name ASC").collect{|sfv| {:from => sfv.from, :to => sfv.to}}
      {:id => self.id, :quantity => sim_fv}
    end
    
    private
    def after_init
      if self.new_record?
        self.active = true
        self.on_hand = 1
      end
    end
  end
end