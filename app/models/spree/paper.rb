module Spree
  class Paper < OptionValue
    before_create :associate_option_type
    before_create :set_presentation
		after_save :check_active
		before_destroy :remove_paper_weight

    has_many :paper_weights, :class_name => "Spree::PaperWeight", :foreign_key => "option_value_id", :dependent => :nullify
    has_many :paper_products, :class_name => "Spree::PaperProduct", :foreign_key => "paper_id", :dependent => :delete_all
    has_many :products, :class_name => "Spree::Product", :through => :paper_products, :dependent => :nullify
    
    attr_accessible :name, :presentation, :active, :paper_weights, :paper_weights_attributes,
                    :option_type_id, :option_type

    validates :name, :presence => true

    accepts_nested_attributes_for :paper_weights, :allow_destroy => true

		scope :enable, lambda { where("active = ?", true) }

    def set_presentation
      self.presentation = self.name.downcase.titleize
    end
    
    def self.masters
      where(:option_value_id => nil, :active => true)
    end

    def self.masters_all
      where(:option_value_id => nil)
    end
    
    def get_weight(weight)
      #values.select { |v| v.name == weight.to_s }.first
      values.where(:name => weight.to_s).first
    end

		def check_active
			self.update_active if(!self.active)
		end

		# Se la carta è disattivata, viene controllato se è usata da qualche prodotto, nel caso affermativo venire tolta.
		def update_active
			Spree::PaperProduct.where("paper_id = ?", self.id).each do |pp|
				pp.delete
			end
		end

		def remove_paper_weight
			self.paper_weights.destroy_all
		end

    private
    def associate_option_type
      #self.update_attributes(:presentation => self.name.downcase)
      self.option_type_id = Spree::OptionType.find_by_name('paper').id if !self.option_type.present?
    end
  end
end
