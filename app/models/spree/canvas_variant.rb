# CANVAS VARIANT
module Spree
  class CanvasVariant < Variant
    before_create :after_init
    belongs_to :canvas_product, :class_name => Spree::CanvasProduct, :foreign_key => "product_id"
    attr_accessible :printer, :papers, :frames, :hanger_accessories, :hanger_accessories_attributes, :plotter
    has_many :hanger_accessories, :class_name => "Spree::HangerAccessory", :foreign_key => "variant_id", :dependent => :destroy
    
    accepts_nested_attributes_for :hanger_accessories, :allow_destroy => true
    
    # Accesso agli option values
    def papers
      self.option_values.where(:option_type_id => Spree::OptionType.find_by_name('coil').id)
    end

    def plotter
      self.option_values.where(:option_type_id => Spree::OptionType.find_by_name('plotter').id).last
    end
    
    # Setto i tipi di carta che per questo prodotto sono le coil (BOBINE)
    def papers= values
      self.papers.each do |p|
        unless values.include?(p.id)
          self.option_values.delete(p)
        end
      end

      values.compact.each do |v|
        unless v.blank?
          ov = Spree::OptionValue.find v
          unless self.papers.include?(ov)
            self.option_values << ov
          end
        end
      end
    end
    
    # Setto la stampante che per questo prodotto e` il plotter
    def plotter= value
      self.option_values.delete(self.plotter) if !self.plotter.blank?
      self.option_values << Spree::Plotter.find(value)
    end
    
    def frames
      self.option_values.where(:option_type_id => Spree::OptionType.find_by_name('frame').id)
    end
    
    def frames= values
      self.frames.each do |f|
        unless values.include?(f.id)
          self.option_values.delete(f)
        end
      end

      values.compact.each do |v|
        unless v.blank?
          ov = Spree::OptionValue.find(v)
          unless self.frames.include?(ov)
            self.option_values << ov
          end
        end
      end
    end
    
    private
    def after_init
      if self.new_record?
        self.active = true
        self.on_hand = 1
      end
    end
  end
end