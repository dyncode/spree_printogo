Spree::Product.class_eval do
  has_many :shipping_date_adjustments, :class_name => "Spree::ShippingDateAdjustment", :dependent => :delete_all

	belongs_to :parent, :class_name => 'Spree::Product', :foreign_key => "parent_id"
  belongs_to :poses, :class_name => "Spree::OptionType", :foreign_key => "poses_id"
  has_many :paper_products, :class_name => "Spree::PaperProduct", :dependent => :delete_all
  has_many :papers, :class_name => "Spree::Paper", :through => :paper_products, :dependent => :nullify
  has_many :children, :class_name => "Spree::Product", :foreign_key => "parent_id", :dependent => :delete_all
  has_many :line_item_promotions, :class_name => "Spree::LineItemPromotion", :foreign_key => "product_id", :dependent => :nullify

  has_one :template, :as => :imageable # Corrisponde al template da scaricare per il formato
  has_one :template_format, :as => :imageable # Corrisponde al template per il formato [l'immagine piccola che viene visualizzata sopra il radiobutton]

  # after_update :change_menu
  # after_create :setting_menu
  # before_save :change_permalink

  attr_accessible :marginality, :min_number_of_copies, :number_of_copies,
									:seo_title, :template, :template_attributes, :description2

  accepts_nested_attributes_for :template

  scope :active, lambda { where("available_on <= ?", Time.now) }
  scope :exclude_cover, lambda { where("parent_id IS NULL").order("name ASC") }
  scope :only_printer, lambda { where("type != 'Spree::CoverFlyerProduct' AND type != 'Spree::SimpleProduct' AND type != 'Spree::AdvancedProduct'").order("name ASC") }
  
  def logo_image
    if !self.images.empty?
      self.images.first.attachment.url(:product)
    end
  end
  
  def active?
    self.available_on && self.available_on <= Time.now
  end
  
  def change_menu
    self.menus.each do |menu|
      menu.visible = false if !self.active?
      menu.visible = true if self.active?
      menu.save!
    end
  end
  
  def change_permalink
    self.permalink = self.name.to_s.to_url
  end
  
  def setting_menu
    case self.type
      when "Spree::BusinessCardProduct"
        Spree::Menu.create(:category => "header", :name => self.name.titleize, :presentation => self.name.titleize, :parent => Spree::Menu.only_parent.find_by_category_and_name("header", "Piccolo Formato"), :linkable => self, :position => 1)
        Spree::Menu.create(:category => "sidebar", :name => self.name.titleize, :presentation => self.name.titleize, :parent => Spree::Menu.only_parent.find_by_category_and_name("sidebar", "Piccolo Formato"), :linkable => self, :position => 1)
      when "Spree::FlayerProduct"
        Spree::Menu.create(:category => "header", :name => self.name.titleize, :presentation => self.name.titleize, :parent => Spree::Menu.only_parent.find_by_category_and_name("header", "Piccolo Formato"), :linkable => self, :position => 2)
        Spree::Menu.create(:category => "sidebar", :name => self.name.titleize, :presentation => self.name.titleize, :parent => Spree::Menu.only_parent.find_by_category_and_name("sidebar", "Piccolo Formato"), :linkable => self, :position => 2)
      when "Spree::LetterheadProduct"
        Spree::Menu.create(:category => "header", :name => self.name.titleize, :presentation => self.name.titleize, :parent => Spree::Menu.only_parent.find_by_category_and_name("header", "Piccolo Formato"), :linkable => self, :position => 4)
        Spree::Menu.create(:category => "sidebar", :name => self.name.titleize, :presentation => self.name.titleize, :parent => Spree::Menu.only_parent.find_by_category_and_name("sidebar", "Piccolo Formato"), :linkable => self, :position => 4)
      when "Spree::PostcardProduct"
        Spree::Menu.create(:category => "header", :name => self.name.titleize, :presentation => self.name.titleize, :parent => Spree::Menu.only_parent.find_by_category_and_name("header", "Piccolo Formato"), :linkable => self, :position => 5)
        Spree::Menu.create(:category => "sidebar", :name => self.name.titleize, :presentation => self.name.titleize, :parent => Spree::Menu.only_parent.find_by_category_and_name("sidebar", "Piccolo Formato"), :linkable => self, :position => 5)
      when "Spree::PlaybillProduct"
        Spree::Menu.create(:category => "header", :name => self.name.titleize, :presentation => self.name.titleize, :parent => Spree::Menu.only_parent.find_by_category_and_name("header", "Piccolo Formato"), :linkable => self, :position => 6)
        Spree::Menu.create(:category => "sidebar", :name => self.name.titleize, :presentation => self.name.titleize, :parent => Spree::Menu.only_parent.find_by_category_and_name("sidebar", "Piccolo Formato"), :linkable => self, :position => 6)
      when "Spree::PosterProduct"
        Spree::Menu.create(:category => "header", :name => self.name.titleize, :presentation => self.name.titleize, :parent => Spree::Menu.only_parent.find_by_category_and_name("header", "Manifesto"), :linkable => self, :position => 1)
        Spree::Menu.create(:category => "sidebar", :name => self.name.titleize, :presentation => self.name.titleize, :parent => Spree::Menu.only_parent.find_by_category_and_name("sidebar", "Manifesto"), :linkable => self, :position => 1)
      when "Spree::PosterFlyerProduct"
        Spree::Menu.create(:category => "header", :name => self.name.titleize, :presentation => self.name.titleize, :parent => Spree::Menu.only_parent.find_by_category_and_name("header", "Manifesto"), :linkable => self, :position => 2)
        Spree::Menu.create(:category => "sidebar", :name => self.name.titleize, :presentation => self.name.titleize, :parent => Spree::Menu.only_parent.find_by_category_and_name("sidebar", "Manifesto"), :linkable => self, :position => 2)
      when "Spree::PosterHighQualityProduct"
        Spree::Menu.create(:category => "header", :name => self.name.titleize, :presentation => self.name.titleize, :parent => Spree::Menu.only_parent.find_by_category_and_name("header", "Manifesto"), :linkable => self, :position => 3)
        Spree::Menu.create(:category => "sidebar", :name => self.name.titleize, :presentation => self.name.titleize, :parent => Spree::Menu.only_parent.find_by_category_and_name("sidebar", "Manifesto"), :linkable => self, :position => 3)
      when "Spree::PaperbackProduct"
        Spree::Menu.create(:category => "header", :name => self.name.titleize, :presentation => self.name.titleize, :parent => Spree::Menu.only_parent.find_by_category_and_name("header", "Brossura"), :linkable => self, :position => 1)
        Spree::Menu.create(:category => "sidebar", :name => self.name.titleize, :presentation => self.name.titleize, :parent => Spree::Menu.only_parent.find_by_category_and_name("sidebar", "Brossura"), :linkable => self, :position => 1)
      when "Spree::StapleProduct"
        Spree::Menu.create(:category => "header", :name => self.name.titleize, :presentation => self.name.titleize, :parent => Spree::Menu.only_parent.find_by_category_and_name("header", "Multipagina"), :linkable => self, :position => 2)
        Spree::Menu.create(:category => "sidebar", :name => self.name.titleize, :presentation => self.name.titleize, :parent => Spree::Menu.only_parent.find_by_category_and_name("sidebar", "Multipagina"), :linkable => self, :position => 2)
      when "Spree::FoldingProduct"
        Spree::Menu.create(:category => "header", :name => self.name.titleize, :presentation => self.name.titleize, :parent => Spree::Menu.only_parent.find_by_category_and_name("header", "Piccolo Formato"), :linkable => self, :position => 3)
        Spree::Menu.create(:category => "sidebar", :name => self.name.titleize, :presentation => self.name.titleize, :parent => Spree::Menu.only_parent.find_by_category_and_name("sidebar", "Piccolo Formato"), :linkable => self, :position => 3)
      when "Spree::BannerProduct"
        Spree::Menu.create(:category => "header", :name => self.name.titleize, :presentation => self.name.titleize, :parent => Spree::Menu.only_parent.find_by_category_and_name("header", "Grande Formato"), :linkable => self, :position => 3)
        Spree::Menu.create(:category => "sidebar", :name => self.name.titleize, :presentation => self.name.titleize, :parent => Spree::Menu.only_parent.find_by_category_and_name("sidebar", "Grande Formato"), :linkable => self, :position => 3)
      when "Spree::PvcProduct"
        Spree::Menu.create(:category => "header", :name => self.name.titleize, :presentation => self.name.titleize, :parent => Spree::Menu.only_parent.find_by_category_and_name("header", "Grande Formato"), :linkable => self, :position => 4)
        Spree::Menu.create(:category => "sidebar", :name => self.name.titleize, :presentation => self.name.titleize, :parent => Spree::Menu.only_parent.find_by_category_and_name("sidebar", "Grande Formato"), :linkable => self, :position => 4)
      when "Spree::SpiralProduct"
        Spree::Menu.create(:category => "header", :name => self.name.titleize, :presentation => self.name.titleize, :parent => Spree::Menu.only_parent.find_by_category_and_name("header", "Multipagina"), :linkable => self, :position => 3)
        Spree::Menu.create(:category => "sidebar", :name => self.name.titleize, :presentation => self.name.titleize, :parent => Spree::Menu.only_parent.find_by_category_and_name("sidebar", "Multipagina"), :linkable => self, :position => 3)
      when "Spree::CanvasProduct"
        Spree::Menu.create(:category => "header", :name => self.name.titleize, :presentation => self.name.titleize, :parent => Spree::Menu.only_parent.find_by_category_and_name("header", "Grande Formato"), :linkable => self, :position => 2)
        Spree::Menu.create(:category => "sidebar", :name => self.name.titleize, :presentation => self.name.titleize, :parent => Spree::Menu.only_parent.find_by_category_and_name("sidebar", "Grande Formato"), :linkable => self, :position => 2)
      when "Spree::RigidProduct"
        Spree::Menu.create(:category => "header", :name => self.name.titleize, :presentation => self.name.titleize, :parent => Spree::Menu.only_parent.find_by_category_and_name("header", "Grande Formato"), :linkable => self, :position => 5)
        Spree::Menu.create(:category => "sidebar", :name => self.name.titleize, :presentation => self.name.titleize, :parent => Spree::Menu.only_parent.find_by_category_and_name("sidebar", "Grande Formato"), :linkable => self, :position => 5)
       when "Spree::StickerProduct"
        Spree::Menu.create(:category => "header", :name => self.name.titleize, :presentation => self.name.titleize, :parent => Spree::Menu.only_parent.find_by_category_and_name("header", "Grande Formato"), :linkable => self, :position => 6)
        Spree::Menu.create(:category => "sidebar", :name => self.name.titleize, :presentation => self.name.titleize, :parent => Spree::Menu.only_parent.find_by_category_and_name("sidebar", "Grande Formato"), :linkable => self, :position => 6)
      when "Spree::SimpleProduct"
        Spree::Menu.create(:category => "header", :name => self.name.titleize, :presentation => self.name.titleize, :parent => Spree::Menu.only_parent.find_by_category_and_name("header", "Prodotti Ready"), :linkable => self, :position => 1)
        Spree::Menu.create(:category => "sidebar", :name => self.name.titleize, :presentation => self.name.titleize, :parent => Spree::Menu.only_parent.find_by_category_and_name("sidebar", "Prodotti Ready"), :linkable => self, :position => 1)
    end
  end
  
  def retrive_formats
    res = []
    case self.type
      when 'Spree::BusinessCardProduct'
        res = self.printer_format_card_option_type.option_values.collect{|ov| [ov.presentation.titleize, ov.id]}
      when 'Spree::FlayerProduct'
        res = self.flayer_variants.collect{|fv| [fv.format.titleize, fv.id]}
      when 'Spree::LetterheadProduct'
        res = self.letterhead_variants.collect{|fv| [fv.format.titleize, fv.id]}
      when 'Spree::PlaybillProduct'
        res = self.playbill_variants.collect{|fv| [fv.format.titleize, fv.id]}
      when 'Spree::PostcardProduct'
        res = self.postcard_variants.collect{|fv| [fv.format.titleize, fv.id]}
      when 'Spree::PosterProduct'
        res = self.poster_printer_formats.collect{|ov| [ov.presentation.titleize, ov.id]}
      when 'Spree::PosterHighQualityProduct'
        res = self.poster_high_quality_printer_formats.collect{|ov| [ov.presentation.titleize, ov.id]}
      when 'Spree::BannerProduct'
        res = [["personalizzato", "0"]]
      when 'Spree::PvcProduct'
        res = [["personalizzato", "0"]]
      when 'Spree::RigidProduct'
        res = [["personalizzato", "0"]]
      when 'Spree::StickerProduct'
        res = [["personalizzato", "0"]]
      when 'Spree::PaperbackProduct'
        res = self.paperback_variants.collect{|pv| [pv.format.titleize, pv.id]}
      when 'Spree::StapleProduct'
        res = self.staple_variants.collect{|sv| [sv.format.titleize, sv.id]}
      when 'Spree::CanvasProduct'
        res = [["personalizzato", "0"]]
      when 'Spree::FoldingProduct'
        res = self.folding_variants.collect{|fv| [fv.format.titleize, fv.id]}
      when 'Spree::SpiralProduct'
        res = self.spiral_variants.collect{|sv| [sv.format.titleize, sv.id]}
      when 'Spree::PosterFlyerProduct'
        res = self.poster_flyer_variants.collect{|pfv| [pfv.format.titleize, pfv.id]}
    end
    res
  end
  
  def retrive_papers
    res = []
    case self.type
      when 'Spree::BusinessCardProduct'
        res = self.get_papers
      when 'Spree::PosterProduct'
        res = self.papers.collect{|p| [p.name, p.id]}
      when 'Spree::PosterHighQualityProduct'
        res = self.papers.collect{|p| [p.name, p.id]}
      when 'Spree::CanvasProduct'
        res = self.canvas_variant.papers.collect{|p| [p.name, p.id]}
      when 'Spree::BannerProduct'
        res = self.banner_variant.papers.collect{|p| [p.name, p.id]}
      when 'Spree::PvcProduct'
        res = self.pvc_variant.papers.collect{|p| [p.name, p.id]}
      else
        res = []
    end
    res
  end
  
  def retrive_covers
    case self.type
      when 'Spree::PaperbackProduct'
        res = true
      when 'Spree::StapleProduct'
        res = true
      when 'Spree::SpiralProduct'
        res = true
      else
        res = false
    end
    res
  end
  
  def retrive_facades
    case self.type
      when 'Spree::PaperbackProduct'
        res = true
      when 'Spree::StapleProduct'
        res = true
      when 'Spree::SpiralProduct'
        res = true
      else
        res = false
    end
    res
  end
  
  def retrive_plasticization
    case self.type
      when 'Spree::BusinessCardProduct'
        res = true
      when 'Spree::FlayerProduct'
        res = true
      when 'Spree::LetterheadProduct'
        res = true
      when 'Spree::PostcardProduct'
        res = true
      when 'Spree::PlaybillProduct'
        res = true
      when 'Spree::PosterFlyerProduct'
        res = true
      when 'Spree::PaperbackProduct'
        res = true
      else
        res = false
    end
    res
  end
  
  def retrive_paper_cover
    case self.type
      when 'Spree::StapleProduct'
        res = [["Stessa grammatura dell'interno", -1]]
      when 'Spree::SpiralProduct'
        res = [["Nessuna", -2],["Stessa grammatura dell'interno", -1]]
      else
        res = []
    end
    res
  end
  
  def retrive_printer_instruction
    case self.type
      when 'Spree::BannerProduct'
        res = "front"
      when 'Spree::PvcProduct'
        res = "front"
      when 'Spree::CanvasProduct'
        res = "front"
      when 'Spree::PosterProduct'
        res = "front"
      when 'Spree::PosterHighQualityProduct'
        res = "front"
      when 'Spree::RigidProduct'
        res = "front"
      when 'Spree::StickerProduct'
        res = "front"
      else
        res = nil
    end
    res
  end
end