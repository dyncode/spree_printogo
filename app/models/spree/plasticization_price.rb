module Spree
  class PlasticizationPrice < OptionValue
    attr_accessible :quantity
    belongs_to :plasticization, :class_name => "Spree::Plasticization", :foreign_key => "option_value_id"
    
    def price
      self.presentation.to_f
    end
  end
end