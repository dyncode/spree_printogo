module Spree
  class LineItemsOptionValues < ActiveRecord::Base
    belongs_to :option_value
    belongs_to :line_item
    belongs_to :line_item_promotion, :class_name => 'Spree::LineItemPromotion', :foreign_key => "line_item_id", :dependent => :delete
    belongs_to :variant, :class_name => 'Spree::Variant', :foreign_key => "variant_parent_id"

    attr_accessible :option_value, :line_item, :variant, :option_value_id, :variant_parent_id, :line_item_id

    def clone
      self.class.new(self.attributes.except('id', 'updated_at', 'created_at', 'line_item_id'))
    end
  end
end