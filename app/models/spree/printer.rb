require "custom_logger"
module Spree
  class Printer < OptionValue
    after_save :associate_option_type
    before_save :check_printer_type

    has_and_belongs_to_many :printer_formats, :class_name => 'Spree::PrinterFormatDefault'

    attr_accessible :start_up_cost_c, :plant_color_c, :price_front_c, :price_front_and_back_c,
                    :start_up_cost_bw, :plant_color_bw, :price_front_bw, :price_front_and_back_bw,
                    :formats, :printer_hourly_cost_bw, :start_time_bw, :average_hourly_print_bw, :cost_ink_bw,
                    :printer_hourly_cost_c, :start_time_c, :average_hourly_print_c, :cost_ink_c, :printer_formats,
                    :waste_paper_bw, :waste_paper_c, :printer_type, :printer_formats, :printer_formats_attributes,
                    :color_type, :min_limit_waiting, :cost_waiting, :printer_format_ids

    accepts_nested_attributes_for :printer_formats, :allow_destroy => true
    
    scope :digital, {:conditions => {:name => "digital"}}
    scope :offset, {:conditions => {:name => "offset"}}
    
    def printer_type
      self.name || "digital"
    end

    def printer_type= value
      self.name= value
    end

    def digital?
      self.name == "digital"
    end

    def color_4?
      self.color_type == 4
    end

    def color_8?
      self.color_type == 8
    end

    #def printer_formats
    #  Spree::OptionValue.where(:name => name, :option_type_id => Spree::OptionType.find_by_name('printer_format').id)
    #end
    #
    #def printer_formats=(value)
    #  if printer_formats.where(:presentation => value).count == 0
    #    Spree::OptionValue.create :name => name,
    #                              :presentation => value,
    #                              :option_type_id => Spree::OptionType.find_by_name('printer_format').id
    #  end
    #end


    #def associate_formats
    #  Printer.formats.each do |format|
    #    self.printer_formats=format
    #  end
    #end
    
    def price_from_back(color, front_back)
      if front_back == "front_back" || front_back == "different_front_back"
        if color
          self.price_front_and_back_c.to_f
        else
          self.price_front_and_back_bw.to_f
        end
      else
        if color
          self.price_front_c.to_f
        else
          self.price_front_bw.to_f
        end
      end
    end

    # Calcola il costo di avviamento sia nel caso della stampante a colori sia in quello bianco e nero.
    def start_up_cost(color = false)
      # Calcolandolo nel seguent modo:
      # (costo_orario * tempo_avviamento)/60
      if color
        (Float(self.printer_hourly_cost_c) * Float(self.start_time_c))/60.to_f
      else
        (Float(self.printer_hourly_cost_bw) * Float(self.start_time_bw))/60.to_f
      end
    end

    # Calcola la media orari ossia la finitura
    def finish(number_of_sheet, paper_init_format, print_color, weight)
      # Calcolo il costo di finitura: che è dato dai seguenti valori:
      #   - printer_hourly_cost
      #   - average_hourly_print (colori o bw)
      #   - number_of_sheet
      #   (number_of_sheet /  average_hourly_print) *  printer_hourly_cost
      total = 0
      if print_color # Se stampo a colori
        if self.digital? && self.average_hourly_print_c > 0 # Se sono in digitale devo controllare se ho il costo della risma
          total = Float(self.printer_hourly_cost_c) * number_of_sheet / self.average_hourly_print_c.to_f if self.average_hourly_print_c > 0
        else # Se sono in offset prendo di sicuro il costo a tagliato
          total =  Float(self.printer_hourly_cost_c) * (number_of_sheet / Float(self.average_hourly_print_c)) if self.average_hourly_print_c > 0
        end
      else
        if self.digital? && self.average_hourly_print_bw > 0 # Se sono in digitale devo controllare se ho il costo della risma
          total = Float(self.printer_hourly_cost_c) * (number_of_sheet / Float(self.average_hourly_print_bw)) if self.average_hourly_print_bw > 0
        else # Se sono in offset prendo di sicuro il costo a tagliato
          total = Float(self.printer_hourly_cost_bw) * (number_of_sheet / Float(self.average_hourly_print_bw)) if self.average_hourly_print_bw > 0
        end
      end
      total
    end

    # Calcola il costo degli impianti, in base al numero degli impianti e inoltre calcola il costo di avviamento per
    # ogni singolo impianto nel caso la stampante sia 4 colori o goni 2 in caso la stampante sia a 8 colori
    def plant_prices(number_of_plant, print_color = false)
      total = 0
      if print_color
        total = Integer(number_of_plant) * self.plant_color_c
      else
        total = Integer(number_of_plant) * self.plant_color_bw
      end
      total
    end

    # Calcolo il costo di attesa se il numero di copiè è più basso del limite.
    def calculate_cost_waiting(number_of_copy, print_color = false)
      if (self.digital? || self.color_4?) && Integer(number_of_copy) <= self.min_limit_waiting
        if print_color
          # CUSTOM_LOGGER.info "ATTESA COLORI: #{(Float(self.printer_hourly_cost_c) * Float(self.cost_waiting))/60.to_f} ***********************************"
          return (Float(self.printer_hourly_cost_c) * Float(self.cost_waiting))/60.to_f
        else
          # CUSTOM_LOGGER.info "ATTESA BIANO/NERO: #{(Float(self.printer_hourly_cost_bw) * Float(self.cost_waiting))/60.to_f} ***********************************"
          return (Float(self.printer_hourly_cost_bw) * Float(self.cost_waiting))/60.to_f
        end
      else
        # CUSTOM_LOGGER.info "ATTESA: 0 ***********************************"
        return  0
      end
    end

    # Calcola il costo dello spreco di fogli per fronte o fronte/retro
    def start_up_waste_paper_cost(front_back, print_color, price_per_sheet)
      total = 0
      if print_color # Se sono a colori
        if front_back # Se sono fronte e retro calcolo il costo delle passate sul foglio e il costo della carta che spreco
          total += (self.price_front_and_back_c.to_f * self.waste_paper_c) + (price_per_sheet * self.waste_paper_c)
        else # Se sono solo su un lato
          total += (self.price_front_c.to_f * self.waste_paper_c) + (price_per_sheet * self.waste_paper_c)
        end
      else # Se sono in biano e nero
        if front_back # Se sono fronte e retro calcolo il costo delle passate sul foglio e il costo della carta che spreco
          total += (self.price_front_and_back_bw.to_f * self.waste_paper_bw) + (price_per_sheet * self.waste_paper_bw)
        else # Se sono solo su un lato
          total += (self.price_front_bw.to_f * self.waste_paper_bw) + (price_per_sheet * self.waste_paper_bw)
        end
      end
      # CUSTOM_LOGGER.info "SPRECO: #{total} ***********************************"
      
      total
    end
    
    # Dato che per i multipagina si stampa sempre front e il calcolo risulta più semplice
    def price_print_multi_page(print_color, number_of_sheet, paper_init_format, weight, price)
      if print_color # Se stampo a colori
          number_of_sheet * price
      else # Se sono in biano e nero
				number_of_sheet * price
      end
    end
    
    # Calcolo tutti i costi della stampante
    # COSTI AVVIAMENTO
    # COSTI SPRECO AVVIAMENTO
    # COSTI ATTESA
    # COSTI FINITURA
    # COSTI IMPIANTI
    def print_cost(line_params, plant_params, step)
      # CUSTOM_LOGGER.info "SONO IN print_cost ***********************************"
      total = 0
      total += self.price_print_multi_page(line_params[:print_color_bool], plant_params[:number_of_sheet], line_params["paper_init_format_#{step}".to_sym], line_params[:weight], line_params["price_from_back_#{step}".to_sym])
      # CUSTOM_LOGGER.info "Aggiungo il costo della stampa colori/biano e nero: #{total} ***********************************"
      
      # Check nel caso siamo con la 8 COLORI CON SCARTO
      if plant_params[:system_params].blank?
        
      end
      # CUSTOM_LOGGER.info "COSTO FOGLIO: #{line_params["price_per_sheet_#{step}".to_sym]} ***********************************"
      # CUSTOM_LOGGER.info "NUMERO FOGLI: #{plant_params[:number_of_sheet]} ***********************************"
      # CUSTOM_LOGGER.info "FORMATO: #{(1/line_params["paper_init_format_#{step}".to_sym][:quantity].to_f)} ***********************************"
      # CUSTOM_LOGGER.info "Calcolo il costo della carta: #{(line_params["price_per_sheet_#{step}".to_sym] * (plant_params[:number_of_sheet]*(1/line_params["paper_init_format_#{step}".to_sym][:quantity].to_f)))} ***********************************"
      total += (line_params["price_per_sheet_#{step}".to_sym] * (plant_params[:number_of_sheet]*(1/line_params["paper_init_format_#{step}".to_sym][:quantity].to_f)))
      # TODO va fatto un ciclo per verificare il numero di avviamento e il numero di attese
      #plant_params[:system_params].each do |pp|
      if self.digital?
        total += self.start_up_cost(line_params[:print_color_bool])
        # CUSTOM_LOGGER.info "total per i costi di avviamento stampa: #{total} ***********************************"
        total += self.start_up_waste_paper_cost((line_params[:front_back].name == "front_back" || line_params[:front_back].name == "different_front_back") ? true : false, line_params[:print_color_bool], line_params["price_per_sheet_#{step}".to_sym])
        # CUSTOM_LOGGER.info "total per i costi di spreco stampa: #{total} ***********************************"
      elsif self.color_8?
        plants_8 = plant_params[:plants]/2
        # CUSTOM_LOGGER.info "plants_8: #{plants_8} ***********************************"
        total += (self.start_up_cost(line_params[:print_color_bool]) * plants_8)
        # CUSTOM_LOGGER.info "total per i costi di avviamento stampa: #{total} ***********************************"
        total += (self.start_up_waste_paper_cost((line_params[:front_back].name == "front_back" || line_params[:front_back].name == "different_front_back") ? true : false, line_params[:print_color_bool], line_params["price_per_sheet_#{step}".to_sym]) * plants_8)
        # CUSTOM_LOGGER.info "total per i costi di spreco stampa: #{total} ***********************************"
      else
        total += (self.start_up_cost(line_params[:print_color_bool]) * plant_params[:plants])
        # CUSTOM_LOGGER.info "total per i costi di avviamento stampa: #{total} ***********************************"
        total += (self.start_up_waste_paper_cost((line_params[:front_back].name == "front_back" || line_params[:front_back].name == "different_front_back") ? true : false, line_params[:print_color_bool], line_params["price_per_sheet_#{step}".to_sym]) * plant_params[:plants])
        # CUSTOM_LOGGER.info "total per i costi di spreco stampa: #{total} ***********************************"
        wait_4 = 0
        plant_params[:system_params].each do |pp|
          wait_4 += 1 if pp[:copy_for_sheet] > pp[:number_of_sheet]
        end
        # CUSTOM_LOGGER.info "wait_4: #{wait_4} ***********************************"
        total += (self.calculate_cost_waiting(plant_params[:number_of_sheet], line_params[:print_color_bool]) * wait_4)
        # CUSTOM_LOGGER.info "total per i costi di attesa stampa: #{total} ***********************************"
      end
      
      total += self.finish(line_params[:number_of_quarters], line_params["paper_init_format_#{step}".to_sym], line_params[:print_color_bool], line_params[:weight])
      # CUSTOM_LOGGER.info "total per i costi del tempo di stampa: #{total} ***********************************"
      #end
      # FINE CICLO
      
      # Calcolo il costo degli impianti (se presenti)
      if !self.digital? && plant_params[:plants] && !plant_params[:plants].blank?
        total += self.plant_prices(plant_params[:plants], line_params[:print_color_bool])
      end
      # CUSTOM_LOGGER.info "total aggiunti anche i costi per gli impianti: #{total} ***********************************"
      total
    end

    def self.formats
      # Questi sono quelli ufficiali
      %w(32x44 35x50 44x64 50x70 64x88 70x100)
    end

    private
    def associate_option_type
      self.update_attribute(:option_type_id, Spree::OptionType.find_by_name('printer').id) if self.option_type.nil?
    end

    def check_printer_type
      if (self.name == "digital")
        self.color_type = nil
        self.plant_color_bw = 0
        self.plant_color_c = 0
      end
    end
  end
end