Spree::User.class_eval do
	belongs_to :bill_address, class_name: 'Spree::Address'
	has_many :user_promotions, :class_name => "Spree::UserPromotion", :dependent => :delete_all
	belongs_to :ship_address, class_name: 'Spree::Address'
	accepts_nested_attributes_for :user_promotions, :allow_destroy => true
	accepts_nested_attributes_for :bill_address, allow_destroy: true
	accepts_nested_attributes_for :ship_address, allow_destroy: true
  
	attr_accessible :user_promotions_attributes, :user_promotions, :bill_address_attributes, :bill_address, :privacy

	def update_with_password(params = {})
		params.delete(:password) if params[:password].blank?
		params.delete(:password_confirmation) if params[:password_confirmation].blank?
		update_attributes(params)
	end
end