# Variante Avanzata
module Spree
  class AdvancedVariant < Variant
    before_create :after_init
    belongs_to :advanced_product, :touch => true, :class_name => "Spree::AdvancedProduct", :foreign_key => "product_id"
    has_many :entities, :class_name => "Spree::Entity", :foreign_key => "variant_id", :dependent => :delete_all
    has_many :simple_attributes, :class_name => "Spree::SimpleAttribute", :foreign_key => "variant_id", :dependent => :delete_all
    
    attr_accessible :entities, :entities_attributes, :format, :simple_attributes, :simple_attributes_attributes
    
    accepts_nested_attributes_for :entities, :allow_destroy => true
    accepts_nested_attributes_for :simple_attributes, :allow_destroy => true

		alias_attribute :format, :sku

    private
    def after_init
      if self.new_record?
        self.active = true
        self.on_hand = 1
      end
    end
  end
end