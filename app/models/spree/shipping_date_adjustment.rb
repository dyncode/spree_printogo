########################################################################################################################
# Questa classi si occupa di rappresentare entro quale data il cliente ha intezione di fari recapitare la merce.
# Nel caso voglia la merce il prima possibile, gli verrà imposto il prezzo pieno, altrimenti gli verrà applicato un
# aggiustamento (che sarà quasi sempre negativo, ma impostabile da back end).
# Più in dettaglio questa classe ha un calculator che tramite il metodo adjust(order) associa all'ordine l'aggiustamento
# seglieto.
########################################################################################################################
module Spree
  class ShippingDateAdjustment < ActiveRecord::Base
    belongs_to :product
    has_many :line_items, :class_name => "Spree::LineItem"
    has_many :line_item_promotions, :class_name => "Spree::LineItemPromotion"
    
    attr_accessible :name, :product_id, :days, :preferred_amount
    validates :name, :product_id, :days, :preferred_amount, :presence => true
    validates_uniqueness_of :days, :scope => [:product_id]
    
    calculated_adjustments

    def self.get_shipping_date_adjustment_by_product product
        ShippingDateAdjustment.where :product_id => product.id
    end

    def init_calculator
      c = generate_calculator 0
      self.calculator = c
    end

    def generate_calculator amount
      c = Spree::Calculator::ShippingLineItem.new
      c.calculable = self
      c.preferred_amount = amount
      c
    end

    def adjust(order)
      # Prima cancello gli eventuali altri calculator associati e poi lo aggiungo
      order.adjustments.where(:originator_type => 'Spree::ShippingDateAdjustment').each { |ad| ad.destroy }
      order.adjustments.create({:amount => amount(order),
                                :source => order,
                                :originator => self,
                                :locked => true,
                                :label => "Spedizione: #{self.days.to_i} Giorni"}, :without_protection => true)
    end

    def amount(order)
      calculator.compute(order)
    end

    def preferred_amount
      calculator.preferred_amount
    end

    def preferred_amount=(value)
      init_calculator if calculator.nil?
      calculator.preferred_amount = value
    end

    def self.shipping_date_adjustment_by_product product
      Spree::ShippingDateAdjustment.where(:product_id => product.id).order("days")
    end

  end
end
