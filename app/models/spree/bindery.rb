require "custom_logger"
# LEGATORIA (comprende al suo interno la piegatura [BENDING], la punzonatura o punto metallico [PUNCHING] e la cucitura [SEAM])
module Spree
  class Bindery < OptionValue
    after_save :associate_option_type
    has_many :bendings, :class_name => "Spree::Bending", :foreign_key => "option_value_id", :dependent => :delete_all
    has_many :punchings, :class_name => "Spree::Punching", :foreign_key => "option_value_id", :dependent => :delete_all
    has_one :seam, :class_name => "Spree::Seam", :foreign_key => "option_value_id", :dependent => :delete
    has_one :sleeve, :class_name => "Spree::Sleeve", :foreign_key => "option_value_id", :dependent => :delete
    has_one :milling, :class_name => "Spree::Milling", :foreign_key => "option_value_id", :dependent => :delete

    attr_accessible :name, :external, :hourly_cost_bending, :hourly_cost_punching, :hourly_cost_seam,
                    :bendings_attributes, :punchings_attributes, :seam_attributes, :bendings, :seam,
                    :punchings, :min_limit_bending, :cost_bending, :min_limit_punching, :cost_punching,
                    :min_limit_seam, :cost_seam, :copy_cost_punching, :cost_sleeve, :min_limit_sleeve,
                    :hourly_cost_sleeve, :sleeve, :sleeve_attributes, :copy_cost_seam, :milling, 
                    :milling_attributes, :copy_cost_milling, :hourly_cost_milling, :min_limit_milling,
                    :cost_milling, :seam_attributes

    validates :name, :presence => true
    validates_inclusion_of :external, :in => [true, false]

    accepts_nested_attributes_for :bendings, :allow_destroy => true
    accepts_nested_attributes_for :punchings, :allow_destroy => true
    accepts_nested_attributes_for :seam, :allow_destroy => true
    accepts_nested_attributes_for :sleeve, :allow_destroy => true
    accepts_nested_attributes_for :milling, :allow_destroy => true

    def external
      self.is_cut || false
    end
    
    def copy_cost_punching
      self.active || false
    end

    def hourly_cost_bending
      self.start_up_cost_c || 0
    end

    def hourly_cost_punching
      self.plant_color_c || 0
    end

    def hourly_cost_seam
      self.price_front_c || 0
    end
    
    def hourly_cost_sleeve
      self.price_front_and_back_c || 0
    end

    def min_limit_bending
      self.start_time_c || 0
    end

    def min_limit_punching
      self.start_time_bw || 0
    end

    def min_limit_seam
      self.average_hourly_print_c || 0
    end
    
    def min_limit_sleeve
      self.average_hourly_print_bw || 0
    end

    def cost_bending
      self.start_up_cost_bw || 0
    end

    def cost_punching
      self.plant_color_bw || 0
    end

    def cost_seam
      self.price_front_bw || 0
    end
    
    def cost_sleeve
      self.price_front_and_back_bw || 0
    end

    def external= value
      self.is_cut = value
    end
    
    def copy_cost_punching= value
      self.active = value
    end

    def hourly_cost_bending= value
      self.start_up_cost_c = value
    end

    def hourly_cost_punching= value
      self.plant_color_c = value
    end

    def hourly_cost_seam= value
      self.price_front_c = value
    end
    
    def hourly_cost_sleeve= value
      self.price_front_and_back_c = value
    end

    def min_limit_bending= value
      self.start_time_c = value
    end

    def min_limit_punching= value
      self.start_time_bw = value
    end

    def min_limit_seam= value
      self.average_hourly_print_c = value
    end
    
    def min_limit_sleeve= value
      self.average_hourly_print_bw = value
    end

    def cost_bending= value
      self.start_up_cost_bw = value
    end

    def cost_punching= value
      self.plant_color_bw = value
    end

    def cost_seam= value
      self.price_front_bw = value
    end
    
    def cost_sleeve= value
      self.price_front_and_back_bw = value
    end
    
    # Calcola il costo della piegatura
    # parametri:
    #   - Integer(line_params[:number_of_copy].presentation) => Integer(number_of_copy.presentation)
    #   - line_params[:system_params] => I parametri ricavati tramite una delle funzioni di calcolo impianti e fogli
    #   - line_params[:waste] => true se c'è scarto false altrimenti
    def compute_working_bending(line_params, bending_cover = true)
      number_of_copy = line_params[:number_of_copy].presentation.to_i
      # CUSTOM_LOGGER.info "number_of_copy: #{line_params[:number_of_copy].presentation} *********************************"
      if self.external############################ SONO CON LA LEGATORIA ESTERNA
        # CUSTOM_LOGGER.info "PIEGO IN ESTERNA ***********************************"
        # Aggiungo il costo per il numero minimo di copie          
        if number_of_copy <= self.min_limit_bending
          line_params[:total] += self.cost_bending.to_f
          # CUSTOM_LOGGER.info "totale dopo aggiunta di costo minimo per piegatura: #{line_params[:total]} *********************************"
        end
        
        line_params[:system_params].each do |h|
          bending = self.bendings.select { |p| p.type_quantity.to_i == h[:quarter] }.first
          if bending
            line_params[:total] += bending.copy_cost.to_f * h[:number_of_sheet].to_i * (number_of_copy/h[:copy_for_sheet].to_i)
            # CUSTOM_LOGGER.info "numero di fogli diviso numero di copie per foglio: #{number_of_copy/h[:copy_for_sheet].to_i} *********************************"
          end
        end
        # CUSTOM_LOGGER.info "totale dopo aggiunta costo piegatura: #{line_params[:total]} *********************************"
        
        # Calcolo la copertina in esterna
        if bending_cover
          bending = self.bendings.select { |p| p.type_quantity.to_i == 4 }.first
          if bending
            line_params[:total] += bending.copy_cost.to_f * number_of_copy
          end
        end
        # CUSTOM_LOGGER.info "totale dopo aggiunta costo piegatura copertina: #{line_params[:total]} *********************************"
      else############################ SONO CON LA LEGATORIA INTERNA
        # CUSTOM_LOGGER.info "PIEGO IN INTERNA ***********************************"
        line_params[:system_params].each do |h|
          bending = self.bendings.select { |p| p.type_quantity.to_i == h[:quarter] }.first
          # CUSTOM_LOGGER.info "bending: #{bending.type_quantity} ***********************************"
          if bending
            # Calcolo il costo di avviamento nel seguente modo:
            # (costo_orario * tempo_avviamento)/60
            line_params[:total] += (Float(self.hourly_cost_bending) * Float(bending.start_time)) / 60
            # CUSTOM_LOGGER.info "totale dopo aggiunta avviamento: #{line_params[:total]} ***********************************"
            # Devo calcolare il costo per le copie: (number_of_sheet /  average_hourly_print) *  printer_hourly_cost
            line_params[:total] += Float(self.hourly_cost_bending) * ((h[:number_of_sheet].to_i * (number_of_copy/h[:copy_for_sheet].to_i)) / Float(bending.average_hourly_bending))
            # CUSTOM_LOGGER.info "totale dopo aggiunta costo per copie: #{line_params[:total]} ***********************************"
          end
        end
        # CUSTOM_LOGGER.info "totale dopo aggiunta costo piegatura: #{line_params[:total]} *********************************"
        
        # Calcolo la copertina in interna
        if bending_cover
          bending = self.bendings.select { |p| p.type_quantity.to_i == 4 }.first
          if bending
            # Calcolo il costo di avviamento nel seguente modo:
            # (costo_orario * tempo_avviamento)/60
            line_params[:total] += (Float(self.hourly_cost_bending) * Float(bending.start_time)) / 60
            # Devo calcolare il costo per le copie: (number_of_sheet /  average_hourly_print) *  printer_hourly_cost
            line_params[:total] += Float(self.hourly_cost_bending) * (number_of_copy / Float(bending.average_hourly_bending))
          end
        end
        # CUSTOM_LOGGER.info "totale dopo aggiunta costo piegatura copertina: #{line_params[:total]} *********************************"
      end
      line_params
    end

    # Calcola il costo del punto metallico
    # parametri:
    #   - Integer(line_params[:number_of_copy].presentation) => Integer(number_of_copy.presentation)
    #   - line_params[:system_params] => I parametri ricavati tramite una delle funzioni di calcolo impianti e fogli
    def compute_working_punching(line_params)
      number_of_copy = Integer(line_params[:number_of_copy].presentation)
      # CUSTOM_LOGGER.info "number_of_copy: #{line_params[:number_of_copy].presentation} *********************************"
      if self.external############################ SONO CON LA LEGATORIA ESTERNA
        # CUSTOM_LOGGER.info "PUNZONO IN ESTERNA *********************************"
        # Aggiungo il costo per il numero minimo di copie          
        if number_of_copy <= self.min_limit_punching
          line_params[:total] += self.cost_punching.to_f
          # CUSTOM_LOGGER.info "totale dopo aggiunta di costo minimo per punzonatura: #{line_params[:total]} *********************************"
        end
        
        if self.copy_cost_punching# Qui ho solo il costo per copia quindi devo reuperare l'unico punching
          punching = self.punchings.first
        else# Qui ho costi diversi in base al numero di quantity
          punching = self.punchings.select { |p| p.type_quantity.to_i == line_params[:system_params].collect { |h| h[:number_of_sheet] }.sum }.first
        end
        # CUSTOM_LOGGER.info "punching: #{punching.copy_cost if punching} *********************************"
        if punching
          line_params[:total] += punching.copy_cost.to_f * number_of_copy
        end
        # CUSTOM_LOGGER.info "totale dopo aggiunta costo punzonatura: #{line_params[:total]} *********************************"
        
        if line_params[:cover] && !self.copy_cost_punching ############################ AGGIUNGO LA COPERTINA
          if self.copy_cost_punching# Qui ho solo il costo per copia quindi devo reuperare l'unico punching
            punching = self.punchings.first
          else# Qui ho costi diversi in base al numero di quantity
            punching = self.punchings.select { |p| p.type_quantity.to_i == 1 }.first
          end
        
          if punching
            line_params[:total] += punching.copy_cost.to_f * number_of_copy
          end
        end
        # CUSTOM_LOGGER.info "totale dopo aggiunta costo punzonatura copertina: #{line_params[:total]} *********************************"
      else############################ SONO CON LA LEGATORIA INTERNA
        # CUSTOM_LOGGER.info "PUNZONO IN INTERNA *********************************"
        punching = self.punchings.select { |p| p.type_quantity.to_i == line_params[:system_params].collect { |h| h[:number_of_sheet] }.sum }.first
        if punching
          # Calcolo il costo di avviamento nel seguente modo:
          # (costo_orario * tempo_avviamento)/60
          line_params[:total] += (self.hourly_cost_punching.to_f * punching.start_time.to_f) / 60
          # CUSTOM_LOGGER.info "totale dopo avviamento punzonatura: #{line_params[:total]} *********************************"
          # Devo calcolare il costo per le copie: (number_of_sheet /  average_hourly_print) *  printer_hourly_cost
          line_params[:total] += self.hourly_cost_punching.to_f *(number_of_copy / punching.average_hourly_punching.to_f)
          # CUSTOM_LOGGER.info "totale dopo costo copie: #{line_params[:total]} *********************************"
        end
        # CUSTOM_LOGGER.info "totale dopo aggiunta punzonatura: #{line_params[:total]} *********************************"
        
        if line_params[:cover] ############################ AGGIUNGO LA COPERTINA
          punching = self.punchings.select { |p| p.type_quantity.to_i == 1 }.first
                
          if punching
            # Calcolo il costo di avviamento nel seguente modo:
            # (costo_orario * tempo_avviamento)/60
            line_params[:total] += (self.hourly_cost_punching.to_f * punching.start_time.to_f) / 60
            # Devo calcolare il costo per le copie: (number_of_sheet /  average_hourly_print) *  printer_hourly_cost
            line_params[:total] += self.hourly_cost_punching.to_f * (number_of_copy / punching.average_hourly_punching.to_f)
          end
        end
        # CUSTOM_LOGGER.info "totale dopo aggiunta costo punzonatura copertina: #{line_params[:total]} *********************************"
      end
      line_params
    end
    
    # Calcola il costo della cucitura
    # parametri:
    #   - Integer(line_params[:number_of_copy].presentation) => Integer(number_of_copy.presentation)
    #   - line_params[:system_params] => I parametri ricavati tramite una delle funzioni di calcolo impianti e fogli
    def compute_working_seam(line_params)
      # CUSTOM_LOGGER.info "******************************** Start scompute_working_seam *********************************"
      number_of_copy = line_params[:number_of_copy].presentation.to_i
      if self.external############################ SONO CON LA LEGATORIA ESTERNA
        # Aggiungo il costo per il numero minimo di copie          
        if number_of_copy <= self.min_limit_seam
          line_params[:total] += self.cost_seam.to_f
        end

        if (self.copy_cost_seam) 
          line_params[:total] += self.seam.copy_cost.to_f * number_of_copy
        else
          line_params[:total] += self.seam.copy_cost.to_f * line_params[:number_of_sheet].to_i
          ############################ AGGIUNGO LA COPERTINA
          if line_params[:cover] && line_params[:default_cover] != 0
            line_params[:total] += self.seam.copy_cost.to_f * number_of_copy
          end
        end
      else ########################## SONO IN INTERNA ##########################
        # Calcolo il costo di avviamento nel seguente modo:
        # (costo_orario * tempo_avviamento)/60
        line_params[:total] += (self.hourly_cost_seam.to_f * self.seam.start_time.to_f) / 60
        # Devo calcolare il costo per le copie: (number_of_sheet /  average_hourly_print) *  printer_hourly_cost
        line_params[:total] += self.hourly_cost_seam.to_f *(line_params[:number_of_sheet].to_i / self.seam.average_hourly_seam.to_f)
                
        ############################ AGGIUNGO LA COPERTINA
        if line_params[:cover] && line_params[:default_cover] != 0 && !self.copy_cost_seam 
          # Calcolo il costo di avviamento nel seguente modo:
          # (costo_orario * tempo_avviamento)/60
          line_params[:total] += (self.hourly_cost_seam.to_f * self.seam.start_time.to_f) / 60
          # Devo calcolare il costo per le copie: (number_of_sheet /  average_hourly_print) *  printer_hourly_cost
          line_params[:total] += self.hourly_cost_seam.to_f * (number_of_copy / self.seam.average_hourly_seam.to_f)
        end
      end
      # CUSTOM_LOGGER.info "***************************** End compute_working_seam *********************************"
      line_params
    end
    
    # Calcola il costo della fresatura
    # parametri:
    #   - Integer(line_params[:number_of_copy].presentation) => Integer(number_of_copy.presentation)
    #   - line_params[:system_params] => I parametri ricavati tramite una delle funzioni di calcolo impianti e fogli
    def compute_working_milling(line_params)
      # CUSTOM_LOGGER.info "******************************** Start compute_working_milling *********************************"
      number_of_copy = line_params[:number_of_copy].presentation.to_i
      if self.external############################ SONO CON LA LEGATORIA ESTERNA
        # CUSTOM_LOGGER.info " ============= ESTERNA TOT: #{line_params[:total]}"
        # Aggiungo il costo per il numero minimo di copie          
        
        if number_of_copy <= self.min_limit_milling
          line_params[:total] += self.cost_milling.to_f
          # CUSTOM_LOGGER.info " ============= Aggiunto costo fisso TOT: #{line_params[:total]}"
          # CUSTOM_LOGGER.info " ============= num of sheet : #{line_params[:number_of_sheet]}"
        end
        if (self.copy_cost_milling) 
          line_params[:total] += self.milling.copy_cost.to_f * number_of_copy
        else
          line_params[:total] += self.milling.copy_cost.to_f * line_params[:number_of_sheet].to_i
          
          # CUSTOM_LOGGER.info " ============= #{line_params[:total]}"
          # CUSTOM_LOGGER.info " ============= self.milling.copy_cost.to_f #{self.milling.copy_cost.to_f}"
          # CUSTOM_LOGGER.info " ============= line_params[:number_of_sheet].to_i #{line_params[:number_of_sheet].to_i}"
          if line_params[:cover] && line_params[:default_cover] != 0 && !self.copy_cost_milling
            ############################ AGGIUNGO LA COPERTINA
            line_params[:total] += self.milling.copy_cost.to_f * number_of_copy
            # CUSTOM_LOGGER.info " =============Aggiungo copertina Tot: #{line_params[:total]}"
            # CUSTOM_LOGGER.info " ============= self.milling.copy_cost.to_f #{self.milling.copy_cost.to_f}"
            # CUSTOM_LOGGER.info " ============= number_of_copy #{number_of_copy}"
          end
        end
      else ########################## SONO IN INTERNA ##########################
        # Calcolo il costo di avviamento nel seguente modo:
        # (costo_orario * tempo_avviamento)/60
        line_params[:total] += (self.hourly_cost_milling.to_f * self.milling.start_time.to_f) / 60
        # Devo calcolare il costo per le copie: (number_of_sheet /  average_hourly_print) *  printer_hourly_cost
        line_params[:total] += self.hourly_cost_milling.to_f *(line_params[:number_of_sheet].to_i / self.milling.average_hourly_milling.to_f)
        if line_params[:cover] && line_params[:default_cover] != 0
          ############################ AGGIUNGO LA COPERTINA
          # Calcolo il costo di avviamento nel seguente modo:
          # (costo_orario * tempo_avviamento)/60
          line_params[:total] += (self.hourly_cost_milling.to_f * self.milling.start_time.to_f) / 60
          # Devo calcolare il costo per le copie: (number_of_sheet /  average_hourly_print) *  printer_hourly_cost
          line_params[:total] += self.hourly_cost_milling.to_f * (number_of_copy / self.seam.average_hourly_milling.to_f)
        end
      end
      # CUSTOM_LOGGER.info "***************************** End compute_working_milling *********************************"
      line_params
    end
    
    # Calcola il costo della copertina senza alette
    # parametri:
    #   - number_of_copy => Integer(number_of_copy.presentation)
    def compute_working_sleeve(line_params)
      # CUSTOM_LOGGER.info "******************************** Start compute_working_sleeve *********************************"
      number_of_copy = line_params[:number_of_copy].presentation.to_i
      if self.external############################ SONO CON LA LEGATORIA ESTERNA
        # Calcolo la copertina in esterna
        line_params[:total] += self.sleeve.copy_cost.to_f * number_of_copy
      else############################ SONO CON LA LEGATORIA INTERNA
        # Calcolo la copertina in interna
        # Calcolo il costo di avviamento nel seguente modo:
        # (costo_orario * tempo_avviamento)/60
        line_params[:total] += (self.hourly_cost_sleeve.to_f * self.sleeve.start_time.to_f) / 60
        # Devo calcolare il costo per le copie: (number_of_sheet /  average_hourly_print) *  printer_hourly_cost
        line_params[:total] += self.hourly_cost_sleeve.to_f * (number_of_copy / self.sleeve.average_hourly_sleeve.to_f)
      end
      # CUSTOM_LOGGER.info "***************************** End compute_working_sleeve *********************************"
      line_params
    end
    
    # Calcola il costo della copertina con alette
    # parametri:
    #   - number_of_copy => Integer(number_of_copy.presentation)
    def compute_working_sleeve_fins(line_params)
      # CUSTOM_LOGGER.info "******************************** compute_working_sleeve_fins *********************************"
      number_of_copy = line_params[:number_of_copy].presentation.to_i
      if self.external############################ SONO CON LA LEGATORIA ESTERNA
        # Calcolo la copertina in esterna
        line_params[:total] += self.sleeve.copy_cost_fins.to_f * number_of_copy
      else############################ SONO CON LA LEGATORIA INTERNA
        # Calcolo la copertina in interna
        # Calcolo il costo di avviamento nel seguente modo:
        # (costo_orario * tempo_avviamento)/60
        line_params[:total] += (self.hourly_cost_sleeve.to_f * self.sleeve.start_time_fins.to_f) / 60
        # Devo calcolare il costo per le copie: (number_of_sheet /  average_hourly_print) *  printer_hourly_cost
        line_params[:total] += self.hourly_cost_sleeve.to_f * (number_of_copy / self.sleeve.average_hourly_sleeve_fins.to_f)
      end
      # CUSTOM_LOGGER.info "***************************** End compute_working_sleeve_fins *********************************"
      line_params
    end
    
    private
    def associate_option_type
      self.update_attribute(:option_type_id, Spree::OptionType.find_by_name('bindery').id) if self.option_type.nil?
    end

  end
end