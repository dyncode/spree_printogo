Spree::Variant.class_eval do
  belongs_to :parent, :class_name => 'Spree::Variant', :foreign_key => "parent_id"
  has_one :limit, :class_name => "Spree::Limit", :dependent => :destroy
  has_many :line_item_promotions, :class_name => "Spree::LineItemPromotion"
  has_many :line_items_option_values, :class_name => "Spree::LineItemsOptionValues", :foreign_key => "variant_parent_id"
  has_many :children, :class_name => "Spree::Variant", :foreign_key => "parent_id"
  
  has_one :template, :as => :imageable # Corrisponde al template da scaricare per il formato
  has_one :template_format, :as => :imageable # Corrisponde al template per il formato [l'immagine piccola che viene visualizzata sopra il radiobutton]
  
  def touch_product
    #product.touch unless is_master?
    true
  end
end