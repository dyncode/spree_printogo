# Rigido Variant
module Spree
  class RigidVariant < Variant
    before_create :after_init
    belongs_to :rigid_product, :class_name => Spree::RigidProduct, :foreign_key => "product_id"
    attr_accessible :plotter, :papers, :eyelet_accessories, :eyelet_accessories_attributes,
                    :laminations, :cutting_coils, :format
    has_many :eyelet_accessories, :class_name => "Spree::EyeletAccessory", :foreign_key => "variant_id", :dependent => :destroy
    
    accepts_nested_attributes_for :eyelet_accessories, :allow_destroy => true

		alias_attribute :format, :sku

    # Accesso agli option values
    def papers
      self.option_values.where(:option_type_id => Spree::OptionType.find_by_name('rigid_support').id)
    end

    def plotter
      self.option_values.where(:option_type_id => Spree::OptionType.find_by_name('plotter').id).last
    end
    
    def laminations
      self.option_values.where(:option_type_id => Spree::OptionType.find_by_name('lamination_type').id)
    end

    def cutting_coils
      self.option_values.where(:option_type_id => Spree::OptionType.find_by_name('cutting_coil_type').id)
    end
    
    # Setto i tipi di carta che per questo prodotto sono le coil (BOBINE)
    def papers= values
      self.papers.each do |p|
        unless values.include?(p.id)
          self.option_values.delete(p)
        end
      end

      values.compact.each do |v|
        unless v.blank?
          ov = Spree::OptionValue.find v
          unless self.papers.include?(ov)
            self.option_values << ov
          end
        end
      end
    end
    
    # Setto la stampante che per questo prodotto e` il plotter
    def plotter= value
      self.option_values.delete(self.plotter) if !self.plotter.blank?
      self.option_values << Spree::Plotter.find(value)
    end
    
    # Setto i tipi di laminatura
    def laminations= values
      self.laminations.each do |l|
        unless values.include?(l.id)
          self.option_values.delete(l)
        end
      end

      values.compact.each do |v|
        unless v.blank?
          ov = Spree::OptionValue.find v
          unless self.laminations.include?(ov)
            self.option_values << ov
          end
        end
      end
    end
    
    # Setto i tipi di sagomatura
    def cutting_coils= values
      self.cutting_coils.each do |cc|
        unless values.include?(cc.id)
          self.option_values.delete(cc)
        end
      end

      values.compact.each do |v|
        unless v.blank?
          ov = Spree::OptionValue.find v
          unless self.cutting_coils.include?(ov)
            self.option_values << ov
          end
        end
      end
    end
    
    # Ritorno un JSON formato da Disposizione => {Tipi di Occhiello}
    def eyelet_accessories_json
      res = []
      self.eyelet_accessories.each do |ea|
        res << {:type => ea.id, :values => JSON.parse(ea.disposition), :limit => ea.available_meter}
      end
      res.to_json
    end
    
    private
    def after_init
      if self.new_record?
        self.active = true
        self.on_hand = 1
      end
    end
  end
end