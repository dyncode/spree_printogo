module Spree
  class PosterHighQualityProduct < PosterProduct
    before_create :add_option_type
    
    def initialize(val = nil)
      super(val)
      self.name = "Manifesto ad Altissima Qualita"
      if self.master.present?
        self.master.price = 0 if self.master.price.nil?
        self.master.on_hand = 1
      else
        self.master = Spree::Variant.new
        self.master.price = 0
        self.master.on_hand = 1
      end
    end

    # devo tornare tutti i tipi di materiali con le relative configurazioni
    def all_value_json
      res = []
      self.papers.map do |p|
        res << {
            :id => p.id,
            :name => p.name,
            :description => p.description.to_s,
        }
      end
      
      res.to_json
    end
    
    def add_option_type
      if self.new_record?
        plotter_ot = Spree::OptionType.find_by_name 'plotter'
        paper_ot = Spree::OptionType.find_by_name 'coil'
        poster_high_quality_printer_format_ot = Spree::OptionType.find_by_name('poster_high_quality_printer_format')
        printer_instructions_ot = Spree::OptionType.find_by_name('printer_instructions')
        
        self.option_types << paper_ot
        self.option_types << poster_high_quality_printer_format_ot
        self.option_types << printer_instructions_ot
        self.option_types << plotter_ot
        
        self.master.option_values << poster_high_quality_printer_format_ot.option_values
      end
    end
    
    def poster_high_quality_printer_format_option_type
      self.option_types.where(:name => "poster_high_quality_printer_format").first
    end
    
    def poster_high_quality_printer_formats(id = nil)
      if id
        self.master.option_values.where(:id => id).first
      else
        self.master.option_values.where(:option_type_id => self.poster_high_quality_printer_format_option_type.id)
      end
    end
  end
end