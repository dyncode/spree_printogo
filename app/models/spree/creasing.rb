require "custom_logger"
# Finitura CORDONATURA
module Spree
  class Creasing < OptionType
    after_initialize :add_option_values

    attr_accessible :hourly_cost, :average_hourly_print, :start_time
    validates :hourly_cost, :average_hourly_print, :start_time, :presence => true

    def add_option_values
      if self.new_record?
        self.name = "creasing"
        self.presentation = "Cordonatura"
        option_values.new :name => 'creasing', :presentation => 'Cordonatura'
      end
    end

    def average_hourly_print
      self.average_hourly_work
    end

    def average_hourly_print= value
      self.average_hourly_work = value
    end

    def compute_working(number_of_copy)
      total = 0
      # Calcolo il costo di avviamento nel seguente modo:
      # (costo_orario * tempo_avviamento)/60
      total += (self.hourly_cost.to_f * self.start_time.to_f) / 60

      # Devo calcolare il costo a copia: (number_of_sheet /  average_hourly_print) *  printer_hourly_cost
      if self.average_hourly_print.to_f > 0
        total += self.hourly_cost.to_f * (number_of_copy / self.average_hourly_print.to_f)
      end
      
      total
    end

  end
end