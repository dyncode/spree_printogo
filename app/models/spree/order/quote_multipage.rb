require "multipage_logger"

module Spree
  class Order < ActiveRecord::Base

	  ######################################
	  # funzione per effettuare il calcolo
	  # del punto metallico
	  # analizza i valori
	  # verifica se esiste una promozione
	  # effettua il preventivo
	  # restituisce il CURRENT ITEM
	  ######################################
		def quote_staple(variant, params, quantity, promo, user = nil)
			begin
				MULTIPAGE_LOGGER.info "INIZIO quote_staple"
				# processo tutti i parametri e creo il current item
				line_params = backfill(variant, params, quantity)
				# Eseguo i calcoli comuni per i multipagina.
				line_params = quote_multi_page(line_params)
				# Calcolo il punto piegatura:
				line_params = quote_banding(line_params)
				# Calcolo il costo della PUNZONATURA [Punto Metallico]
				line_params = line_params[:limit].bindery(line_params[:printer]).compute_working_punching(line_params)
				# Aggiungo la marginalità
				line_params[:total] += ((line_params[:total] * line_params[:current_item].product.marginality) / 100)
				# Aggiungo il costo totale e il peso del prodotto al current_item
				MULTIPAGE_LOGGER.info "weight_line_item: #{line_params[:weight_line_item]}"
				line_params[:current_item].price = line_params[:total]
				line_params[:current_item].weight = Float(line_params[:weight_line_item])

				# Aggiungo gli adjustments
				add_adjustments(line_params, params, user)

				MULTIPAGE_LOGGER.info "FINE quote_staple"
				line_params[:current_item]
			rescue Exception => e
				logger.info "#################################### #{e} ####################################"
				logger.info "####################################"
				logger.info "#{e.backtrace.join("\n")}"
				logger.info "####################################"
			end
		end

		######################################
		# funzione per effettuare il calcolo
		# della brossura
		# analizza i valori
		# verifica se esiste una promozione
		# effettua il preventivo
		# restituisce il CURRENT ITEM
		######################################
		def quote_paperback(variant, params, quantity, promo, user = nil)
			begin
				# devo mettere i valori mancanti dento il params:
				# params[:order][:cover_bw] = params[:order][:print_color]
				# if !params[:order][:plasticization].blank?
# 					tmp = params[:order][:plasticization].split '-'
# 					params[:order][:plasticization_option] = "Solo copertina - #{tmp[1].strip}"
# 					params[:order][:plasticization] = tmp[0].strip
# 				end
				if(!params[:order][:plasticization].blank?)
					temp_plastification_type = params[:order][:plasticization]
					plastification_type = (temp_plastification_type.match(/^polished_film/).blank?) ? "matt_film" : "polished_film"
					plastification_type_fb = (temp_plastification_type.match(/front_back/).blank?) ? "front_back_cover_plasticization" : "front_cover_plasticization"
					params[:order][:plasticization_option] = plastification_type_fb
					params[:order][:plasticization] = plastification_type
				end
				# processo tutti i parametri e creo il current item
				line_params = backfill(variant, params, quantity)

				# Eseguo i calcoli comuni per i multipagina.
				line_params = quote_multi_page(line_params)
				# Calcolo il punto piegatura
				line_params = quote_banding(line_params)
				if line_params[:variant].product.finishing == "seam"
				  # Calcolo il costo della CUCITURA [Brossura]
				  line_params = line_params[:limit].bindery(line_params[:printer]).compute_working_seam(line_params)
				else
				  # Calcolo il costo della FRESATURA [Brossura]
				  line_params = line_params[:limit].bindery(line_params[:printer]).compute_working_milling(line_params)
				end
				# Calcolo il costo della copertina [CON ALETTE, SENZA ALETTE]
				if line_params[:sleeve] && line_params[:sleeve].presentation == "Senza Alette"
				  # Calcolo il costo della COPERTINA SENZA ALETTE [Brossura]
				  line_params = line_params[:limit].bindery(line_params[:printer]).compute_working_sleeve(line_params)
				else
				  # Calcolo il costo della COPERTINA CON ALETTE [Brossura]
				  line_params = line_params[:limit].bindery(line_params[:printer]).compute_working_sleeve_fins(line_params)
				end
				# Aggiungo la marginalità
				line_params[:total] += ((line_params[:total] * line_params[:current_item].product.marginality) / 100)
				# Aggiungo il costo totale e il peso del prodotto al current_item
				line_params[:current_item].price = line_params[:total]
				line_params[:current_item].weight = line_params[:weight_line_item].to_f
				# Aggiungo gli adjustments
				add_adjustments(line_params, params, user)

        line_params[:current_item]
      rescue Exception => e
        logger.info "#################################### #{e} ####################################"
        logger.info "####################################"
        logger.info "#{e.backtrace.join("\n")}"
        logger.info "####################################"
      end
    end


    ######################################
    # funzione per effettuare il calcolo
    # della spirale ad anelli
    # analizza i valori
    # verifica se esiste una promozione
    # effettua il preventivo
    # restituisce il CURRENT ITEM
    ######################################
    def quote_spiral(variant, params, quantity, promo, user = nil)
      begin
        # processo tutti i parametri e creo il current item
        line_params = backfill(variant, params, quantity)
        # line_params = fetch_values_spiral(variant, params, quantity)
        line_params = skip_promotion(line_params, variant)
        if !promo || !line_params[:skip]
          # Eseguo i calcoli comuni per i multipagina.
          line_params = quote_multi_page(line_params)
          # Calcolo il punto piegatura:
          #line_params = banding_calculate(line_params)
          # Calcolo il costo della SPIRALATURA
          line_params = spiraling_calculate(line_params)
          
          # Aggiungo la marginalità
          line_params[:total] += ((line_params[:total] * line_params[:current_item].product.marginality) / 100)
          # Aggiungo il costo totale e il peso del prodotto al current_item
          line_params[:current_item].price = line_params[:total]
          line_params[:current_item].weight = Float(line_params[:weight_line_item])
          # Aggiungo gli adjustments
          add_adjustments(line_params, params, user)
        else
          line_params = set_promotion(line_params)
          self.line_items << line_params[:current_item]
        end

        line_params[:current_item]
      rescue Exception => e
        logger.info "#################################### #{e} ####################################"
        logger.info "####################################"
        logger.info "#{e.backtrace.join("\n")}"
        logger.info "####################################"
      end
    end


    ######################################
    # Recupero le informazioni
    # analizzando i parametri ricevuti
    # e salvando i nuovi valori
    # all'interno di un unico array
    ######################################
    def backfill(variant, params, quantity)
      MULTIPAGE_LOGGER.info "### INIZIO backfill"
      current_item = Spree::LineItem.new(:quantity => quantity)
      current_item.variant = variant
      print_color_ot = Spree::OptionType.find_by_name('printer_color')

			######################################
			# Devo cercare l'OptionValue corrispondente al NOME della grammatura
			# Salvo il numero di copie selezionato o inserito a mano nella specifica input
			######################################
      line_params = {
          :variant => variant,
          :limit => current_item.variant.limit,
          :cut => current_item.product.cut,
          :paper => Spree::Paper.find(params[:order][:paper]),
          :number_of_facades => Spree::OptionValue.create(:name => :number_of_facades, :presentation => params[:order][:number_of_facades]),
          :print_color => print_color_ot.option_values.where(:name => params[:order][:print_color].to_s).first,
          :weight => Spree::Paper.find(params[:order][:paper]).paper_weights.where("name = '#{params[:order][:weight].to_s}'").first,
          :number_of_copy => Spree::OptionValue.create(:name => :number_of_copy, :presentation => params[:order][:number_of_copy])
      }
      # MULTIPAGE_LOGGER.info "Taglio: #{line_params[:cut]}"
#       MULTIPAGE_LOGGER.info "Carta: #{line_params[:paper].name}"
#       MULTIPAGE_LOGGER.info "Numero di facciate: #{line_params[:number_of_facades].presentation}"
#       MULTIPAGE_LOGGER.info "Tipologia di stampa: #{line_params[:print_color].name}"
#       MULTIPAGE_LOGGER.info "Grammatura: #{line_params[:weight].name}"
#       MULTIPAGE_LOGGER.info "Numero di copie: #{line_params[:number_of_copy].presentation}"

      ######################################
      # Verifico se esiste una plastificazione
      # Seleziono la plastificazione corrispondente
      # per effettuare successivamente il preventivo
      ######################################
      line_params[:plasticization_option] = params[:order][:plasticization_option]
      if params[:order][:plasticization_option] == "front_cover_plasticization"
        line_params[:plastification] = Spree::OptionValue.where(:name => "#{params[:order][:plasticization]}_front",
                                                                :option_type_id => Spree::OptionType.find_by_name("plasticization")).first
        MULTIPAGE_LOGGER.info "Plastificazione: #{line_params[:plastification].name}"
      elsif params[:order][:plasticization_option] == "front_back_cover_plasticization" || params[:order][:plasticization_option] == "all_plasticization"
        line_params[:plastification] = Spree::OptionValue.where(:name => "#{params[:order][:plasticization]}_front_back",
                                                                :option_type_id => Spree::OptionType.find_by_name("plasticization")).first
        # MULTIPAGE_LOGGER.info "Plastificazione: #{line_params[:plastification].name}"
      end

      line_params[:print_color_bool] = line_params[:print_color].name == "true" ? true : false
      # MULTIPAGE_LOGGER.info "Copertina biano/nero: #{params[:order][:cover_bw].to_s}"
      if params[:order][:cover_bw]
        line_params[:print_color_cover] = print_color_ot.option_values.where(:name => params[:order][:cover_bw].to_s).first
        # MULTIPAGE_LOGGER.info "Stampa copertina: #{line_params[:print_color_cover].name}"
      end
      line_params[:print_color_cover_bool] = line_params[:print_color_cover].name == "true" ? true : false

      ######################################
      # Verifico se esiste la copertina
      # -1 ~> ho la setta grammatura dell'interno
      # -2 ~> sono nella spirale ed ho la copertina cartoncino
      # altrimenti e` stata selezionata una carta ed una grammatura differenti
      ######################################
      if params[:order][:paper_cover] == "-1"
        line_params.merge!({
            :cover => false,
            :paper_cover => line_params[:paper],
            :cover_weight => line_params[:weight],
            :number_of_facades_internal => Integer(params[:order][:number_of_facades])
        })

        if line_params[:print_color_cover_bool] != line_params[:print_color_bool]
          line_params[:cover] = true
          line_params[:number_of_facades_internal] = (Integer(params[:order][:number_of_facades]) - 4)
        end
      elsif params[:order][:paper_cover] == "-2"
        line_params.merge!({
            :cover => false,
            :default_cover => params[:order][:default_cover],
            :number_of_facades_internal => Integer(params[:order][:number_of_facades])
        })
        MULTIPAGE_LOGGER.info "Copertina standard spirale: #{line_params[:default_cover]}"
      else
        line_params.merge!({
            :cover => true,
            :paper_cover => Spree::Paper.find(params[:order][:paper_cover]),
            :cover_weight => Spree::Paper.find(params[:order][:paper_cover]).paper_weights.where("name = '#{params[:order][:paper_cover_weight].to_s}'").first,
            :number_of_facades_internal => (Integer(params[:order][:number_of_facades]) - 4)
        })
        MULTIPAGE_LOGGER.info "Carta copertina: #{line_params[:paper_cover].name}"
        MULTIPAGE_LOGGER.info "Grammatura copertina: #{line_params[:cover_weight].weight}"
      end
      MULTIPAGE_LOGGER.info "Copertina separata? #{line_params[:cover]}"
      MULTIPAGE_LOGGER.info "Numero di facciate interno: #{line_params[:number_of_facades_internal]}"

      ######################################
      # Imposto l'orientamento fisso
      # a verticale per tutti i multipagina
      # Verifico la tipologia di stampa selezionata
      # sia per l'interno che per la copertina
      ######################################
      line_params[:orientation] = Spree::OptionValue.find_by_name('vertical')
      if params[:order][:instructions].blank?
        front_back_ot = Spree::OptionType.find_by_name('printer_instructions')
        line_params[:front_back] = front_back_ot.option_values.where("name = 'different_front_back'").first
        # Serve per la formula del CoverFlyer (COPERTINA)
        line_params[:front_back_cover] = front_back_ot.option_values.where(:name => "different_front_back").first
        MULTIPAGE_LOGGER.info "Copertina fronte e retro: #{line_params[:front_back_cover].presentation}"
      else
        front_back_ot = Spree::OptionType.find_by_name('printer_instructions')
        line_params[:front_back] = front_back_ot.option_values.where("name = '#{params[:order][:instructions].to_s}'").first
        # Serve per la formula del CoverFlyer (COPERTINA)
        line_params[:front_back_cover] = front_back_ot.option_values.where(:name => "#{params[:order][:instructions].to_s}").first
        MULTIPAGE_LOGGER.info "Copertina fronte e retro: #{line_params[:front_back_cover].presentation}"
      end

      ######################################
      # Campo necessario solamente per la spirale
      ######################################
      if params[:order][:spiraling]
        line_params[:spiraling] = Spree::SpiralingQuantity.find(params[:order][:spiraling])
      end

      if params[:order][:sleeve]
        line_params[:sleeve] = Spree::OptionValue.create(:name => :sleeve, :presentation => params[:order][:sleeve])
      end

      # Aggiungo i dati per la variante (l'interno del punto metallico)
      current_item.line_items_option_values.new :option_value => line_params[:paper], :variant_parent_id => variant.id
      current_item.line_items_option_values.new :option_value => line_params[:weight], :variant_parent_id => variant.id
      current_item.line_items_option_values.new :option_value => line_params[:print_color], :variant_parent_id => variant.id
      current_item.line_items_option_values.new :option_value => line_params[:front_back], :variant_parent_id => variant.id
      current_item.line_items_option_values.new :option_value => line_params[:number_of_copy], :variant_parent_id => variant.id
      current_item.line_items_option_values.new :option_value => line_params[:number_of_facades], :variant_parent_id => variant.id
      current_item.line_items_option_values.new :option_value => line_params[:spiraling], :variant_parent_id => variant.id if params[:order][:spiraling]
      current_item.line_items_option_values.new :option_value => line_params[:sleeve], :variant_parent_id => variant.id if params[:order][:sleeve]
      current_item.line_items_option_values.new :option_value => line_params[:plastification], :variant_parent_id => variant.id if params[:order][:plasticization_option] == "Tutto"
      # Aggiungo i dati per la copertina
      current_item.line_items_option_values.new :option_value => line_params[:orientation], :variant_parent_id => variant.cover_variant.id
      current_item.line_items_option_values.new :option_value => line_params[:front_back_cover], :variant_parent_id => variant.cover_variant.id
      current_item.line_items_option_values.new :option_value => line_params[:paper_cover], :variant_parent_id => variant.cover_variant.id
      current_item.line_items_option_values.new :option_value => line_params[:cover_weight], :variant_parent_id => variant.cover_variant.id
      current_item.line_items_option_values.new :option_value => line_params[:plastification], :variant_parent_id => variant.cover_variant.id unless params[:order][:plasticization_option] == "Nessuno"
      # la quantità sarà uno
      current_item.line_items_option_values.new :option_value => line_params[:print_color_cover], :variant_parent_id => variant.cover_variant.id

      self.line_items << current_item
      line_params[:current_item] = current_item

      ######################################
      # Calcoli comuni a tutti i multipagina
      # Calcolo numero quartini
      # Recupero il totale delle copie
      # Recupero le stampanti se bianco e nero o colori
      # Ricavo i limiti per la copertina eventuale
      ######################################
      line_params.merge!({
          :number_of_quarters => (line_params[:number_of_facades_internal] / 4) * Integer(line_params[:number_of_copy].presentation),
          :total_copy => Integer(line_params[:number_of_copy].presentation) * (line_params[:number_of_facades_internal]/2).ceil,
          :printer_multi => current_item.variant.limit.printer_multi(line_params[:print_color_bool]),
          :limit_cover => current_item.variant.cover_variant.limit
      })
			MULTIPAGE_LOGGER.info "current_item.variant: #{current_item.variant.id}"
			MULTIPAGE_LOGGER.info "current_item.variant.cover_variant: #{current_item.variant.children.collect{|cv| cv.id}}"
      MULTIPAGE_LOGGER.info "Numero di quartini: #{line_params[:number_of_quarters]}"
      MULTIPAGE_LOGGER.info "Totale copie: #{line_params[:total_copy]}"
      MULTIPAGE_LOGGER.info "Stampante: #{line_params[:printer_multi].collect{|i, pm| pm.presentation}.join(", ")}"

      # Aggiungo la data di spedizione
      line_params[:order_calculator] = params[:order_calculator]
      if !line_params[:order_calculator][:data].is_date?
        day = Spree::ShippingDateAdjustment.find(line_params[:order_calculator][:data]).days
        line_params[:shipping_date] = Spree::OptionValue.create(:name => "shipping_date", :presentation => day.business_day.from_now)
      else
        line_params[:shipping_date] = Spree::OptionValue.create(:name => "shipping_date", :presentation => line_params[:order_calculator][:data].to_date)
      end
      line_params[:file_check] = params[:order][:file_check]
      MULTIPAGE_LOGGER.info "###### FINE backfill"

      line_params
    end


    # Calcola il costo della piegatura per le pagine interne e in caso che la copertina abbia grammatura differente
    # rispetto alle pagine interne, viene calcolato anche il costo della cordonatura.
    #
    # parametri:
    #     - cover_weight
    #     - weight
    #     - current_item
    #     - number_of_copy
    #     - number_of_systems
    #     - points_of_convenience
    #     - print_color
    #     - number_of_quarters
    def quote_banding(line_params = {})
      MULTIPAGE_LOGGER.info "### INIZIO banding_calculate"
      # Calcolo il punto piegatura:
      if line_params[:cover]
        bending_cover = true
        if line_params[:cover_weight] != line_params[:weight]
          line_params[:total] += line_params[:current_item].product.creasing.compute_working(Integer(line_params[:number_of_copy].presentation))
          MULTIPAGE_LOGGER.info "totale dopo aggiunta costo cordonatura copertina: #{line_params[:total]}"
          bending_cover = false
        end
      else
        bending_cover = false
      end
      # Calcolo la piegaura (contenuto e nel caso aggiungo la copertina)
      line_params = line_params[:limit].bindery(line_params[:printer]).compute_working_bending(line_params, bending_cover)
      MULTIPAGE_LOGGER.info "###### FINE banding_calculate"
      
      line_params
    end
    
    # Calcola il costo della spiralatura
    #
    # parametri:
    #     - number_of_copy
    def spiraling_calculate(line_params = {})
      MULTIPAGE_LOGGER.info "SONO IN spiraling_calculate *********************************"
      if line_params[:spiraling].spiraling.external
        MULTIPAGE_LOGGER.info "SONO IN ESTERNA *********************************"
        MULTIPAGE_LOGGER.info "number_of_copy: #{line_params[:number_of_copy].presentation.to_i} *********************************"
        # aggiungo il costo di avviamento della spiralatura
        if line_params[:number_of_copy].presentation.to_i < line_params[:spiraling].spiraling.hourly_cost.to_i
          line_params[:total] += line_params[:spiraling].spiraling.average_hourly_work.to_f
        end
        # aggiungo il costo a copia
        copy_cost = line_params[:spiraling].spiraling.spiraling_copy_costs.where("average_hourly_print_bw <= ? AND average_hourly_print_c >= ?", line_params[:number_of_copy].presentation.to_i, line_params[:number_of_copy].presentation.to_i)
        copy_cost.blank? ? cost = 0.0 : cost = copy_cost.first.copy_cost.to_f
        line_params[:total] += line_params[:number_of_copy].presentation.to_i * cost
        # aggiungo il costo delle spirali secondo il formato e le copie
        line_params[:total] += line_params[:number_of_copy].presentation.to_i * (line_params[:spiraling].copy_cost.to_f * line_params[:variant].height)# get_width_and_height(line_params[:variant].format)[:height])
      else
        MULTIPAGE_LOGGER.info "SONO IN INTERNA *********************************"
        # aggiungo il costo di avviamento della spiralatura
        line_params[:total] += line_params[:spiraling].spiraling.hourly_cost.to_f * (line_params[:spiraling].spiraling.start_time.to_f/60.to_f)
        # aggiungo il costo di spiralatura
        line_params[:total] += line_params[:spiraling].spiraling.hourly_cost.to_f * (line_params[:number_of_copy].presentation.to_i/line_params[:spiraling].spiraling.average_hourly_work.to_f) if line_params[:spiraling].spiraling.average_hourly_work.to_f > 0
        # aggiungo il costo delle spirali secondo il formato e le copie
        MULTIPAGE_LOGGER.info "line_params[:spiraling].copy_cost: #{line_params[:spiraling].copy_cost} *********************************"
        #MULTIPAGE_LOGGER.info "get_width_and_height(line_params[:variant].format)[:height]: #{get_width_and_height(line_params[:variant].format)[:height]} *********************************"
        line_params[:total] += line_params[:number_of_copy].presentation.to_i * (line_params[:spiraling].copy_cost.to_f * line_params[:variant].height)# get_width_and_height(line_params[:variant].format)[:height])
      end
      MULTIPAGE_LOGGER.info "FINE spiraling_calculate *********************************"
      line_params
    end

    # Questa funzione esegue tutte le operazione per i multi pagina
    # parametri: un hash composto dai seguenti valri:
    # => paper
    # => number_of_facades
    # => print_color
    # => plasticization_option
    # => plastification
    # => weight
    # => number_of_copy
    # => paper_cover
    # => cover_weight
    # => front_back_cover
    # => print_color_bw
    # => current_item
    # => number_of_facades_internal
    # => number_of_quarters
    # => quarter
    # => price_kg
    # => total_copy
    # => paper_init_format
    # => printer
    #
    def quote_multi_page(line_params = {})
      MULTIPAGE_LOGGER.info "### INIZIO quote_multi_page"
      line_params[:total] = 0
      line_params[:weight_line_item] = 0
      line_params[:printer_multi].each do |k, p|
				MULTIPAGE_LOGGER.info "Sto usando la stampante: #{p.presentation}"
        # Recupero il numero di quartini associato alla stampante e in base al numero di copie.
        line_params["quarter_#{k}".to_sym] = line_params[:limit].quarters(line_params[:print_color_bool], p)
				MULTIPAGE_LOGGER.info "line_params[:print_color_bool]: #{line_params[:print_color_bool]}"
				MULTIPAGE_LOGGER.info "line_params[:limit]: #{line_params[:limit].to_json}"
        MULTIPAGE_LOGGER.info "Pose (quartini) della stampante: #{line_params["quarter_#{k}".to_sym].name}"
        # Recupero il costo della stampa
        line_params["price_from_back_#{k}".to_sym] = p.price_from_back(line_params[:print_color_bool], line_params[:front_back].name)
        MULTIPAGE_LOGGER.info "Prezzo fronte/retro della stampante: #{line_params["price_from_back_#{k}".to_sym]}"
        # Ricavo il formato di stampa
        line_params["paper_format_#{k}".to_sym] = line_params[:limit].printer_format_multi(line_params[:print_color_bool], p).presentation
        MULTIPAGE_LOGGER.info "Formato della carta settato sulla stampante: #{line_params["paper_format_#{k}".to_sym]}"
        # Recupero il formato iniziale
        line_params["paper_init_format_#{k}".to_sym] = get_start_format(line_params[:limit].printer_format_multi(line_params[:print_color_bool], p).presentation)
        MULTIPAGE_LOGGER.info "Formato iniziale della carta di stampa della stampante: #{line_params["paper_init_format_#{k}".to_sym]}"

				if p.digital?
          MULTIPAGE_LOGGER.info "###### STAMPO IN DIGITALE"
          if line_params[:weight].ream?
						MULTIPAGE_LOGGER.info "###### HO LA CARTA IN RISMA"
            line_params = quote_multipage_paper_ream(line_params, p, k.to_s)
						# line_params["number_of_sheet_#{k}".to_sym] = line_params["print_option_#{k}".to_sym][:number_of_sheet]
          else
						MULTIPAGE_LOGGER.info "###### HO LA CARTA IN TAGLIATO"
            line_params = quote_multipage_paper_cut(line_params, p, k.to_s)
            line_params["number_of_sheet_#{k}".to_sym] = line_params["print_option_#{k}".to_sym][:number_of_sheet]
          end
        else
          MULTIPAGE_LOGGER.info "###### STAMPO IN OFFSET"
					MULTIPAGE_LOGGER.info "###### HO LA CARTA IN TAGLIATO"
          line_params = quote_multipage_paper_cut(line_params, p, k.to_s)
          line_params["number_of_sheet_#{k}".to_sym] = line_params["print_option_#{k}".to_sym][:number_of_sheet]
        end
      end
      line_params[:weight_line_item] = ((line_params[:variant].width.to_f / 100.to_f) * (line_params[:variant].height.to_f / 100.to_f)) * (line_params[:weight].weight.to_f / 1000) * (line_params[:number_of_facades_internal].to_f/2) * line_params[:number_of_copy].presentation.to_i
      
      # Controllo i due totali e prendo il migliore
      if line_params[:total_low].to_f < line_params[:total_high].to_f
        line_params[:quarter] = line_params[:quarter_low]
        line_params[:price_from_back] = line_params[:price_from_back_low]
        line_params[:paper_init_format] = line_params[:paper_init_format_low]
        line_params[:number_of_sheet] = line_params[:number_of_sheet_low] if line_params[:number_of_sheet_low]
        line_params[:price_per_sheet] = line_params[:price_per_sheet_low]
        line_params[:printer] = line_params[:printer_multi][:low]
        line_params[:paper_format] = line_params[:paper_format_low]
        line_params[:total] += line_params[:total_low]
        #line_params[:weight_line_item] += line_params[:weight_option_low]
        print_option_low = line_params[:print_option_low]
        line_params = line_params.merge(print_option_low)
      else
        line_params[:quarter] = line_params[:quarter_high]
        line_params[:price_from_back] = line_params[:price_from_back_high]
        line_params[:paper_init_format] = line_params[:paper_init_format_high]
        line_params[:number_of_sheet] = line_params[:number_of_sheet_high] if line_params[:number_of_sheet_high]
        line_params[:price_per_sheet] = line_params[:price_per_sheet_high]
        line_params[:printer] = line_params[:printer_multi][:high]
        line_params[:paper_format] = line_params[:paper_format_high]
        line_params[:total] += line_params[:total_high]
        #line_params[:weight_line_item] += line_params[:weight_option_high]
        print_option_high = line_params[:print_option_high]
        line_params = line_params.merge(print_option_high)
      end
			MULTIPAGE_LOGGER.info "###### FINE PREVENTIVO PER COSTI DI STAMPA DELL'INTERNO"
      
      ####      Fine costo della stampa      ####
      # Costo della plastificazione, in questo caso si intende fronte e retro
      if line_params[:plasticization_option] == "all_plasticization"
        line_params[:total] += Spree::Order.quote_plasticization(line_params[:plastification].name,
                                                                        line_params[:number_of_sheet],
                                                                        line_params[:paper_format])
        MULTIPAGE_LOGGER.info "Totale dopo la plastificazione se TUTTO: #{line_params[:total]}"
      end

      MULTIPAGE_LOGGER.info "Totale prima della copertina: #{line_params[:total]}"
      if line_params[:cover]
        # Calcolo della copertina:
        line_params = quote_cover(line_params)
        line_params[:total] += Float(line_params[:total_cover])
        line_params[:weight_line_item] += Float(line_params[:weight_line_item_cover])
        MULTIPAGE_LOGGER.info "Totale dopo aggiunta costo copertina: #{line_params[:total]}"
      else
        if line_params[:default_cover] && line_params[:default_cover] != "0"
          line_params[:total] += line_params[:variant].default_cover_price.to_f * line_params[:number_of_copy].presentation.to_i
          if ((line_params[:plasticization_option] == "front_cover_plasticization") || (line_params[:plasticization_option] == "front_back_cover_plasticization"))
            # Aggiungo il prezzo per la plastificazione se presente
            line_params[:total] += plasticization_price_cover(line_params) if line_params[:plastification]
            MULTIPAGE_LOGGER.info "Totale dopo plastificazione della copertina: #{line_params[:total]}" if line_params[:plastification]
          end
        end
      end
      
       #gestisco caso in cui la copertina e' della stessa carta dell'interno ma e' richiesta una plastificazione solo fronte o fronte e retro
      if !line_params[:cover] && ((line_params[:plasticization_option] == "front_cover_plasticization") || (line_params[:plasticization_option] == "front_back_cover_plasticization"))
				line_params[:number_of_sheet_cover] = 1
				line_params[:paper_format_cover] = line_params[:paper_format]
        if line_params[:plasticization_option] == "front_cover_plasticization"
          MULTIPAGE_LOGGER.info "Solo copertina - solo fronte #{line_params[:total]}"
          line_params[:total] += plasticization_price_cover(line_params) if line_params[:plastification]
          MULTIPAGE_LOGGER.info "totale dopo di Solo copertina - solo fronte #{line_params[:total]}"
        end
        if line_params[:plasticization_option] == "front_back_cover_plasticization"
          MULTIPAGE_LOGGER.info "totale prima di Solo copertina - fronte e retro #{line_params[:total]}"
          line_params[:total] += plasticization_price_cover(line_params) if line_params[:plastification]
          MULTIPAGE_LOGGER.info "totale dopo di Solo copertina - fronte e retro #{line_params[:total]}"
        end
      end
      MULTIPAGE_LOGGER.info "### FINE quote_multi_page"

      line_params
    end

    # Ricavo il costo e i valori necessari per la carta in formato RISMA per i formati MULTIPAGINA
    def quote_multipage_paper_ream(line_params, printer, step)
      line_params["total_#{step}".to_sym] = 0
      MULTIPAGE_LOGGER.info "SONO IN get_multi_paper_ream *********************************"
      # Sono in risma
      # TODO calcolare il costo di un foglio

      # Devo recuperare il numero dei fogli, # impianti, il peso del prodotto e aggiungerlo al totale
      #number_of_sheet = r[:number_of_sheet]
      #weight_line_item = r[:weight_line_item] #(number_of_sheet *Integer(number_of_copy.presentation)) * (weight.name.to_f / 1000)
      #plants = r[:plants]

      line_params = Spree::Order.paper_cost_in_risma_multipage(line_params, Float(line_params[:weight].cost_ream), Integer(line_params[:number_of_copy].presentation), Float(line_params["quarter_#{step}".to_sym].name), step)
      MULTIPAGE_LOGGER.info "price_per_sheet: #{line_params[:price_per_sheet]} *********************************"
      
      # Devo effettuare il doppio preventivo
      total = 0
      if printer.digital?
        # passo il valore 3 alla funzione get_number_of_sheet in quanto la considero come una 8 COLORI SENZA SCARTO
        plant_params_unic = Spree::Order.quote_number_of_sheet(line_params, 3, step)
				MULTIPAGE_LOGGER.info "*************************"
				MULTIPAGE_LOGGER.info "Preventivo digitale"
				MULTIPAGE_LOGGER.info "#{plant_params_unic}"
        total += (line_params["price_per_sheet_#{step}".to_sym] * (plant_params_unic[:number_of_sheet]*(1/line_params["paper_init_format_#{step}".to_sym][:quantity].to_f).ceil))
				MULTIPAGE_LOGGER.info "*************************"
				MULTIPAGE_LOGGER.info "totale price_per_sheet_"
				MULTIPAGE_LOGGER.info "#{(line_params["price_per_sheet_#{step}".to_sym] * (plant_params_unic[:number_of_sheet]*(1/line_params["paper_init_format_#{step}".to_sym][:quantity].to_f).ceil))}"
        total += printer.print_cost(line_params, plant_params_unic, step)
        MULTIPAGE_LOGGER.info "Totale ottenuto: #{total}"
				MULTIPAGE_LOGGER.info "*************************"
      else
        if printer.color_4?
          total_1 = 0
          # passo il valore 1 alla funzione get_number_of_sheet in quanto la considero come una 4 COLORI
          plant_params_1 = Spree::Order.quote_number_of_sheet(line_params, 1, step)
          total_1 += (line_params["price_per_sheet_#{step}".to_sym] * (plant_params_1[:number_of_sheet]*(1/line_params["paper_init_format_#{step}".to_sym][:quantity].to_f).ceil))
          total_1 += printer.print_cost(line_params, plant_params_1, step)

          total_2 = 0
          # passo il valore 3 alla funzione get_number_of_sheet in quanto la considero come una 8 COLORI SENZA SCARTO
          plant_params_2 = Spree::Order.quote_number_of_sheet(line_params, 3, step)
          total_2 += (line_params["price_per_sheet_#{step}".to_sym] * (plant_params_2[:number_of_sheet]*(1/line_params["paper_init_format_#{step}".to_sym][:quantity].to_f).ceil))
          total_2 += printer.print_cost(line_params, plant_params_2, step)

          # Confronto i due risultati e mi salvo il minore
          if total_1 > total_2
            total = total_2
            plant_params_unic = plant_params_2
          else
            total = total_1
            plant_params_unic = plant_params_1
          end
        else
          # CALCOLO IL PREVENTIVO NORMALE
          total_1 = 0
          # passo il valore 2 alla funzione get_number_of_sheet in quanto la considero come una 8 COLORI CON SCARTO
          plant_params_1 = Spree::Order.quote_number_of_sheet(line_params, 2, step)
          total_1 += (line_params["price_per_sheet_#{step}".to_sym] * (line_params[:number_of_sheet]*(1/line_params["paper_init_format_#{step}".to_sym][:quantity].to_f).ceil))
          total_1 += printer.print_cost(line_params, plant_params_1, step)

          total_2 = 0
          # passo il valore 3 alla funzione get_number_of_sheet in quanto la considero come una 8 COLORI SENZA SCARTO
          plant_params_2 = Spree::Order.quote_number_of_sheet(line_params, 3, step)
          total_2 += (line_params["price_per_sheet_#{step}".to_sym] * (line_params[:number_of_sheet]*(1/line_params["paper_init_format_#{step}".to_sym][:quantity].to_f).ceil))
          total_2 += printer.print_cost(line_params, plant_params_2, step)

          # Confronto i due risultati e mi salvo il minore
          if total_1 > total_2
            total = total_2
            plant_params_unic = plant_params_2
          else
            total = total_1
            plant_params_unic = plant_params_1
          end
        end # TERMINO IL CONFRONTO DELL'OFFSET
      end
      
      # Non devo salvare subito il totale e i parametri ricavati perche` devo fare il confronto anche con la seconda stampante
      line_params["total_#{step}".to_sym] += total
      line_params["print_option_#{step}".to_sym] = plant_params_unic
      line_params
    end

    # Ricavo il costo e i valori necessari per la carta in formato TAGLIATO per i formati MULTIPAGINA
    def quote_multipage_paper_cut(line_params, printer, step)
      MULTIPAGE_LOGGER.info "### INIZIO quote_multipage_paper_cut"
      # Recupero il prezzo al kg
      line_params[:price_kg] = Float(line_params[:weight].cost_cut)
      MULTIPAGE_LOGGER.info "Prezzo al KG: #{line_params[:price_kg]}"
      # Calcolo il prezzo per foglio è dato da:
      # supponendo di avere:
      #   - 64x88 cm
      #   - 150 gr
      #   - 0,90 €/kg
      # avrò: PREZZO PER FOGLIO= (64/100)*(88/100)*PREZZO PER KILO * GRAMMATURA FOGLIO IN KG
      line_params["price_per_sheet_#{step}".to_sym] = (line_params["paper_init_format_#{step}".to_sym][:master][0] / 100.to_f) * (line_params["paper_init_format_#{step}".to_sym][:master][1] / 100.to_f) *
          (line_params[:price_kg] * (Float(line_params[:weight].name) / 1000.to_f))
      MULTIPAGE_LOGGER.info "Prezzo del foglio in formato padre: #{line_params["price_per_sheet_#{step}".to_sym]}"

			# Devo effettuare il doppio preventivo
			total = 0
			total_1 = 0
			total_2 = 0
			####################
			# STAMPO IN DIGITALE
			####################
			if printer.digital?
				# passo il valore 3 alla funzione quote_number_of_sheet in quanto la considero come una 8 COLORI SENZA SCARTO
        plant_params_unic = Spree::Order.quote_number_of_sheet(line_params, 3, step)
				MULTIPAGE_LOGGER.info "*************************"
				MULTIPAGE_LOGGER.info "Preventivo digitale"
				MULTIPAGE_LOGGER.info "#{plant_params_unic}"
        total += printer.print_cost(line_params, plant_params_unic, step)
        MULTIPAGE_LOGGER.info "Totale ottenuto: #{total}"
				MULTIPAGE_LOGGER.info "*************************"
      else
        if printer.color_4?
          # passo il valore 1 alla funzione get_number_of_sheet in quanto la considero come una 4 COLORI
          plant_params_1 = Spree::Order.quote_number_of_sheet(line_params, 1, step)
					MULTIPAGE_LOGGER.info "*************************"
					MULTIPAGE_LOGGER.info "Primo preventivo 4 Colori"
					MULTIPAGE_LOGGER.info "#{plant_params_1}"
          total_1 += printer.print_cost(line_params, plant_params_1, step)
          MULTIPAGE_LOGGER.info "Totale ottenuto: #{total_1}"
					MULTIPAGE_LOGGER.info "*************************"
          # passo il valore 3 alla funzione get_number_of_sheet in quanto la considero come una 8 COLORI SENZA SCARTO
          plant_params_2 = Spree::Order.quote_number_of_sheet(line_params, 3, step)
					MULTIPAGE_LOGGER.info "*************************"
					MULTIPAGE_LOGGER.info "Primo preventivo 8 Colori senza scarto"
					MULTIPAGE_LOGGER.info "#{plant_params_2}"
          total_2 += printer.print_cost(line_params, plant_params_2, step)
          MULTIPAGE_LOGGER.info "Totale ottenuto: #{total_2}"
					MULTIPAGE_LOGGER.info "*************************"

          # Confronto i due risultati e mi salvo il minore
          if total_1 > total_2
            total = total_2
            plant_params_unic = plant_params_2
          else
            total = total_1
            plant_params_unic = plant_params_1
          end
        else
          # CALCOLO IL PREVENTIVO NORMALE
          # passo il valore 2 alla funzione get_number_of_sheet in quanto la considero come una 8 COLORI CON SCARTO
          plant_params_1 = Spree::Order.quote_number_of_sheet(line_params, 2, step)
					MULTIPAGE_LOGGER.info "*************************"
					MULTIPAGE_LOGGER.info "Primo preventivo 8 Colori con scarto"
					MULTIPAGE_LOGGER.info "#{plant_params_1}"
          total_1 += printer.print_cost(line_params, plant_params_1, step)
          MULTIPAGE_LOGGER.info "Totale ottenuto: #{total_1}"
					MULTIPAGE_LOGGER.info "*************************"
          # passo il valore 3 alla funzione get_number_of_sheet in quanto la considero come una 8 COLORI SENZA SCARTO
          plant_params_2 = Spree::Order.quote_number_of_sheet(line_params, 3, step)
					MULTIPAGE_LOGGER.info "*************************"
					MULTIPAGE_LOGGER.info "Primo preventivo 8 Colori senza scarto"
					MULTIPAGE_LOGGER.info "#{plant_params_2}"
          total_2 += printer.print_cost(line_params, plant_params_2, step)
          MULTIPAGE_LOGGER.info "Totale ottenuto: #{total_2}"
					MULTIPAGE_LOGGER.info "*************************"

          # Confronto i due risultati e mi salvo il minore
          if total_1 > total_2
            total = total_2
            plant_params_unic = plant_params_2
          else
            total = total_1
            plant_params_unic = plant_params_1
          end
        end # TERMINO IL CONFRONTO DELL'OFFSET
      end

			MULTIPAGE_LOGGER.info "*************************"
			MULTIPAGE_LOGGER.info "Totale dopo confronto tra i due"
			MULTIPAGE_LOGGER.info "#{total}"
			MULTIPAGE_LOGGER.info "*************************"
      line_params["total_#{step}".to_sym] = total
      line_params["print_option_#{step}".to_sym] = plant_params_unic
      MULTIPAGE_LOGGER.info "### FINE quote_multipage_paper_cut"
			
      line_params
    end

    # Restituisce il numero di fogli in base al numero di copie, quindi verificando la se supare o no i vari punti di
    # convenienza
    # parametri:
    #   limit               => current_item.variant.limit
    #   print_color         => print_color == "true" ? true : false
    #   number_of_facades   => Float(number_of_facades.presentation)
    #   quarter             => Float(quarter.name)
    #   number_of_copy      => Integer(number_of_copy.presentation)
    #   weight              => weight.name.to_f
    #   cut                 => current_item.variant.product.cut
    #   cut_number          => Float(quarter.cut_number)
    #   paper_init_format   => paper_init_format
    #   check               => {1 => 4 COLORI, 2 => 8 COLORI CON SCARTO, 3 => 8 COLORI SENZA SCARTO}
    #
    # hash ritornato:
    #   {:number_of_sheet => number_of_sheet, :weight_line_item => weight_line_item, :cut_price => cut_price }
    #   :number_of_sheet    => il numero di fogli sempra valorizzato
    #   :weight_line_item   => peso del prodotto sempra valorizzato
    #   :cut_price          => i costo del taglio valorizzato sempre ma ha un valore > 0 solo se si stampa sotto il
    #                          primo punto di conveninza
    #
    # Devo vrificare se il numero di copie è maggiore o minore del punto di convenienza e in caso sia maggiore
    # bisgona verificare se il numero di copie è maggiore o minore del secondo punto di convenienza che tiene conto
    # dello scarto o meno.
    def self.quote_number_of_sheet(line_params, check, step)
      #calculate_cut = false
      number_of_sheet_h = {}
      plants = 0

      if check == 2
        # SCARTO CON 8 COLORI
        MULTIPAGE_LOGGER.info "###### Inizio calcolo 8 colori con scarto"
        number_of_sheet = (line_params[:number_of_facades_internal] / (Float(line_params["quarter_#{step}".to_sym].name) * 4)).ceil * Integer(line_params[:number_of_copy].presentation)
        #plants = ((line_params[:number_of_facades_internal] / 2) / Float(line_params[:quarter].name)).ceil
        number_of_sheet_h = get_plants_and_sheet_8_colors(Float(line_params["quarter_#{step}".to_sym].name), line_params[:number_of_facades_internal])
        
        {
            :number_of_sheet => number_of_sheet,
            :plants => number_of_sheet_h[:plants],
            :system_params => number_of_sheet_h[:params],
            :waste => true
        }
      elsif check == 3
        # Devo fare il doppio preventivo per la 8 COLORI
        MULTIPAGE_LOGGER.info "###### Inizio calcolo 8 colori senza scarto"
        number_of_sheet_h = quote_systems_and_sheet(Float(line_params["quarter_#{step}".to_sym].name), line_params[:number_of_facades_internal], Integer(line_params[:number_of_copy].presentation))
        #number_of_sheet = number_of_sheet_h[:number_of_sheet] * Integer(line_params[:number_of_copy].presentation)
        
        {
            :number_of_sheet => number_of_sheet_h[:number_of_sheet],
            :plants => number_of_sheet_h[:plants],
            :system_params => number_of_sheet_h[:params],
            :waste => false
        }
      elsif check == 1
        # Devo fare il doppio preventivo per la 4 COLORI
        MULTIPAGE_LOGGER.info "###### Inizio calcolo 4 colori"
        number_of_sheet_h = quote_plants_and_sheet_4_colors(Float(line_params["quarter_#{step}".to_sym].name), line_params[:number_of_facades_internal])
        number_of_sheet = number_of_sheet_h[:number_of_sheet] * Integer(line_params[:number_of_copy].presentation)
        
        {
            :number_of_sheet => number_of_sheet,
            :plants => number_of_sheet_h[:plants],
            :system_params => number_of_sheet_h[:params],
            :waste => false
        }
      end
    end


    # Calcola il costo della carta se sono in risma
    # parametri:
    #       weight: option_value
    #       number_of_copy: nel caso del multipagina deve essere (numero_di_facciate / 2) * numero_di_copie
    #       quarter: sono i quartini o le pose (sono degli option_value dove il campo name è qullo che ci interessa)
    def self.paper_cost_in_risma_multipage(line_params, weight, number_of_copy, quarter, step)
      # Calcolo il prezzo per foglio che consiste nel prezzo della risma / # di fogli ossia 1000
      line_params["price_per_sheet_#{step}".to_sym] = weight / 1000.to_f
      MULTIPAGE_LOGGER.info "price_per_sheet: #{line_params["price_per_sheet_#{step}".to_sym]} *********************************"
      # Ottengo i numeri di fogli
      line_params["number_of_sheet_#{step}".to_sym] = (number_of_copy.to_f / quarter.to_f).ceil
      MULTIPAGE_LOGGER.info "number_of_sheet: #{line_params["number_of_sheet_#{step}".to_sym]} *********************************"
      # costo della carta
      line_params["total_#{step}".to_sym] += line_params["price_per_sheet_#{step}".to_sym] * line_params["number_of_sheet_#{step}".to_sym]
      MULTIPAGE_LOGGER.info "total: #{line_params["total_#{step}".to_sym]} *********************************"
      line_params
    end

    # Ritorna un array di hash composto da {quartini , numero di volte da usare il quartino e divisore}.
    # I parametri passati devono essere il numero di quartini impostato sulla stampante e il numero di facciate che si
    # vuole stampare (naturalmente esclusa la copertina se presente).
    # I parametri ritornati sono il numero di quartini di riferimento.
    # La quantità ossia quanti impianti per quartino servono, per esempio se i quarti sono 8 e la quantità 3 si dovranno
    # fare 3 (*2) impianti di quel tipo. (Si moltiplica per 2 perché bisogna farli fronte e retro.)
    # Il  <tt> divisor </tt> server per fare i calcoli dopo, se il divisor è 0 si moltiplica normalmente la quantity
    # per il numero di copie da fare, mentre se invece divisor è maggiore di zero bisogna fare:
    #
    #     n_of_copy * quantity * (1/divisor)
    #
    # ==== Examples
    #
    #      quote_systems(quarter_of_printer, number_of_facades) =>
    #                               [{:quarter=>8, :quantity=>3, :divisor=>0}, {:quarter=>2, :quantity=>1, :divisor=>4}]
    #
    def self.quote_systems(quarter_of_printer, number_of_facades)
      # Mi calcolo il numero di quarti che devo stampare in base al numero di facciate tramite questa formula:
      #   (n_facciate/4) * 2
      fatt = !(Integer(quarter_of_printer) % 3 == 0)
      quarter_to_print = ((number_of_facades.to_f / 4.to_f) * 2.to_f).to_f
      MULTIPAGE_LOGGER.info "SONO IN quote_systems => quarter_to_print #{quarter_to_print} *********************************"

      # Moltiplico il numero di quarti della stampante per due, perchè devo farli diventare per fronte e retro.
      quarter_of_printer = Float(quarter_of_printer) * 2

      # Chiamo la funzione ricorsiva per il calcolo
      Spree::Order.quote_systems_recursive(quarter_of_printer, quarter_to_print, (fatt ? 1 : 0), fatt)
    end

    def self.quote_systems_recursive(quarter_of_printer, quarter_to_print, divisor, fatt)
      r = []
      quantity = Integer(quarter_to_print) / Integer(quarter_of_printer)
      r << {:quarter => Integer(quarter_of_printer) * 2, :number_of_sheet => quantity, :copy_for_sheet => (divisor != 0) ? divisor : 1 } if quantity > 0
      rest = Integer(quarter_to_print) % Integer(quarter_of_printer)
      if fatt
        divisor *= 2
      else
        divisor += 3
      end
      if rest > 0
        if fatt
          div_tmp = Integer(quarter_of_printer) / 2
        else
          if Integer(quarter_to_print) % 3 == 0
            div_tmp = Integer(quarter_of_printer) / 2
          else
            div_tmp = Integer(quarter_of_printer) / 3
            if div_tmp % 3 == 0 || div_tmp == 1
              div_tmp = Integer(quarter_of_printer) / 2
            end
          end
        end
        r = r + Spree::Order.quote_systems_recursive(div_tmp, rest, divisor, fatt)
      end
      r
    end

    # number_of_color => è il numero dei colori della stampante, può essere 4 o 8, nel caso sia 8 colori bisogna fare
    # "l'ottimizzazione sulla carta" mentre, nel caso sia 4 colori bisogna fare l'ottimizzazione degli impianti
    def self.quote_systems_and_sheet(quarter_of_printer, number_of_facades, number_of_copy, number_of_color = nil)
      # Todo da valutare se tenere il parametro: number_of_color
      sum = 0
      tmp = Spree::Order.quote_systems(quarter_of_printer, number_of_facades)
      ret = {}

      tmp.each do |v|
        MULTIPAGE_LOGGER.info "v[:number_of_sheet]: #{v[:number_of_sheet]} *********************************"
        if Float(v[:copy_for_sheet]) == 0
          sum += Integer(v[:number_of_sheet]) * Integer(number_of_copy)
        else
          sum += Integer(v[:number_of_sheet]) * Integer(number_of_copy) * (1/Float(v[:copy_for_sheet]))
        end
      end

      # Per ottenere degli impianti devo moltiplicare la somma ti tutte le quantity * 2
      {:number_of_sheet => sum, :plants => tmp.map { |v| v[:number_of_sheet] }.sum* 2, :params => tmp}
    end

    # Calcola il costo della plastificazione front e back in automatico
    def self.quote_plasticization(plasticization_type, number_of_sheet, printer_format)
			t = (plasticization_type.match(/^polished_film/).blank?) ? "matt_film" : "polished_film"
      # t = plasticization_type.split('-')
      price_for_sheet = Spree::Plasticization.get_plasticization_price_for_sheet(t.strip, number_of_sheet, printer_format)
			MULTIPAGE_LOGGER.info "Costo plastificazione per foglio: #{price_for_sheet.price}"
      r = price_for_sheet.price * number_of_sheet * 2
			MULTIPAGE_LOGGER.info "Costo plastificazione per tutto: #{r}"
      r += Spree::Plasticization.get_plasticization_price_start_up(number_of_sheet)
    end

    # Calcolo del Tempo di taglio:
    # da documentazione:
    #     Tempo di taglio= (fogli stampa/fogli risma)*(N° tagli/taglia lminuto)
    # parametri:
    #   pose => può essere sia pose che quarter
    def self.cost_cutting_time(number_of_sheet, weight, cut, cut_number, paper_init_format = nil)
      if paper_init_format
        number_of_sheet = (number_of_sheet / paper_init_format[:quantity].to_f).ceil
      end

      paper_per_ream = Spree::Weight.find_by_name Integer(weight)
      cut_in_min = cut.average_hourly_print / 60
      (((Float(number_of_sheet) / Float(paper_per_ream.ream)).ceil) * (cut_number / Float(cut_in_min)))
    end

    ####################################################################################################################
    #                                           Calculate cover                                                        #
    ####################################################################################################################
    # parametri
    #   paper_cover
    #   print_color_cover
    #   number_of_copy
    #   limit_cover
    #   cover_weight
    def quote_cover(line_params)
      MULTIPAGE_LOGGER.info "### INIZIO quote_cover"
      #  passo uno: trovare il formato della carta da cui si parte x esempio: 35*50 deriva 70*100
      #line_params[:paper_format_cover] = Spree::Order.get_paper_format_cover(line_params[:number_of_copy].presentation.to_i * 2, line_params[:print_color_cover_bool], limit)
      ## Recupero il numero di pose in base al numero di copie e se stampa in colori o bianco e nero
      #line_params[:pose_cover] = Spree::Order.get_pose_cover(line_params[:number_of_copy].presentation.to_i * 2, print_color, limit)
      ## Recupero la stampante in base al numero di copie e se stampa in colori o bianco e nero
      ## devo vedere se faccio fronte e retro calcolo il doppio numero di copie => sarà sempre fronte e retro
      #line_params[:printer_cover] = Spree::Order.get_printer_cover((number_of_copy * 2), print_color, limit)
      line_params = Spree::Order.quote_paper_cover(line_params)
      line_params[:paper_init_format_cover] = get_start_format(line_params[:paper_format_cover])
      MULTIPAGE_LOGGER.info "Formato carta padre per la copertina: #{line_params[:paper_init_format_cover].to_json}"
      line_params = backfill_cover(line_params, true)
      
      line_params[:weight_line_item_cover] = line_params[:weight_line_item_cover].to_f
      MULTIPAGE_LOGGER.info "### FINISCO quote_cover"
      line_params
      #{:total => total, :weight_line_item => weight_line_item}
    end

    # ricavo il tipo di carta a seconda della stampante selezionata
    # Digitale  =>  Risma e nel caso Tagliato
    # Offset  =>  Solo Tagliato
    def backfill_cover(line_params, front_back = false)
      MULTIPAGE_LOGGER.info "### INIZIO backfill_paper_cover"
      line_params[:total_cover] = 0
      line_params[:weight_line_item_cover] = 0
      if line_params[:printer_cover].digital?
        MULTIPAGE_LOGGER.info "STAMPO COPERTINA IN DIGITALE"
        if line_params[:cover_weight].ream?
          line_params = quote_cover_paper_ream(line_params)
        else
          line_params = quote_cover_paper_cut(line_params)
        end
				line_params = quote_printer_cover(line_params, front_back)
      else
        MULTIPAGE_LOGGER.info "STAMPO COPERTINA IN OFFSET"
        line_params = quote_cover_paper_cut(line_params)
        line_params = quote_printer_cover(line_params, front_back)
      end

      # Aggiungo il prezzo per la plastificazione se presente
      line_params[:total_cover] += plasticization_price_cover(line_params) if line_params[:plastification]
      MULTIPAGE_LOGGER.info "totale dopo plastificazione della copertina: #{line_params[:total_cover]} *********************************" if line_params[:plastification]

      # Aggiungo il costo di avviamento del tagliato
      line_params[:total_cover] += (line_params[:variant].cover_variant.product.cut.hourly_cost * line_params[:variant].cover_variant.product.cut.start_time)/60
      MULTIPAGE_LOGGER.info "totale dopo avviamento taglio copertina: #{line_params[:total_cover]} *********************************"
      # Aggiungo il costo del tempo di taglio
      line_params[:total_cover] += quote_cutting_time_cover(line_params[:variant].cover_variant.product.cut, line_params[:number_of_sheet_cover], line_params[:cover_weight], line_params[:pose_cover])
      MULTIPAGE_LOGGER.info "totale dopo taglio copertina: #{line_params[:total_cover]} *********************************"

      line_params
    end

    # Ricavo il costo e tutti i valori necessari per la carta in formato TAGLIATO
    def quote_cover_paper_cut(line_params)
      MULTIPAGE_LOGGER.info "### INIZIO quote_cover_paper_cut"
      # Calcolo il costo della carta
      line_params[:price_kg_cover] = Float(line_params[:cover_weight].cost_cut)
      MULTIPAGE_LOGGER.info "price_kg_cover: #{line_params[:price_kg_cover]}"
      # Calcolo il prezzo per foglio è dato da:
      # supponendo di avere:
      #   - 64x88 cm
      #   - 150 gr
      #   - 0,90 €/kg
      # avrò: PREZZO PER FOGLIO= (64/100)*(88/100)*PREZZO PER KILO * GRAMMATURA FOGLIO IN KG
      line_params[:price_per_sheet_cover] = (line_params[:paper_init_format_cover][:master][0] / 100.to_f) * (line_params[:paper_init_format_cover][:master][1] / 100.to_f) * (line_params[:price_kg_cover] * (line_params[:cover_weight].name.to_f / 1000.to_f))
      MULTIPAGE_LOGGER.info "price_per_sheet_cover: #{line_params[:price_per_sheet_cover]}"
      # Ottengo i numero di fogli
      line_params[:number_of_sheet_cover] = ((line_params[:number_of_copy].presentation.to_i) / line_params[:pose_cover].name.to_f).ceil
      MULTIPAGE_LOGGER.info "number_of_copy: #{line_params[:number_of_copy].presentation.to_f}"
      MULTIPAGE_LOGGER.info "pose_cover: #{line_params[:pose_cover].name}"
      MULTIPAGE_LOGGER.info "number_of_sheet_cover: #{line_params[:number_of_sheet_cover]}"
      # Divido il numero di fogli per la quantità di fogli che ci stanno sul formato più grande
      line_params[:total_cover] += (line_params[:price_per_sheet_cover] * (line_params[:number_of_sheet_cover]/line_params[:paper_init_format_cover][:quantity].to_f).ceil)
      MULTIPAGE_LOGGER.info "totale dopo aggiunta costo fogli copertina: #{line_params[:total_cover]}"
      # aggiungo il costo della passata
      line_params[:total_cover] += line_params[:number_of_sheet_cover] * line_params[:price_front_back_cover]
      MULTIPAGE_LOGGER.info "totale dopo aggiunta passate copertina: #{line_params[:total_cover]}"
      line_params[:number_of_sheet] +=line_params[:number_of_sheet_cover]
			MULTIPAGE_LOGGER.info "### FINE quote_cover_paper_cut"
      line_params
    end

    # Ricavo il costo per la stampante se TAGLIATO
    def quote_printer_cover(line_params, front_back)
      MULTIPAGE_LOGGER.info "### INIZIO quote_printer_cover_cut *********************************"
			if front_back
				if line_params[:number_of_sheet_cover] < line_params[:printer_cover].min_limit_waiting.to_i
					line_params[:total_cover] += line_params[:printer_cover].cost_waiting.to_f
				end
			end

			if line_params[:printer_cover].color_4?
        #Aggiungo costo impianto
				if(line_params[:pose_cover].name.to_f > 1)
	        if line_params[:print_color_cover_bool] && line_params[:printer_cover].plant_color_c
	          line_params[:total_cover] += Float(line_params[:printer_cover].plant_color_c)
	        elsif line_params[:printer_cover].plant_color_bw
	          line_params[:total_cover] += Float(line_params[:printer_cover].plant_color_bw)
	        end
				else
	        if line_params[:print_color_cover_bool] && line_params[:printer_cover].plant_color_c
	          if front_back
	            line_params[:total_cover] += (Float(line_params[:printer_cover].plant_color_c) * 2)
	          else
	            line_params[:total_cover] += Float(line_params[:printer_cover].plant_color_c)
	          end
	        elsif line_params[:printer_cover].plant_color_bw
	          if front_back
	            line_params[:total_cover] += (Float(line_params[:printer_cover].plant_color_bw) * 2)
	          else
	            line_params[:total_cover] += Float(line_params[:printer_cover].plant_color_bw)
	          end
	        end
				end
      else
        #Aggiungo avviamento se presente
				if line_params[:print_color_cover_bool] && line_params[:printer_cover].plant_color_c
					line_params[:total_cover] += (Float(line_params[:printer_cover].plant_color_c) * 2)
				elsif line_params[:printer_cover].plant_color_bw
					line_params[:total_cover] += (Float(line_params[:printer_cover].plant_color_bw) * 2)
				end
			end
			MULTIPAGE_LOGGER.info "totale dopo aggiunta costo impianti stampa copertina: #{line_params[:total_cover]} *********************************"
      
      # Devo moltiplicare per due il numero di fogli nel caso siano fronte e retro
      if front_back
        molt = 2
      else
        molt = 1
      end
      
      # Aggiungo costo di avviamento
      # Calcolandolo nel seguent modo:
      # (costo_orario * tempo_avviamento)/60
      # Calcolo il costo di finitura: che è dato dai seguenti valori:
      #   - printer_hourly_cost
      #   - average_hourly_print (colori o bw)
      #   - number_of_sheet
      #   (number_of_sheet /  average_hourly_print) *  printer_hourly_cost
      if line_params[:print_color_cover_bool]
        line_params[:total_cover] += Float(line_params[:printer_cover].printer_hourly_cost_c) * Float(line_params[:printer_cover].start_time_c)/60.to_f
        if line_params[:printer_cover].average_hourly_print_c > 0
          line_params[:total_cover] += Float(line_params[:printer_cover].printer_hourly_cost_c) * ((line_params[:number_of_sheet_cover] * molt) / Float(line_params[:printer_cover].average_hourly_print_c))
        end
        # calcolo il costo dello spreco di fogli
        if front_back
          line_params[:total_cover] += line_params[:printer_cover].start_up_waste_paper_cost(front_back, true, line_params[:price_per_sheet_cover])
        else
          line_params[:total_cover] += line_params[:printer_cover].start_up_waste_paper_cost(front_back, true, line_params[:price_per_sheet_cover])
        end
      else
        line_params[:total_cover] += Float(line_params[:printer_cover].printer_hourly_cost_bw) * Float(line_params[:printer_cover].start_time_bw)/60.to_f
        if line_params[:printer_cover].average_hourly_print_bw > 0
          line_params[:total_cover] += Float(line_params[:printer_cover].printer_hourly_cost_bw) * ((line_params[:number_of_sheet_cover] * molt) / Float(line_params[:printer_cover].average_hourly_print_bw))
        end
        # calcolo il costo dello spreco di fogli
        if front_back
          line_params[:total_cover] += line_params[:printer_cover].start_up_waste_paper_cost(front_back, true, line_params[:price_per_sheet_cover])
        else
          line_params[:total_cover] += line_params[:printer_cover].start_up_waste_paper_cost(front_back, true, line_params[:price_per_sheet_cover])
        end
      end
      MULTIPAGE_LOGGER.info "totale dopo aggiunta costo stampa copertina: #{line_params[:total_cover]} *********************************"
      # NB: il peso è in grammi quindi lo devo trasformare in kg
      line_params[:weight_line_item_cover] = (line_params[:variant].cover_variant.width.to_f / 100.to_f) * (line_params[:variant].cover_variant.height.to_f / 100.to_f) * (line_params[:cover_weight].weight.to_f / 1000.to_f) * line_params[:number_of_copy].presentation.to_i
      #line_params[:weight_line_item_cover] += (line_params[:number_of_sheet_cover]/line_params[:paper_init_format_cover][:quantity].to_f) * (line_params[:cover_weight].weight.to_f / 1000)
      MULTIPAGE_LOGGER.info "peso copertina: #{line_params[:weight_line_item_cover]} *********************************"
      
      line_params
    end

    # Ricavo il costo e i valori necessari per la carta in formato RISMA
    def quote_cover_paper_ream(line_params)
      MULTIPAGE_LOGGER.info "### INIZIO quote_cover_paper_ream"
      # Non è formato tagliato quindi si calcola in risma
      line_params[:price_ream_cover] = Float(line_params[:cover_weight].cost_ream)
      MULTIPAGE_LOGGER.info "price_ream_cover: #{line_params[:price_ream_cover]}"
      # Calcolo il prezzo per foglio che consiste nel prezzo della risma / # di fogli ossia 1000
      line_params[:price_per_sheet_cover] = line_params[:price_ream_cover] / 1000.to_f
      MULTIPAGE_LOGGER.info "price_per_sheet_cover: #{line_params[:price_per_sheet_cover]}"
      # Ottengo i numeri di fogli
      line_params[:number_of_sheet_cover] = (Float(line_params[:number_of_copy].presentation) / Float(line_params[:pose_cover].name).to_f).ceil
      MULTIPAGE_LOGGER.info "number_of_sheet_cover: #{line_params[:number_of_sheet_cover]}"
      # inizio a calcolare il totale
      line_params[:total_cover] += line_params[:price_per_sheet_cover] * line_params[:number_of_sheet_cover]
      MULTIPAGE_LOGGER.info "totale dopo aggiunta costo fogli copertina: #{line_params[:total_cover]}"
      # aggiungo il costo della carta
      line_params[:total_cover] += line_params[:number_of_sheet_cover] * line_params[:price_front_back_cover]
      MULTIPAGE_LOGGER.info "totale dopo aggiunta passate copertina: #{line_params[:total_cover]}"
      line_params[:number_of_sheet] +=line_params[:number_of_sheet_cover]
			MULTIPAGE_LOGGER.info "### FINE quote_cover_paper_ream"
      line_params
    end

    # Calcolo del Tempo di taglio:
    # da documentazione:
    #     Tempo di taglio= (fogli stampa/fogli risma)*(N° tagli/taglia lminuto)
    def quote_cutting_time_cover(cut, number_of_sheet, weight, pose)
      paper_per_ream = Spree::Weight.find_by_name(Integer(weight.name))
      cut_in_min = cut.average_hourly_print / 60
      (((Float(number_of_sheet) / Float(paper_per_ream.ream)).ceil) * (Float(pose.cut_number) / Float(cut_in_min)))
    end

    def self.quote_paper_cover(line_params)
      MULTIPAGE_LOGGER.info "### INIZIO quote_paper_cover"
      if line_params[:front_back_cover]
        qt_c = line_params[:limit_cover].quantity_c / 2
        qt_bw = line_params[:limit_cover].quantity_bw / 2
      else
        qt_c = line_params[:limit_cover].quantity_c
        qt_bw = line_params[:limit_cover].quantity_bw
      end

      number_of_copy = line_params[:number_of_copy].presentation.to_i * 2
      if line_params[:print_color_cover_bool]
        if number_of_copy > qt_c
          line_params[:paper_format_cover] = line_params[:limit_cover].format_after_c.presentation
          line_params[:pose_cover] = line_params[:limit_cover].pose_after_c
          line_params[:printer_cover] = line_params[:limit_cover].printer_after_c
        else
          line_params[:paper_format_cover] = line_params[:limit_cover].format_before_c.presentation
          line_params[:pose_cover] = line_params[:limit_cover].pose_before_c
          line_params[:printer_cover] = line_params[:limit_cover].printer_before_c
        end
        if line_params[:front_back_cover]
          line_params[:price_front_back_cover] = line_params[:printer_cover].price_front_and_back_c
        else
          line_params[:price_front_back_cover] = line_params[:printer_cover].price_front_c
        end
      else
        if number_of_copy > qt_bw
          line_params[:paper_format_cover] = line_params[:limit_cover].format_after_bw.presentation
          line_params[:pose_cover] = line_params[:limit_cover].pose_after_bw
          line_params[:printer_cover] = line_params[:limit_cover].printer_after_bw
        else
          line_params[:paper_format_cover] = line_params[:limit_cover].format_before_bw.presentation
          line_params[:pose_cover] = line_params[:limit_cover].pose_before_bw
          line_params[:printer_cover] = line_params[:limit_cover].printer_before_c
        end
        if line_params[:front_back_cover]
          line_params[:price_front_back_cover] = line_params[:printer_cover].price_front_and_back_bw
        else
          line_params[:price_front_back_cover] = line_params[:printer_cover].price_front_bw
        end
      end
      MULTIPAGE_LOGGER.info "Formato copertina: #{line_params[:paper_format_cover]}"
      MULTIPAGE_LOGGER.info "Numero pose copertina: #{line_params[:pose_cover].name}"
			MULTIPAGE_LOGGER.info "ID pose copertina: #{line_params[:pose_cover].id}"
      MULTIPAGE_LOGGER.info "stampante copertina: #{line_params[:printer_cover].presentation}"
      MULTIPAGE_LOGGER.info "Prezzo stampa fronte e retro: #{line_params[:price_front_back_cover]}"
			MULTIPAGE_LOGGER.info "### FINISCO quote_paper_cover"
      line_params
    end

    # Per calcolare il prezzo della plastificazione mi serve sapere:
    #   - tipo plastificazione (Film Lucida o  Film Opaca)
    #   - numero di fogli da stampare
    #   - formato del foglio della stampante
    # def plasticization_price_cover(line_params)
#       tot = 0
#       MULTIPAGE_LOGGER.info "SONO IN plasticization_price *********************************"
#       if !Spree::Plasticization.first.max_quantity.blank? && line_params[:number_of_sheet_cover] <= Spree::Plasticization.first.max_quantity
#         tot += Spree::Plasticization.first.price_start_up.to_f
#         MULTIPAGE_LOGGER.info "aggiungo costo avvio plastificazione per la copertina: #{tot} *********************************"
#       end
#       t = line_params[:plastification].name.split('-')
#       price_for_sheet = Spree::Plasticization.get_plasticization_price_for_sheet(t[0].strip, line_params[:number_of_sheet_cover], line_params[:paper_format_cover])
#       r = price_for_sheet.price.to_f * line_params[:number_of_sheet_cover]
#       if t[1].strip == "fronte e retro" # PLASTIFICO ENTRAMBI I LATI
#         tot += r * 2
#       else # PLASTIFICO SOLO IL FRONTE
#         tot += r
#       end
#       MULTIPAGE_LOGGER.info "aggiungo costo plastificazione copertina: #{tot} *********************************"
#       tot
#     end
		def plasticization_price_cover(line_params)
			tot = 0
			if !Spree::Plasticization.first.max_quantity.blank? && line_params[:number_of_sheet_cover] <= Spree::Plasticization.first.max_quantity
				tot += Spree::Plasticization.first.price_start_up.to_f
			end
			t = line_params[:plastification].name
			plastification_type = (t.match(/^polished_film/).blank?) ? "matt_film" : "polished_film"
			price_for_sheet = Spree::Plasticization.get_plasticization_price_for_sheet(plastification_type, line_params[:number_of_sheet_cover], line_params[:paper_format_cover])
			r = price_for_sheet.price.to_f * line_params[:number_of_sheet_cover]
			if !t.match(/front_back/).blank? # PLASTIFICO ENTRAMBI I LATI
				tot += r * 2
			else # PLASTIFICO SOLO IL FRONTE
				tot += r
			end
			tot
		end

    # Vengo raccolte tutte le operazioni da effettuare sulla copertina.
    # Le operazioni fatte sulla copertina sono:
    #   - cordonatura
    #   - costo segnature
    #   - costo taglio
    #
    # parametri
    #   creasing        => current_item.product.creasing
    #   paper           => paper_cover
    #   weight          => Integer(weight.name)
    #   weight_cover    => Float(cover_weight.name)
    #   number_of_copy  => Integer(number_of_copy.presentation)
    #
    def processes_cover creasing, paper, weight, weight_cover, number_of_copy
      total = 0

      if weight_cover > weight
        total += creasing.compute_working(number_of_copy)
      end
      total
    end

    ####################################################################################################################
    #                                                NUOVO CALCOLO                                                     #
    ####################################################################################################################
    def self.quote_plants_and_sheet_4_colors(quarter_of_printer, facades)
			MULTIPAGE_LOGGER.info "*************************"
      MULTIPAGE_LOGGER.info "Quartini della stampante: #{quarter_of_printer}"
      MULTIPAGE_LOGGER.info "Facciate da stampare: #{facades}"
      sheets = 0
      r = {}
      # Devo vedere quanti impianti mi servono:
      MULTIPAGE_LOGGER.info "(facciate/4 % quartini): #{(facades/4.to_f) % quarter_of_printer}"
      MULTIPAGE_LOGGER.info "PRIMA DELL'IF #{(quarter_of_printer > 3 && (quarter_of_printer % 3 == 0) && (facades/4.to_f) > (quarter_of_printer/2.to_f) && (facades/4.to_f) % quarter_of_printer > (quarter_of_printer/2.to_f))} *********************************"
      if (quarter_of_printer > 3 && (quarter_of_printer % 3 == 0) && (facades/4.to_f) > (quarter_of_printer/2.to_f) && (facades/4.to_f) % quarter_of_printer > (quarter_of_printer/2.to_f))
        MULTIPAGE_LOGGER.info "VADO IN quote_4_on_3_colors"
        r = Spree::Order.quote_4_on_3_colors(quarter_of_printer, facades)
      else
        MULTIPAGE_LOGGER.info "VADO IN quote_4_colors"
        calc = Spree::Order.quote_4_colors(quarter_of_printer, facades)
        
        MULTIPAGE_LOGGER.info "CONTINUO IN get_plants_and_sheet_4_colors *********************************"
        # SOMMO IL NUMERO DI IMPIANTI/DIVISORE
        s = calc.sum { |q| q[:value]/Float(q[:divisor]) }
        MULTIPAGE_LOGGER.info "s: #{s} *********************************"
        # ARROTONDO IL TOTALE OTTENUTO
        dec = s - Integer(s).ceil
        MULTIPAGE_LOGGER.info "dec: #{dec}"
        # RICAVO IL NUMERO DI IMPIANTI REALE MOLTIPLICANDO QUELLO OTTENUTO * 2
        plants = (Integer(s) * 2) + (dec * (dec <= 0.5 ? 1 : 2)).ceil
        MULTIPAGE_LOGGER.info "Impianti: #{plants}"
        h_r = []

        ####################### CALCOLO SE SONO CON QUARTINI DIVISIBILI PER 3 #######################
        if (quarter_of_printer % 3) == 0
          MULTIPAGE_LOGGER.info "SONO CON QUARTINI DIVISIBILI PER 3"
          h = {}
          if calc.any? { |d| d[:divisor] == 1 }
            number_of_sheet = calc.select { |d| d[:divisor] == 1 }.first[:value]
            number_of_sheet.times do |tn|
              h[:quarter] = quarter_of_printer * 4
              h[:copy_for_sheet] = 1
              #h[:value] = calc.collect { |j| j[:value] }
              h[:divisor] = 1
              h[:number_of_sheet] = 1
              h_r << h
            end
          end
          h = {}
          t = calc.sum do |d|
            if d[:divisor] != 1
              d[:value] / Float(d[:divisor])
            else
              0
            end
          end
          if t > 0
            i = 1
            number_of_sheet = 1
            z = calc.count - (calc.any? { |d| d[:divisor] == 1 } ? 1 : 0)
            if t <= 0.5
              i = 2
              t_f = t * 2
              t = t * 2
              z = (z > 1) ? z/2 : z

              while (t < 1 && t_f <= 0.5)
                if t_f <= (1 - t)
                  i += 2
                end
                t += t_f
              end
            end
            
            h[:copy_for_sheet] = i / z
            div = calc.select { |d| d[:divisor] != 1 }.first[:divisor]
            if (div % 3 == 0)
              sp = 0
              calc.each do |d|
                if d[:divisor] != 1
                  sp += quarter_of_printer * (d[:value] / Float(d[:divisor]))
                end
              end
              h[:divisor] = quarter_of_printer / Integer(sp)
            else
              h[:divisor] = div
            end
            h[:number_of_sheet] = number_of_sheet
            h[:quarter] = (1/t.to_f).ceil * 4
            h_r << h
          end
          #h_r.sum {|ns| ns[:number_of_sheet] }
          r = {:number_of_sheet => h_r.sum { |hr| hr[:number_of_sheet]/hr[:copy_for_sheet].to_f }.round(2), :plants => plants, :params => h_r}
          ####################### CALCOLO SE SONO CON QUARTINI DIVISIBILI PER 2 #######################
        else
          MULTIPAGE_LOGGER.info "SONO CON QUARTINI DIVISIBILI PER 2"
          calc.each do |v|
            h = {}
            if v[:divisor] != 1
              t = v[:value] / Float(v[:divisor])
              t_f = t
              i = 1
              number_of_sheet = 1
              while t != 1 && t < 1
                t += t_f
                i += 1
              end
            else
              number_of_sheet = v[:value]
              i = 1
            end
            h[:copy_for_sheet] = i
            #h[:value] = v[:value]
            h[:divisor] = v[:divisor]
            h[:number_of_sheet] = number_of_sheet
            h[:quarter] = (quarter_of_printer * 4)/h[:copy_for_sheet]
            h_r << h
            plants = 0
            h_r.each do |hr|
              if hr[:copy_for_sheet] > hr[:number_of_sheet]
                plants += (1 * hr[:number_of_sheet])
              else
                plants += (2 * hr[:number_of_sheet])
              end
            end
          end
          #h_r.sum {|ns| ns[:number_of_sheet] }
          r = {:number_of_sheet => h_r.sum { |hr| hr[:number_of_sheet]/hr[:copy_for_sheet].to_f }.round(2), :plants => plants, :params => h_r}
        end ####################### CALCOLO SE SONO CON QUARTINI DIVISIBILI PER 3 #######################
      end # CHIUDO L'IF DEL CONTROLLO SE SONO SOPRA LA META` DEI QUARTINI DI STAMPA
      r
    end

    # Mi ritorna un array composto da hash composto così:
    #       - value: numero di impianti (bisogna valutare se effettivamente va moltiplicato per due oppure si può
    #                fare tutto nella stessa facciata )
    #       - divisor: numero di volte che un quartino (inteso che necessita di due impianti) riesce a starci nel foglio
    def self.quote_4_colors(quarter_of_printer, facades)
      MULTIPAGE_LOGGER.info "SONO IN calc_4_colors *********************************"
      quarter_to_print = (facades.to_f / 4.to_f).to_f
      
      MULTIPAGE_LOGGER.info "quarter_to_print: #{quarter_to_print} *********************************"
      MULTIPAGE_LOGGER.info "VADO IN IN calc_4_colors_rec *********************************"
      Spree::Order.quote_4_colors_rec(quarter_of_printer, quarter_to_print)
    end

    def self.quote_4_colors_rec(quarter_of_printer, quarter_to_print, divisor = 1)
      MULTIPAGE_LOGGER.info "SONO IN IN calc_4_colors_rec *********************************"
      ret = []
      r1 = Integer(quarter_to_print.to_f / quarter_of_printer.to_f)
      MULTIPAGE_LOGGER.info "r1: #{r1} *********************************"
      ret << {:value => r1, :divisor => divisor} if r1 > 0
      MULTIPAGE_LOGGER.info "ret: #{ret} *********************************"
      
      rest = (quarter_to_print.to_f % quarter_of_printer.to_f)
      MULTIPAGE_LOGGER.info "rest: #{rest} *********************************"

      if rest > 0
        tmp = quote_calculate_divisor(quarter_of_printer, divisor)
        ret += Spree::Order.quote_4_colors_rec(tmp[:quarter_of_printer], rest, tmp[:divisor])
      end
      ret
    end

    # Questa funziona si occupa di valutare che divisore restituire e di calcolare il nuovo quarter_of_printer
    def self.quote_calculate_divisor(quarter_of_printer, divisor)
      if ((Integer(quarter_of_printer) % 3) == 0)
        if divisor == 1
          divisor = 3
        else
          divisor *= 2
        end

        quarter_of_printer = quarter_of_printer / 3.to_f
      else
        divisor *= 2
        quarter_of_printer = quarter_of_printer / 2.to_f
      end

      {:divisor => divisor, :quarter_of_printer => quarter_of_printer}
    end

    # Calcolo il numero di impianti e di fogli che ottengo nel caso mi trovi con:
    #   quarter_of_printer % 3 == 0
    #   facades / 4 > quarter_of_printer / 2
    def self.quote_4_on_3_colors(quarter_of_printer, facades)
      plants = 0
      number_of_sheet = 0
      h = []
      quarter_to_print = facades / 4
      if quarter_to_print > quarter_of_printer # mi creo gli impianti interi con 1 foglio e 2 impianti
        plants = (quarter_to_print / Integer(quarter_of_printer) * 2)
        number_of_sheet = quarter_to_print / Integer(quarter_of_printer)
        number_of_sheet.times do |t|
          h += [{:divisor => 1, :copy_for_sheet => 1, :number_of_sheet => 1, :quarter => (quarter_of_printer * 4)}]
        end
        quarter_to_print = (quarter_to_print % quarter_of_printer)
      end
      # Partiamo con il primo passo
      half_quarter_of_printer = quarter_of_printer / 2
      # Secondo passo
      rest_quarter_to_print = quarter_to_print - half_quarter_of_printer
      # Terzo passo
      check = (half_quarter_of_printer / Float(rest_quarter_to_print))
      # Quarto passo
      if (check < 1) # Se ci troviamo nel caso in cui avanziamo sempre
        h += [{:divisor => 1, :copy_for_sheet => 1, :number_of_sheet => 1, :quarter => (check * 4)}]
        #number_of_sheet + 1
        {:plants => plants + 2, :number_of_sheet => h.sum { |t| t[:number_of_sheet]/t[:copy_for_sheet].to_f }.round(2), :params => h}
      else # Se possiamo ottimizzare la carta
        h += [{:divisor => 2, :copy_for_sheet => 2, :number_of_sheet => 1, :quarter => (quarter_of_printer * 4)/2}, {:divisor => check, :copy_for_sheet => (check * 2), :number_of_sheet => 1, :quarter => (quarter_of_printer * 4)/(check * 2)}]
        #number_of_sheet + 2
        {:plants => plants + 2, :number_of_sheet => h.sum { |t| t[:number_of_sheet]/t[:copy_for_sheet].to_f }.round(2), :params => h}
      end
    end

    # Calcolo il numero di fogli e impianti per la 8 COLORI CON SCARTO
    def self.get_plants_and_sheet_8_colors(quarter_of_printer, facades)
      plants = 0
      number_of_sheet = 0
      h = []
      quarter_to_print = facades / 4

      # Caso in cui il mio numero di facciate iniziale diviso 4 sia maggiore dei quartini di stampa
      if quarter_to_print > quarter_of_printer
        h += [{:divisor => 1, :copy_for_sheet => 1, :number_of_sheet => (quarter_to_print/quarter_of_printer.to_i), :quarter => (quarter_of_printer * 4)}]
        plants += (quarter_to_print/quarter_of_printer.to_i) * 2
        quarter_to_print = quarter_to_print - (quarter_of_printer * (quarter_to_print/quarter_of_printer.to_i))
      end
      
      if quarter_to_print > 0
        if quarter_to_print > (quarter_of_printer / 2)
          h += [{:divisor => 1, :copy_for_sheet => 1, :number_of_sheet => 1, :quarter => (quarter_to_print * 4)}]
          plants += 2
        else
          plants += 2
          h += [{:divisor => 1, :copy_for_sheet => (quarter_of_printer / quarter_to_print), :number_of_sheet => 1, :quarter => quarter_to_print * 4}]
        end
      end
      {:plants => plants, :number_of_sheet => h.sum { |t| t[:number_of_sheet]/t[:copy_for_sheet].to_f }.round(2), :params => h}
    end
  end
end
