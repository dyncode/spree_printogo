module Spree
  class Order < ActiveRecord::Base
    def calculate_poster_line_item(line_item)
      # devo claolare il prezzo  e recuperare il peso dal nome della carta
      master = line_item.variant
      paper = line_item.line_items_option_values.select { |ov| ov.option_value.option_type.name == 'coil' if ov.option_value && ov.option_value.option_type }.first.option_value
      color = line_item.line_items_option_values.select { |ov| ov.option_value.option_type.name == 'printer_color' if ov.option_value && ov.option_value.option_type }.first.option_value

      gr = Integer(get_weight_from_name(paper.name))
      #format_paper = line_item.line_items_option_values.select { |ov| ov.option_value.option_type.name == 'poster_printer_format' if ov.option_value && ov.option_value.option_type  }.first.option_value
      format_paper = line_item.retrive_option_value_from_option_type_name('poster_printer_format')
      if format_paper.name == "custom"
        h = {:w => (Float(line_item.line_items_option_values.select { |ov| ov.option_value.name == 'custom_width' }.first.option_value.presentation) / 100.to_f),
             :h => (Float(line_item.line_items_option_values.select { |ov| ov.option_value.name == 'custom_height' }.first.option_value.presentation) / 100.to_f)}
      else
        h = get_width_height(format_paper.presentation)
      end
      #quantity = line_item.line_items_option_values.select { |ov| ov.option_value.name == 'quantity' }.first.option_value.presentation
      number_of_copy = line_item.retrive_option_value_from_name('number_of_copy').presentation.to_i
      cost_for_one_copy = (Float(h[:w]) * Float(h[:h])) * Float(paper.price)
      total = cost_for_one_copy * Integer(number_of_copy)
      # Calcolo il costo di avviamento del plotter
      total += Float(paper.option_type.printer_machine.plotter_hourly_cost) * (Float(paper.option_type.printer_machine.start_time_c)/60.to_f)

      # Calcolo il costo del plotter nel caso sia in a colori o bianco e nero:
      if color.name == 'true'
        total += Float(paper.option_type.printer_machine.cost_printing_c) * (Float(h[:w]) * Float(h[:h])) * Integer(number_of_copy)
      else
        total += Float(paper.option_type.printer_machine.cost_printing_bw) * (Float(h[:w]) * Float(h[:h])) * Integer(number_of_copy)
      end

      weight_line_item = (Float(h[:w]) * Float(h[:h])) * (gr / 1000.to_f)
      {:total => total, :weight_line_item => weight_line_item}
    end


    def calculate_flayer_line_item(line_item)
      total = 0
      weight_line_item = 0

      # Recupero la dimensione del foglio iniziale, nel caso sia stata scelta una carta TAGLIATO, altrimenti il costo
      # si basa sulla risma e non a kg
      paper = Spree::OptionValue.find line_item.line_items_option_values.select { |ov| ov.option_value.type == 'Spree::Paper' unless ov.option_value.nil? }.first.option_value_id
      if paper.option_type.name == 'tagliato'
        # E' in formato tagliato quindi si calcola a kg:
        #  passo uno: trovare il formato della carta da cui si parte x esempio: 35*50 deriva 70*100
        print_color = line_item.line_items_option_values.select { |ov| ov.option_value.option_type.name == "print_color" if ov.option_value && ov.option_value.option_type }.first
        number_of_copy = line_item.retrive_option_value_from_name('number_of_copy').presentation.to_i
        paper_format = get_paper_format_old(number_of_copy, print_color, line_item.variant.limit)
        # Recupero il numero di pose in base al numero di copie e se stampa i colori o bianco e nero
        pose = get_pose_old(number_of_copy, print_color, line_item.variant.limit)
        # Recupero la stampante in base al numero di copie e se stampa i colori o bianco e nero
        # devo vedere se faccio fronte e retro calcolo il doppio numero di copie
        front_back = line_item.line_items_option_values.select { |ov| ov.option_value.option_type.name == "front_back" if ov.option_value && ov.option_value.option_type }.first
        molt = 1
        if front_back.option_value.name == "equal_front_back" || front_back.option_value.name == "different_front_back"
          molt = 2
        end
        printer = get_printer_old((number_of_copy * molt), print_color, line_item.variant.limit)

        paper_init_format = get_start_format(paper_format)
        weight = line_item.line_items_option_values.select { |ov| ov.option_value.option_value_id == paper.id if ov.option_value }.first.option_value.name
        price_kg = Float(line_item.line_items_option_values.select { |o| o.option_value.class == Spree::Paper and o.option_value.option_type.nil? }.first.option_value.presentation)

        # Calcolo il prezzo per foglio è dato da:
        # supponendo di avere:
        #   - 64x88 cm
        #   - 150 gr
        #   - 0,90 €/kg
        # avrò: PREZZO PER FOGLIO= (64/100)*(88/100)*PREZZO PER KILO * GRAMMATURA FOGLIO IN KG
        price_per_sheet = (paper_init_format[:master][0] / 100.to_f) * (paper_init_format[:master][1] / 100.to_f) * (price_kg * (Float(weight) / 1000.to_f))

        # Ottengo i numero di fogli
        number_of_sheet = (Float(number_of_copy) / Float(pose.presentation).to_f).ceil
        # Divido il numero di fogli per la quantità di fogli che ci stanno sul formato più grande
        total += (price_per_sheet * (number_of_sheet/paper_init_format[:quantity].to_f).ceil)

        # Recupero se fronte e retro o solo fronte e poi recupero il prezzo per foglio
        #front_back = line_item.line_items_option_values.select { |ov| ov.option_value.option_type.name == "front_back" if ov.option_value && ov.option_value.option_type }.first
        print_color = line_item.line_items_option_values.select { |ov| ov.option_value.option_type.name == "print_color" if ov.option_value && ov.option_value.option_type }.first


         if front_back.option_value.name == "equal_front_back" || front_back.option_value.name == "different_front_back"
          if print_color.option_value.name == "true"
            price_front_back = printer.price_front_and_back_c
          else
            price_front_back = printer.price_front_and_back_bw
          end
        else
          if print_color.option_value.name == "true"
            price_front_back = printer.price_front_c
          else
            price_front_back = printer.price_front_bw
          end
        end
        total += (number_of_sheet/paper_init_format[:quantity].to_f).ceil * price_front_back

        # Aggiungo costo di avviamento
        # Calcolandolo nel seguent modo:
        # (costo_orario * tempo_avviamento)/60
        if print_color.option_value.name == "true"
          total += Float(printer.printer_hourly_cost_c) * Float(printer.start_time_c)/60.to_f
        else
          total += Float(printer.printer_hourly_cost_bw) * Float(printer.start_time_bw)/60.to_f
        end
        
        # Aggiungo costo per scarto di fogli
        total += printer.start_up_waste_paper_cost((front_back.option_value.name == "equal_front_back" || front_back.option_value.name == "different_front_back") ? true : false, (print_color.option_value.name == "true") ? true : false)

        # Calcolo il costo di finitura: che è dato dai seguenti valori:
        #   - printer_hourly_cost
        #   - average_hourly_print (colori o bw)
        #   - number_of_sheet
        #   (number_of_sheet /  average_hourly_print) *  printer_hourly_cost
        if print_color.option_value.name == 'true'
          hourly_cost = Float(printer.printer_hourly_cost_c) *
              (((number_of_sheet/paper_init_format[:quantity].to_f).ceil) / Float(printer.average_hourly_print_c))
        else
          hourly_cost = Float(printer.printer_hourly_cost_bw) *
              (((number_of_sheet/paper_init_format[:quantity].to_f).ceil) / Float(printer.average_hourly_print_bw))
        end
        total += hourly_cost

        # Calcolo il costo di finitura: so che costo orario è di un tot
        #hourly_cost = Float(line_item.variant.product.hourly_cost_option_type.option_values.first.presentation)
        #finish = Float(line_item.variant.finish_option_values.name)
        #hour = 60.to_f
        #total += ((finish/hour)*hourly_cost)

        # Aggiungo il prezzo per la plastificazione se presente
        plastification = line_item.line_items_option_values.select { |ov| ov.option_value.option_type.name == 'plasticization' if ov.option_value && ov.option_value.option_type }.first
        total += price_for_plasticization(plastification.option_value.name, number_of_sheet, paper_format, front_back.option_value.name, paper_init_format) if plastification


        #Aggiungo avviamento se presente
        if print_color.option_value.name == "true" && printer.plant_color_c
          total += Float(printer.plant_color_c)
        elsif printer.plant_color_bw
          total += Float(printer.plant_color_bw)
        end

        # NB: il peso è in grammi quindi lo devo trasformare in kg
        weight_line_item = (number_of_sheet/paper_init_format[:quantity].to_f) * (weight.to_f / 1000)

        # Aggiungo il costo di avviamento del tagliato
        total += (line_item.variant.product.cut.hourly_cost * line_item.variant.product.cut.start_time)/60

        # Aggiungo il costo del tempo di taglio
        total += cutting_time(line_item, number_of_sheet, weight, paper_init_format, pose)
      else
        # Non è formato tagliato quindi si calcola in risma

        price_ream = Float(line_item.weight.presentation.gsub(',', '.'))

        # Calcolo il prezzo per foglio che consiste nel prezzo della risma / # di fogli ossia 1000
        price_per_sheet = price_ream / 1000.to_f
        # Ottengo i numeri di fogli
        number_of_sheet = (Float(line_item.variant.number_of_copy_option_values.name) / Float(line_item.variant.poses_option_values.name).to_f).ceil
        # inizio a calcolare il totale
        total += price_per_sheet * number_of_sheet

        # Recupero se fronte e retro o solo fronte e poi recupero il prezzo per foglio
        front_back = line_item.line_items_option_values.select { |ov| ov.option_value.option_type.name == "front_back" if ov.option_value.option_type }.first
        print_color = line_item.line_items_option_values.select { |ov| ov.option_value.option_type.name == "print_color" if ov.option_value.option_type }.first


        if front_back.option_value.name == "front_back"
          if print_color.option_value.name == "true"
            price_front_back = line_item.variant.printer_option_values.price_front_and_back_c
          else
            price_front_back = line_item.variant.printer_option_values.price_front_and_back_bw
          end
        else
          if print_color.option_value.name == "true"
            price_front_back = line_item.variant.printer_option_values.price_front_c
          else
            price_front_back = line_item.variant.printer_option_values.price_front_bw
          end
        end

        total += number_of_sheet * price_front_back

        # Aggiungo costo di avviamento
        # Calcolandolo nel seguente modo:
        # (costo_orario * tempo_avviamento)/60
        if print_color.option_value.name == "true"
          total += Float(line_item.variant.printer_option_values.printer_hourly_cost_c) * Float(line_item.variant.printer_option_values.start_time_c)/60.to_f
        else
          total += Float(line_item.variant.printer_option_values.printer_hourly_cost_bw) * Float(line_item.variant.printer_option_values.start_time_bw)/60.to_f
        end
        
        # Aggiungo costo per scarto di fogli
        total += printer.start_up_waste_paper_cost((front_back.option_value.name == "front_back") ? true : false, (print_color.option_value.name == "true") ? true : false)

        # Calcolo il costo di finitura: che è dato dai seguenti valori:
        #   - printer_hourly_cost
        #   - average_hourly_print (colori o bw)
        #   - number_of_sheet
        #   (number_of_sheet /  average_hourly_print) *  printer_hourly_cost
        if print_color.option_value.name == 'true'
          hourly_cost = Float(line_item.variant.printer_option_values.printer_hourly_cost_c) *
              (number_of_sheet/Float(line_item.variant.printer_option_values.average_hourly_print_c))
        else
          hourly_cost = Float(line_item.variant.printer_option_values.printer_hourly_cost_bw) *
              (number_of_sheet/Float(line_item.variant.printer_option_values.average_hourly_print_bw))
        end
        total += hourly_cost

        #hourly_cost = Float(line_item.variant.product.hourly_cost_option_type.option_values.first.presentation)
        #finish = Float(line_item.variant.finish_option_values.name)
        #hour = 60.to_f
        #total += ((finish/hour)*hourly_cost)

        # Aggiungo il prezzo per la plastificazione se presente
        plastification = line_item.line_items_option_values.select { |ov| ov.option_value.option_type.name == 'plasticization' if ov.option_value && ov.option_value.option_type }.first
        total += price_for_plasticization(plasticization_type, number_of_sheet, paper_format, front_back.option_value.name) if plastification


        if print_color.option_value.name == "true" && line_item.variant.printer_option_values.plant_color_c
          total += Float(line_item.variant.printer_option_values.plant_color_c)
        elsif line_item.variant.printer_option_values.plant_color_bw
          total += Float(line_item.variant.printer_option_values.plant_color_bw)
        end

        # Aggiungo il costo di avviamento del tagliato
        total += (line_item.variant.cut.product.hourly_cost * line_item.variant.product.start_time)/60
        # Aggiungo il costo del tempo di taglio
        total += cutting_time(line_item, number_of_sheet, weight)


      end
      {:total => total, :weight_line_item => weight_line_item}
    end


    def calculate_business_card_line_item(line_item)
      total = 0
      weight_line_item = 0
      # Recupero il formato di stampa (per esempio 32*45)
      ####     OLD    ####
      #printer = line_item.variant.printer_format_option_values
      #printer = printer.name
      #printer_format = line_item.variant.option_values.select { |ov| ov.name == printer && ov.type == nil }.first.presentation
      ####   END OLD  ####
      # Per recuperare il foramto mi serve il numero di copie e se stampo in bianco e nero o a colori!
      #print_color = line_item.line_items_option_values.select { |ov| ov.option_value.option_type.name == "print_color" if ov.option_value && ov.option_value.option_type }.first
      #number_of_copies = line_item.quantity
      #printer_format = get_printer_format(number_of_copies, print_color, line_item.variant.limit)
      
      # Recupero la dimensione del foglio iniziale, nel caso sia stata scelta una carta TAGLIATO, altrimenti il costo
      # si basa sulla risma e non a kg
      paper = Spree::OptionValue.find line_item.line_items_option_values.select { |ov| ov.option_value.type == 'Spree::Paper' unless ov.option_value.nil? }.first.option_value_id
      
      if paper.option_type.name == 'tagliato'
        # E' in formato tagliato quindi si calcola a kg:
        #  passo uno: trovare il formato della carta da cui si parte x esempio: 35*50 deriva 70*100
        print_color = line_item.line_items_option_values.select { |ov| ov.option_value.option_type.name == "print_color" if ov.option_value && ov.option_value.option_type }.first
        logger.info "print_color : #{print_color.option_value.name}"
        number_of_copy = line_item.retrive_option_value_from_name('number_of_copy').presentation.to_i
        paper_format = get_paper_format_old(number_of_copy, print_color, line_item.variant.limit)
        logger.info "paper_format : #{paper_format.to_json}"
        # Recupero il numero di pose in base al numero di copie e se stampa i colori o bianco e nero
        pose = get_pose_old(number_of_copy, print_color, line_item.variant.limit)
        logger.info "pose : #{pose.to_json}"
        # Recupero la stampante in base al numero di copie e se stampa i colori o bianco e nero
        printer = get_printer_old(number_of_copy, print_color, line_item.variant.limit)
        logger.info "printer : #{printer.to_json}"
        ####     OLD    ####
        #paper_format = line_item.variant.printer_format_option_values.presentation
        ####   END OLD  ####
        paper_init_format = get_start_format(paper_format)
        logger.info "paper_init_format : #{paper_init_format.to_json}"
        weight = line_item.line_items_option_values.select { |ov| ov.option_value.option_value_id == paper.id if ov.option_value }.first.option_value.name
        price_kg = Float(line_item.line_items_option_values.select { |o| o.option_value.class == Spree::Paper and o.option_value.option_type.nil? }.first.option_value.presentation)
        logger.info "weight : #{weight}"
        # Calcolo il prezzo per foglio è dato da:
        # supponendo di avere:
        #   - 64x88 cm
        #   - 150 gr
        #   - 0,90 €/kg
        # avrò: PREZZO PER FOGLIO= (64/100)*(88/100)*PREZZO PER KILO * GRAMMATURA FOGLIO IN KG
        price_per_sheet = (paper_init_format[:master][0] / 100.to_f) * (paper_init_format[:master][1] / 100.to_f) * (price_kg * (Float(weight) / 1000.to_f))
        logger.info "price_per_sheet : #{price_per_sheet.to_json}"
        # Ottengo i numero di fogli
        number_of_sheet = (Float(number_of_copy) / Float(pose.presentation).to_f).ceil
        # Divido il numero di fogli per la quantità di fogli che ci stanno sul formato più grande
        total += (price_per_sheet * (number_of_sheet/paper_init_format[:quantity].to_f).ceil)

        # Recupero se fronte e retro o solo fronte e poi recupero il prezzo per foglio
        front_back = line_item.line_items_option_values.select { |ov| ov.option_value.option_type.name == "front_back" if ov.option_value && ov.option_value.option_type }.first
        #print_color = line_item.line_items_option_values.select { |ov| ov.option_value.option_type.name == "print_color" if ov.option_value && ov.option_value.option_type }.first
        
        if front_back.option_value.name == "front_back"
          if print_color.option_value.name == "true"
            price_front_back = printer.price_front_and_back_c
          else
            price_front_back = printer.price_front_and_back_bw
          end
        else
          if print_color.option_value.name == "true"
            price_front_back = printer.price_front_c
          else
            price_front_back = printer.price_front_bw
          end
        end
        total += (number_of_sheet/paper_init_format[:quantity].to_f).ceil * price_front_back
        
        # Aggiungo costo di avviamento
        # Calcolandolo nel seguent modo:
        # (costo_orario * tempo_avviamento)/60
        if print_color.option_value.name == "true"
          total += Float(printer.printer_hourly_cost_c) * Float(printer.start_time_c)/60.to_f
        else
          total += Float(printer.printer_hourly_cost_bw) * Float(printer.start_time_bw)/60.to_f
        end
        
        # Calcolo il costo di finitura: che è dato dai seguenti valori:
        #   - printer_hourly_cost
        #   - average_hourly_print (colori o bw)
        #   - number_of_sheet
        #   (number_of_sheet /  average_hourly_print) *  printer_hourly_cost
        if print_color.option_value.name == 'true'
          hourly_cost = Float(printer.printer_hourly_cost_c) *
              (((number_of_sheet/paper_init_format[:quantity].to_f).ceil) / Float(printer.average_hourly_print_c))
        else
          hourly_cost = Float(printer.printer_hourly_cost_bw) *
              (((number_of_sheet/paper_init_format[:quantity].to_f).ceil) / Float(printer.average_hourly_print_bw))
        end
        total += hourly_cost
        
        # Calcolo il costo di finitura: so che costo orario è di un tot
        #hourly_cost = Float(line_item.variant.product.hourly_cost_option_type.option_values.first.presentation)
        #finish = Float(line_item.variant.finish_option_values.name)
        #hour = 60.to_f
        #total += ((finish/hour)*hourly_cost)
        
        # Aggiungo il prezzo per la plastificazione se presente
        plastification = line_item.line_items_option_values.select { |ov| ov.option_value.option_type.name == 'plasticization' if ov.option_value && ov.option_value.option_type }.first
        total += price_for_plasticization(plastification.option_value.name, number_of_sheet, paper_format, front_back.option_value.name, paper_init_format) if plastification
        
        #Aggiungo avviamento se presente
        if print_color.option_value.name == "true" && printer.plant_color_c
          total += Float(printer.plant_color_c)
        elsif printer.plant_color_bw
          total += Float(printer.plant_color_bw)
        end
        
        # NB: il peso è in grammi quindi lo devo trasformare in kg
        weight_line_item = (number_of_sheet/paper_init_format[:quantity].to_f) * (weight.to_f / 1000)
        
        # Aggiungo il costo di avviamento del tagliato
        total += (line_item.variant.product.cut.hourly_cost * line_item.variant.product.cut.start_time)/60
        
        # Aggiungo il costo del tempo di taglio
        total += cutting_time(line_item, number_of_sheet, weight, paper_init_format, pose)
      else
        # Non è formato tagliato quindi si calcola in risma
        number_of_copy = line_item.retrive_option_value_from_name('number_of_copy').presentation.to_i
        price_ream = Float(line_item.weight.presentation.gsub(',', '.'))
        logger.info "price_ream : #{price_ream.to_json}"
        # Calcolo il prezzo per foglio che consiste nel prezzo della risma / # di fogli ossia 1000
        price_per_sheet = price_ream / 1000.to_f
        logger.info "price_per_sheet : #{price_per_sheet.to_json}"
        # Ottengo i numeri di fogli
        number_of_sheet = (Float(number_of_copy) / Float(line_item.variant.poses_option_values.name).to_f).ceil
        # inizio a calcolare il totale
        total += price_per_sheet * number_of_sheet
        logger.info "total : #{total.to_json}"
        
        # Recupero se fronte e retro o solo fronte e poi recupero il prezzo per foglio
        front_back = line_item.line_items_option_values.select { |ov| ov.option_value.option_type.name == "front_back" if ov.option_value.option_type }.first
        print_color = line_item.line_items_option_values.select { |ov| ov.option_value.option_type.name == "print_color" if ov.option_value.option_type }.first
        logger.info "print_color : #{print_color.to_json}"
        
        if front_back.option_value.name == "front_back"
          if print_color.option_value.name == "true"
            price_front_back = line_item.variant.printer_option_values.price_front_and_back_c
          else
            price_front_back = line_item.variant.printer_option_values.price_front_and_back_bw
          end
        else
          if print_color.option_value.name == "true"
            price_front_back = line_item.variant.printer_option_values.price_front_c
          else
            price_front_back = line_item.variant.printer_option_values.price_front_bw
          end
        end
        total += number_of_sheet * price_front_back
        
        # Aggiungo costo di avviamento
        # Calcolandolo nel seguente modo:
        # (costo_orario * tempo_avviamento)/60
        if print_color.option_value.name == "true"
          total += Float(line_item.variant.printer_option_values.printer_hourly_cost_c) * Float(line_item.variant.printer_option_values.start_time_c)/60.to_f
        else
          total += Float(line_item.variant.printer_option_values.printer_hourly_cost_bw) * Float(line_item.variant.printer_option_values.start_time_bw)/60.to_f
        end
        
        # Calcolo il costo di finitura: che è dato dai seguenti valori:
        #   - printer_hourly_cost
        #   - average_hourly_print (colori o bw)
        #   - number_of_sheet
        #   (number_of_sheet /  average_hourly_print) *  printer_hourly_cost
        if print_color.option_value.name == 'true'
          hourly_cost = Float(line_item.variant.printer_option_values.printer_hourly_cost_c) *
              (number_of_sheet/Float(line_item.variant.printer_option_values.average_hourly_print_c))
        else
          hourly_cost = Float(line_item.variant.printer_option_values.printer_hourly_cost_bw) *
              (number_of_sheet/Float(line_item.variant.printer_option_values.average_hourly_print_bw))
        end
        total += hourly_cost
        
        #hourly_cost = Float(line_item.variant.product.hourly_cost_option_type.option_values.first.presentation)
        #finish = Float(line_item.variant.finish_option_values.name)
        #hour = 60.to_f
        #total += ((finish/hour)*hourly_cost)
        
        # Aggiungo il prezzo per la plastificazione se presente
        plastification = line_item.line_items_option_values.select { |ov| ov.option_value.option_type.name == 'plasticization' if ov.option_value && ov.option_value.option_type }.first
        total += price_for_plasticization(plasticization_type, number_of_sheet, paper_format, front_back.option_value.name) if plastification
        
        if print_color.option_value.name == "true" && line_item.variant.printer_option_values.plant_color_c
          total += Float(line_item.variant.printer_option_values.plant_color_c)
        elsif line_item.variant.printer_option_values.plant_color_bw
          total += Float(line_item.variant.printer_option_values.plant_color_bw)
        end
        
        # Aggiungo il costo di avviamento del tagliato
        total += (line_item.variant.cut.product.hourly_cost * line_item.variant.product.start_time)/60
        # Aggiungo il costo del tempo di taglio
        total += cutting_time(line_item, number_of_sheet, weight)
      end
      {:total => total, :weight_line_item => weight_line_item}
    end
    
    # ================================ Other Function =============================== #
    # Ricavo il formato di carta di partenza da quello indicato
    def get_start_format(format)
      if pfd = Spree::PrinterFormatDefault.find_by_name(format)
        if pfd.master.present?
          master = [pfd.master.base.to_f, pfd.master.height.to_f]
        else
          master = [pfd.base.to_f, pfd.height.to_f]
        end
        {:master => master, :quantity => pfd.quantity}
      else
        nil
      end
    end

    # Calcolo del Tempo di taglio:
    # da documentazione:
    #     Tempo di taglio= (fogli stampa/fogli risma)*(N° tagli/taglia lminuto)
    def cutting_time(line_item, number_of_sheet, weight, paper_init_format = nil, pose = nil)

      if paper_init_format
        number_of_sheet = (number_of_sheet / paper_init_format[:quantity].to_f).ceil
      end

      paper_per_ream = Spree::Weight.find_by_name Integer(weight)
      cut_in_min = line_item.variant.product.cut.average_hourly_print / 60
      if pose
        (((Float(number_of_sheet) / Float(paper_per_ream.ream)).ceil) * (Float(pose.cut_number) / Float(cut_in_min)))
      else
        (((Float(number_of_sheet) / Float(paper_per_ream.ream)).ceil) * (Float(line_item.variant.poses.cut_number) / Float(cut_in_min)))
      end
    end

    # Per calcolare il prezzo della plastificazione mi serve saper:
    #   - tipo plastificazione (Film Lucida o  Film Opaca)
    #   - numero di fogli da stampare
    #   - formato del foglio della stampante
    def price_for_plasticization(plasticization_type, number_of_sheet, printer_format, front_back, paper_init_format = nil)
      t = plasticization_type.split('-')
      price_for_sheet = Spree::Plasticization.get_plasticization_price_for_sheet(t[0].strip, number_of_sheet, printer_format)
      r = price_for_sheet.price * number_of_sheet
      r = r * 2 if t[1].strip == "fronte e retro"
      # Aggiungo il costo di avviamento per il taglio
      r += Spree::Plasticization.get_plasticization_price_start_up(number_of_sheet)
    end

    # Recupera il peso dal nome, dovuto al fatto che il nome sarà: "nome componente 123gr"
    # valuto le seguenti possibilità:
    #   - nome componente 123gr
    #   - nome componente 123 gr
    #   - nome componente123gr
    #   - nome componente123 gr
    def get_weight_from_name(name)
      name.gsub(/[0-9]*( gr|gr)/).first.gsub("gr", '').strip!
    end

    # Dalla stringa 45x50cm recupera la larghezza e l'altezza, restituisce un hash di questo formato:
    # {:w => val, :h => val}
    # inoltre valuta se l'unità di misura è in metri o in centimetri. In questo ultimo caso esegue la conversione in
    # metri, così da essere più agevolati nel calcolo.
    def get_width_height(values)
      r = values.gsub(/[0-9]+/)
      unit = values.gsub(/(cm|m)$/).first
      if unit == "m"
        {:w => r.next, :h => r.next}
      else
        {:w => Float(r.next) / 100.to_f, :h => Float(r.next) / 100.to_f}
      end
    end

    def get_paper_format_old(number_of_copy, print_color, limit)
      if print_color.option_value.name == "true"
        if number_of_copy > limit.quantity_c
          limit.format_after_c.presentation
        else
          limit.format_before_c.presentation
        end
      else
        if number_of_copy > limit.quantity_bw
          limit.format_after_bw.presentation
        else
          limit.format_before_bw.presentation
        end
      end
    end

    def get_pose_old(number_of_copy, print_color, limit)
      if print_color.option_value.name == "true"
        if number_of_copy > limit.quantity_c
          limit.pose_after_c
        else
          limit.pose_before_c
        end
      else
        if number_of_copy > limit.quantity_bw
          limit.pose_after_bw
        else
          limit.pose_before_bw
        end
      end
    end

    def get_printer_old(number_of_copy, print_color, limit)
      if print_color.option_value.name == "true"
        if number_of_copy > limit.quantity_c
          limit.printer_after_c
        else
          limit.printer_before_c
        end
      else
        if number_of_copy > limit.quantity_bw
          limit.printer_after_bw
        else
          limit.printer_before_c
        end
      end
    end
  end
end