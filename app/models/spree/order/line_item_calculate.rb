######################################################################################################
#                                        Product Calculate                                           #
######################################################################################################
require "custom_logger"
require "spiral_logger"

module Spree
  class Order < ActiveRecord::Base
    #################################################################################################################
    #                                          Valuto BusinessCardVariant                                           #
    #################################################################################################################
    def business_card_calculate(variant, params, quantity, promo, user = nil)
      begin
        line_params = fetch_values_business(params, variant, quantity)
        line_params = skip_promotion(line_params, variant)
        if !promo || !line_params[:skip]
          # TODO devo effettuare una diversa verifica a seconda della stampante per ricavare il costo della carta
          line_params = fetch_paper(line_params)

          # Aggiungo la marginalità
          line_params[:total] += ((line_params[:total] * line_params[:current_item].product.marginality) / 100)
          # CUSTOM_LOGGER.info "totale dopo marginalita: #{line_params[:total]} *********************************"
        
          # Abbino i nuovi valori torvati al current_item
          line_params[:current_item].price = line_params[:total]
          line_params[:current_item].weight = line_params[:weight_line_item]

          add_adjustments(line_params, params, user)
        else
          line_params = set_promotion(line_params)
          self.line_items << line_params[:current_item]
        end

        line_params[:current_item]
      rescue Exception => e
        logger.info "#################################### #{e} ####################################"
        logger.info "####################################"
        logger.info "#{e.backtrace.join("\n")}"
        logger.info "####################################"
      end
    end

    #################################################################################################################
    #                                           Valuto PosterProduct                                                #
    #################################################################################################################
    def poster_calculate(variant, params, quantity, promo, user = nil)
      begin
        # Recupero tutte le informazioni per i calcoli successivi
        # CUSTOM_LOGGER.info "VADO IN fetch_values_poster *********************************"
        line_params = fetch_values_poster(params, variant, quantity)
        # CUSTOM_LOGGER.info "ESCO DA fetch_values_poster *********************************"
        line_params = skip_promotion(line_params, variant.master)
        if !promo || !line_params[:skip]
          # Calcolo i costi stampa e finiture
          # CUSTOM_LOGGER.info "VADO IN fetch_paper_poster *********************************"
          line_params = fetch_paper_poster(line_params)
          # CUSTOM_LOGGER.info "ESCO DA fetch_paper_poster *********************************"
        
          # Aggiungo la marginalità
          line_params[:total] += ((line_params[:total] * line_params[:current_item].product.marginality) / 100)
          # CUSTOM_LOGGER.info "totale dopo marginalita`: #{line_params[:total]} *********************************"
        
          # Abbino i nuovi valori torvati al current_item
          line_params[:current_item].price = line_params[:total]
          line_params[:current_item].weight = line_params[:weight_line_item]
        
          # Aggiungo gli adjustments
          add_adjustments(line_params, params, user)
        else
          line_params = set_promotion(line_params)
          self.line_items << line_params[:current_item]
        end

        line_params[:current_item]
      rescue Exception => e
        logger.info "##################################POSTER #{e} ####################################"
        logger.info "####################################"
        logger.info "#{e.backtrace.join("\n")}"
        logger.info "####################################"
      end
    end
    
    #################################################################################################################
    #                                           Valuto PosterHighQualityProduct                                                #
    #################################################################################################################
    def poster_high_quality_calculate(variant, params, quantity, promo, user = nil)
      begin
        # Recupero tutte le informazioni per i calcoli successivi
        # CUSTOM_LOGGER.info "VADO IN fetch_values_poster_high_quality *********************************"
        line_params = fetch_values_poster_high_quality(params, variant, quantity)
        # CUSTOM_LOGGER.info "ESCO DA fetch_values_poster_high_quality *********************************"
        line_params = skip_promotion(line_params, variant.master)
        if !promo || !line_params[:skip]
          # Calcolo i costi stampa e finiture
          # CUSTOM_LOGGER.info "VADO IN fetch_paper_poster *********************************"
          line_params = fetch_paper_poster(line_params)
          # CUSTOM_LOGGER.info "ESCO DA fetch_paper_poster *********************************"
        
          # Aggiungo la marginalità
          line_params[:total] += ((line_params[:total] * line_params[:current_item].product.marginality) / 100)
          # CUSTOM_LOGGER.info "totale dopo marginalita`: #{line_params[:total]} *********************************"
        
          # Abbino i nuovi valori torvati al current_item
          line_params[:current_item].price = line_params[:total]
          line_params[:current_item].weight = line_params[:weight_line_item]
        
          # Aggiungo gli adjustments
          add_adjustments(line_params, params, user)
        else
          line_params = set_promotion(line_params)
          self.line_items << line_params[:current_item]
        end

        line_params[:current_item]
      rescue Exception => e
        logger.info "##################################POSTER #{e} ####################################"
        logger.info "####################################"
        logger.info "#{e.backtrace.join("\n")}"
        logger.info "####################################"
      end
    end
    
    #################################################################################################################
    #                                           Valuto PosterFlyer                                                #
    #################################################################################################################
    def poster_flyer_calculate(variant, params, quantity, promo, user = nil)
      begin
        line_params = fetch_values_flyer(params, variant, quantity)
        line_params = skip_promotion(line_params, variant)
        if !promo || !line_params[:skip]
          # TODO devo effettuare una diversa verifica a seconda della stampante per ricavare il costo della carta
          line_params = fetch_paper(line_params, true)

          # Aggiungo la marginalità
          line_params[:total] += ((line_params[:total] * line_params[:current_item].product.marginality) / 100)

          # Abbino i nuovi valori torvati al current_item
          line_params[:current_item].price = line_params[:total]
          line_params[:current_item].weight = line_params[:weight_line_item]

          add_adjustments(line_params, params, user)
        else
          line_params = set_promotion(line_params)
          self.line_items << line_params[:current_item]
        end

        line_params[:current_item]
      rescue Exception => e
        logger.info "#################################### #{e} ####################################"
        logger.info "####################################"
        logger.info "#{e.backtrace.join("\n")}"
        logger.info "####################################"
      end
    end

    #################################################################################################################
    #                                           Valuto FlayerProduct                                                #
    #################################################################################################################
    def flyer_calculate(variant, params, quantity, promo, user = nil)
      begin
        line_params = fetch_values_flyer(params, variant, quantity)
        line_params = skip_promotion(line_params, variant)
        if !promo || !line_params[:skip]
          # TODO devo effettuare una diversa verifica a seconda della stampante per ricavare il costo della carta
          line_params = fetch_paper(line_params, true)

          # Aggiungo la marginalità
          line_params[:total] += ((line_params[:total] * line_params[:current_item].product.marginality) / 100)

          # Abbino i nuovi valori torvati al current_item
          line_params[:current_item].price = line_params[:total]
          line_params[:current_item].weight = line_params[:weight_line_item]

          add_adjustments(line_params, params, user)
        else
          line_params = set_promotion(line_params)
          self.line_items << line_params[:current_item]
        end
        
        line_params[:current_item]
      rescue Exception => e
        logger.info "#################################### #{e} ####################################"
        logger.info "####################################"
        logger.info "#{e.backtrace.join("\n")}"
        logger.info "####################################"
      end
    end
    
    #################################################################################################################
    #                                           Valuto LetterheadProduct                                                #
    #################################################################################################################
    def letterhead_calculate(variant, params, quantity, promo, user = nil)
      begin
        line_params = fetch_values_flyer(params, variant, quantity)
        line_params = skip_promotion(line_params, variant)
        if !promo || !line_params[:skip]
          # TODO devo effettuare una diversa verifica a seconda della stampante per ricavare il costo della carta
          line_params = fetch_paper(line_params, true)

          # Aggiungo la marginalità
          line_params[:total] += ((line_params[:total] * line_params[:current_item].product.marginality) / 100)

          # Abbino i nuovi valori torvati al current_item
          line_params[:current_item].price = line_params[:total]
          line_params[:current_item].weight = line_params[:weight_line_item]

          add_adjustments(line_params, params, user)
        else
          line_params = set_promotion(line_params)
          self.line_items << line_params[:current_item]
        end

        line_params[:current_item]
      rescue Exception => e
        logger.info "#################################### #{e} ####################################"
        logger.info "####################################"
        logger.info "#{e.backtrace.join("\n")}"
        logger.info "####################################"
      end
    end
    
    #################################################################################################################
    #                                           Valuto PostcardProduct                                                #
    #################################################################################################################
    def postcard_calculate(variant, params, quantity, promo, user = nil)
      begin
        line_params = fetch_values_flyer(params, variant, quantity)
        line_params = skip_promotion(line_params, variant)
        if !promo || !line_params[:skip]
          # TODO devo effettuare una diversa verifica a seconda della stampante per ricavare il costo della carta
          line_params = fetch_paper(line_params, true)

          # Aggiungo la marginalità
          line_params[:total] += ((line_params[:total] * line_params[:current_item].product.marginality) / 100)

          # Abbino i nuovi valori torvati al current_item
          line_params[:current_item].price = line_params[:total]
          line_params[:current_item].weight = line_params[:weight_line_item]

          add_adjustments(line_params, params, user)
        else
          line_params = set_promotion(line_params)
          self.line_items << line_params[:current_item]
        end

        line_params[:current_item]
      rescue Exception => e
        logger.info "#################################### #{e} ####################################"
        logger.info "####################################"
        logger.info "#{e.backtrace.join("\n")}"
        logger.info "####################################"
      end
    end
    
    #################################################################################################################
    #                                           Valuto PlaybillProduct                                                #
    #################################################################################################################
    def playbill_calculate(variant, params, quantity, promo, user = nil)
      begin
        line_params = fetch_values_flyer(params, variant, quantity)
        line_params = skip_promotion(line_params, variant)
        if !promo || !line_params[:skip]
          # TODO devo effettuare una diversa verifica a seconda della stampante per ricavare il costo della carta
          line_params = fetch_paper(line_params, true)

          # Aggiungo la marginalità
          line_params[:total] += ((line_params[:total] * line_params[:current_item].product.marginality) / 100)

          # Abbino i nuovi valori torvati al current_item
          line_params[:current_item].price = line_params[:total]
          line_params[:current_item].weight = line_params[:weight_line_item]

          add_adjustments(line_params, params, user)
        else
          line_params = set_promotion(line_params)
          self.line_items << line_params[:current_item]
        end

        line_params[:current_item]
      rescue Exception => e
        logger.info "#################################### #{e} ####################################"
        logger.info "####################################"
        logger.info "#{e.backtrace.join("\n")}"
        logger.info "####################################"
      end
    end
    
    #################################################################################################################
    #                                           Valuto Pieghevole                                                   #
    #################################################################################################################
    def folding_calculate(variant, params, quantity, promo, user = nil)
      begin
        # Recupero tutte le informazioni per i calcoli successivi
        # CUSTOM_LOGGER.info "VADO IN fetch_values_folding *********************************"
        line_params = fetch_values_folding(params, variant, quantity)
        # CUSTOM_LOGGER.info "ESCO DA fetch_values_folding *********************************"
        line_params = skip_promotion(line_params, variant)
        if !promo || !line_params[:skip]
          # Calcolo i costi di carta e stampante compresa plastificazione e taglio
          # CUSTOM_LOGGER.info "VADO IN fetch_paper *********************************"
          line_params = fetch_paper(line_params, true)
          # CUSTOM_LOGGER.info "ESCO DA fetch_paper *********************************"
          # Calcolo i costi per la piegha
          # CUSTOM_LOGGER.info "VADO IN folding_calculate_paper *********************************"
          line_params = folding_calculate_paper(line_params)
          # Aggiungo la marginalità
          line_params[:total] += ((line_params[:total] * line_params[:current_item].product.marginality) / 100)
          # CUSTOM_LOGGER.info "totale dopo marginalita`: #{line_params[:total]} *********************************"
        
          # Abbino i nuovi valori torvati al current_item
          line_params[:current_item].price = line_params[:total]
          line_params[:current_item].weight = line_params[:weight_line_item]

          add_adjustments(line_params, params, user)
        else
          line_params = set_promotion(line_params)
          self.line_items << line_params[:current_item]
        end

        line_params[:current_item]
      rescue Exception => e
        logger.info "#################################### #{e} ####################################"
        logger.info "####################################"
        logger.info "#{e.backtrace.join("\n")}"
        logger.info "####################################"
      end
    end
    
    ####################################################################################################################
    #                                            Gestione Striscione                                                   #
    ####################################################################################################################
    def banner_calculate(variant, params, quantity, promo, user = nil)
      begin
        # Recupero tutte le informazioni per i calcoli successivi
        # CUSTOM_LOGGER.info "VADO IN fetch_values_banner *********************************"
        line_params = fetch_values_banner(params, variant, quantity)
        # CUSTOM_LOGGER.info "ESCO DA fetch_values_banner *********************************"
        line_params = skip_promotion(line_params, variant)
        if !promo || !line_params[:skip]
          # Calcolo i costi stampa e finiture
          # CUSTOM_LOGGER.info "VADO IN fetch_paper_banner *********************************"
          line_params = fetch_paper_banner(line_params)
          # CUSTOM_LOGGER.info "ESCO DA fetch_paper_banner *********************************"
        
          # Aggiungo la marginalità
          line_params[:total] += ((line_params[:total] * line_params[:current_item].product.marginality) / 100)
          # CUSTOM_LOGGER.info "totale dopo marginalita`: #{line_params[:total]} *********************************"
        
          # Abbino i nuovi valori torvati al current_item
          line_params[:current_item].price = line_params[:total]
          line_params[:current_item].weight = line_params[:weight_line_item]
        
          # Aggiungo gli adjustments
          add_adjustments(line_params, params, user)
        else
          line_params = set_promotion(line_params)
          self.line_items << line_params[:current_item]
        end

        line_params[:current_item]
      rescue Exception => e
        logger.info "#################################### #{e} ####################################"
        logger.info "####################################"
        logger.info "#{e.backtrace.join("\n")}"
        logger.info "####################################"
      end
    end
    
    ####################################################################################################################
    #                                            Gestione PVC Adesivo                                                   #
    ####################################################################################################################
    def pvc_calculate(variant, params, quantity, promo, user = nil)
      begin
        # Recupero tutte le informazioni per i calcoli successivi
        # CUSTOM_LOGGER.info "VADO IN fetch_values_pvc *********************************"
        line_params = fetch_values_pvc(params, variant, quantity)
        # CUSTOM_LOGGER.info "ESCO DA fetch_values_pvc *********************************"
        line_params = skip_promotion(line_params, variant)
        if !promo || !line_params[:skip]
          # Calcolo i costi stampa e finiture
          # CUSTOM_LOGGER.info "VADO IN fetch_paper_pvc *********************************"
          line_params = fetch_paper_pvc(line_params)
          # CUSTOM_LOGGER.info "ESCO DA fetch_paper_pvc *********************************"
        
          # Aggiungo la marginalità
          line_params[:total] += ((line_params[:total] * line_params[:current_item].product.marginality) / 100)
          # CUSTOM_LOGGER.info "totale dopo marginalita`: #{line_params[:total]} *********************************"
        
          # Abbino i nuovi valori torvati al current_item
          line_params[:current_item].price = line_params[:total]
          line_params[:current_item].weight = line_params[:weight_line_item]
        
          # Aggiungo gli adjustments
          add_adjustments(line_params, params, user)
        else
          line_params = set_promotion(line_params)
          self.line_items << line_params[:current_item]
        end

        line_params[:current_item]
      rescue Exception => e
        logger.info "#################################### #{e} ####################################"
        logger.info "####################################"
        logger.info "#{e.backtrace.join("\n")}"
        logger.info "####################################"
      end
    end
    
    ####################################################################################################################
    #                                            Gestione Canvas                                                   #
    ####################################################################################################################
    def canvas_calculate(variant, params, quantity, promo, user = nil)
      begin
        # Recupero tutte le informazioni per i calcoli successivi
        # CUSTOM_LOGGER.info "VADO IN fetch_values_canvas *********************************"
        line_params = fetch_values_canvas(params, variant, quantity)
        # CUSTOM_LOGGER.info "ESCO DA fetch_values_canvas *********************************"
        line_params = skip_promotion(line_params, variant)
        if !promo || !line_params[:skip]
          # Calcolo i costi stampa e finiture
          # CUSTOM_LOGGER.info "VADO IN fetch_paper_canvas *********************************"
          line_params = fetch_paper_canvas(line_params)
          # CUSTOM_LOGGER.info "ESCO DA fetch_paper_canvas *********************************"
        
          # Aggiungo la marginalità
          line_params[:total] += ((line_params[:total] * line_params[:current_item].product.marginality) / 100)
          # CUSTOM_LOGGER.info "totale dopo marginalita`: #{line_params[:total]} *********************************"
        
          # Abbino i nuovi valori torvati al current_item
          line_params[:current_item].price = line_params[:total]
          line_params[:current_item].weight = line_params[:weight_line_item]
        
          # Aggiungo gli adjustments
          add_adjustments(line_params, params, user)
        else
          line_params = set_promotion(line_params)
          self.line_items << line_params[:current_item]
        end

        line_params[:current_item]
      rescue Exception => e
        logger.info "#################################### #{e} ####################################"
        logger.info "####################################"
        logger.info "#{e.backtrace.join("\n")}"
        logger.info "####################################"
      end
    end
    
    ####################################################################################################################
    #                                            Gestione Supporto Rigido                                              #
    ####################################################################################################################
    def rigid_calculate(variant, params, quantity, promo, user = nil)
      begin
        # Recupero tutte le informazioni per i calcoli successivi
        # CUSTOM_LOGGER.info "VADO IN fetch_values_rigid *********************************"
        line_params = fetch_values_rigid(params, variant, quantity)
        # CUSTOM_LOGGER.info "ESCO DA fetch_values_rigid *********************************"
        line_params = skip_promotion(line_params, variant)
        if !promo || !line_params[:skip]
          # Calcolo i costi stampa e finiture
          # CUSTOM_LOGGER.info "VADO IN fetch_paper_rigid *********************************"
          line_params = fetch_paper_rigid(line_params)
          # CUSTOM_LOGGER.info "ESCO DA fetch_paper_rigid *********************************"
        
          # Aggiungo la marginalità
          line_params[:total] += ((line_params[:total] * line_params[:current_item].product.marginality) / 100)
          # CUSTOM_LOGGER.info "totale dopo marginalita`: #{line_params[:total]} *********************************"
        
          # Abbino i nuovi valori torvati al current_item
          line_params[:current_item].price = line_params[:total]
          line_params[:current_item].weight = line_params[:weight_line_item]
        
          # Aggiungo gli adjustments
          add_adjustments(line_params, params, user)
        else
          line_params = set_promotion(line_params)
          self.line_items << line_params[:current_item]
        end
        
        line_params[:current_item]
      rescue Exception => e
        logger.info "#################################### #{e} ####################################"
        logger.info "####################################"
        logger.info "#{e.backtrace.join("\n")}"
        logger.info "####################################"
      end
    end
    
    ####################################################################################################################
    #                                            Gestione Etichette                                             #
    ####################################################################################################################
    def sticker_calculate(variant, params, quantity, promo, user = nil)
      begin
        # Recupero tutte le informazioni per i calcoli successivi
        # CUSTOM_LOGGER.info "VADO IN fetch_values_sticker *********************************"
        line_params = fetch_values_sticker(params, variant, quantity)
        # CUSTOM_LOGGER.info "ESCO DA fetch_values_sticker *********************************"
        line_params = skip_promotion(line_params, variant)
        if !promo || !line_params[:skip]
          # Calcolo i costi stampa e finiture
          # CUSTOM_LOGGER.info "VADO IN fetch_paper_sticker *********************************"
          line_params = fetch_paper_sticker(line_params)
          # CUSTOM_LOGGER.info "ESCO DA fetch_paper_sticker *********************************"
        
          # Aggiungo la marginalità
          line_params[:total] += ((line_params[:total] * line_params[:current_item].product.marginality) / 100)
          # CUSTOM_LOGGER.info "totale dopo marginalita`: #{line_params[:total]} *********************************"
        
          # Abbino i nuovi valori torvati al current_item
          line_params[:current_item].price = line_params[:total]
          line_params[:current_item].weight = line_params[:weight_line_item]
        
          # Aggiungo gli adjustments
          add_adjustments(line_params, params, user)
        else
          line_params = set_promotion(line_params)
          self.line_items << line_params[:current_item]
        end
        
        line_params[:current_item]
      rescue Exception => e
        logger.info "#################################### #{e} ####################################"
        logger.info "####################################"
        logger.info "#{e.backtrace.join("\n")}"
        logger.info "####################################"
      end
    end
    ####################################################################################################################
    #                                            Gestione punto metallico                                              #
    ####################################################################################################################
    def staple_calculate(variant, params, quantity, promo, user = nil)
      begin
        # CUSTOM_LOGGER.info "******************************** START staple_calculate *********************************"
        # percuero tutti i parametri e creo il current item
        line_params = fetch_values(variant, params, quantity)
        line_params = skip_promotion(line_params, variant)
        if !promo || !line_params[:skip]
          # Eseguo i calcoli comuni per i multipagina.
          line_params = multi_page_calculate(line_params)
          # Calcolo il punto piegatura:
          line_params = banding_calculate(line_params)
          # Calcolo il costo della PUNZONATURA [Punto Metallico]
          line_params = line_params[:limit].bindery(line_params[:printer]).compute_working_punching(line_params)
          # Aggiungo la marginalità
          line_params[:total] += ((line_params[:total] * line_params[:current_item].product.marginality) / 100)
          # Aggiungo il costo totale e il peso del prodotto al current_item
          # CUSTOM_LOGGER.info "******************************** weight_line_item: #{line_params[:weight_line_item]} *********************************"
          line_params[:current_item].price = line_params[:total]
          line_params[:current_item].weight = Float(line_params[:weight_line_item])
        
          # Aggiungo gli adjustments
          add_adjustments(line_params, params, user)
        else
          line_params = set_promotion(line_params)
          self.line_items << line_params[:current_item]
        end
        
        # CUSTOM_LOGGER.info "******************************** END staple_calculate *********************************"
        line_params[:current_item]
      rescue Exception => e
        logger.info "#################################### #{e} ####################################"
        logger.info "####################################"
        logger.info "#{e.backtrace.join("\n")}"
        logger.info "####################################"
      end
    end

    ####################################################################################################################
    #                                            Gestione Brossura                                                     #
    ####################################################################################################################
    def paperback_calculate(variant, params, quantity, promo, user = nil)
      begin
        # devo mettere i valori mancanti dento il params:
        # params[:order][:cover_bw] = params[:order][:print_color]
        if !params[:order][:plasticization].blank?
          tmp = params[:order][:plasticization].split '-'
          params[:order][:plasticization_option] = "Solo copertina - #{tmp[1].strip}"
          params[:order][:plasticization] = tmp[0].strip
        end
        # percuero tutti i parametri e creo il current item
        line_params = fetch_values(variant, params, quantity)
        line_params = skip_promotion(line_params, variant)
        if !promo || !line_params[:skip]
          # Eseguo i calcoli comuni per i multipagina.
          line_params = multi_page_calculate(line_params)
          # Calcolo il punto piegatura
          line_params = banding_calculate(line_params)
          if line_params[:variant].product.finishing == "seam"
            # Calcolo il costo della CUCITURA [Brossura]
            line_params = line_params[:limit].bindery(line_params[:printer]).compute_working_seam(line_params)
          else
            # Calcolo il costo della FRESATURA [Brossura]
            line_params = line_params[:limit].bindery(line_params[:printer]).compute_working_milling(line_params)
          end
          # Calcolo il costo della copertina [CON ALETTE, SENZA ALETTE]
          if line_params[:sleeve] && line_params[:sleeve].presentation == "Senza Alette"
            # Calcolo il costo della COPERTINA SENZA ALETTE [Brossura]
            line_params = line_params[:limit].bindery(line_params[:printer]).compute_working_sleeve(line_params)
          else
            # Calcolo il costo della COPERTINA CON ALETTE [Brossura]
            line_params = line_params[:limit].bindery(line_params[:printer]).compute_working_sleeve_fins(line_params)
          end
          # Aggiungo la marginalità
          line_params[:total] += ((line_params[:total] * line_params[:current_item].product.marginality) / 100)
          # Aggiungo il costo totale e il peso del prodotto al current_item
          line_params[:current_item].price = line_params[:total]
          line_params[:current_item].weight = line_params[:weight_line_item].to_f
          # Aggiungo gli adjustments
          add_adjustments(line_params, params, user)
        else
          line_params = set_promotion(line_params)
          self.line_items << line_params[:current_item]
        end

        line_params[:current_item]
      rescue Exception => e
        logger.info "#################################### #{e} ####################################"
        logger.info "####################################"
        logger.info "#{e.backtrace.join("\n")}"
        logger.info "####################################"
      end
    end
    
    ####################################################################################################################
    #                                                   Gestione Spirale                                               #
    ####################################################################################################################
    def spiral_calculate(variant, params, quantity, promo, user = nil)
      begin
        # percuero tutti i parametri e creo il current item
        line_params = fetch_values(variant, params, quantity)
        # line_params = fetch_values_spiral(variant, params, quantity)
        line_params = skip_promotion(line_params, variant)
        if !promo || !line_params[:skip]
          # Eseguo i calcoli comuni per i multipagina.
          line_params = multi_page_calculate(line_params)
          # Calcolo il punto piegatura:
          #line_params = banding_calculate(line_params)
          # Calcolo il costo della SPIRALATURA
          line_params = spiraling_calculate(line_params)
          
          # Aggiungo la marginalità
          line_params[:total] += ((line_params[:total] * line_params[:current_item].product.marginality) / 100)
          # Aggiungo il costo totale e il peso del prodotto al current_item
          line_params[:current_item].price = line_params[:total]
          line_params[:current_item].weight = Float(line_params[:weight_line_item])
          # Aggiungo gli adjustments
          add_adjustments(line_params, params, user)
        else
          line_params = set_promotion(line_params)
          self.line_items << line_params[:current_item]
        end

        line_params[:current_item]
      rescue Exception => e
        logger.info "#################################### #{e} ####################################"
        logger.info "####################################"
        logger.info "#{e.backtrace.join("\n")}"
        logger.info "####################################"
      end
    end
    
    ####################################################################################################################
    #                                            Gestione SimpleProduct                                                     #
    ####################################################################################################################
    def simple_product_calculate(variant, params, quantity, user = nil)
      begin
        line_params = Hash.new()
        line_params[:variant] = variant
        # Aggiungo la variante come nuovo line item, la quantità sarà sempre 1
        current_item = Spree::LineItem.new(:quantity => quantity)
        current_item.variant = variant
        
        line_params[:number_of_copy] = Spree::OptionValue.create(:name => "number_of_copy", :presentation => params[:order][:number_of_copy])
        line_params[:order_calculator] = params[:order_calculator]
        day = Spree::ShippingDateAdjustment.find(line_params[:order_calculator][:data]).days
        line_params[:shipping_date] = Spree::OptionValue.create(:name => "shipping_date", :presentation => day.business_day.from_now)
        
        current_item.line_items_option_values.new :option_value => Spree::OptionValue.create(:name => "format", :presentation => variant.format)
        current_item.line_items_option_values.new :option_value => Spree::OptionValue.create(:name => "weight", :presentation => (variant.product.master.weight / 100.to_f) * line_params[:number_of_copy].presentation.to_i)
        current_item.line_items_option_values.new :option_value => line_params[:number_of_copy]
        
        self.line_items << current_item
        line_params[:current_item] = current_item

        # creo le variabili per il totale e il peso
        if variant.format_simple_variants.any?{|fsv| fsv.from.blank? || fsv.from == 0}
          line_params[:total] = variant.format_simple_variants.find_by_waste_paper_c(line_params[:number_of_copy].presentation.to_i).price.to_f * line_params[:number_of_copy].presentation.to_i
        else
          res = Array.new()
          check = false
          tot = 0
          variant.format_simple_variants.each do |fsv|
            # CUSTOM_LOGGER.info "fsv: #{fsv.from} - #{fsv.to} *********************************"
            # Verifico se il termine del range e` 0 ossia infinito
            # CUSTOM_LOGGER.info "Verifico se il termine del range e` 0 ossia infinito *********************************"
            # CUSTOM_LOGGER.info "check: #{check} *********************************"
            # CUSTOM_LOGGER.info "check: #{fsv.to} > 0 && #{!check} *********************************"
            if fsv.to.to_i >= 0 && !check
              # CUSTOM_LOGGER.info "fsv.to.to_i: #{fsv.to.to_i} *********************************"
              # Verifico se il valore delle copie e` compreso nel range specifico
              # CUSTOM_LOGGER.info "line_params[:number_of_copy]: #{line_params[:number_of_copy].presentation.to_i} *********************************"
              # CUSTOM_LOGGER.info "include: #{(fsv.from.to_i..fsv.to.to_i).include?(line_params[:number_of_copy].presentation.to_i)} *********************************"
              to_range = (fsv.to.to_i > 0) ? fsv.to.to_i : (line_params[:number_of_copy].presentation.to_i + 1)
              if (fsv.from.to_i..to_range).include?(line_params[:number_of_copy].presentation.to_i)
                # CUSTOM_LOGGER.info "OK IL NUMERO DI COPIE E` NEL RANGE *********************************"
                if res.blank?
                  # CUSTOM_LOGGER.info "OK RES E` VUOTO *********************************"
                  # CUSTOM_LOGGER.info "fsv.price: #{fsv.start_time_bw} *********************************"
                  tot += fsv.start_time_bw.to_f * line_params[:number_of_copy].presentation.to_i
                  # CUSTOM_LOGGER.info "RES E` VUOTO tot: #{tot} *********************************"
                  check = true
                  # CUSTOM_LOGGER.info "check: #{check} *********************************"
                else
                  # CUSTOM_LOGGER.info "OK RES NON E` VUOTO *********************************"
                  # CUSTOM_LOGGER.info "fsv.price: #{fsv.start_time_bw} *********************************"
                  # CUSTOM_LOGGER.info "(line_params[:number_of_copy].presentation.to_i - res.last.to.to_i): #{(line_params[:number_of_copy].presentation.to_i - res.last.to.to_i)}"
                  tot += fsv.start_time_bw.to_f * (line_params[:number_of_copy].presentation.to_i - res.last.to.to_i)
                  # CUSTOM_LOGGER.info "RES NON E` VUOTO tot: #{tot} *********************************"
                  check = true
                  # CUSTOM_LOGGER.info "check: #{check} *********************************"
                end
              else
                # CUSTOM_LOGGER.info "OK IL NUMERO DI COPIE NON E` NEL RANGE *********************************"
                res << fsv
              end # FINE IF INCLUDE IN RANGE
            else
              if !check
                tot += fsv.price.to_f * (line_params[:number_of_copy].presentation.to_i - res.first.to.to_i)
                # CUSTOM_LOGGER.info "fsv.to.to_i >= 0 && !check tot: #{tot} *********************************"
                # CUSTOM_LOGGER.info "(line_params[:number_of_copy].presentation.to_i - res.first.to.to_i): #{(line_params[:number_of_copy].presentation.to_i - res.first.to.to_i)} *********************************"
              end
            end # FINE IF INFINITO
          end # FINE EACH
          tot += res.sum{|r| ((r.to.to_i + 1)-r.from.to_i) * r.price.to_f}
          line_params[:total] = tot
        end
        
        line_params[:weight_line_item] = variant.product.master.weight * line_params[:number_of_copy].presentation.to_i
        
        # Aggiungo la marginalità
        line_params[:total] += ((line_params[:total] * line_params[:current_item].variant.product.marginality) / 100)
        # Aggiungo il costo totale e il peso del prodotto al current_item
        line_params[:current_item].price = line_params[:total]
        line_params[:current_item].weight = line_params[:weight_line_item].to_f
        
        # Aggiungo gli adjustments
        add_adjustments(line_params, params, user)

        line_params[:current_item]
      rescue Exception => e
        logger.info "#################################### #{e} ####################################"
        logger.info "####################################"
        logger.info "#{e.backtrace.join("\n")}"
        logger.info "####################################"
      end
    end
    
    def advanced_product_calculate(variant, params, quantity, user = nil)
      begin
        line_params = Hash.new()
        line_params[:variant] = variant
        # Aggiungo la variante come nuovo line item, la quantità sarà sempre 1
        current_item = Spree::LineItem.new(:quantity => quantity)
        current_item.variant = variant
        
        line_params[:number_of_copy] = Spree::OptionValue.create(:name => "number_of_copy", :presentation => params[:order][:number_of_copy])
        line_params[:entity] = variant.entities.find(params[:entity_id])
        line_params[:order_calculator] = params[:order_calculator]
        day = Spree::ShippingDateAdjustment.find(line_params[:order_calculator][:data]).days
        line_params[:shipping_date] = Spree::OptionValue.create(:name => "shipping_date", :presentation => day.business_day.from_now)
        
        line_params[:entity].entity_attribute_values.each do |eav|
          current_item.line_items_option_values.new :option_value => Spree::OptionValue.create(:name => eav.simple_attribute.name, :presentation => eav.simple_value.presentation)
        end
        current_item.line_items_option_values.new :option_value => line_params[:number_of_copy]
        
        self.line_items << current_item
        line_params[:current_item] = current_item
        line_params[:weight_line_item] = line_params[:entity].weight.to_f * line_params[:number_of_copy].presentation.to_i
        
        # Aggiungo la marginalità
        line_params[:total] = line_params[:entity].price.to_f * line_params[:number_of_copy].presentation.to_i
        line_params[:total] += ((line_params[:total] * line_params[:current_item].variant.product.marginality) / 100)
        # Aggiungo il costo totale e il peso del prodotto al current_item
        line_params[:current_item].price = line_params[:total]
        line_params[:current_item].weight = line_params[:weight_line_item]
        
        # Aggiungo gli adjustments
        add_adjustments(line_params, params, user)
        
        line_params[:current_item]
      rescue Exception => e
        logger.info "#################################### #{e} ####################################"
        logger.info "####################################"
        logger.info "#{e.backtrace.join("\n")}"
        logger.info "####################################"
      end
    end

    # Aggiungo gli adjustments al current item
    def add_adjustments(line_params, params, user = nil)
      if line_params[:order_calculator] && line_params[:order_calculator][:data] && !line_params[:order_calculator][:data].is_date?
        # Associo anche l'adjustment per la data entro la quale si vuole ottenere la merce
        #line_params[:current_item].shipping_date_adjustment = Spree::ShippingDateAdjustment.find line_params[:order_calculator][:data]
        line_params[:shipping_date_adjustment] = Spree::ShippingDateAdjustment.find(line_params[:order_calculator][:data])
        line_params[:shipping_date_adjustment].adjust(line_params[:current_item])
        line_params[:current_item].shipping_date = line_params[:shipping_date].presentation
      end

      # Aggiungo le tasse come adjustment al line_item
      #tax_rate = Spree::TaxRate.first
      #tax_rate.adjust(line_params[:current_item])

      if user
        # Aggiungo gli sconti utente come adjustment al line_item

        if user.user_promotions.any? { |up| up.product == line_params[:current_item].product }
          user.user_promotions.select { |up| up.product == line_params[:current_item].product }.first.adjust(line_params[:current_item])

        elsif !user.user_promotions.empty?
          user.user_promotions.select { |up| up.product.blank? }.first.adjust(line_params[:current_item])
        end
      end
      
      if params[:order][:file_check] && params[:order][:file_check] != "0"
        Spree::FileCheck.new(:amount => Spree::Config[:file_check_cost].to_f).adjust(line_params[:current_item])
      end
    end

    # Calcola il costo della piegatura per le pagine interne e in caso che la copertina abbia grammatura differente
    # rispetto alle pagine interne, viene calcolato anche il costo della cordonatura.
    #
    # parametri:
    #     - cover_weight
    #     - weight
    #     - current_item
    #     - number_of_copy
    #     - number_of_systems
    #     - points_of_convenience
    #     - print_color
    #     - number_of_quarters
    def banding_calculate(line_params = {})
      # CUSTOM_LOGGER.info "********************** Start banding_calculate *********************************"
      # Calcolo il punto piegatura:
      if line_params[:cover]
        bending_cover = true
        if line_params[:cover_weight] != line_params[:weight]
          line_params[:total] += line_params[:current_item].product.creasing.compute_working(Integer(line_params[:number_of_copy].presentation))
          # CUSTOM_LOGGER.info "********************** totale dopo aggiunta costo cordonatura copertina: #{line_params[:total]} *********************************"
          bending_cover = false
        end
      else
        bending_cover = false
      end
      # Calcolo la piegaura (contenuto e nel caso aggiungo la copertina)
      line_params = line_params[:limit].bindery(line_params[:printer]).compute_working_bending(line_params, bending_cover)
      # CUSTOM_LOGGER.info "********************** End banding_calculate *********************************"
      line_params
    end
    
    # Calcola il costo della spiralatura
    #
    # parametri:
    #     - number_of_copy
    def spiraling_calculate(line_params = {})
      # CUSTOM_LOGGER.info "SONO IN spiraling_calculate *********************************"
      if line_params[:spiraling].spiraling.external
        # CUSTOM_LOGGER.info "SONO IN ESTERNA *********************************"
        # CUSTOM_LOGGER.info "number_of_copy: #{line_params[:number_of_copy].presentation.to_i} *********************************"
        # aggiungo il costo di avviamento della spiralatura
        if line_params[:number_of_copy].presentation.to_i < line_params[:spiraling].spiraling.hourly_cost.to_i
          line_params[:total] += line_params[:spiraling].spiraling.average_hourly_work.to_f
        end
        # aggiungo il costo a copia
        copy_cost = line_params[:spiraling].spiraling.spiraling_copy_costs.where("average_hourly_print_bw <= ? AND average_hourly_print_c >= ?", line_params[:number_of_copy].presentation.to_i, line_params[:number_of_copy].presentation.to_i)
        copy_cost.blank? ? cost = 0.0 : cost = copy_cost.first.copy_cost.to_f
        line_params[:total] += line_params[:number_of_copy].presentation.to_i * cost
        # aggiungo il costo delle spirali secondo il formato e le copie
        line_params[:total] += line_params[:number_of_copy].presentation.to_i * (line_params[:spiraling].copy_cost.to_f * line_params[:variant].height)# get_width_and_height(line_params[:variant].format)[:height])
      else
        # CUSTOM_LOGGER.info "SONO IN INTERNA *********************************"
        # aggiungo il costo di avviamento della spiralatura
        line_params[:total] += line_params[:spiraling].spiraling.hourly_cost.to_f * (line_params[:spiraling].spiraling.start_time.to_f/60.to_f)
        # aggiungo il costo di spiralatura
        line_params[:total] += line_params[:spiraling].spiraling.hourly_cost.to_f * (line_params[:number_of_copy].presentation.to_i/line_params[:spiraling].spiraling.average_hourly_work.to_f) if line_params[:spiraling].spiraling.average_hourly_work.to_f > 0
        # aggiungo il costo delle spirali secondo il formato e le copie
        # CUSTOM_LOGGER.info "line_params[:spiraling].copy_cost: #{line_params[:spiraling].copy_cost} *********************************"
        ## CUSTOM_LOGGER.info "get_width_and_height(line_params[:variant].format)[:height]: #{get_width_and_height(line_params[:variant].format)[:height]} *********************************"
        line_params[:total] += line_params[:number_of_copy].presentation.to_i * (line_params[:spiraling].copy_cost.to_f * line_params[:variant].height)# get_width_and_height(line_params[:variant].format)[:height])
      end
      # CUSTOM_LOGGER.info "FINE spiraling_calculate *********************************"
      line_params
    end

    # Questa funzione esegue tutte le operazione per i multi pagina
    #
    # parametri: un hash composto dai seguenti valri:
    #       - paper
    #       - number_of_facades
    #       - print_color
    #       - plasticization_option
    #       - plastification
    #       - weight
    #       - number_of_copy
    #       - paper_cover
    #       - cover_weight
    #       - front_back_cover
    #       - print_color_bw
    #       - current_item
    #       - number_of_facades_internal
    #       - number_of_quarters
    #       - quarter
    #       - price_kg
    #       - total_copy
    #       - paper_init_format
    #       - printer
    #
    def multi_page_calculate(line_params = {})
      # CUSTOM_LOGGER.info "SONO IN multi_page_calculate *********************************"
      line_params[:total] = 0
      line_params[:weight_line_item] = 0
      line_params[:printer_multi].each do |k, p|
        # Recupero il numero di quartini associato alla stampante e in base al numero di copie.
        line_params["quarter_#{k}".to_sym] = line_params[:limit].quarters(line_params[:print_color_bool], p)
        # CUSTOM_LOGGER.info "quarter: #{line_params["quarter_#{k}".to_sym].name} *********************************"
        # Recupero il costo della stampa
        line_params["price_from_back_#{k}".to_sym] = p.price_from_back(line_params[:print_color_bool], line_params[:front_back].name)
        # CUSTOM_LOGGER.info "price_from_back: #{line_params["price_from_back_#{k}".to_sym]} *********************************"
        # Recupero il formato iniziale
        line_params["paper_init_format_#{k}".to_sym] = get_start_format(line_params[:limit].printer_format_multi(line_params[:print_color_bool], p).presentation)
        # CUSTOM_LOGGER.info "paper_init_format: #{line_params["paper_init_format_#{k}".to_sym]} *********************************"
        # Ricavo il formato di stampa
        line_params["paper_format_#{k}".to_sym] = line_params[:limit].printer_format_multi(line_params[:print_color_bool], p).presentation
        # CUSTOM_LOGGER.info "paper_format: #{line_params["paper_format_#{k}".to_sym]} *********************************"
        if p.digital?
          # CUSTOM_LOGGER.info "******************************** STAMPO IN DIGITALE *********************************"
          if line_params[:weight].ream?
            line_params = get_multi_paper_ream(line_params, p, k.to_s)
            # CUSTOM_LOGGER.info "VADO IN get_multi_paper_ream *********************************"
            #line_params["weight_option_#{k}".to_sym] = (line_params["number_of_sheet_#{k}".to_sym]) * (line_params[:weight].weight.to_f / 1000)
            # VA CONSIDERATA COME UNA OTTO SENZA SCARTO
            #line_params = get_multi_printer_ream(line_params)
          else
            line_params = get_multi_paper_cut(line_params, p, k.to_s)
            line_params["number_of_sheet_#{k}".to_sym] = line_params["print_option_#{k}".to_sym][:number_of_sheet]
            #line_params["weight_option_#{k}".to_sym] = (line_params["number_of_sheet_#{k}".to_sym]/line_params["paper_init_format_#{k}".to_sym][:quantity].to_f) * (line_params[:weight].weight.to_f / 1000)
            #line_params = get_multi_printer_cut(line_params)
          end
        else
          # CUSTOM_LOGGER.info "******************************** STAMPO IN OFFSET *********************************"
          line_params = get_multi_paper_cut(line_params, p, k.to_s)
          line_params["number_of_sheet_#{k}".to_sym] = line_params["print_option_#{k}".to_sym][:number_of_sheet]
          #line_params["weight_option_#{k}".to_sym] = (line_params["number_of_sheet_#{k}".to_sym]/line_params["paper_init_format_#{k}".to_sym][:quantity].to_f) * (line_params[:weight].weight.to_f / 1000)
          #line_params = get_multi_printer_cut(line_params)
        end
      end
      line_params[:weight_line_item] = ((line_params[:variant].width.to_f / 100.to_f) * (line_params[:variant].height.to_f / 100.to_f)) * (line_params[:weight].weight.to_f / 1000) * (line_params[:number_of_facades_internal].to_f/2) * line_params[:number_of_copy].presentation.to_i
      
      # Controllo i due totali e prendo il migliore
      if line_params[:total_low].to_f < line_params[:total_high].to_f
        line_params[:quarter] = line_params[:quarter_low]
        line_params[:price_from_back] = line_params[:price_from_back_low]
        line_params[:paper_init_format] = line_params[:paper_init_format_low]
        line_params[:number_of_sheet] = line_params[:number_of_sheet_low] if line_params[:number_of_sheet_low]
        line_params[:price_per_sheet] = line_params[:price_per_sheet_low]
        line_params[:printer] = line_params[:printer_multi][:low]
        line_params[:paper_format] = line_params[:paper_format_low]
        line_params[:total] += line_params[:total_low]
        #line_params[:weight_line_item] += line_params[:weight_option_low]
        print_option_low = line_params[:print_option_low]
        line_params = line_params.merge(print_option_low)
      else
        line_params[:quarter] = line_params[:quarter_high]
        line_params[:price_from_back] = line_params[:price_from_back_high]
        line_params[:paper_init_format] = line_params[:paper_init_format_high]
        line_params[:number_of_sheet] = line_params[:number_of_sheet_high] if line_params[:number_of_sheet_high]
        line_params[:price_per_sheet] = line_params[:price_per_sheet_high]
        line_params[:printer] = line_params[:printer_multi][:high]
        line_params[:paper_format] = line_params[:paper_format_high]
        line_params[:total] += line_params[:total_high]
        #line_params[:weight_line_item] += line_params[:weight_option_high]
        print_option_high = line_params[:print_option_high]
        line_params = line_params.merge(print_option_high)
      end
      
      ####      Fine costo della stampa      ####
      # Costo della plastificazione, in questo caso si intende fronte e retro
      if line_params[:plasticization_option] == "all_plasticization"
        line_params[:total] += Spree::Order.price_for_plasticization_fb(line_params[:plastification].name,
                                                                        line_params[:number_of_sheet],
                                                                        line_params[:paper_format])
        # CUSTOM_LOGGER.info "total post PLASTIFICATION: #{line_params[:total]} *********************************"
      end
      # CUSTOM_LOGGER.info "totale prima della copertina: #{line_params[:total]} *********************************"
      if line_params[:cover]
        # Calcolo della copertina:
        line_params = calculate_cover(line_params)
        line_params[:total] += Float(line_params[:total_cover])
        line_params[:weight_line_item] += Float(line_params[:weight_line_item_cover])
        # CUSTOM_LOGGER.info "totale dopo aggiunta costo copertina: #{line_params[:total]} *********************************"
      else
        if line_params[:default_cover] && line_params[:default_cover] != "0"
          line_params[:total] += line_params[:variant].default_cover_price.to_f * line_params[:number_of_copy].presentation.to_i
					line_params[:number_of_sheet_cover] = 1
					line_params[:paper_format_cover] = line_params[:paper_format]
          if ((line_params[:plasticization_option] == "front_cover_plasticization") || (line_params[:plasticization_option] == "front_back_cover_plasticization"))
            # Aggiungo il prezzo per la plastificazione se presente
            line_params[:total] += plasticization_price_cover(line_params) if line_params[:plastification]
            # CUSTOM_LOGGER.info "totale dopo plastificazione della copertina: #{line_params[:total]} *********************************" if line_params[:plastification]
          end
        end
      end
      
       #gestisco caso in cui la copertina e' della stessa carta dell'interno ma e' richiesta una plastificazione solo fronte o fronte e retro
			 if(!line_params[:no_cover])
	      if !line_params[:cover] && ((line_params[:plasticization_option] == "front_cover_plasticization") || (line_params[:plasticization_option] == "front_back_cover_plasticization"))
					line_params[:number_of_sheet_cover] = 1
					line_params[:paper_format_cover] = line_params[:paper_format]
	        if line_params[:plasticization_option] == "front_cover_plasticization"
	          # CUSTOM_LOGGER.info "[][][][][][][][][][][][][][][]tot prima di Solo copertina - solo fronte #{line_params[:total]}"
	          line_params[:total] += plasticization_price_cover(line_params) if line_params[:plastification]
	          # CUSTOM_LOGGER.info "[][][][][][][][][][][][][][][]tot dopo di Solo copertina - solo fronte #{line_params[:total]}"
	        end
	        if line_params[:plasticization_option] == "front_back_cover_plasticization"
	          # CUSTOM_LOGGER.info "[][][][][][][][][][][][][][][]tot prima di Solo copertina - fronte e retro #{line_params[:total]}"
	          line_params[:total] += plasticization_price_cover(line_params) if line_params[:plastification]
	          # CUSTOM_LOGGER.info "[][][][][][][][][][][][][][][]tot dopo di Solo copertina - fronte e retro #{line_params[:total]}"
	        end
	      end
			end

      line_params
    end

    # Restituisce il numero di fogli in base al numero di copie, quindi verificando la se supare o no i vari punti di
    # convenienza
    # parametri:
    #   limit               => current_item.variant.limit
    #   print_color         => print_color == "true" ? true : false
    #   number_of_facades   => Float(number_of_facades.presentation)
    #   quarter             => Float(quarter.name)
    #   number_of_copy      => Integer(number_of_copy.presentation)
    #   weight              => weight.name.to_f
    #   cut                 => current_item.variant.product.cut
    #   cut_number          => Float(quarter.cut_number)
    #   paper_init_format   => paper_init_format
    #   check               => {1 => 4 COLORI, 2 => 8 COLORI CON SCARTO, 3 => 8 COLORI SENZA SCARTO}
    #
    # hash ritornato:
    #   {:number_of_sheet => number_of_sheet, :weight_line_item => weight_line_item, :cut_price => cut_price }
    #   :number_of_sheet    => il numero di fogli sempra valorizzato
    #   :weight_line_item   => peso del prodotto sempra valorizzato
    #   :cut_price          => i costo del taglio valorizzato sempre ma ha un valore > 0 solo se si stampa sotto il
    #                          primo punto di conveninza
    #
    # Devo vrificare se il numero di copie è maggiore o minore del punto di convenienza e in caso sia maggiore
    # bisgona verificare se il numero di copie è maggiore o minore del secondo punto di convenienza che tiene conto
    # dello scarto o meno.
    def self.get_number_of_sheet(line_params, check, step)
      #calculate_cut = false
      number_of_sheet_h = {}
      plants = 0
      
      if check == 2
        # SCARTO CON 8 COLORI
        # CUSTOM_LOGGER.info "SONO IN get_number_of_sheet CON check 2 *********************************"
        number_of_sheet = (line_params[:number_of_facades_internal] / (Float(line_params["quarter_#{step}".to_sym].name) * 4)).ceil * Integer(line_params[:number_of_copy].presentation)
        #plants = ((line_params[:number_of_facades_internal] / 2) / Float(line_params[:quarter].name)).ceil
        number_of_sheet_h = get_plants_and_sheet_8_colors(Float(line_params["quarter_#{step}".to_sym].name), line_params[:number_of_facades_internal])
        
        {
            :number_of_sheet => number_of_sheet,
            :plants => number_of_sheet_h[:plants],
            :system_params => number_of_sheet_h[:params],
            :waste => true
        }
      elsif check == 3
        # Devo fare il doppio preventivo per la 8 COLORI
        # CUSTOM_LOGGER.info "SONO IN get_number_of_sheet CON check 3 *********************************"
        number_of_sheet_h = number_of_systems_and_sheet(Float(line_params["quarter_#{step}".to_sym].name), line_params[:number_of_facades_internal], Integer(line_params[:number_of_copy].presentation))
        #number_of_sheet = number_of_sheet_h[:number_of_sheet] * Integer(line_params[:number_of_copy].presentation)
        
        {
            :number_of_sheet => number_of_sheet_h[:number_of_sheet],
            :plants => number_of_sheet_h[:plants],
            :system_params => number_of_sheet_h[:params],
            :waste => false
        }
      elsif check == 1
        # Devo fare il doppio preventivo per la 4 COLORI
        # CUSTOM_LOGGER.info "SONO IN get_number_of_sheet CON check 1 *********************************"
        number_of_sheet_h = get_plants_and_sheet_4_colors(Float(line_params["quarter_#{step}".to_sym].name), line_params[:number_of_facades_internal])
        number_of_sheet = number_of_sheet_h[:number_of_sheet] * Integer(line_params[:number_of_copy].presentation)
        
        {
            :number_of_sheet => number_of_sheet,
            :plants => number_of_sheet_h[:plants],
            :system_params => number_of_sheet_h[:params],
            :waste => false
        }
      end

      # TODO da spostare sia peso che taglio
      #weight_line_item = number_of_sheet * (line_params[:weight].name.to_f / 1000)
      ## CUSTOM_LOGGER.info "weight_line_item: #{weight_line_item}"
      #if calculate_cut
      # Devo calcolcare il taglio quindi sono nella stampa in digitale
      # Aggiungo il costo di avviamento del tagliato
      #cut_price += (line_params[:cut].hourly_cost * line_params[:cut].start_time)/60
      # Aggiungo il costo del tempo di taglio
      #cut_price += Spree::Order.cost_cutting_time(number_of_sheet, line_params[:weight].name.to_f, line_params[:cut], Float(line_params[:quarter].cut_number), line_params[:paper_init_format])
      #end
      #line_params[:weight_line_item] = weight_line_item
      #line_params[:cut_price] = cut_price
    end


    # Calcola il costo della carta se sono in risma
    # parametri:
    #       weight: option_value
    #       number_of_copy: nel caso del multipagina deve essere (numero_di_facciate / 2) * numero_di_copie
    #       quarter: sono i quartini o le pose (sono degli option_value dove il campo name è qullo che ci interessa)
    def self.paper_cost_in_risma_multipage(line_params, weight, number_of_copy, quarter, step)
      # Calcolo il prezzo per foglio che consiste nel prezzo della risma / # di fogli ossia 1000
      line_params["price_per_sheet_#{step}".to_sym] = weight / 1000.to_f
      # CUSTOM_LOGGER.info "price_per_sheet: #{line_params["price_per_sheet_#{step}".to_sym]} *********************************"
      # Ottengo i numeri di fogli
      line_params["number_of_sheet_#{step}".to_sym] = (number_of_copy.to_f / quarter.to_f).ceil
      # CUSTOM_LOGGER.info "number_of_sheet: #{line_params["number_of_sheet_#{step}".to_sym]} *********************************"
      # costo della carta
      line_params["total_#{step}".to_sym] += line_params["price_per_sheet_#{step}".to_sym] * line_params["number_of_sheet_#{step}".to_sym]
      # CUSTOM_LOGGER.info "total: #{line_params["total_#{step}".to_sym]} *********************************"
      line_params
    end

    # Ritorna un array di hash composto da {quartini , numero di volte da usare il quartino e divisore}.
    # I parametri passati devono essere il numero di quartini impostato sulla stampante e il numero di facciate che si
    # vuole stampare (naturalmente esclusa la copertina se presente).
    # I parametri ritornati sono il numero di quartini di riferimento.
    # La quantità ossia quanti impianti per quartino servono, per esempio se i quarti sono 8 e la quantità 3 si dovranno
    # fare 3 (*2) impianti di quel tipo. (Si moltiplica per 2 perché bisogna farli fronte e retro.)
    # Il  <tt> divisor </tt> server per fare i calcoli dopo, se il divisor è 0 si moltiplica normalmente la quantity
    # per il numero di copie da fare, mentre se invece divisor è maggiore di zero bisogna fare:
    #
    #     n_of_copy * quantity * (1/divisor)
    #
    # ==== Examples
    #
    #      get_number_of_systems(quarter_of_printer, number_of_facades) =>
    #                               [{:quarter=>8, :quantity=>3, :divisor=>0}, {:quarter=>2, :quantity=>1, :divisor=>4}]
    #
    def self.number_of_systems(quarter_of_printer, number_of_facades)
      # Mi calcolo il numero di quarti che devo stampare in base al numero di facciate tramite questa formula:
      #   (n_facciate/ n_quarti_stampante) * 2
      fatt = !(Integer(quarter_of_printer) % 3 == 0)
      quarter_to_print = ((number_of_facades.to_f / 4.to_f) * 2.to_f).to_f
      # CUSTOM_LOGGER.info "SONO IN number_of_systems => quarter_to_print #{quarter_to_print} *********************************"

      # Moltiplico il numero di quarti della stampante per due, perchè devo farli diventare per fronte e retro.
      quarter_of_printer = Float(quarter_of_printer) * 2

      # Chiamo la funzione ricorsiva per il calcolo
      Spree::Order.number_of_systems_recursive(quarter_of_printer, quarter_to_print, (fatt ? 1 : 0), fatt)
    end

    def self.number_of_systems_recursive(quarter_of_printer, quarter_to_print, divisor, fatt)
      r = []
      quantity = Integer(quarter_to_print) / Integer(quarter_of_printer)
      r << {:quarter => Integer(quarter_of_printer) * 2, :number_of_sheet => quantity, :copy_for_sheet => (divisor != 0) ? divisor : 1 } if quantity > 0
      rest = Integer(quarter_to_print) % Integer(quarter_of_printer)
      if fatt
        divisor *= 2
      else
        divisor += 3
      end
      if rest > 0
        if fatt
          div_tmp = Integer(quarter_of_printer) / 2
        else
          if Integer(quarter_to_print) % 3 == 0
            div_tmp = Integer(quarter_of_printer) / 2
          else
            div_tmp = Integer(quarter_of_printer) / 3
            if div_tmp % 3 == 0 || div_tmp == 1
              div_tmp = Integer(quarter_of_printer) / 2
            end
          end
        end
        r = r + Spree::Order.number_of_systems_recursive(div_tmp, rest, divisor, fatt)
      end
      r
    end

    # number_of_color => è il numero dei colori della stampante, può essere 4 o 8, nel caso sia 8 colori bisogna fare
    # "l'ottimizzazione sulla carta" mentre, nel caso sia 4 colori bisogna fare l'ottimizzazione degli impianti
    def self.number_of_systems_and_sheet(quarter_of_printer, number_of_facades, number_of_copy, number_of_color = nil)
      # Todo da valutare se tenere il parametro: number_of_color
      sum = 0
      tmp = Spree::Order.number_of_systems(quarter_of_printer, number_of_facades)
      ret = {}

      tmp.each do |v|
        # CUSTOM_LOGGER.info "v[:number_of_sheet]: #{v[:number_of_sheet]} *********************************"
        if Float(v[:copy_for_sheet]) == 0
          sum += Integer(v[:number_of_sheet]) * Integer(number_of_copy)
        else
          sum += Integer(v[:number_of_sheet]) * Integer(number_of_copy) * (1/Float(v[:copy_for_sheet]))
        end
      end

      # Per ottenere degli impianti devo moltiplicare la somma ti tutte le quantity * 2
      {:number_of_sheet => sum, :plants => tmp.map { |v| v[:number_of_sheet] }.sum* 2, :params => tmp}
    end


    #def self.price_print_multi_page(print_color, printer, number_of_sheet, paper_init_format= nil)
    #  if print_color
    #    return (number_of_sheet/paper_init_format[:quantity].to_f).ceil * printer.price_front_and_back_c if paper_init_format
    #    number_of_sheet * printer.price_front_and_back_c
    #  else
    #    return (number_of_sheet/paper_init_format[:quantity].to_f).ceil * printer.price_front_and_back_bw if paper_init_format
    #    number_of_sheet * printer.price_front_and_back_bw
    #  end
    #end

    # Calcola il costo della plastificazione front e back in automatico
    def self.price_for_plasticization_fb(plasticization_type, number_of_sheet, printer_format)
      # t = plasticization_type.split('-')
			t = (plasticization_type.match(/^polished_film/).blank?) ? "matt_film" : "polished_film"
      price_for_sheet = Spree::Plasticization.get_plasticization_price_for_sheet(t.strip, number_of_sheet, printer_format)
      r = price_for_sheet.price * number_of_sheet * 2
      r += Spree::Plasticization.get_plasticization_price_start_up(number_of_sheet)
    end

    # Calcolo del Tempo di taglio:
    # da documentazione:
    #     Tempo di taglio= (fogli stampa/fogli risma)*(N° tagli/taglia lminuto)
    # parametri:
    #   pose => può essere sia pose che quarter
    def self.cost_cutting_time(number_of_sheet, weight, cut, cut_number, paper_init_format = nil)
      if paper_init_format
        number_of_sheet = (number_of_sheet / paper_init_format[:quantity].to_f).ceil
      end

      paper_per_ream = Spree::Weight.find_by_name Integer(weight)
      cut_in_min = cut.average_hourly_print / 60
      (((Float(number_of_sheet) / Float(paper_per_ream.ream)).ceil) * (cut_number / Float(cut_in_min)))
    end

    ####################################################################################################################
    #                                           Calculate cover                                                        #
    ####################################################################################################################
    # parametri
    #   paper_cover
    #   print_color_cover
    #   number_of_copy
    #   limit_cover
    #   cover_weight
    def calculate_cover(line_params)
      # CUSTOM_LOGGER.info "SONO IN calculate_cover *********************************"
      #  passo uno: trovare il formato della carta da cui si parte x esempio: 35*50 deriva 70*100
      #line_params[:paper_format_cover] = Spree::Order.get_paper_format_cover(line_params[:number_of_copy].presentation.to_i * 2, line_params[:print_color_cover_bool], limit)
      ## Recupero il numero di pose in base al numero di copie e se stampa i colori o bianco e nero
      #line_params[:pose_cover] = Spree::Order.get_pose_cover(line_params[:number_of_copy].presentation.to_i * 2, print_color, limit)
      ## Recupero la stampante in base al numero di copie e se stampa i colori o bianco e nero
      ## devo vedere se faccio fronte e retro calcolo il doppio numero di copie => sarà sempre fronte e retro
      #line_params[:printer_cover] = Spree::Order.get_printer_cover((number_of_copy * 2), print_color, limit)
      line_params = Spree::Order.get_paper_information_cover(line_params)
      line_params[:paper_init_format_cover] = get_start_format(line_params[:paper_format_cover])
      # CUSTOM_LOGGER.info "paper_init_format_cover: #{line_params[:paper_init_format_cover].to_json} *********************************"
      line_params = fetch_paper_cover(line_params, true)
      
      line_params[:weight_line_item_cover] = line_params[:weight_line_item_cover].to_f
      
      line_params
      #{:total => total, :weight_line_item => weight_line_item}
    end


    # Calcolo del Tempo di taglio:
    # da documentazione:
    #     Tempo di taglio= (fogli stampa/fogli risma)*(N° tagli/taglia lminuto)
    def cutting_time_cover(cut, number_of_sheet, weight, pose)
      paper_per_ream = Spree::Weight.find_by_name(Integer(weight.name))
      cut_in_min = cut.average_hourly_print / 60
      (((Float(number_of_sheet) / Float(paper_per_ream.ream)).ceil) * (Float(pose.cut_number) / Float(cut_in_min)))
    end

    def self.get_paper_information_cover(line_params)
      # CUSTOM_LOGGER.info "SONO IN get_paper_information_cover *********************************"
      if line_params[:front_back_cover]
        qt_c = line_params[:limit_cover].quantity_c / 2
        qt_bw = line_params[:limit_cover].quantity_bw / 2
      else
        qt_c = line_params[:limit_cover].quantity_c
        qt_bw = line_params[:limit_cover].quantity_bw
      end
      
      number_of_copy = line_params[:number_of_copy].presentation.to_i * 2
      if line_params[:print_color_cover_bool]
        if number_of_copy > qt_c
          line_params[:paper_format_cover] = line_params[:limit_cover].format_after_c.presentation
          line_params[:pose_cover] = line_params[:limit_cover].pose_after_c
          line_params[:printer_cover] = line_params[:limit_cover].printer_after_c
        else
          line_params[:paper_format_cover] = line_params[:limit_cover].format_before_c.presentation
          line_params[:pose_cover] = line_params[:limit_cover].pose_before_c
          line_params[:printer_cover] = line_params[:limit_cover].printer_before_c
        end
        if line_params[:front_back_cover]
          line_params[:price_front_back_cover] = line_params[:printer_cover].price_front_and_back_c
        else
          line_params[:price_front_back_cover] = line_params[:printer_cover].price_front_c
        end
      else
        if number_of_copy > qt_bw
          line_params[:paper_format_cover] = line_params[:limit_cover].format_after_bw.presentation
          line_params[:pose_cover] = line_params[:limit_cover].pose_after_bw
          line_params[:printer_cover] = line_params[:limit_cover].printer_after_bw
        else
          line_params[:paper_format_cover] = line_params[:limit_cover].format_before_bw.presentation
          line_params[:pose_cover] = line_params[:limit_cover].pose_before_bw
          line_params[:printer_cover] = line_params[:limit_cover].printer_before_c
        end
        if line_params[:front_back_cover]
          line_params[:price_front_back_cover] = line_params[:printer_cover].price_front_and_back_bw
        else
          line_params[:price_front_back_cover] = line_params[:printer_cover].price_front_bw
        end
      end
      # CUSTOM_LOGGER.info "paper_format_cover: #{line_params[:paper_format_cover]} *********************************"
      # CUSTOM_LOGGER.info "pose_cover: #{line_params[:pose_cover].name} *********************************"
      # CUSTOM_LOGGER.info "printer_cover: #{line_params[:printer_cover].presentation} *********************************"
      # CUSTOM_LOGGER.info "price_front_back_cover: #{line_params[:price_front_back_cover]} *********************************"
      line_params
    end

    def self.get_pose_cover(number_of_copy, print_color, limit)
      if print_color.name == "true"
        if number_of_copy > limit.quantity_c
          return limit.pose_after_c
        else
          return limit.pose_before_c
        end
      else
        if number_of_copy > limit.quantity_bw
          return limit.pose_after_bw
        else
          return limit.pose_before_bw
        end
      end
    end

    def self.get_printer_cover(number_of_copy, print_color, limit)
      if print_color.name == "true"
        if number_of_copy > limit.quantity_c
          limit.printer_after_c
        else
          limit.printer_before_c
        end
      else
        if number_of_copy > limit.quantity_bw
          limit.printer_after_bw
        else
          limit.printer_before_c
        end
      end
    end

    # Vengo raccolte tutte le operazioni da effettuare sulla copertina.
    # Le operazioni fatte sulla copertina sono:
    #   - cordonatura
    #   - costo segnature
    #   - costo taglio
    #
    # parametri
    #   creasing        => current_item.product.creasing
    #   paper           => paper_cover
    #   weight          => Integer(weight.name)
    #   weight_cover    => Float(cover_weight.name)
    #   number_of_copy  => Integer(number_of_copy.presentation)
    #
    def processes_cover creasing, paper, weight, weight_cover, number_of_copy
      total = 0

      if weight_cover > weight
        total += creasing.compute_working(number_of_copy)
      end
      total
    end

    ####################################################################################################################
    #                                                NUOVO CALCOLO                                                     #
    ####################################################################################################################
    def self.get_plants_and_sheet_4_colors(quarter_of_printer, facades)
      # SPIRAL_LOGGER.info "quarter_of_printer: #{quarter_of_printer} *********************************"
      # SPIRAL_LOGGER.info "facades: #{facades} *********************************"
      sheets = 0
      r = {}
      # Devo vedere quanti impianti mi servono:
      # SPIRAL_LOGGER.info "(facciate/4 % quartini): #{(facades/4.to_f) % quarter_of_printer} *********************************"
      # SPIRAL_LOGGER.info "PRIMA DELL'IF #{(quarter_of_printer > 3 && (quarter_of_printer % 3 == 0) && (facades/4.to_f) > (quarter_of_printer/2.to_f) && (facades/4.to_f) % quarter_of_printer > (quarter_of_printer/2.to_f))} *********************************"
      if (quarter_of_printer > 3 && (quarter_of_printer % 3 == 0) && (facades/4.to_f) > (quarter_of_printer/2.to_f) && (facades/4.to_f) % quarter_of_printer > (quarter_of_printer/2.to_f))
        # SPIRAL_LOGGER.info "VADO IN calc_4_on_3_colors *********************************"
        r = Spree::Order.calc_4_on_3_colors(quarter_of_printer, facades)
      else
        # SPIRAL_LOGGER.info "VADO IN calc_4_colors *********************************"
        calc = Spree::Order.calc_4_colors(quarter_of_printer, facades)
        
        # SPIRAL_LOGGER.info "CONTINUO IN get_plants_and_sheet_4_colors *********************************"
        # SOMMO IL NUMERO DI IMPIANTI/DIVISORE
        s = calc.sum { |q| q[:value]/Float(q[:divisor]) }
        # SPIRAL_LOGGER.info "s: #{s} *********************************"
        # ARROTONDO IL TOTALE OTTENUTO
        dec = s - Integer(s).ceil
        # SPIRAL_LOGGER.info "dec: #{dec} *********************************"
        # RICAVO IL NUMERO DI IMPIANTI REALE MOLTIPLICANDO QUELLO OTTENUTO * 2
        plants = (Integer(s) * 2) + (dec * (dec <= 0.5 ? 1 : 2)).ceil
        # SPIRAL_LOGGER.info "plants: #{plants} *********************************"
        h_r = []

        ####################### CALCOLO SE SONO CON QUARTINI DIVISIBILI PER 3 #######################
        if (quarter_of_printer % 3) == 0
          # SPIRAL_LOGGER.info "SONO CON QUARTINI DIVISIBILI PER 3 *********************************"
          h = {}
          if calc.any? { |d| d[:divisor] == 1 }
            number_of_sheet = calc.select { |d| d[:divisor] == 1 }.first[:value]
            number_of_sheet.times do |tn|
              h[:quarter] = quarter_of_printer * 4
              h[:copy_for_sheet] = 1
              #h[:value] = calc.collect { |j| j[:value] }
              h[:divisor] = 1
              h[:number_of_sheet] = 1
              h_r << h
            end
          end
          h = {}
          t = calc.sum do |d|
            if d[:divisor] != 1
              d[:value] / Float(d[:divisor])
            else
              0
            end
          end
          if t > 0
            i = 1
            number_of_sheet = 1
            z = calc.count - (calc.any? { |d| d[:divisor] == 1 } ? 1 : 0)
            if t <= 0.5
              i = 2
              t_f = t * 2
              t = t * 2
              z = (z > 1) ? z/2 : z

              while (t < 1 && t_f <= 0.5)
                if t_f <= (1 - t)
                  i += 2
                end
                t += t_f
              end
            end
            
            h[:copy_for_sheet] = i / z
            div = calc.select { |d| d[:divisor] != 1 }.first[:divisor]
            if (div % 3 == 0)
              sp = 0
              calc.each do |d|
                if d[:divisor] != 1
                  sp += quarter_of_printer * (d[:value] / Float(d[:divisor]))
                end
              end
              h[:divisor] = quarter_of_printer / Integer(sp)
            else
              h[:divisor] = div
            end
            h[:number_of_sheet] = number_of_sheet
            h[:quarter] = (1/t.to_f).ceil * 4
            h_r << h
          end
          #h_r.sum {|ns| ns[:number_of_sheet] }
          r = {:number_of_sheet => h_r.sum { |hr| hr[:number_of_sheet]/hr[:copy_for_sheet].to_f }.round(2), :plants => plants, :params => h_r}
          ####################### CALCOLO SE SONO CON QUARTINI DIVISIBILI PER 2 #######################
        else
          # SPIRAL_LOGGER.info "SONO CON QUARTINI DIVISIBILI PER 2 *********************************"
          calc.each do |v|
            h = {}
            if v[:divisor] != 1
              t = v[:value] / Float(v[:divisor])
              t_f = t
              i = 1
              number_of_sheet = 1
              while t != 1 && t < 1
                t += t_f
                i += 1
              end
            else
              number_of_sheet = v[:value]
              i = 1
            end
            h[:copy_for_sheet] = i
            #h[:value] = v[:value]
            h[:divisor] = v[:divisor]
            h[:number_of_sheet] = number_of_sheet
            h[:quarter] = (quarter_of_printer * 4)/h[:copy_for_sheet]
            h_r << h
            plants = 0
            h_r.each do |hr|
              if hr[:copy_for_sheet] > hr[:number_of_sheet]
                plants += (1 * hr[:number_of_sheet])
              else
                plants += (2 * hr[:number_of_sheet])
              end
            end
          end
          #h_r.sum {|ns| ns[:number_of_sheet] }
          r = {:number_of_sheet => h_r.sum { |hr| hr[:number_of_sheet]/hr[:copy_for_sheet].to_f }.round(2), :plants => plants, :params => h_r}
        end ####################### CALCOLO SE SONO CON QUARTINI DIVISIBILI PER 3 #######################
      end # CHIUDO L'IF DEL CONTROLLO SE SONO SOPRA LA META` DEI QUARTINI DI STAMPA
      r
    end

    # Mi ritorna un array composto da hash composto così:
    #       - value: numero di impianti (bisogna valutare se effettivamente va moltiplicato per due oppure si può
    #                fare tutto nella stessa facciata )
    #       - divisor: numero di volte che un quartino (inteso che necessita di due impianti) riesce a starci nel foglio
    def self.calc_4_colors(quarter_of_printer, facades)
      # SPIRAL_LOGGER.info "SONO IN calc_4_colors *********************************"
      quarter_to_print = (facades.to_f / 4.to_f).to_f
      
      # SPIRAL_LOGGER.info "quarter_to_print: #{quarter_to_print} *********************************"
      # SPIRAL_LOGGER.info "VADO IN IN calc_4_colors_rec *********************************"
      Spree::Order.calc_4_colors_rec(quarter_of_printer, quarter_to_print)
    end

    def self.calc_4_colors_rec(quarter_of_printer, quarter_to_print, divisor = 1)
      # SPIRAL_LOGGER.info "SONO IN IN calc_4_colors_rec *********************************"
      ret = []
      r1 = (quarter_to_print.to_f / quarter_of_printer.to_f)
      # SPIRAL_LOGGER.info "r1: #{r1} *********************************"
      ret << {:value => r1, :divisor => divisor} if r1 > 0
      # SPIRAL_LOGGER.info "ret: #{ret} *********************************"
      
      rest = (quarter_to_print.to_f % quarter_of_printer.to_f)
      # SPIRAL_LOGGER.info "rest: #{rest} *********************************"

      if rest > 0
        tmp = calculate_divisor(quarter_of_printer, divisor)
        ret += Spree::Order.calc_4_colors_rec(tmp[:quarter_of_printer], rest, tmp[:divisor])
      end
      ret
    end

    # Questa funziona si occupa di valutare che divisore restituire e di calcolare il nuovo quarter_of_printer
    def self.calculate_divisor(quarter_of_printer, divisor)
      if ((Integer(quarter_of_printer) % 3) == 0)
        if  divisor == 1
          divisor = 3
        else
          divisor *= 2
        end

        quarter_of_printer = quarter_of_printer / 3.to_f
      else
        divisor *= 2
        quarter_of_printer = quarter_of_printer / 2.to_f
      end

      {:divisor => divisor, :quarter_of_printer => quarter_of_printer}
    end

    # Calcolo il numero di impianti e di fogli che ottengo nel caso mi trovi con:
    #   quarter_of_printer % 3 == 0
    #   facades / 4 > quarter_of_printer / 2
    def self.calc_4_on_3_colors(quarter_of_printer, facades)
      plants = 0
      number_of_sheet = 0
      h = []
      quarter_to_print = facades / 4
      if quarter_to_print > quarter_of_printer # mi creo gli impianti interi con 1 foglio e 2 impianti
        plants = (quarter_to_print / Integer(quarter_of_printer) * 2)
        number_of_sheet = quarter_to_print / Integer(quarter_of_printer)
        number_of_sheet.times do |t|
          h += [{:divisor => 1, :copy_for_sheet => 1, :number_of_sheet => 1, :quarter => (quarter_of_printer * 4)}]
        end
        quarter_to_print = (quarter_to_print % quarter_of_printer)
      end
      # Partiamo con il primo passo
      half_quarter_of_printer = quarter_of_printer / 2
      # Secondo passo
      rest_quarter_to_print = quarter_to_print - half_quarter_of_printer
      # Terzo passo
      check = (half_quarter_of_printer / Float(rest_quarter_to_print))
      # Quarto passo
      if (check < 1) # Se ci troviamo nel caso in cui avanziamo sempre
        h += [{:divisor => 1, :copy_for_sheet => 1, :number_of_sheet => 1, :quarter => (check * 4)}]
        #number_of_sheet + 1
        {:plants => plants + 2, :number_of_sheet => h.sum { |t| t[:number_of_sheet]/t[:copy_for_sheet].to_f }.round(2), :params => h}
      else # Se possiamo ottimizzare la carta
        h += [{:divisor => 2, :copy_for_sheet => 2, :number_of_sheet => 1, :quarter => (quarter_of_printer * 4)/2}, {:divisor => check, :copy_for_sheet => (check * 2), :number_of_sheet => 1, :quarter => (quarter_of_printer * 4)/(check * 2)}]
        #number_of_sheet + 2
        {:plants => plants + 2, :number_of_sheet => h.sum { |t| t[:number_of_sheet]/t[:copy_for_sheet].to_f }.round(2), :params => h}
      end
    end

    # Calcolo il numero di fogli e impianti per la 8 COLORI CON SCARTO
    def self.get_plants_and_sheet_8_colors(quarter_of_printer, facades)
      plants = 0
      number_of_sheet = 0
      h = []
      quarter_to_print = facades / 4

      # Caso in cui il mio numero di facciate iniziale diviso 4 sia maggiore dei quartini di stampa
      if quarter_to_print > quarter_of_printer
        h += [{:divisor => 1, :copy_for_sheet => 1, :number_of_sheet => (quarter_to_print/quarter_of_printer.to_i), :quarter => (quarter_of_printer * 4)}]
        plants += (quarter_to_print/quarter_of_printer.to_i) * 2
        quarter_to_print = quarter_to_print - (quarter_of_printer * (quarter_to_print/quarter_of_printer.to_i))
      end
      
      if quarter_to_print > 0
        if quarter_to_print > (quarter_of_printer / 2)
          h += [{:divisor => 1, :copy_for_sheet => 1, :number_of_sheet => 1, :quarter => (quarter_to_print * 4)}]
          plants += 2
        else
          plants += 2
          h += [{:divisor => 1, :copy_for_sheet => (quarter_of_printer / quarter_to_print), :number_of_sheet => 1, :quarter => quarter_to_print * 4}]
        end
      end
      {:plants => plants, :number_of_sheet => h.sum { |t| t[:number_of_sheet]/t[:copy_for_sheet].to_f }.round(2), :params => h}
    end
  end
end
