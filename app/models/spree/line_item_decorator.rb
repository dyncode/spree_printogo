Spree::LineItem.class_eval do
  #belongs_to :shipping_method, :class_name => "Spree::ShippingMethod", :foreign_key => "shipping_method_id"
  belongs_to :shipping_date_adjustment, :class_name => "Spree::ShippingDateAdjustment", :foreign_key => "shipping_date_adjustment_id"
  belongs_to :line_item_promotion, :class_name => "Spree::LineItemPromotion", :foreign_key => "line_item_promotion_id"
  has_many :line_items_option_values, :class_name => "Spree::LineItemsOptionValues", :foreign_key => "line_item_id", :dependent => :delete_all
  #has_many :adjustments, :as => :adjustable, :dependent => :destroy, :order => "created_at ASC" # aggiunto per moigliorare efficenza DB
  
  before_destroy :remove_adj_and_liov
  
  attr_accessible :file_attachment, :weight, :file_attachment_url, :notes, :process_name
  
  has_attached_file :file_attachment,
                    :url => '/spree/line_item/:id/:style/:basename.:extension',
                    :path => ':rails_root/public/spree/line_item/:id/:style/:basename.:extension'
  
  validates_attachment_size :file_attachment, :less_than => 15.megabyte, :unless => Proc.new {|li| li[:file_attachment].nil?}
  
  def options_text
    str = Array.new
    unless self.ad_hoc_option_values.empty?
      #TODO: group multi-select options (e.g. toppings)
      str << self.ad_hoc_option_values.each { |pov|
        "#{pov.option_value.option_type.presentation} = #{pov.option_value.presentation}"
      }.join(',')
    end # unless empty?

    unless self.product_customizations.empty?
      self.product_customizations.each do |customization|
        price_adjustment = (customization.price == 0) ? "" : " (#{format_price customization.price})"
        str << "#{customization.product_customization_type.presentation}#{price_adjustment}"
        customization.customized_product_options.each do |option|
          next if option.empty?

          if option.customization_image?
            str << "#{option.customizable_product_option.presentation} = #{File.basename option.customization_image.url}"
          else
            str << "#{option.customizable_product_option.presentation} = #{option.value}"
          end
        end # each option
      end # each customization
    end # unless empty?

    unless self.variant.option_values.empty?
      self.variant.option_values.each do |ov|
        str << "#{ov.option_type.presentation} = #{ov.presentation}"
      end
    end

    str.join('\n')
  end

  def amount
    (price) + adjustments.eligible.map(&:amount).sum
  end
  
  def option_for_advanced
    if self.variant.product.is_a?(Spree::AdvancedProduct)
      self.line_items_option_values.collect{|liov| "<tr class='#{ActionController::Base.helpers.cycle('','odd')}'><td class='bold'>#{liov.option_value.name rescue ""}:</td><td>#{liov.option_value.presentation rescue ""}</td></tr>" if (liov.option_value.name != "number_of_copy")}.join().html_safe
    else
      nil
    end
  end
  
  # Formato selezionato e inserito
  def format
    if self.variant.product.is_a?(Spree::BusinessCardProduct)
      tmp = self.line_items_option_values.joins(:option_value).where("spree_option_values.option_type_id = ?", Spree::OptionType.find_by_name("printer_format_card").id)
      tmp.last.option_value.presentation
    elsif ((self.variant.product.is_a?(Spree::PosterProduct) || self.variant.product.is_a?(Spree::PosterHighQualityProduct)) && !self.variant.product.is_a?(Spree::PosterFlyerProduct))
      tmp2 = self.line_items_option_values.joins(:option_value).where("spree_option_values.option_type_id = ?", Spree::OptionType.find_by_name("poster_printer_format").id)
      if !tmp2.empty? && tmp2.last.option_value.presentation != "personalizzato"
        tmp2.last.option_value.presentation
      else
        # vuol dire che il formato nn c'è quindi devo prendere il formato custom:
        if (width = self.line_items_option_values.joins(:option_value).where("spree_option_values.name = 'custom_width'").first && height = self.line_items_option_values.joins(:option_value).where("spree_option_values.name = 'custom_height'").first)
          width = self.line_items_option_values.joins(:option_value).where("spree_option_values.name = 'custom_width'").first
          height = self.line_items_option_values.joins(:option_value).where("spree_option_values.name = 'custom_height'").first
          "#{width.option_value.presentation}x#{height.option_value.presentation}cm"
        else
          ""
        end
      end
    elsif (self.variant.product.is_a?(Spree::RigidProduct)) || (self.variant.product.is_a?(Spree::StickerProduct))
      tmp2 = self.line_items_option_values.joins(:option_value).where("spree_option_values.option_type_id = ?", Spree::OptionType.find_by_name("rigid_printer_format").id) if (self.variant.product.is_a?(Spree::RigidProduct))
      tmp2 = self.line_items_option_values.joins(:option_value).where("spree_option_values.option_type_id = ?", Spree::OptionType.find_by_name("sticker_printer_format").id) if (self.variant.product.is_a?(Spree::StickerProduct))
      if !tmp2.empty? && tmp2.last.option_value.presentation != "personalizzato"
        tmp2.last.option_value.presentation
      else
        # vuol dire che il formato nn c'è quindi devo prendere il formato custom:
        if (width = self.line_items_option_values.joins(:option_value).where("spree_option_values.name = 'custom_width'").first && height = self.line_items_option_values.joins(:option_value).where("spree_option_values.name = 'custom_height'").first)
          width = self.line_items_option_values.joins(:option_value).where("spree_option_values.name = 'custom_width'").first
          height = self.line_items_option_values.joins(:option_value).where("spree_option_values.name = 'custom_height'").first
          "#{width.option_value.presentation}x#{height.option_value.presentation}cm"
        else
          ""
        end
      end
    elsif self.variant.product.is_a?(Spree::BannerProduct)
      width = self.line_items_option_values.joins(:option_value).where("spree_option_values.name = 'custom_width'").first.option_value
      height = self.line_items_option_values.joins(:option_value).where("spree_option_values.name = 'custom_height'").first.option_value
      "#{width.presentation}x#{height.presentation}cm"
    elsif self.variant.product.is_a?(Spree::CanvasProduct)
      # vuol dire che il formato nn c'è quindi devo prendere il formato custom:
      width = self.line_items_option_values.joins(:option_value).where("spree_option_values.name = 'custom_width'").first.option_value
      height = self.line_items_option_values.joins(:option_value).where("spree_option_values.name = 'custom_height'").first.option_value
      "#{width.presentation}x#{height.presentation}cm"
    elsif self.variant.product.is_a?(Spree::SimpleProduct)
      tmp = self.line_items_option_values.joins(:option_value).where("spree_option_values.name = 'format'").first.option_value
      tmp.blank? ? nil : tmp.presentation
    else
      self.variant.format
    end
  end
  
  # Formato selezionato e inserito
  def format_promo
    if self.variant.product.is_a?(Spree::BusinessCardProduct)
      self.retrive_option_value_from_option_type_name('printer_format_card').id rescue nil
    elsif (self.variant.product.is_a?(Spree::PosterProduct) && !self.variant.product.is_a?(Spree::PosterHighQualityProduct))
      tmp = self.retrive_option_value_from_option_type_name('poster_printer_format') rescue nil
      if !tmp.blank? && tmp.name != "custom"
        tmp.id
      else
        self.retrive_option_value_from_name('custom').id rescue nil
      end
    elsif self.variant.product.is_a?(Spree::PosterHighQualityProduct)
      tmp = self.retrive_option_value_from_option_type_name('poster_high_quality_printer_format') rescue nil
      if !tmp.blank? && tmp.name != "custom"
        tmp.id
      else
        self.retrive_option_value_from_name('custom').id rescue nil
      end
    elsif (self.variant.product.is_a?(Spree::CanvasProduct) || self.variant.product.is_a?(Spree::BannerProduct) || self.variant.product.is_a?(Spree::RigidProduct))
      nil
    else
      self.variant.id rescue nil
    end
  end
  
  # Tipo di orientamento [VERTICALE, ORIZZONTALE]
  def orientation
    tmp = self.line_items_option_values.joins(:option_value).where("spree_option_values.option_type_id = ?", Spree::OptionType.find_by_name("printer_orientation").id)
    tmp.empty? ? nil : tmp.first.option_value.presentation.capitalize
  end
  
  # Tipo di orientamento bobina [VERTICALE, ORIZZONTALE]
  def orientation_coil
    tmp = self.line_items_option_values.joins(:option_value).where("spree_option_values.option_type_id = ?", Spree::OptionType.find_by_name("printer_orientation_coil").id)
    tmp.empty? ? nil : tmp.first.option_value.presentation.capitalize
  end
  
  # Tipo di stampa [FRONTE, FRONTE-RETRO]
  def printing_instructions
    tmp = self.line_items_option_values.joins(:option_value).where("spree_option_values.option_type_id = ?", Spree::OptionType.find_by_name("printer_instructions").id)
    tmp.empty? ? nil : tmp.first.option_value.presentation
  end

	def option_value_config_name(other = "")
		if (self.variant.product.is_a?(Spree::PosterProduct) || self.variant.product.is_a?(Spree::BannerProduct) || self.variant.product.is_a?(Spree::CanvasProduct)) && !self.variant.product.is_a?(Spree::PosterFlyerProduct) || self.variant.product.is_a?(Spree::StickerProduct)
			I18n.t("line_item_coil_#{other}".to_sym) unless line_items_option_values.joins(:option_value).where("spree_option_values.option_type_id = ?", Spree::OptionType.find_by_name("coil").id).first.option_value.blank?
		elsif self.variant.product.is_a?(Spree::RigidProduct)
			I18n.t("line_item_rigid_support_#{other}".to_sym) unless line_items_option_values.joins(:option_value).where("spree_option_values.option_type_id = ?", Spree::OptionType.find_by_name("rigid_support").id).first.option_value.blank?
		else
			tmp = self.line_items_option_values.joins(:option_value).where("spree_option_values.type = 'Spree::Paper'")
			tmp.empty? ? nil : I18n.t("line_item_paper_#{other}".to_sym)
		end
	end

  # Tipo di carta
  def paper
    if (self.variant.product.is_a?(Spree::PosterProduct) || self.variant.product.is_a?(Spree::BannerProduct) || self.variant.product.is_a?(Spree::CanvasProduct)) && !self.variant.product.is_a?(Spree::PosterFlyerProduct) || self.variant.product.is_a?(Spree::StickerProduct)
      line_items_option_values.joins(:option_value).where("spree_option_values.option_type_id = ?", Spree::OptionType.find_by_name("coil").id).first.option_value.name
    elsif self.variant.product.is_a?(Spree::RigidProduct)
      line_items_option_values.joins(:option_value).where("spree_option_values.option_type_id = ?", Spree::OptionType.find_by_name("rigid_support").id).first.option_value.name
    else
      tmp = self.line_items_option_values.joins(:option_value).where("spree_option_values.type = 'Spree::Paper'")
      tmp.empty? ? nil : tmp.first.option_value.presentation
    end
  end

  # Grammatura della carta [PAPERWEIGHT / COIL]
  def paper_weight
    if (self.variant.product.is_a?(Spree::PosterProduct) || self.variant.product.is_a?(Spree::BannerProduct) || self.variant.product.is_a?(Spree::CanvasProduct)) && !self.variant.product.is_a?(Spree::PosterFlyerProduct)
      line_items_option_values.joins(:option_value).where("spree_option_values.option_type_id = ?", Spree::OptionType.find_by_name("coil").id).first.option_value.weight
    elsif self.variant.product.is_a?(Spree::RigidProduct)
      line_items_option_values.joins(:option_value).where("spree_option_values.option_type_id = ?", Spree::OptionType.find_by_name("rigid_support").id).first.option_value.weight
    else
      tmp = self.line_items_option_values.joins(:option_value).where("spree_option_values.type = 'Spree::PaperWeight'")
      tmp.empty? ? nil : tmp.first.option_value.name
    end
  end
  
  # Plastificazione
  def plasticization
    if self.variant.product.is_a?(Spree::SimpleProduct) && self.variant.product.is_a?(Spree::AdvancedProduct)
      nil
    else
      tmp = self.line_items_option_values.joins(:option_value).where("spree_option_values.option_type_id = ?", Spree::OptionType.find_by_name("plasticization").id).first
      tmp.nil? ? "Nessuna" : tmp.option_value.presentation.capitalize
    end
  end

  #Finitura Bobine
  def lamination
    if self.variant.product.is_a?(Spree::SimpleProduct) && self.variant.product.is_a?(Spree::AdvancedProduct)
      nil
    elsif self.variant.product.is_a?(Spree::PvcProduct) || self.variant.product.is_a?(Spree::RigidProduct)
      tmp = self.line_items_option_values.joins(:option_value).where("spree_option_values.option_type_id = ?", Spree::OptionType.find_by_name("lamination_type").id).first
      tmp.nil? ? "Nessuna" : tmp.option_value.name.capitalize
    end
  end

  # Lavorazione plotter
  def processing_plotter
    #self.line_items_option_values.each do |o|
    #  # CUSTOM_LOGGER.info " self.line_items_option_values ::::: id :#{o.option_value_id}__#{Spree::OptionValue.find_by_id(o.option_value_id).name}__#{Spree::OptionValue.find_by_id(o.option_value_id).presentation}" if !o.option_value_id.nil?      
    #end
    if self.variant.product.is_a?(Spree::StickerProduct)
       tmp = ""
       self.line_items_option_values.joins(:option_value).where("spree_option_values.option_type_id = ?", Spree::OptionType.find_by_name("processing_plotter_type").id).each do |pp|
         tmp = tmp + " " + pp.option_value.name if !tmp.nil?
        end
        tmp == "" ? "Nessuna" : tmp
    end
  end
  
  def cutting_coil
    if self.variant.product.is_a?(Spree::RigidProduct) || self.variant.product.is_a?(Spree::StickerProduct)
       tmp = self.line_items_option_values.joins(:option_value).where("spree_option_values.option_type_id = ?", Spree::OptionType.find_by_name("cutting_coil_type").id).first
       #tmp.nil? ? "Nessuna" : tmp.option_value.presentation
       tmp.nil? ? "Nessuna" : tmp.option_value.name.capitalize
    end
  end

  def sided
    self.line_items_option_values.joins(:option_value).where("spree_option_values.name = 'sided'").first.option_value.presentation rescue nil
  end

  def eyelet
    self.line_items_option_values.joins(:option_value).where("spree_option_values.name = 'eyelet'").first.option_value.presentation rescue nil
  end

  def eyelet_accessory_quantity
    self.line_items_option_values.joins(:option_value).where("spree_option_values.name = 'eyelet_accessory_quantity'").first.option_value.presentation rescue nil
  end

  def eyelet_accessory_disposition
    self.line_items_option_values.joins(:option_value).where("spree_option_values.name = 'eyelet_accessory_disposition'").first.option_value.presentation rescue nil
  end
  
  # Copie
  def number_of_copy
    self.line_items_option_values.joins(:option_value).where("spree_option_values.name = 'number_of_copy'").first.option_value.presentation rescue nil
  end
  
  # numero di copie per bobina
  def number_of_stickers
    self.line_items_option_values.joins(:option_value).where("spree_option_values.name = 'number_of_stickers'").first.option_value.presentation rescue nil
  end
  
  # Tipo di stampa [COLORI / BIANO-NERO]
  def print_color
    if (self.variant.product.is_a?(Spree::PosterProduct) || self.variant.product.is_a?(Spree::BannerProduct) || self.variant.product.is_a?(Spree::CanvasProduct) || self.variant.product.is_a?(Spree::RigidProduct) || self.variant.product.is_a?(Spree::StickerProduct))&& !self.variant.product.is_a?(Spree::PosterFlyerProduct)
      line_items_option_values.joins(:option_value).where("spree_option_values.option_type_id = ?", Spree::OptionType.find_by_name("plotter_printing_cost").id).first.option_value.name.titleize
    else
      tmp = self.line_items_option_values.joins(:option_value).where("spree_option_values.option_type_id = ?", Spree::OptionType.find_by_name("printer_color").id)
      tmp.empty? ? nil : tmp.first.option_value.presentation
    end
  end
  
  # Numero di facciate
  def number_of_facades
    if (self.variant.product.is_a?(Spree::StapleProduct) || self.variant.product.is_a?(Spree::PaperbackProduct) || self.variant.product.is_a?(Spree::SpiralProduct)) &&
        self.line_items_option_values.where(:variant_parent_id => self.variant.children.first.id)
      self.line_items_option_values.joins(:option_value).where("spree_option_values.name = 'number_of_facades'").first.option_value
    end
  end
  
  # Tipo di piegha
  def folding
    tmp = self.line_items_option_values.joins(:option_value).where("spree_option_values.option_type_id = ?", Spree::OptionType.find_by_name("folding_format").id)
    if !tmp.blank? && !tmp.empty?
      tmp.first.option_value
    else
      nil
    end
  end
  
  # Tipo di copertina [CON ALETTE, SENZA ALETTE]
  def sleeve
    tmp = self.line_items_option_values.joins(:option_value).where("spree_option_values.name = 'sleeve'")
    if !tmp.blank? && !tmp.empty?
      tmp.first.option_value
    else
      nil
    end
  end
  
  # Tipo di spiralatura
  def spiraling
    tmp = self.line_items_option_values.joins(:option_value).where("spree_option_values.option_type_id = ?", Spree::OptionType.find_by_name("spiraling").id)
    if !tmp.blank? && !tmp.empty?
      tmp.first.option_value
    else
      nil
    end
  end
  
  # Tipo di Telaio
  def frame
    tmp = self.line_items_option_values.joins(:option_value).where("spree_option_values.option_type_id = ?", Spree::OptionType.find_by_name("frame").id)
    if !tmp.blank? && !tmp.empty?
      tmp.first.option_value
    else
      nil
    end
  end
  
  # Tipo di carta per la copertina
  def cover_paper
    if self.variant.children.present?
      self.line_items_option_values.where(:variant_parent_id => self.variant.children.first.id).joins(:option_value).where("spree_option_values.type = 'Spree::Paper'").first.option_value.presentation rescue nil
    else
      nil
    end
  end
  
  # Grammatura della carta per la copertina [PAPERWEIGHT / COIL]
  def cover_paper_weight
    if self.variant.children.present?
      self.line_items_option_values.where(:variant_parent_id => self.variant.children.first.id).joins(:option_value).where("spree_option_values.type = 'Spree::PaperWeight'").first.option_value.name rescue nil
    else
      nil
    end
  end
  
  # Tipo di stampa copertina [COLORI / BIANO-NERO]
  def cover_print_color
    if self.variant.children.present?
      self.line_items_option_values.where(:variant_parent_id => self.variant.children.first.id).joins(:option_value).where("spree_option_values.option_type_id = ?", Spree::OptionType.find_by_name("printer_color").id).first.option_value rescue nil
    else
      nil
    end
  end
  
  # Data di consegna
  #def shipping_date
  #  Spree::ShippingDateAdjustment.find(adjustments.select{ |adj| adj.originator_type == "Spree::ShippingDateAdjustment" }.last.originator_id) rescue nil
  #end
  
  # solo IVA
  #def total_vat
  #  adjustments.select { |adj| adj.originator_type == "Spree::TaxRate" }.first.amount rescue 0
  #end
  
  def total_filecheck
    adjustments.select { |adj| adj.originator_type == "Spree::FileCheck" }.first.amount rescue 0
  end
  
  #def total_with_vat_and_shipping
  #  total_with_vat# + total_shipping
  #end
  
  #def total_with_vat
  #  total + total_vat
  #end
  
  def total
    tot = 0
    adjustments.each { |ad| tot += ad.amount if ad.originator_type != "Spree::FileCheck" }
    (price) + tot
  end
  
  def total_whitout_date
    tot = 0
    adjustments.each { |ad| tot += ad.amount if ad.originator_type != "Spree::ShippingDateAdjustment" && ad.originator_type != "Spree::FileCheck" }
    (price) + tot
  end

  #def total_shipping
  #  adjustments.select { |adj| adj.originator_type == "Spree::ShippingMethod" }.first.amount rescue 0
  #end

  def retrive_option_value_from_option_type_name(option_type_name)
    self.line_items_option_values.select { |ov| ov.option_value.option_type.name == option_type_name if ov.option_value && ov.option_value.option_type }.first.option_value
  end

  def retrive_option_value_from_name(option_value_name)
    self.line_items_option_values.select { |ov| ov.option_value.name == option_value_name if ov.option_value }.first.option_value
  end

  private
  # Rimuove gli adjustment e i line_items_option_values associati alla line_item
  def remove_adj_and_liov
    # Rimuovo gli adjustment
    deleted_adjustments = self.adjustments.delete_all
    deleted_adjustments.each { |da| da.destroy }
    # Rimuovo i line_items_option_values
    deleted_liov = self.line_items_option_values.delete_all
    deleted_liov.each { |l| l.destroy }
    # Ripopolo la quantity della promozione
    if line_item_promotion.present?
      if !line_item_promotion.unlimited
        line_item_promotion.quantity += 1
        line_item_promotion.save
      end
    end
    self.order.check_line_item_exists
  end
end