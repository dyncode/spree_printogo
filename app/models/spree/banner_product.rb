# Striscione
module Spree
  class BannerProduct < Product
    before_create :add_option_type
    attr_accessible :min_width, :min_height, :max_width, :max_height, :banner_variant, :banner_variant_attributes, :seo_title
    has_one :banner_variant, :class_name => Spree::BannerVariant, :foreign_key => "product_id"
    
    accepts_nested_attributes_for :banner_variant
    
    scope :only_banner, lambda { where("type = 'Spree::BannerProduct'") }
    
    def min_width
      self.max_weight_cover
    end
    
    def min_height
      self.max_facades_weight_cover
    end
    
    def min_width=value
      self.max_weight_cover = value
    end
    
    def min_height=value
      self.max_facades_weight_cover = value
    end
    
    def initialize(val = nil)
      super(val)
      if self.new_record?
        self.name = "Striscione"
        #self.available_on = Time.now if self.available_on.nil?
        self.master.price = 0 if self.master && self.master.price.nil?
      end
    end

    # devo tornare tutti i tipi di materiali con le relative configurazioni
    def all_value_json
      res = []
      self.banner_variant.papers.map do |p|
        res << {
            :id => p.id,
            :name => p.name,
            :description => p.description.to_s,
        }
      end
      
      res.to_json
    end

    # Accesso agli option types
    def print_colors
      self.option_types.where(:name => "print_color").first.option_values
    end

    def papers_option_type
      self.option_types.where(:name => "coil").first
    end

    def plotter_option_type
      self.option_types.where(:name => "plotter").first
    end

    def instructions_option_type
      self.option_types.where(:name => "printer_instructions").first
    end
    
    def directings
      self.option_types.where(:name => "printer_orientation").first
    end
    
    private
    def add_option_type
      if self.new_record?
        print_color_ot = Spree::OptionType.find_by_name('printer_color')
        plotter_ot = Spree::OptionType.find_by_name('plotter')
        paper_ot = Spree::OptionType.find_by_name('coil')
        printer_instructions_ot = Spree::OptionType.find_by_name('printer_instructions')
        printer_orientation_ot = Spree::OptionType.find_by_name('printer_orientation')
        
        heat_sealing_ot = Spree::OptionType.find_by_name('heat_sealing')
        reinforcement_perimeter_ot = Spree::OptionType.find_by_name('reinforcement_perimeter')
        pocket_ot = Spree::OptionType.find_by_name('pocket')
        
        self.option_types << print_color_ot
        self.option_types << paper_ot
        self.option_types << printer_instructions_ot
        self.option_types << plotter_ot
        self.option_types << printer_orientation_ot
        
        self.option_types << heat_sealing_ot
        self.option_types << reinforcement_perimeter_ot
        
        self.option_types << pocket_ot
      end
    end
  end
end