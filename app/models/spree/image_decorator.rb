Spree::Image.class_eval do
  has_attached_file :attachment,
                    :styles => { :mini => '80x74#', :small => '160x142#', :product => '215x199#', :medium => '320x296#', :large => '500x463#' },
                    :default_style => :product,
                    :url => '/spree/products/:id/:style/:basename.:extension',
                    :path => ':rails_root/public/spree/products/:id/:style/:basename.:extension'
end