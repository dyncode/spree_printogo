Spree::OptionValue.class_eval do
  attr_accessible :type, :option_type_id, :start_up_cost_c, :plant_color_c, :price_front_c, :price_front_and_back_c,
                  :start_up_cost_bw, :plant_color_bw, :price_front_bw, :price_front_and_back_bw,
                  :printer_hourly_cost_bw, :start_time_bw, :average_hourly_print_bw, :cost_ink_bw,
                  :printer_hourly_cost_c, :start_time_c, :average_hourly_print_c, :cost_ink_c, :active
  
  has_many :values, :class_name => "Spree::Paper", :foreign_key => "option_value_id", :dependent => :nullify
  has_many :line_items_option_values, :class_name => "Spree::OptionValue", :foreign_key => "option_value_id", :dependent => :delete_all
  belongs_to :paper, :class_name => "Spree::Paper", :foreign_key => "id"

  has_one :template, :as => :imageable, :dependent => :delete # Corrisponde al template da scaricare per il formato
  has_one :template_format, :as => :imageable, :dependent => :delete # Corrisponde al template per il formato [l'immagine piccola che viene visualizzata sopra il radiobutton]
  
  accepts_nested_attributes_for :template_format, :allow_destroy => true

	# default_scope where(:deleted_at => nil)

	# PERSIST OPTION AND NOT DELETE
	# override the delete method to set deleted_at value
	# instead of actually deleting the product.
	# def delete
		# self.update_column(:deleted_at, Time.now)
	# end

	# def destroy
		# touch :deleted_at
	# end

	# use deleted? rather than checking the attribute directly. this
	# allows extensions to override deleted? if they want to provide
	# their own definition.
	# def deleted?
		# !!deleted_at
	# end
end