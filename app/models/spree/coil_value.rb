# Bobine
module Spree
  class CoilValue < OptionValue
    after_save :associate_option_type
    attr_accessible :price, :height, :description, :width, :sided, :weight, :waste
    
    validates_presence_of :price, :height, :width, :description, :weight
    
    before_validation :check_weight
    
    def price
      self.price_front_bw
    end

    def height
      self.price_front_and_back_bw
    end
    
    def width
      self.price_front_and_back_c
    end
    
    # BIFACCIALE (TWO-SIDED)
    def sided
      self.is_cut
    end
    
    def description
      self.presentation
    end
    
    def weight
      #self.name.gsub(/[0-9]*( gr|gr)/).first.gsub("gr",'').strip
      self.start_time_c
    end
    
    def waste
      self.start_up_cost_bw || 0
    end

    def price= value
      self.price_front_bw=value
    end

    def height= value
      self.price_front_and_back_bw=value
    end
    
    def width= value
      self.price_front_and_back_c=value
    end
    
    def sided= value
      self.is_cut=value
    end
    
    def description= value
      self.presentation=value
    end
    
    def weight= value
      self.start_time_c=value
    end
    
    def waste= value
      self.start_up_cost_bw=value
    end
    
    def check_weight
      #self.name.gsub(/[0-9]*( gr|gr)/).first.gsub("gr",'').strip rescue errors[:error] << "Occorre specificare i grammi tipo 300 gr"
      self.start_time_c
    end
    
    private
    def associate_option_type
      self.update_attribute(:option_type_id, Spree::OptionType.find_by_name('coil').id) if self.option_type.nil?
    end
  end
end