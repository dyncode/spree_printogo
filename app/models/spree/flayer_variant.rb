module Spree
	class FlayerVariant < Variant
		attr_accessible :limit, :paper, :paper_weight, :format, :images

		before_create :after_init
		belongs_to :flayer_product, :class_name => Spree::FlayerProduct, :foreign_key => "product_id"
		has_one :limit, :class_name => "Spree::Limit", :foreign_key => "variant_id"
		has_many :paper_products, :class_name => "Spree::PaperProduct", :foreign_key => "product_id"

		alias_attribute :format, :sku

    def get_selected_values_json
      a = []
      self.paper_products.each do |pp|
        a << {'weight' => {:quantity => pp.paper_weight.weight, :id => pp.paper_weight.id, :value => pp.paper_weight.presentation, :paper => {:name => pp.paper.name, :id => pp.paper.id, :presentation => pp.paper.presentation}}}
      end
      a.to_json
    end
    
    def get_paper_product_json
      h = []
      self.paper_products.each do |paper_page|
        h << {:paper_name => paper_page.paper.name, :paper_id => paper_page.paper.id, :weight => paper_page.paper_weight.weight}
      end
      h.to_json
    end

    private
    def after_init
      if self.new_record?
        self.limit = Spree::Limit.new if self.limit.nil?
        self.on_hand = 1
      end
    end

  end
end