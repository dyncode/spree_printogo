Spree::Banner.class_eval do
  belongs_to :bannerable, :polymorphic => true
  
  attr_accessible :bannerable, :bannerable_id, :bannerable_type, :position
  
  def is_promotion?
    self.bannerable_type == "Spree::LineItemPromotion" && !self.bannerable_id.blank?
  end
  
  def promotion
    if self.is_promotion?
      Spree::LineItemPromotion.find(self.bannerable_id)
    else
      nil
    end
  end
end