# Questa classe si occupa di gestire il punto di conveniza.
# Nello stesso oggetto sono definiti i valori sia per la stampa a colori sia per quella in bainco e nero.
# Per la stampa in bianco e nero il funzionamento è il segunete (lo stesso vale per quella a colori):
# La propietà "quantity_bw" indica il punto di conveniza, di sotto del quale viene indicato di usare alcuni
# parametri:
#        - printer_before_bw
#        - pose_before_bw
#        - format_before_bw
#        - printer_after_bw
#        - pose_after_bw
#        - format_after_bw
module Spree
  class Limit < ActiveRecord::Base
    # :bindery_c, :bindery_before_min_c_id, :bindery_before_max_c_id, :bindery_after_c_id,
    # :bindery_bw, :bindery_before_min_bw_id, :bindery_before_max_bw_id, :bindery_after_bw_id,
    
    attr_accessible :printer_before_c, :printer_after_c, :pose_before_c, :pose_after_c, :format_before_c, :format_after_c,
                    :printer_before_c_id, :printer_after_c_id, :pose_before_c_id, :pose_after_c_id, :format_before_c_id, :format_after_c_id,
                    :quarter_before_c_id, :quarter_after_c_id, :quantity_c,
                    :printer_before_bw, :printer_after_bw, :pose_before_bw, :pose_after_bw, :format_before_bw, :format_after_bw,
                    :printer_before_bw_id, :printer_after_bw_id, :pose_before_bw_id, :pose_after_bw_id, :format_before_bw_id, :format_after_bw_id,
                    :quarter_before_bw_id, :quarter_after_bw_id, :quantity_bw,
                    :bindery_before_id, :bindery_after_id, :variant

    # Colori minore punto di convenienza
    belongs_to :printer_before_c, :class_name => "Spree::OptionValue", :foreign_key => "printer_before_c_id"
    belongs_to :pose_before_c, :class_name => "Spree::OptionValue", :foreign_key => "pose_before_c_id"
    belongs_to :format_before_c, :class_name => "Spree::PrinterFormatDefault", :foreign_key => "format_before_c_id"
    belongs_to :quarter_before_c, :class_name => "Spree::OptionValue", :foreign_key => "quarter_before_c_id"
    #belongs_to :bindery_before_min_c, :class_name => "Spree::OptionValue", :foreign_key => "bindery_before_min_c_id"
    #belongs_to :bindery_before_max_c, :class_name => "Spree::OptionValue", :foreign_key => "bindery_before_max_c_id"
    # Colori maggiore punto di convenienza
    belongs_to :printer_after_c, :class_name => "Spree::OptionValue", :foreign_key => "printer_after_c_id"
    belongs_to :pose_after_c, :class_name => "Spree::OptionValue", :foreign_key => "pose_after_c_id"
    belongs_to :format_after_c, :class_name => "Spree::PrinterFormatDefault", :foreign_key => "format_after_c_id"
    belongs_to :quarter_after_c, :class_name => "Spree::OptionValue", :foreign_key => "quarter_after_c_id"
    #belongs_to :bindery_after_c, :class_name => "Spree::OptionValue", :foreign_key => "bindery_after_c_id"

    # B/N minore punto di convenienza
    belongs_to :printer_before_bw, :class_name => "Spree::OptionValue", :foreign_key => "printer_before_bw_id"
    belongs_to :pose_before_bw, :class_name => "Spree::OptionValue", :foreign_key => "pose_before_bw_id"
    belongs_to :format_before_bw, :class_name => "Spree::PrinterFormatDefault", :foreign_key => "format_before_bw_id"
    belongs_to :quarter_before_bw, :class_name => "Spree::OptionValue", :foreign_key => "quarter_before_bw_id"
    #belongs_to :bindery_before_min_bw, :class_name => "Spree::OptionValue", :foreign_key => "bindery_before_min_bw_id"
    #belongs_to :bindery_before_max_bw, :class_name => "Spree::OptionValue", :foreign_key => "bindery_before_max_bw_id"
    # B/N maggiore punto di convenienza
    belongs_to :printer_after_bw, :class_name => "Spree::OptionValue", :foreign_key => "printer_after_bw_id"
    belongs_to :pose_after_bw, :class_name => "Spree::OptionValue", :foreign_key => "pose_after_bw_id"
    belongs_to :format_after_bw, :class_name => "Spree::PrinterFormatDefault", :foreign_key => "format_after_bw_id"
    belongs_to :quarter_after_bw, :class_name => "Spree::OptionValue", :foreign_key => "quarter_after_bw_id"
    #belongs_to :bindery_after_bw, :class_name => "Spree::OptionValue", :foreign_key => "bindery_after_bw_id"
    belongs_to :variant, :class_name => "Spree::Variant", :foreign_key => "variant_id"
    
    belongs_to :bindery_before, :class_name => "Spree::OptionValue", :foreign_key => "bindery_before_id"
    belongs_to :bindery_after, :class_name => "Spree::OptionValue", :foreign_key => "bindery_after_id"

    def product
      self.variant.product
    end

    def quarters(print_bw, printer)
      if !print_bw
        if printer == printer_before_bw
          quarter_before_bw
        else
          quarter_after_bw
        end
      else
        if printer == printer_before_c
          quarter_before_c
        else
          quarter_after_c
        end
      end
    end

    def printer(print_bw, quantity, molt = 1)
      if print_bw
        if quantity <= quantity_bw * molt
          printer_before_bw
        else
          printer_after_bw
        end
      else
        if quantity <= quantity_c * molt
          printer_before_c
        else
          printer_after_c
        end
      end
    end
    
    # ritorna un HASH con le due stampanti a seconda se B/N o C
    def printer_multi(print_bw)
      if !print_bw
        {:low => printer_before_bw, :high => printer_after_bw}
      else
        {:low => printer_before_c, :high => printer_after_c}
      end
    end
    
    def printer_format(print_bw, quantity, molt = 1)
      if print_bw
        if quantity <= quantity_bw * molt
          format_before_bw
        else
          format_after_bw
        end
      else
        if quantity <= quantity_c * molt
          format_before_c
        else
          format_after_c
        end
      end
    end
    
    # ritorna il formato a seconda della stampante selezionata
    def printer_format_multi(print_bw, printer)
      if print_bw
        if printer == printer_before_bw
          format_before_bw
        else
          format_after_bw
        end
      else
        if printer == printer_before_c
          format_before_c
        else
          format_after_c
        end
      end
    end
    
    # ritorna la legatoria a seconda della stampante scelta
    def bindery(printer)
      if (printer == printer_before_bw) || (printer == printer_before_c)
        bindery_before
      else
        bindery_after
      end
    end
  end
end
