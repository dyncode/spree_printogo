require_dependency 'spree/calculator'

module Spree
  class Calculator::UserPromotionLineItem < Calculator
    preference :amount, :decimal, :default => 0

    attr_accessible :preferred_amount

    def self.description
      I18n.t(:user_promotion_per_line_item)
    end
    
    def compute(computable)
      case computable
      when Spree::Order
        compute_order(computable)
      when Spree::LineItem
        compute_line_item(computable)
      end
    end
    
    private

    def promo
      self.calculable
    end

    def compute_order(order)
      order.line_items.each do |line_item|
        if line_item.product == promo.product && line_item.number_of_copy.to_i >= promo.number_of_copy.to_i
          deduced_total_by_promo(line_item.price, promo)
        elsif line_item.order.user == promo.user
          deduced_total_by_promo(line_item.price, promo)
        else
          0
        end
      end
    end

    def compute_line_item(line_item)
      if promo.product.present?
        if line_item.product == promo.product && line_item.number_of_copy.to_i >= promo.number_of_copy.to_i
          deduced_total_by_promo(line_item.price, promo)
        else
          0
        end
      else
        if line_item.order.user == promo.user
          deduced_total_by_promo(line_item.price, promo)
        else
          0
        end
      end
    end

    def round_to_two_places(amount)
      BigDecimal.new(amount.to_s).round(2, BigDecimal::ROUND_HALF_UP)
    end

    def deduced_total_by_promo(total, promo)
      round_to_two_places(total * promo.amount)
    end
    
  end
end