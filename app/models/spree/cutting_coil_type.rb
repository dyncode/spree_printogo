# TIPI TAGLIO BOBINE
module Spree
  class CuttingCoilType < OptionValue
    after_save :associate_option_type
    belongs_to :pvc_product, :class_name => "Spree::PvcProduct", :foreign_key => "variant_id"
    belongs_to :rigid_product, :class_name => "Spree::RigidProduct", :foreign_key => "variant_id"
    belongs_to :sticker_product, :class_name => "Spree::StickerProduct", :foreign_key => "variant_id"
    belongs_to :cutting_coil, :class_name => "Spree::CuttingCoil", :foreign_key => "option_value_id"
    attr_accessible :available_meter, :cutting_coil, :cutting_coil_id, :option_value_id,
                    :hourly_cost, :start_time, :plant, :min_copies_active
    
    attr_accessor :_destroy
    
    def start_time # Tempo di avviamento
      self.start_time_bw
    end
    
    def hourly_cost # Costo orario
      self.average_hourly_print_bw
    end
    
    def available_meter # Costo al metro lineare
      self.plant_color_bw
    end
    
    def plant # Costo impianto
      self.price_front_bw || 0
    end
    
    def min_copies_active # Minimo copie per attivo
      self.waste_paper_bw || 0
    end
    
    def start_time=value
      self.start_time_bw = value
    end
    
    def hourly_cost=value
      self.average_hourly_print_bw = value
    end
    
    def available_meter=value
      self.plant_color_bw = value
    end
    
    def plant=value
      self.price_front_bw = value
    end
    
    def min_copies_active=value
      self.waste_paper_bw = value
    end
    
    private
    def associate_option_type
      self.update_attribute(:option_type_id, Spree::OptionType.find_by_name('cutting_coil_type').id) if self.option_type.nil?
    end
  end
end