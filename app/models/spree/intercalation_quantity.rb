module Spree
  class IntercalationQuantity < OptionValue
    before_create :set_presentation
    attr_accessible :hourly_cost, :average_hourly, :start_time, :name
    validates :hourly_cost, :average_hourly, :start_time, :name, :presence => true

    def hourly_cost
      self.printer_hourly_cost_bw
    end

    def average_hourly
      self.average_hourly_print_bw
    end

    def start_time
      self.start_time_bw
    end

    def hourly_cost=value
      self.printer_hourly_cost_bw=value
    end

    def average_hourly=value
      self.average_hourly_print_bw=value
    end

    def start_time=value
      self.start_time_bw=value
    end

    private
    def set_presentation
      self.presentation=self.name
    end

  end
end