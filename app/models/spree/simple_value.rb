# Valore Semplice
module Spree
  class SimpleValue < ActiveRecord::Base
    has_many :simple_attributes, :class_name => "Spree::SimpleValue", :foreign_key => "attribute_id"
    has_many :entity_attribute_values, :class_name => "Spree::EntityAttributeValue", :foreign_key => "value_id", :dependent => :delete_all
    
    attr_accessible :simple_values, :simple_values_attributes, :entity_attribute_values,
                    :entity_attribute_values_attributes, :presentation, :position
    
    validates :presentation, :position, :presence => true
  end
end