# Corrisponde al template per il formato [l'immagine piccola che viene visualizzata sopra il radiobutton]
module Spree
  class TemplateFormat < Template
    attr_accessible :attachment, :imageable, :imageable_id, :imageable_type
    belongs_to :imageable, :polymorphic => true
    
    has_attached_file :attachment,
                      :styles => {:mini => '32x32>', :normal => '53x63#'},
                      :default_style => :mini,
                      :url => '/spree/template_format/:id/:style/:basename.:extension',
                      :path => ':rails_root/public/spree/template_format/:id/:style/:basename.:extension'
    
  end
end