module Spree
  #TODO Da rendere superclasss o cancellare
  class PrintingMachine < OptionValue
    attr_accessible :start_up_cost_c, :plant_color_c, :price_front_c, :price_front_and_back_c,
                    :start_up_cost_bw, :plant_color_bw, :price_front_bw, :price_front_and_back_bw,
                    :formats, :printer_hourly_cost_bw, :start_time_bw, :average_hourly_print_bw, :cost_ink_bw,
                    :printer_hourly_cost_c, :start_time_c, :average_hourly_print_c, :cost_ink_c
  end
end