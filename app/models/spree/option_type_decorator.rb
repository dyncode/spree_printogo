Spree::OptionType.class_eval do
  attr_accessible :text
  has_many :papers, :class_name => "Spree::Paper", :foreign_key => "option_type_id"

  def printers
    Spree::Printer.all
  end

  def binderies
    Spree::Bindery.all
  end
end