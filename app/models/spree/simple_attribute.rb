# Attributo Semplice
module Spree
  class SimpleAttribute < ActiveRecord::Base
    belongs_to :advanced_variant, :class_name => "Spree::AdvancedVariant", :foreign_key => "variant_id"
    belongs_to :advanced_product, :class_name => "Spree::AdvancedProduct", :foreign_key => "variant_id"
    has_many :simple_values, :class_name => "Spree::SimpleValue", :foreign_key => "attribute_id", :dependent => :delete_all
    has_many :entity_attribute_values, :class_name => "Spree::EntityAttributeValue", :foreign_key => "attribute_id", :dependent => :delete_all
    
    attr_accessible :simple_values, :simple_values_attributes, :entity_attribute_values, :position, :optional,
                    :entity_attribute_values_attributes, :name, :presentation
    
    validates :name, :presentation, :position, :presence => true
    
    default_scope order("position ASC")
    
    accepts_nested_attributes_for :simple_values, :allow_destroy => true
  end
end